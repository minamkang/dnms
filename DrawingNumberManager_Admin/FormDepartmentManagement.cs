﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

//다른프로젝트 참조
using DrawingNumberManager;

namespace DrawingNumberManager_Admin
{
    public partial class FormDepartmentManagement : Form
    {
        private string edit_mode = "";

        public FormDepartmentManagement()
        {
            InitializeComponent();
        }

        private void FormDepartmentManagement_Load(object sender, EventArgs e)
        {
            BindDgvDepartment();
            Init_InputValue(Const.EditMode_None);
        }
        private void FormUserManagement_Load(object sender, EventArgs e)
        {
            BindDgvDepartment();
            Init_InputValue(Const.EditMode_None);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            BindDgvDepartment();
        }

        private void dgvDepartment_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < -1)
                return;

            txtCode.Text = this.dgvDepartment.Rows[e.RowIndex].Cells["CODE"].Value.ToString();
            txtValue.Text = this.dgvDepartment.Rows[e.RowIndex].Cells["VALUE"].Value.ToString();
            txtSortKey.Text = this.dgvDepartment.Rows[e.RowIndex].Cells["SORT"].Value.ToString();
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            Init_InputValue(Const.EditMode_Insert);
        }

        private void btnModify_Click(object sender, EventArgs e)
        {
            Init_InputValue(Const.EditMode_Modify);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            Init_InputValue(Const.EditMode_Delete);
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            switch (edit_mode)
            {
                case Const.EditMode_Insert:
                    DepartmentCodeUID('I');
                    break;
                case Const.EditMode_Modify:
                    DepartmentCodeUID('U');
                    break;
                case Const.EditMode_Delete:
                    DepartmentCodeUID('D');
                    break;
            }
            Init_InputValue(Const.EditMode_None);
            BindDgvDepartment();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Init_InputValue(Const.EditMode_None);
            BindDgvDepartment();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BindDgvDepartment()
        {
			DataTable result = new DataTable();
			SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC department_code_info"), StaticData.db_connetion_string);
			query.Fill(result);
			dgvDepartment.DataSource = result;

        }

        private void DepartmentCodeUID(char flagUID)
        {
            SqlConnection connection = new SqlConnection(StaticData.db_connetion_string);
            connection.Open();

            try
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
				cmd.CommandText = "department_code_IUD";

                cmd.Parameters.Add("@Code", SqlDbType.Char, 1).Value = txtCode.Text;
                cmd.Parameters.Add("@Value", SqlDbType.VarChar, 10).Value = txtValue.Text;
                cmd.Parameters.Add("@Sort", SqlDbType.Int).Value = int.Parse(txtSortKey.Text);
                cmd.Parameters.Add("@IUD_Flag", SqlDbType.VarChar, 1).Value = flagUID;

                int excuteCount = cmd.ExecuteNonQuery();
                if (excuteCount != 1)
                {
                    MessageBox.Show(string.Format("DepartmentCodeUID가 [{0}]를 하지 못하였습니다. ", flagUID));
                }
                connection.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show("DepartmentUID \r" + ex.ToString());
            }
            connection.Close();
        }

        private void Init_InputValue(string EditMode)
        {
            switch (EditMode)
            {
                case Const.EditMode_Insert:
                    SetClear();
                    this.txtCode.Enabled = true;
                    this.txtValue.Enabled = true;
                    this.txtSortKey.Enabled = true;
                    this.dgvDepartment.Enabled = false;
                    this.btnModify.Visible = false;
                    this.btnDelete.Visible = false;
                    this.btnInsert.Enabled = false;
                    this.btnOK.Visible = true;
                    this.btnCancel.Visible = true;
                    this.btnClose.Visible = false;
                    break;
                case Const.EditMode_Modify:
                    this.txtCode.Enabled = false;
                    this.txtValue.Enabled = true;
                    this.txtSortKey.Enabled = true;
                    this.dgvDepartment.Enabled = false;
                    this.btnInsert.Visible = false;
                    this.btnDelete.Visible = false;
                    this.btnModify.Enabled = false;
                    this.btnOK.Visible = true;
                    this.btnCancel.Visible = true;
                    this.btnClose.Visible = false;
                    break;
                case Const.EditMode_Delete:
                    this.txtCode.Enabled = false;
                    this.txtValue.Enabled = false;
                    this.txtSortKey.Enabled = false;
                    this.dgvDepartment.Enabled = false;
                    this.btnInsert.Visible = false;
                    this.btnModify.Visible = false;
                    this.btnDelete.Enabled = false;
                    this.btnOK.Visible = true;
                    this.btnCancel.Visible = true;
                    this.btnClose.Visible = false;
                    break;
                case Const.EditMode_None:
                    this.txtCode.Enabled = false;
                    this.txtValue.Enabled = false;
                    this.txtSortKey.Enabled = false;
                    this.dgvDepartment.Enabled = true;
                    this.btnInsert.Visible = true;
                    this.btnModify.Visible = true;
                    this.btnDelete.Visible = true;
                    this.btnInsert.Enabled = true;
                    this.btnModify.Enabled = true;
                    this.btnDelete.Enabled = true;
                    this.btnOK.Visible = false;
                    this.btnCancel.Visible = false;
                    this.btnClose.Visible = true;
                    break;
                default:
                    break;
            }
            edit_mode = EditMode;
        }

        private void SetClear()
        {
            this.txtCode.Text = "";
            this.txtValue.Text = "";
            this.txtSortKey.Text = "";
        }





    }
}
