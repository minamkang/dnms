﻿namespace DrawingNumberManager_Admin
{
    partial class FormManagement
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnUserManagement = new System.Windows.Forms.Button();
            this.btnDepartmentCodeManagement = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnUserManagement
            // 
            this.btnUserManagement.Location = new System.Drawing.Point(12, 12);
            this.btnUserManagement.Name = "btnUserManagement";
            this.btnUserManagement.Size = new System.Drawing.Size(91, 36);
            this.btnUserManagement.TabIndex = 0;
            this.btnUserManagement.Text = "사용자 관리";
            this.btnUserManagement.UseVisualStyleBackColor = true;
            this.btnUserManagement.Click += new System.EventHandler(this.btnUserManagement_Click);
            // 
            // btnDepartmentCodeManagement
            // 
            this.btnDepartmentCodeManagement.Location = new System.Drawing.Point(12, 54);
            this.btnDepartmentCodeManagement.Name = "btnDepartmentCodeManagement";
            this.btnDepartmentCodeManagement.Size = new System.Drawing.Size(91, 36);
            this.btnDepartmentCodeManagement.TabIndex = 1;
            this.btnDepartmentCodeManagement.Text = "부서코드 관리";
            this.btnDepartmentCodeManagement.UseVisualStyleBackColor = true;
            this.btnDepartmentCodeManagement.Click += new System.EventHandler(this.btnCodeManagement_Click);
            // 
            // FormManagement
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(221, 96);
            this.Controls.Add(this.btnDepartmentCodeManagement);
            this.Controls.Add(this.btnUserManagement);
            this.Name = "FormManagement";
            this.Text = "Management";
            this.Load += new System.EventHandler(this.FormManagement_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnUserManagement;
        private System.Windows.Forms.Button btnDepartmentCodeManagement;


    }
}

