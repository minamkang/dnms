﻿namespace DrawingNumberManager_Admin
{
    partial class FormUserManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPW = new System.Windows.Forms.Label();
            this.txtPW = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblID = new System.Windows.Forms.Label();
            this.txtID = new System.Windows.Forms.TextBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnModify = new System.Windows.Forms.Button();
            this.btnInsert = new System.Windows.Forms.Button();
            this.dgvUserInfo = new System.Windows.Forms.DataGridView();
            this.lblUserId = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.lblDepartment = new System.Windows.Forms.Label();
            this.cmbDepartment = new System.Windows.Forms.ComboBox();
            this.txtSerachUserID = new System.Windows.Forms.TextBox();
            this.cmbAllUserYN = new System.Windows.Forms.ComboBox();
            this.gbAccessLevel = new System.Windows.Forms.GroupBox();
            this.cbPartManagement = new System.Windows.Forms.CheckBox();
            this.cbNoteWrite = new System.Windows.Forms.CheckBox();
            this.cbLoginYn = new System.Windows.Forms.CheckBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblTel = new System.Windows.Forms.Label();
            this.txtTel = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUserInfo)).BeginInit();
            this.gbAccessLevel.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblPW
            // 
            this.lblPW.AutoSize = true;
            this.lblPW.Location = new System.Drawing.Point(875, 78);
            this.lblPW.Name = "lblPW";
            this.lblPW.Size = new System.Drawing.Size(62, 12);
            this.lblPW.TabIndex = 34;
            this.lblPW.Text = "Password";
            // 
            // txtPW
            // 
            this.txtPW.Location = new System.Drawing.Point(963, 75);
            this.txtPW.Name = "txtPW";
            this.txtPW.PasswordChar = '*';
            this.txtPW.Size = new System.Drawing.Size(222, 21);
            this.txtPW.TabIndex = 33;
            this.txtPW.TextChanged += new System.EventHandler(this.txtPW_TextChanged);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(875, 105);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(39, 12);
            this.lblName.TabIndex = 32;
            this.lblName.Text = "Name";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(963, 102);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(224, 21);
            this.txtName.TabIndex = 31;
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.Location = new System.Drawing.Point(875, 51);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(16, 12);
            this.lblID.TabIndex = 30;
            this.lblID.Text = "ID";
            // 
            // txtID
            // 
            this.txtID.BackColor = System.Drawing.SystemColors.Window;
            this.txtID.Location = new System.Drawing.Point(963, 48);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(222, 21);
            this.txtID.TabIndex = 29;
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(1110, 477);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 35);
            this.btnClose.TabIndex = 28;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(1029, 477);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 35);
            this.btnCancel.TabIndex = 27;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(948, 477);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 35);
            this.btnOK.TabIndex = 26;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(170, 477);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 35);
            this.btnDelete.TabIndex = 25;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnModify
            // 
            this.btnModify.Location = new System.Drawing.Point(89, 477);
            this.btnModify.Name = "btnModify";
            this.btnModify.Size = new System.Drawing.Size(75, 35);
            this.btnModify.TabIndex = 24;
            this.btnModify.Text = "Modify";
            this.btnModify.UseVisualStyleBackColor = true;
            this.btnModify.Click += new System.EventHandler(this.btnModify_Click);
            // 
            // btnInsert
            // 
            this.btnInsert.Location = new System.Drawing.Point(8, 477);
            this.btnInsert.Name = "btnInsert";
            this.btnInsert.Size = new System.Drawing.Size(75, 35);
            this.btnInsert.TabIndex = 23;
            this.btnInsert.Text = "Insert";
            this.btnInsert.UseVisualStyleBackColor = true;
            this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
            // 
            // dgvUserInfo
            // 
            this.dgvUserInfo.AllowUserToAddRows = false;
            this.dgvUserInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUserInfo.Location = new System.Drawing.Point(9, 51);
            this.dgvUserInfo.Name = "dgvUserInfo";
            this.dgvUserInfo.ReadOnly = true;
            this.dgvUserInfo.RowTemplate.Height = 23;
            this.dgvUserInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvUserInfo.Size = new System.Drawing.Size(860, 416);
            this.dgvUserInfo.TabIndex = 22;
            this.dgvUserInfo.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUserInfo_RowEnter);
            // 
            // lblUserId
            // 
            this.lblUserId.AutoSize = true;
            this.lblUserId.Location = new System.Drawing.Point(12, 20);
            this.lblUserId.Name = "lblUserId";
            this.lblUserId.Size = new System.Drawing.Size(46, 12);
            this.lblUserId.TabIndex = 36;
            this.lblUserId.Text = "User ID";
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(503, 9);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 35);
            this.btnSearch.TabIndex = 37;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // lblDepartment
            // 
            this.lblDepartment.AutoSize = true;
            this.lblDepartment.Location = new System.Drawing.Point(875, 133);
            this.lblDepartment.Name = "lblDepartment";
            this.lblDepartment.Size = new System.Drawing.Size(69, 12);
            this.lblDepartment.TabIndex = 39;
            this.lblDepartment.Text = "Department";
            // 
            // cmbDepartment
            // 
            this.cmbDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDepartment.FormattingEnabled = true;
            this.cmbDepartment.Location = new System.Drawing.Point(965, 130);
            this.cmbDepartment.Name = "cmbDepartment";
            this.cmbDepartment.Size = new System.Drawing.Size(220, 20);
            this.cmbDepartment.TabIndex = 42;
            // 
            // txtSerachUserID
            // 
            this.txtSerachUserID.BackColor = System.Drawing.SystemColors.Window;
            this.txtSerachUserID.Location = new System.Drawing.Point(176, 17);
            this.txtSerachUserID.Name = "txtSerachUserID";
            this.txtSerachUserID.Size = new System.Drawing.Size(94, 21);
            this.txtSerachUserID.TabIndex = 35;
            this.txtSerachUserID.TextChanged += new System.EventHandler(this.txtSerachUserID_TextChanged);
            // 
            // cmbAllUserYN
            // 
            this.cmbAllUserYN.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAllUserYN.FormattingEnabled = true;
            this.cmbAllUserYN.Items.AddRange(new object[] {
            "All",
            "individual"});
            this.cmbAllUserYN.Location = new System.Drawing.Point(78, 17);
            this.cmbAllUserYN.Name = "cmbAllUserYN";
            this.cmbAllUserYN.Size = new System.Drawing.Size(92, 20);
            this.cmbAllUserYN.TabIndex = 43;
            this.cmbAllUserYN.SelectedIndexChanged += new System.EventHandler(this.cmbAllUserYN_SelectedIndexChanged);
            // 
            // gbAccessLevel
            // 
            this.gbAccessLevel.Controls.Add(this.cbPartManagement);
            this.gbAccessLevel.Controls.Add(this.cbNoteWrite);
            this.gbAccessLevel.Controls.Add(this.cbLoginYn);
            this.gbAccessLevel.Location = new System.Drawing.Point(877, 166);
            this.gbAccessLevel.Name = "gbAccessLevel";
            this.gbAccessLevel.Size = new System.Drawing.Size(190, 93);
            this.gbAccessLevel.TabIndex = 44;
            this.gbAccessLevel.TabStop = false;
            this.gbAccessLevel.Text = "Access Level";
            // 
            // cbPartManagement
            // 
            this.cbPartManagement.AutoSize = true;
            this.cbPartManagement.Location = new System.Drawing.Point(22, 65);
            this.cbPartManagement.Name = "cbPartManagement";
            this.cbPartManagement.Size = new System.Drawing.Size(124, 16);
            this.cbPartManagement.TabIndex = 2;
            this.cbPartManagement.Text = "Part Management";
            this.cbPartManagement.UseVisualStyleBackColor = true;
            this.cbPartManagement.CheckedChanged += new System.EventHandler(this.cbPartManagement_CheckedChanged);
            // 
            // cbNoteWrite
            // 
            this.cbNoteWrite.AutoSize = true;
            this.cbNoteWrite.Location = new System.Drawing.Point(22, 43);
            this.cbNoteWrite.Name = "cbNoteWrite";
            this.cbNoteWrite.Size = new System.Drawing.Size(81, 16);
            this.cbNoteWrite.TabIndex = 1;
            this.cbNoteWrite.Text = "Note Write";
            this.cbNoteWrite.UseVisualStyleBackColor = true;
            this.cbNoteWrite.CheckedChanged += new System.EventHandler(this.cbNoteWrite_CheckedChanged);
            // 
            // cbLoginYn
            // 
            this.cbLoginYn.AutoSize = true;
            this.cbLoginYn.Location = new System.Drawing.Point(22, 20);
            this.cbLoginYn.Name = "cbLoginYn";
            this.cbLoginYn.Size = new System.Drawing.Size(76, 16);
            this.cbLoginYn.TabIndex = 0;
            this.cbLoginYn.Text = "Login YN";
            this.cbLoginYn.UseVisualStyleBackColor = true;
            this.cbLoginYn.CheckedChanged += new System.EventHandler(this.cbLoginYn_CheckedChanged);
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(875, 273);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(37, 12);
            this.lblEmail.TabIndex = 46;
            this.lblEmail.Text = "Email";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(963, 270);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(222, 21);
            this.txtEmail.TabIndex = 45;
            // 
            // lblTel
            // 
            this.lblTel.AutoSize = true;
            this.lblTel.Location = new System.Drawing.Point(875, 298);
            this.lblTel.Name = "lblTel";
            this.lblTel.Size = new System.Drawing.Size(23, 12);
            this.lblTel.TabIndex = 48;
            this.lblTel.Text = "Tel";
            // 
            // txtTel
            // 
            this.txtTel.Location = new System.Drawing.Point(963, 295);
            this.txtTel.Name = "txtTel";
            this.txtTel.Size = new System.Drawing.Size(224, 21);
            this.txtTel.TabIndex = 47;
            // 
            // FormUserManagement
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1197, 519);
            this.Controls.Add(this.lblTel);
            this.Controls.Add(this.txtTel);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.gbAccessLevel);
            this.Controls.Add(this.cmbAllUserYN);
            this.Controls.Add(this.cmbDepartment);
            this.Controls.Add(this.lblDepartment);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.lblUserId);
            this.Controls.Add(this.txtSerachUserID);
            this.Controls.Add(this.lblPW);
            this.Controls.Add(this.txtPW);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.lblID);
            this.Controls.Add(this.txtID);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnModify);
            this.Controls.Add(this.btnInsert);
            this.Controls.Add(this.dgvUserInfo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FormUserManagement";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormUserManagement";
            this.Load += new System.EventHandler(this.FormUserManagement_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvUserInfo)).EndInit();
            this.gbAccessLevel.ResumeLayout(false);
            this.gbAccessLevel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPW;
        private System.Windows.Forms.TextBox txtPW;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnModify;
        private System.Windows.Forms.Button btnInsert;
        private System.Windows.Forms.DataGridView dgvUserInfo;
        private System.Windows.Forms.Label lblUserId;
        private System.Windows.Forms.Button btnSearch;
		private System.Windows.Forms.Label lblDepartment;
        private System.Windows.Forms.ComboBox cmbDepartment;
        private System.Windows.Forms.TextBox txtSerachUserID;
        private System.Windows.Forms.ComboBox cmbAllUserYN;
		private System.Windows.Forms.GroupBox gbAccessLevel;
		private System.Windows.Forms.CheckBox cbLoginYn;
		private System.Windows.Forms.CheckBox cbNoteWrite;
        private System.Windows.Forms.CheckBox cbPartManagement;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblTel;
        private System.Windows.Forms.TextBox txtTel;
    }
}