﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using DrawingNumberManager;

namespace DrawingNumberManager_Admin
{
    public partial class FormManagement : Form
    {
        public FormManagement()
        {
            InitializeComponent();
        }

        private void FormManagement_Load(object sender, EventArgs e)
        {
			Ini clsIni = new Ini();
			clsIni.LoadIni();
        }

        private void btnUserManagement_Click(object sender, EventArgs e)
        {
			FormUserManagement frmUserMng = new FormUserManagement();
            frmUserMng.ShowDialog();
        }

        private void btnAuthorityManagement_Click(object sender, EventArgs e)
        {

        }

        private void btnCodeManagement_Click(object sender, EventArgs e)
        {
            FormDepartmentManagement frmDepartmentMng = new FormDepartmentManagement();
            frmDepartmentMng.ShowDialog();
        }


    }
}
