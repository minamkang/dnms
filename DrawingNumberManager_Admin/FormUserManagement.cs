﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Collections;


//다른프로젝트 참조
using DrawingNumberManager;

namespace DrawingNumberManager_Admin
{
    public partial class FormUserManagement : Form
    {
        string edit_mode = "";
		string selectedUserKey;
		int first_pw_length;
		bool revisePw = false;
		Authority classAuthority = null;
		
		bool[] AccessBool = new bool[8];
		BitArray AccessLevel = new BitArray(8);
		
        public FormUserManagement()
        {
            InitializeComponent();
        }

        private void FormUserManagement_Load(object sender, EventArgs e)
        {
            cmbAllUserYN.SelectedIndex = 0;
            txtSerachUserID.Visible = false;
            
            BindCmbDepartment();
            BindDgvUserInfo();
            Init_InputValue(Const.EditMode_None);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (cmbAllUserYN.SelectedIndex == 1 && txtSerachUserID.Text.Length == 0)
                return;

			BindDgvUserInfo();
        }

        private void dgvUserInfo_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < -1)
                return;

            txtID.Text = this.dgvUserInfo.Rows[e.RowIndex].Cells["ID"].Value.ToString();
            txtName.Text = this.dgvUserInfo.Rows[e.RowIndex].Cells["이름"].Value.ToString();
            txtPW.Text = this.dgvUserInfo.Rows[e.RowIndex].Cells["Password"].Value.ToString();
			classAuthority = new Authority(txtID.Text);
			
            cbLoginYn.Checked = classAuthority.LoginYn;
			cbNoteWrite.Checked = classAuthority.NoteWrite;
            cbPartManagement.Checked = classAuthority.PartManagement;
            string strDepartment = this.dgvUserInfo.Rows[e.RowIndex].Cells["부서코드"].Value.ToString();
            cmbDepartment.SelectedValue = strDepartment;
			selectedUserKey = this.dgvUserInfo.Rows[e.RowIndex].Cells["KEY_ID"].Value.ToString();

            txtEmail.Text = this.dgvUserInfo.Rows[e.RowIndex].Cells["EMAIL"].Value.ToString();
            txtTel.Text = this.dgvUserInfo.Rows[e.RowIndex].Cells["TEL"].Value.ToString();
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            Init_InputValue(Const.EditMode_Insert);
        }

        private void btnModify_Click(object sender, EventArgs e)
        {
            Init_InputValue(Const.EditMode_Modify);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            Init_InputValue(Const.EditMode_Delete);
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
			User user = new User();
            switch (edit_mode)
            {
                case Const.EditMode_Insert:
					user.ID = txtID.Text;
					user.PASSWORD = txtPW.Text;
					user.Name = txtName.Text;
					user.DepartmentCode = cmbDepartment.Text[0];
					user.AccessLevel = getIntFromBitArray(AccessLevel);


					bool insertResult = User.Create(user);
					if (insertResult == false)
                        MessageBox.Show("사용자가 Insert되지 않았습니다.");
                    break;
                case Const.EditMode_Modify:
					user.ID = txtID.Text;
					user.PASSWORD = txtPW.Text;
					user.Name = txtName.Text;
					user.DepartmentCode = cmbDepartment.Text[0];
					user.AccessLevel = getIntFromBitArray(AccessLevel);
					bool ModifyResult = User.Modify(user, revisePw);
					revisePw = false; //초기화

					if (ModifyResult == false)
                        MessageBox.Show("사용자가 Update되지 않았습니다.");
                    break;
                case Const.EditMode_Delete:
					user.ID = txtID.Text;
					bool RemoveResult = User.Remove(user);
					if (RemoveResult == false)
                        MessageBox.Show("사용자가 Remove되지 않았습니다.");
                    break;
            }

            Init_InputValue(Const.EditMode_None);
			BindDgvUserInfo();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Init_InputValue(Const.EditMode_None);
			BindDgvUserInfo();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

		private void BindDgvUserInfo()
        {
			try
			{
				DataTable user_Info = new DataTable();
				//모든 사용자 정보
				if (cmbAllUserYN.SelectedIndex == 0)
				{
					SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC user_infomation 0"), StaticData.db_connetion_string);
					query.Fill(user_Info);
				}
				//DataGrideView에서 Select한 사용자 선택
				else
				{
					for (int i = 0; i < dgvUserInfo.SelectedCells.Count; i++)
					{
						if (dgvUserInfo.Rows[i].Cells[0].Value.ToString() == txtSerachUserID.Text)
						{
							dgvUserInfo.Rows[i].Selected = true;
							dgvUserInfo.CurrentCell = dgvUserInfo.Rows[i].Cells[0];
							break;
						}
					}
				}

				if (cmbAllUserYN.SelectedIndex == 0)
					txtSerachUserID.Text = "";

				if (user_Info != null)
				{
					if (user_Info.Rows.Count == 0)
						return;

					user_Info.Columns[0].ColumnName = "ID";
					user_Info.Columns[1].ColumnName = "Password";
					user_Info.Columns[2].ColumnName = "이름";
					user_Info.Columns[3].ColumnName = "부서코드";
					user_Info.Columns[4].ColumnName = "권한레벨";
					user_Info.Columns[5].ColumnName = "KEY_ID";

					dgvUserInfo.DataSource = user_Info;
					//Columns 이름 재정의

					dgvUserInfo.Columns["ID"].Width = 80;
					dgvUserInfo.Columns["Password"].Width = 130;
					dgvUserInfo.Columns["이름"].Width = 60;
					dgvUserInfo.Columns["부서코드"].Width = 80;
					dgvUserInfo.Columns["권한레벨"].Width = 100;

					//화면상에만 보이지 않도록 처리 -- DB에서 자동으로 발번을 함

					dgvUserInfo.Columns["KEY_ID"].Visible = false;

					btnSearch.Visible = false;
				}
			}
			catch (Exception ex)
			{
				ex.ToString();
			}
        }

        private void BindCmbDepartment()
        {
            try
            {
				DataTable result = new DataTable();
				SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC department_code_info"), StaticData.db_connetion_string);
				query.Fill(result);

				if (result != null)
                {
                    cmbDepartment.ValueMember = "CODE";
                    cmbDepartment.DisplayMember = "VALUE";
					cmbDepartment.DataSource = result;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("BindCmbDepartment \r" + ex.ToString());
            }
        }


        private void Init_InputValue(string strEditMode)
        {
            switch (strEditMode)
            {
                case Const.EditMode_Insert:
                    SetClear();
                    this.txtSerachUserID.Enabled = false;
                    this.txtID.Enabled = true;
                    this.txtName.Enabled = true;
                    this.txtPW.Enabled = true;
					this.gbAccessLevel.Enabled = true;
                    this.cmbDepartment.Enabled = true;
                    this.dgvUserInfo.Enabled = false;
                    this.btnModify.Visible = false;
                    this.btnDelete.Visible = false;
                    this.btnSearch.Enabled = false;
                    this.txtSerachUserID.Enabled = false;
                    this.btnOK.Visible = true;
                    this.btnCancel.Visible = true;
                    this.btnInsert.Enabled = false;
                    this.btnClose.Visible = false;
					this.cbLoginYn.Checked = true;
					this.cbNoteWrite.Checked = false;
                    this.txtTel.Enabled = true;
                    this.txtEmail.Enabled = true;
                    break;
                case Const.EditMode_Modify:
                    this.txtSerachUserID.Enabled = false;
                    this.txtID.Enabled = false;
                    this.txtName.Enabled = true;
                    this.txtPW.Enabled = true;
					this.gbAccessLevel.Enabled = true;
                    this.cmbDepartment.Enabled = true;
                    this.dgvUserInfo.Enabled = false;
                    this.btnInsert.Visible = false;
                    this.btnDelete.Visible = false;
                    this.btnSearch.Enabled = false;
                    this.txtSerachUserID.Enabled = false;
                    this.btnOK.Visible = true;
                    this.btnCancel.Visible = true;
                    this.btnModify.Enabled = false;
                    this.btnClose.Visible = false;
					first_pw_length = this.txtPW.Text.Length;
                    this.txtTel.Enabled = true;
                    this.txtEmail.Enabled = true;
                    break;
                case Const.EditMode_Delete:
                    this.txtSerachUserID.Enabled = false;
                    this.txtID.Enabled = false;
                    this.txtName.Enabled = false;
                    this.txtPW.Enabled = false;
					this.gbAccessLevel.Enabled = false;
                    this.cmbDepartment.Enabled = false;
                    this.dgvUserInfo.Enabled = false;
                    this.btnInsert.Visible = false;
                    this.btnModify.Visible = false;
                    this.btnSearch.Enabled = false;
                    this.txtSerachUserID.Enabled = false;
                    this.btnOK.Visible = true;
                    this.btnCancel.Visible = true;
                    this.btnDelete.Enabled = false;
                    this.btnClose.Visible = false;
                    this.txtTel.Enabled = false;
                    this.txtEmail.Enabled = false;
                    break;
                case Const.EditMode_None:
                    this.txtID.Enabled = false;
                    this.txtName.Enabled = false;
                    this.txtPW.Enabled = false;
					this.gbAccessLevel.Enabled = false;
                    this.cmbDepartment.Enabled = false;
                    this.dgvUserInfo.Enabled = true;
                    this.dgvUserInfo.Enabled = true;
                    this.btnInsert.Visible = true;
                    this.btnModify.Visible = true;
                    this.btnDelete.Visible = true;
                    this.btnSearch.Enabled = true;
                    this.txtSerachUserID.Enabled = true;
                    this.btnOK.Visible = false;
                    this.btnCancel.Visible = false;
                    this.btnInsert.Enabled = true;
                    this.btnModify.Enabled = true;
                    this.btnDelete.Enabled = true;
                    this.btnClose.Visible = true;
                    this.txtTel.Enabled = false;
                    this.txtEmail.Enabled = false;
                    break;
                default:
                    break;
            }
            edit_mode = strEditMode;
        }

        private void SetClear()
        {
            this.txtID.Text = "";
            this.txtPW.Text = "";
            this.txtName.Text = "";

            this.cmbDepartment.SelectedIndex = 0;
			this.cbLoginYn.Checked = true;
        }

        private void cmbAllUserYN_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbAllUserYN.SelectedIndex < 0)
                return;

            if (cmbAllUserYN.SelectedIndex == 0)
            {
                this.txtSerachUserID.Visible = false;
                this.btnSearch.Visible = true;
                this.txtSerachUserID.Text = "";
            }
            else if (cmbAllUserYN.SelectedIndex == 1)
            {
                this.txtSerachUserID.Visible = true;
                this.btnSearch.Visible = false;
				txtID.Text = "";
				txtName.Text = "";
				txtPW.Text = "";
            }
        }

        private void txtSerachUserID_TextChanged(object sender, EventArgs e)
        {
            if (txtSerachUserID.Text.Length > 0)
                btnSearch.Visible = true;
            else
            {
                if(cmbAllUserYN.SelectedIndex == 0)
                    btnSearch.Visible = true;
                else
                    btnSearch.Visible = false;
            }
        }

		private void cbLoginYn_CheckedChanged(object sender, EventArgs e)
		{
			if (cbLoginYn.Checked == true)
				AccessLevel.Set(0, true);
			else
				AccessLevel.Set(0, false);
		}

		private void cbNoteWrite_CheckedChanged(object sender, EventArgs e)
		{
			if (cbNoteWrite.Checked == true)
				AccessLevel.Set(1, true);
			else
				AccessLevel.Set(1, false);
		}

        private void cbPartManagement_CheckedChanged(object sender, EventArgs e)
        {
            if (cbPartManagement.Checked == true)
                AccessLevel.Set(2, true);
            else
                AccessLevel.Set(2, false);
        }


		private void txtPW_TextChanged(object sender, EventArgs e)
		{
			if (edit_mode == Const.EditMode_Modify)
				if (first_pw_length != txtPW.Text.Length)
					revisePw = true;
		}

		private int getIntFromBitArray(BitArray bitArray)
		{
			int value = 0;
			for (int i = 0; i < bitArray.Count; i++)
			{
				if (bitArray[i])
					value += Convert.ToInt16(Math.Pow(2, i));
			}
			return value;
		}


 


    }
}
