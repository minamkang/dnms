﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Collections;

namespace DrawingNumberManager
{
    public partial class FormCommonPart : Form
    {
        private DataRow drTarget;
        private string drawingFlag;
        private int maker_code;
        private bool is_Modify = false;

        public FormCommonPart(DataRow drSource, bool isModify)
        {
            drTarget = drSource;
            is_Modify = isModify;

            InitializeComponent();
        }

        private void FormCommonPart_Load(object sender, EventArgs e)
        {
            SetData();
        }

        private void txtQty_TextChanged(object sender, EventArgs e)
        {
            if (txtQty.Text != "")
            {
                txtQty.Text = String.Format("{0:#,###}", int.Parse(txtQty.Text.Replace(",", "")));   //swkang 20201210
                txtQty.SelectionStart = txtQty.TextLength;
                txtQty.SelectionLength = 0;

                CalculationPrice();
            }
        }

        private void txtUnitPrice_TextChanged(object sender, EventArgs e)
        {
            if (txtUnitPrice.Text != "")
            {
                txtUnitPrice.Text = String.Format("{0:#,###}", int.Parse(txtUnitPrice.Text.Replace(",", "")));   //swkang 20201210
                txtUnitPrice.SelectionStart = txtUnitPrice.TextLength;
                txtUnitPrice.SelectionLength = 0;
                CalculationPrice();
            }
        }

        private void btnMaker_Click(object sender, EventArgs e)
        {
            FormMaker frmCompany = new FormMaker(false);
            if (frmCompany.ShowDialog() == DialogResult.OK)
            {
                txtMaker.Text = frmCompany.MakerName;
                maker_code = frmCompany.MakerCode;
            }
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            ArrayList alDetail = new ArrayList();
            string revice_code = "";
            DataTable result = null;
            SqlDataAdapter query = null;

            string key_id = drTarget.ItemArray[0].ToString();
            int user_key = StaticData.user_key;
            string project_code = string.Format("{0}{1}", cmbModel.SelectedValue.ToString(), cmbPart.SelectedValue.ToString());

            string DN = drTarget.ItemArray[7].ToString();
            drawingFlag = DN.Substring(0, 1);

            if (is_Modify == true)
            {
                if (drawingFlag == "E" || drawingFlag == "M")
                {
                    //TDN Revsion에 내용을 수정 한다.
                    result = new DataTable();
                    revice_code = drTarget.ItemArray[6].ToString().Substring(7, 1);
                    query = new SqlDataAdapter(String.Format("EXEC modify_detail_TDN {0}, '{1}', {2}, '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', {9}, {10}, {11}, {12}",
                        key_id, revice_code, StaticData.user_key, project_code, txtType.Text, txtPartName.Text, txtSpecfication.Text, txtMaterial.Text, txtAfterProcess.Text,
                        txtQty.Text.Replace(",", ""), maker_code, txtUnitPrice.Text.Replace(",", ""), txtPrice.Text.Replace(",", "")), StaticData.db_connetion_string);
                    query.Fill(result);
                }
                else
                {
                    //ODN Revsion에 내용을 수정 한다.
                    revice_code = drTarget.ItemArray[5].ToString();
                    result = new DataTable();
                    query = new SqlDataAdapter(String.Format("EXEC modify_detail_ODN {0}, '{1}', {2}, '{3}', '{4}', '{5}', '{6}', '{7}', {8}, {9}, {10}, {11}",
                        key_id, revice_code, StaticData.user_key, txtType.Text, txtPartName.Text, txtSpecfication.Text, txtMaterial.Text, txtAfterProcess.Text,
                        txtQty.Text.Replace(",", ""), maker_code, txtUnitPrice.Text.Replace(",", ""), txtPrice.Text.Replace(",", "")), StaticData.db_connetion_string);
                    query.Fill(result);
                }

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                //swkang 20200708 -- Part를 등록하지 않으면 Return
                if (cmbModel.SelectedValue.ToString() == "" || cmbPart.SelectedValue.ToString() == "")
                {
                    MessageBox.Show("Model과 Part는 반드시 등록해야 합니다.", "경고");
                    return;
                }

                if (drawingFlag == "E" || drawingFlag == "M")
                    revice_code = drTarget.ItemArray[6].ToString().Substring(7, 1);
                else
                    revice_code = drTarget.ItemArray[5].ToString();

                result = new DataTable();
                query = new SqlDataAdapter(String.Format("EXEC create_common_part {0}, '{1}', {2}, {3}, '{4}', '{5}', '{6}'",
                        key_id, revice_code, StaticData.user_key, txtQty.Text.Replace(",", ""), project_code, DN, txtNote.Text), StaticData.db_connetion_string);
                query.Fill(result);

                this.DialogResult = DialogResult.OK;
                this.Close();
            } 
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        } 

        private void SetData()
        {
            drTarget.ItemArray[8].ToString();


            for (short i = 1; i < 3; i++)
                BindCmbBox(i);

            if (drTarget.ItemArray[4].ToString() != "")
                lblDrawingNumber.Text = string.Format("DN:{0}_{1}", drTarget.ItemArray[4].ToString(), drTarget.ItemArray[5].ToString());
            else
            {
                if (drTarget.ItemArray[6].ToString() != "")
                    lblDrawingNumber.Text = string.Format("DN:{0}", drTarget.ItemArray[6].ToString());
            }

            txtPartName.Text = drTarget.ItemArray[10].ToString();
            txtType.Text = drTarget.ItemArray[11].ToString();
            txtSpecfication.Text = drTarget.ItemArray[12].ToString();
            txtMaterial.Text = drTarget.ItemArray[13].ToString();
            txtAfterProcess.Text = drTarget.ItemArray[14].ToString();
            txtQty.Text = drTarget.ItemArray[15].ToString();
            txtMaker.Text = drTarget.ItemArray[16].ToString();
            txtUnitPrice.Text = drTarget.ItemArray[17].ToString();
            txtPrice.Text = drTarget.ItemArray[18].ToString();
            txtNote.Text = drTarget.ItemArray[20].ToString();

            if (is_Modify)
            {
                if (drTarget.ItemArray[8] != null)
                    cmbModel.SelectedValue = GetCodeName(drTarget.ItemArray[8].ToString());
                if (drTarget.ItemArray[9] != null)
                    cmbPart.SelectedValue = GetCodeName(drTarget.ItemArray[9].ToString());

                if (lblDrawingNumber.Text != "DN") 
                {
                    //가도번인 있는 경우는 전체 수정이 가능
                    drawingFlag = lblDrawingNumber.Text.Substring(3, 1);
                    if (drawingFlag == "M" || drawingFlag == "E")
                    {
                        cmbModel.Enabled = true;
                        cmbPart.Enabled = true;
                    }
                    else
                    {
                        cmbModel.Enabled = false;
                        cmbPart.Enabled = false;
                    }

                    txtPartName.Enabled = true;
                    txtType.Enabled = true;
                    txtSpecfication.Enabled = true;
                    txtMaterial.Enabled = true;
                    txtAfterProcess.Enabled = true;
                    btnMaker.Enabled = true;
                    txtUnitPrice.Enabled = true;
                    txtPrice.Enabled = true;
                    txtNote.Enabled = false;
                }
            }
            else
                txtNote.Enabled = true;
        }

        private void BindCmbBox(short sType)
        {
            DataTable result = new DataTable();
            SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC code_info {0}", sType), StaticData.db_connetion_string);
            query.Fill(result);

            if (result != null)
            {
                if (sType == 1)
                {
                    result.Rows.Add("", "");
                    cmbModel.ValueMember = "CODE_NAME";
                    cmbModel.DisplayMember = "CODE_VALUE";
                    cmbModel.DataSource = result;
                    cmbModel.SelectedValue = "";
                }
                else if (sType == 2)
                {
                    result.Rows.Add("", "");
                    cmbPart.ValueMember = "CODE_NAME";
                    cmbPart.DisplayMember = "CODE_VALUE";
                    cmbPart.DataSource = result;
                    cmbPart.SelectedValue = "";
                }
            }
        }

        private string GetCodeName(string strCodeValue)
        {
            string result_value = "";
            
            try
            {    
                DataTable result = new DataTable();
                SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC get_code_name '{0}'", strCodeValue), StaticData.db_connetion_string);
                query.Fill(result);

                if (result.Rows.Count == 1)
                    result_value = result.Rows[0]["CODE_NAME"].ToString();
            }
            catch(Exception e)
            {
                e.ToString();
            }

            return result_value;
        }

        private void CalculationPrice()
        {
            if (txtQty.Text == "" || txtUnitPrice.Text == "")
                return;

            try
            {
                int qty = int.Parse(txtQty.Text.Replace(",", ""));
                int unit_price = int.Parse(txtUnitPrice.Text.Replace(",", ""));
                int price = unit_price * qty;

#if false
                txtPrice.Text = price.ToString();
#else
                txtPrice.Text = string.Format("{0:#,###}", price); //swkang 20201210
#endif
            }
            catch (Exception ex)
            {
                txtQty.Text = "0";
                txtUnitPrice.Text = "0";
                txtPrice.Text = "0";
                ex.ToString();
            }
        }

    }
}
