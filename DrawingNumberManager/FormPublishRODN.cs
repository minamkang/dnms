﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace DrawingNumberManager
{
    public partial class FormPublishRODN : Form
    {
        private string revise_official_drawing_number;
        private string input_drawing_number;
        private bool whether_Last_Revision_Code;

        private int maker_code;

        //swkang 20160908 add
        private string new_order_type;

        private DataTable detail_content;


        public FormPublishRODN(string strReviseDN, string strInputODN, bool whetherLastRevisionCode, string strNewOrderType, DataTable Detail_content)
        {
            revise_official_drawing_number = strReviseDN;
            input_drawing_number = strInputODN;
            whether_Last_Revision_Code = whetherLastRevisionCode;
            new_order_type = strNewOrderType;
            detail_content = Detail_content;
            
            InitializeComponent();
        }

        private void FormPublishRODN_Load(object sender, EventArgs e)
        {
            this.Text = Const.FORM_REVISE_DRAWING_NUMBER;

            if (whether_Last_Revision_Code == false)
            {
                string strMessage = "";
                strMessage += "입력하신 도면은 최신도면이 아닙니다.\r";
                strMessage += string.Format("최신 도면번호는  [{0}]입니다 \r", revise_official_drawing_number);
                strMessage += "그래도 갱신을 하려면 '예(Y)', 그렇지 않으면 '아니오(N)' 버튼을 누르세요.";

                if (MessageBox.Show(strMessage, "Warning", MessageBoxButtons.YesNo) == DialogResult.No)
                    this.Close();
            }
            lblInputDrawingNumber.Text = "입력도면:" + input_drawing_number;
            this.Height = 460;


            //기존 정보 Read
            txtType.Text = CheckDBNull(detail_content.Rows[0]["TYPE"]);
            txtPartName.Text = CheckDBNull(detail_content.Rows[0]["PART_NAME"]);
            txtSpecfication.Text = CheckDBNull(detail_content.Rows[0]["SPECFICATION"]);
            txtMaterial.Text = CheckDBNull(detail_content.Rows[0]["MATERIAL"]);
            txtAfterProcess.Text = CheckDBNull(detail_content.Rows[0]["APROCESS"]);
            txtQty.Text = CheckDBNull(detail_content.Rows[0]["QUANTITY"]);
            txtMaker.Text = CheckDBNull(detail_content.Rows[0]["NAME"]);
            txtUnitPrice.Text = CheckDBNull(detail_content.Rows[0]["UNIT_PRICE"]);
            txtPrice.Text = CheckDBNull(detail_content.Rows[0]["PRICE"]);
        }

        private void btnRevise_Click(object sender, EventArgs e)
        {
            OfficialDrawingNumber currentODN = null;
            
            //swkang 20190428 추가
            ArrayList al_Content = new ArrayList();
            al_Content.Add(txtType.Text);
            al_Content.Add(txtPartName.Text);
            al_Content.Add(txtSpecfication.Text);
            al_Content.Add(txtMaterial.Text);
            al_Content.Add(txtAfterProcess.Text);
            if (txtQty.Text == "")
                txtQty.Text = "0";
            al_Content.Add(txtQty.Text);

            if (txtMaker.Text == "")
                al_Content.Add(0);
            else
                al_Content.Add(maker_code);

            if (txtUnitPrice.Text == "")
                txtUnitPrice.Text = "0";
            al_Content.Add(txtUnitPrice.Text);

            if (txtPrice.Text == "")
                txtPrice.Text = "0";
            al_Content.Add(txtPrice.Text);

            if(new_order_type.Length == 0)
                currentODN = new OfficialDrawingNumber(revise_official_drawing_number, "", txtNote.Text, al_Content);
            else
                currentODN = new OfficialDrawingNumber(revise_official_drawing_number, txtNote.Text, new_order_type, "");

            string revisionOfficialDrawingNumber = currentODN.ToString();

            if (revisionOfficialDrawingNumber.Length == 11)
            {
                txtRevisionCode.Text = revisionOfficialDrawingNumber.Substring(10, 1);
                revisionOfficialDrawingNumber = revisionOfficialDrawingNumber.Substring(0, 10);
                if (revisionOfficialDrawingNumber.Length == Const.OFFICIAL_DRAWING_NO_LENGTH)
                {
                    ReviseDrawingNumberDisplayPanel.Location = new Point(8, 7);
                    ReviseDrawingNumberDisplayPanel.Visible = true;

                    txtReviseOfficialDrawNumber.Text = revisionOfficialDrawingNumber;

                    initializeDetailContent();
                    this.Height = 162;

                    txtNote.Text = "";
                    txtNote.Focus();
                }
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtNote.Text = "";

            initializeDetailContent();
            txtNote.Focus();
        }

        private void btnMain_Click(object sender, EventArgs e)
        {
            this.Close();
            DialogResult = DialogResult.OK;

            this.Height = 460;
        }

        private void btnMaker_Click(object sender, EventArgs e)
        {
            FormMaker frmCompany = new FormMaker(false);
            if (frmCompany.ShowDialog() == DialogResult.OK)
            {
                txtMaker.Text = frmCompany.MakerName;
                maker_code = frmCompany.MakerCode;
            }
        }

        private void txtReviseOfficialDrawNumber_TextChanged(object sender, EventArgs e)
        {
            ((TextBox)sender).SelectAll();
            ((TextBox)sender).Copy();
        }

        private void txtQty_TextChanged(object sender, EventArgs e)
        {
            if (txtQty.Text != "")
            {
                txtQty.Text = String.Format("{0:#,###}", int.Parse(txtQty.Text.Replace(",", "")));   //swkang 20201210
                txtQty.SelectionStart = txtQty.TextLength;
                txtQty.SelectionLength = 0;
                CalculationPrice();
            }
        }

        private void txtUnitPrice_TextChanged(object sender, EventArgs e)
        {
            if (txtUnitPrice.Text != "")
            {
                txtUnitPrice.Text = String.Format("{0:#,###}", int.Parse(txtUnitPrice.Text.Replace(",", "")));   //swkang 20201210
                txtUnitPrice.SelectionStart = txtUnitPrice.TextLength;
                txtUnitPrice.SelectionLength = 0;
                CalculationPrice();
            }
        }

        private void txtPrice_TextChanged(object sender, EventArgs e)
        {
            if (txtPrice.Text != "")
            {
                txtPrice.Text = String.Format("{0:#,###}", int.Parse(txtPrice.Text.Replace(",", "")));   //swkang 20201210
                txtPrice.SelectionStart = txtPrice.TextLength;
                txtPrice.SelectionLength = 0;
                CalculationPrice();
            }
        }

        private void CalculationPrice()
        {
            if (txtQty.Text == "" || txtUnitPrice.Text == "")
                return;

            try
            {
                int qty = int.Parse(txtQty.Text.Replace(",", ""));
                int unit_price = int.Parse(txtUnitPrice.Text.Replace(",", ""));
                int price = unit_price * qty;

#if false
                txtPrice.Text = price.ToString();
#else
                txtPrice.Text = string.Format("{0:#,###}", price); //swkang 20201210
#endif
            }
            catch (Exception ex)
            {
                txtQty.Text = "0";
                txtUnitPrice.Text = "0";
                txtPrice.Text = "0";
                ex.ToString();
            }
        }

        private string CheckDBNull(object oCheckData)
        {
            string result = "";
            if (oCheckData != DBNull.Value)
                result = oCheckData.ToString();

            return result;
        }

        private void initializeDetailContent()
        {
            txtType.Text = "";
            txtPartName.Text = "";
            txtSpecfication.Text = "";
            txtMaterial.Text = "";
            txtAfterProcess.Text = "";
            txtQty.Text = "0";
            txtMaker.Text = "";
            txtUnitPrice.Text = "0";
            txtPrice.Text = "0";
        }

    }
}
