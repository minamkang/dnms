﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using Microsoft.Office.Interop.Excel;

namespace DrawingNumberManager
{
    public partial class FormPartList : Form
    {
        bool columnAdded = false;		//최초 컬럼 표현할때 쓰인다.
        bool ServerSearch = false;		//날짜관련 Data가 변경 되면 Server에서 Search여부
        int user_level = 0;

        System.Data.DataTable result = null;
        System.Data.DataTable tempResult = null;
        Drawing dwg = new Drawing();
        ArrayList al_Drwaing = null;

        public FormPartList(int iUserLevel)
        {
            user_level = iUserLevel;
            InitializeComponent();
        }

        private void FormPartList_Load(object sender, EventArgs e)
        {
            this.Text = Const.FORM_PART_LIST;
            ServerSearch = true;

            //Excel 프로그램이 설치 되었는지 여부를 판단하여 Excel버튼을 보여질지 여부 판단
            Type officeType = Type.GetTypeFromProgID("Excel.Application");
            if (officeType == null)
                btnExcel.Visible = false;

            if (user_level != 7)
                tsmiDelete.Visible = false;

            BindUserID();

            //콤보박스 초기화
            for (short i = 1; i <= 2; i++)
                BindCmbBox(i);

            DateTime Now = DateTime.Now.Date;
            dtpickerStartDate.Value = Now.AddDays(-1);
            dtpickerEndDate.Value = Now.AddDays(1).AddSeconds(-1);

            BindDgvPartList();
            ButtonEnable();    
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            ServerSearch = true;
            BindDgvPartList();
            IsServerDrawingFile();
            ButtonEnable();
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            al_Drwaing = new ArrayList();

            for (int i = 0; i < dgvPartList.Rows.Count; i++)
            {
                if (dgvPartList.Rows[i].Cells["DownLoad"].Value == null)
                    continue;

                if (bool.Parse(dgvPartList.Rows[i].Cells["DownLoad"].Value.ToString()))
                    CreateDrawingName(i);
            }

            ucPbPartList.Visible = true;

            int SearchedDrawingCount = 0;
            if (dwg.Progress_Downloads(al_Drwaing, ucPbPartList, ref SearchedDrawingCount))
            {
                ucPbPartList.Visible = false;
                MessageBox.Show(string.Format("복사:{0}/전체:{1}\r\n파일 다운로드가 완료 되었습니다.", SearchedDrawingCount, al_Drwaing.Count), "경고");
            }
        }

        private void btnExcel_Click(object sender, EventArgs e)
        {
            Microsoft.Office.Interop.Excel._Application app = null;
            Workbook wkbook = null;
            Sheets sheetCollection = null;
            Worksheet wksheet = null;
            Range range = null;

            try
            {
                ucPbPartList.Visible = true;
                ucPbPartList.StatusFontColor = Color.Red;
                ucPbPartList.StatusText = "Excel 작업";

                Object oMissing = System.Reflection.Missing.Value;
                app = new Microsoft.Office.Interop.Excel.Application();
                app.Visible = false;
                app.UserControl = true;
                System.Globalization.CultureInfo ci = System.Globalization.CultureInfo.InvariantCulture;
                wkbook = app.Workbooks.Add(oMissing);
                sheetCollection = app.Worksheets;
                wksheet = (Worksheet)sheetCollection.get_Item(1);
                wksheet.Name = "Part List";

                // 제목 
                range = (Range)wksheet.Cells[1, 7];
                range.Value2 = "파트 리스트";
                range.Font.Size = 20;
                range.Font.Bold = true;

                // 컬럼명 추출
                for (int j = 1; j < dgvPartList.Columns.Count; j++)
                {
                    if (dgvPartList.Columns[j].Visible.Equals(true))
                    {
                        range = (Range)wksheet.Cells[3, j - 1];
                        range.Value2 = dgvPartList.Columns[j].HeaderText;

                        if (range.Value2 == "NOTE")
                        {
                            range.Value2 = "";
                            range = (Range)wksheet.Cells[3, j - 2];
                            range.Value2 = dgvPartList.Columns[j].HeaderText;
                        }

                        range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightBlue);
                    }
                }

                // 데이터 추출
                for (int i = 0; i < dgvPartList.Rows.Count; i++)
                {
                    for (int j = 1; j < dgvPartList.Columns.Count; j++)
                    {
                        if (dgvPartList.Columns[j].Visible.Equals(true))
                        {
                            range = (Range)wksheet.Cells[i + 4, j - 1];
                            if (j == 20) //공용부품
                            {
                                if (dgvPartList.Rows[i].Cells[j].Value.ToString() == "2")
                                {
                                    range.Value2 = "O";

                                    string Row = string.Format("A{0}", i + 4);
                                    string Columns = string.Format("T{0}", i + 4);
                                    range = wksheet.Range[Row, Columns];
                                    range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightGray);
                                }
                                else
                                    range.Value2 = "";
                            }
                            else
                                range.Value2 = dgvPartList.Rows[i].Cells[j].Value.ToString();
                        }
                    }

                    ucPbPartList.Message = string.Format("{0}행/{1}행.", i, dgvPartList.Rows.Count);
                    ucPbPartList.Progress = (int)(i * 100 / dgvPartList.Rows.Count);
                    System.Windows.Forms.Application.DoEvents();
                }

                //사이즈 조절
                wksheet.Columns.AutoFit();
                wksheet.Rows.AutoFit();
                app.Visible = true;

                btnExcel.Enabled = false;
                ucPbPartList.Visible = false;

                ucPbPartList.StatusFontColor = Color.Black;
                ucPbPartList.StatusText = "";
                ucPbPartList.Message = "";
                ucPbPartList.Progress = 0;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        private void tsmiAdd_Click(object sender, EventArgs e)
        {
            dgvPartListEdit(dgvPartList, false);
        }

        private void tsmiModify_Click(object sender, EventArgs e)
        {
            dgvPartListEdit(dgvPartList, true);
        }

        private void tsmiDelete_Click(object sender, EventArgs e)
        {
            if (dgvPartList.Rows[dgvPartList.CurrentRow.Index].Cells["공용"].Value.ToString() == "2")
            {
                int key = int.Parse(dgvPartList.Rows[dgvPartList.CurrentRow.Index].Cells["KEY_ID"].Value.ToString());

                string revise = dgvPartList.Rows[dgvPartList.CurrentRow.Index].Cells["REV"].Value.ToString();
                if (revise.Trim() == "")
                {
                    string tdn = dgvPartList.Rows[dgvPartList.CurrentRow.Index].Cells["(T)도번"].Value.ToString();
                    revise = tdn.Substring(7, 1);
                }

                string model = dgvPartList.Rows[dgvPartList.CurrentRow.Index].Cells["모델"].Value.ToString();
                string Part = dgvPartList.Rows[dgvPartList.CurrentRow.Index].Cells["파트"].Value.ToString();
                string dn = dgvPartList.Rows[dgvPartList.CurrentRow.Index].Cells["T도번"].Value.ToString();

                SqlDataAdapter query = null;
                string temp = String.Format("EXEC delete_common_part {0}, '{1}', '{2}', '{3}', '{4}'", key, revise, model, Part, dn);

                query = new SqlDataAdapter(String.Format("EXEC delete_common_part {0}, '{1}', '{2}', '{3}', '{4}'", key, revise, model, Part, dn), StaticData.db_connetion_string);
                query.Fill(result);

                ServerSearch = true;
                BindDgvPartList();
            }
        }

        private void cmbModel_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDgvPartList();
        }

        private void cmbPart_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDgvPartList();
        }

        private void cmbUserID_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDgvPartList();
        }

        private void txtPartName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                BindDgvPartList();
        }

        private void txtNote_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                BindDgvPartList();
        }

        private void dgvPartList_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            IsServerDrawingFile();
        }


        private void BindUserID()
        {
            System.Data.DataTable result = new System.Data.DataTable();
            SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC user_infomation 0"), StaticData.db_connetion_string);
            query.Fill(result);

            if (result != null)
            {
                result.DefaultView.Sort = "NAME ASC";
                result.Rows.Add("ALL", "ALL", "ALL", "ALL", "ALL");
                cmbUserID.ValueMember = "ID";
                cmbUserID.DisplayMember = "NAME";
                cmbUserID.DataSource = result;
                cmbUserID.SelectedValue = "ALL";
            }
        }

        private void BindCmbBox(short sType)
        {
            System.Data.DataTable result = new System.Data.DataTable();
            SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC code_info {0}", sType), StaticData.db_connetion_string);
            query.Fill(result);
            result.Rows.Add("", "");

            if (result != null)
            {
                if (sType == 1)
                {
                    cmbModel.ValueMember = "CODE_NAME";
                    cmbModel.DisplayMember = "CODE_VALUE";
                    cmbModel.DataSource = result;
                    cmbModel.SelectedIndex = cmbModel.Items.Count - 1;
                }
                else if (sType == 2)
                {
                    cmbPart.ValueMember = "CODE_NAME";
                    cmbPart.DisplayMember = "CODE_VALUE";
                    cmbPart.DataSource = result;
                    cmbPart.SelectedIndex = cmbPart.Items.Count - 1;
                }
            }
        }

        private void ButtonEnable()
        {
            if (dgvPartList.Rows.Count > 0)
                btnExcel.Enabled = true;
            else
                btnExcel.Enabled = false;
        }

        private void BindDgvPartList()
        {
            if(ServerSearch)
            {
                result = new System.Data.DataTable();
                tempResult = new System.Data.DataTable();

                //Data History정보를 모두 읽어온다.
                SqlDataAdapter query = null;

                if (txtDrawingNumber.Text.Length == 0 && txtDrawingNumber.Text == "")
                    query = new SqlDataAdapter(String.Format("EXEC part_list '{0}', '{1}', ' '",
                        dtpickerStartDate.Value.ToString(Const.DATE_TIME_FORMAT), dtpickerEndDate.Value.ToString(Const.DATE_TIME_FORMAT)), StaticData.db_connetion_string);
                else if (txtDrawingNumber.Text.Length > 0 && txtDrawingNumber.Text != "")
                    query = new SqlDataAdapter(String.Format("EXEC part_list '', '', '{0}%'", txtDrawingNumber.Text), StaticData.db_connetion_string);

                query.Fill(result);
                ServerSearch = false;

                result.Columns[1].ColumnName = "생성일자";
                result.Columns[2].ColumnName = "생성자";
                result.Columns[3].ColumnName = "수정자";
                result.Columns[4].ColumnName = "眞도번";
                result.Columns[5].ColumnName = "REV";
                result.Columns[6].ColumnName = "假도번";
                result.Columns[7].ColumnName = "T도번";
                result.Columns[8].ColumnName = "모델";
                result.Columns[9].ColumnName = "파트";
                result.Columns[10].ColumnName = "품명";
                result.Columns[11].ColumnName = "종류";
                result.Columns[12].ColumnName = "규격";
                result.Columns[13].ColumnName = "재질";
                result.Columns[14].ColumnName = "후처리";
                result.Columns[15].ColumnName = "수량";
                result.Columns[16].ColumnName = "업체";
                result.Columns[17].ColumnName = "단가";
                result.Columns[18].ColumnName = "금액";
                result.Columns[19].ColumnName = "공용";
                result.Columns[20].ColumnName = "Note";

                dgvPartList.DataSource = result;
                dgvPartListFormated();
            }

            tempResult = result.Copy();

            if (tempResult != null && tempResult.Rows.Count > 0)
            {
                DataRow[] dtrow = null;

                string Model = cmbModel.Text.ToString();
                string Part = cmbPart.Text.ToString();
                string ID = cmbUserID.SelectedValue.ToString();
                string PartName = txtPartName.Text;
                string Note = txtNote.Text;

                if (txtPartName.Text.Length > 0)
                    PartName = txtPartName.Text.Replace("\r\n", "");
                if (txtNote.Text.Length > 0)
                    Note = txtNote.Text.Replace("\r\n", "");

                if (Model != "" && Part == "" && PartName == "" && ID == "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("모델 = '{0}' ", Model));
                else if (Model == "" && Part != "" && PartName == "" && ID == "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("파트 = '{0}'", Part));
                else if (Model == "" && Part == "" && PartName != "" && ID == "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("품명 LIKE '%{0}%'", PartName));
                else if (Model == "" && Part == "" && PartName == "" && ID != "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("생성자 = '{0}'", ID));
                else if (Model == "" && Part == "" && PartName == "" && ID == "ALL" && Note != "")
                    dtrow = tempResult.Select(string.Format("NOTE LIKE '%{0}%'", Note));

                else if (Model != "" && Part != "" && PartName == "" && ID == "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("모델 = '{0}' AND 파트 = '{1}'", Model, Part));
                else if (Model != "" && Part == "" && PartName != "" && ID == "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("모델 = '{0}' AND 품명 LIKE '%{1}%'", Model, PartName));
                else if (Model != "" && Part == "" && PartName == "" && ID != "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("모델 = '{0}' AND 생성자 = '{1}'", Model, ID));
                else if (Model != "" && Part == "" && PartName == "" && ID == "ALL" && Note != "")
                    dtrow = tempResult.Select(string.Format("모델 = '{0}' AND NOTE LIKE '%{1}%'", Model, Note));
                else if (Model == "" && Part != "" && PartName != "" && ID == "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("파트 = '{0}' AND 품명 LIKE '%{1}%'", Part, PartName));
                else if (Model == "" && Part != "" && PartName == "" && ID != "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("파트 = '{0}' AND 생성자 = '{1}'", Part, ID));
                else if (Model == "" && Part != "" && PartName == "" && ID == "ALL" && Note != "")
                    dtrow = tempResult.Select(string.Format("파트 = '{0}' AND NOTE LIKE '%{1}%'", Part, Note));
                else if (Model == "" && Part == "" && PartName != "" && ID != "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("품명 LIKE '%{0}%' AND 생성자 = '{1}'", PartName, ID));
                else if (Model == "" && Part == "" && PartName != "" && ID == "ALL" && Note != "")
                    dtrow = tempResult.Select(string.Format("품명 LIKE '%{0}%' AND NOTE LIKE '%{1}%'", PartName, Note));
                else if (Model == "" && Part == "" && PartName == "" && ID != "ALL" && Note != "")
                    dtrow = tempResult.Select(string.Format("생성자 = '{0}' AND NOTE LIKE '%{1}%'", ID, Note));

                else if (Model != "" && Part != "" && PartName != "" && ID == "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("모델 = '{0}' AND 파트 = '{1}' AND 품명 LIKE '%{2}%'", Model, Part, PartName));
                else if (Model != "" && Part != "" && PartName == "" && ID != "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("모델 = '{0}' AND 파트 = '{1}' AND 생성자 = '{2}'", Model, Part, ID));
                else if (Model != "" && Part != "" && PartName == "" && ID == "ALL" && Note != "")
                    dtrow = tempResult.Select(string.Format("모델 = '{0}' AND 파트 = '{1}' AND NOTE LIKE '%{2}%'", Model, Part, Note));
                else if (Model != "" && Part == "" && PartName != "" && ID != "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("모델 = '{0}' AND 품명 LIKE '%{1}%' AND 생성자 = '{2}'", Model, PartName, ID));
                else if (Model != "" && Part == "" && PartName == "" && ID != "ALL" && Note != "")
                    dtrow = tempResult.Select(string.Format("모델 = '{0}' AND 생성자 = '{1}' AND NOTE LIKE '%{2}%'", Model, ID, Note));

                else if (Model == "" && Part != "" && PartName != "" && ID != "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("파트 = '{0}' AND 품명 LIKE '%{1}%' AND 생성자 = '{2}'", Part, PartName, ID));
                else if (Model == "" && Part != "" && PartName != "" && ID != "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("파트 = '{0}' AND 생성자 = '{1}' AND NOTE LIKE '%{2}%' ", Part, ID, Note));
                else if (Model == "" && Part == "" && PartName != "" && ID != "ALL" && Note != "")
                    dtrow = tempResult.Select(string.Format("품명 LIKE '%{0}%' AND 생성자 = '{1}' AND Note LIKE '%{2}%'", PartName, ID, Note));

                else if (Model != "" && Part != "" && PartName != "" && ID != "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("모델 = '{0}' AND 파트 = '{1}' AND 품명 LIKE '%{2}%' AND 생성자 = '{3}'", Model, Part, PartName, ID));
                else if (Model != "" && Part == "" && PartName != "" && ID != "ALL" && Note != "")
                    dtrow = tempResult.Select(string.Format("모델 = '{0}' AND 품명 LIKE '%{1}%' AND 생성자 = '{2}' AND NOTE LIKE '%{3}%'", Model, PartName, ID, Note));
                else if (Model != "" && Part != "" && PartName == "" && ID != "ALL" && Note != "")
                    dtrow = tempResult.Select(string.Format("모델 = '{0}' AND 파트 = '{1}' AND 생성자 = '{2}' AND NOTE LIKE '%{3}%'", Model, Part, ID, Note));
                else if (Model == "" && Part != "" && PartName != "" && ID != "ALL" && Note != "")
                    dtrow = tempResult.Select(string.Format("파트 = '{0}' AND 품명 LIKE '%{1}%' AND 생성자 = '{2}' AND NOTE LIKE '%{3}%'", Part, PartName, ID, Note));
                else if (Model != "" && Part != "" && PartName != "" && ID == "ALL" && Note != "")
                    dtrow = tempResult.Select(string.Format("모델 = '{0}' AND 파트 = '{1}' AND 품명 LIKE '%{2}%' AND Note LIKE '%{3}%'", Model, Part, PartName, Note));

                else if (Model != "" && Part != "" && PartName != "" && ID != "ALL" && Note != "")
                    dtrow = tempResult.Select(string.Format("모델 = '{0}' AND 파트 = '{1}' AND 품명 LIKE '%{2}%' AND 생성자 = '{3}' AND Note LIKE '%{4}%'", Model, Part, PartName, ID, Note));

                else if (Model == "" && Part == "" && PartName == "" && ID == "ALL" && Note == "")
                {
                    dgvPartList.DataSource = tempResult;
                    IsServerDrawingFile();
                }

                if (dtrow == null)
                    return;

                if (dtrow.Length > 0)
                    tempResult = dtrow.CopyToDataTable();
                else
                    tempResult.Rows.Clear();

                dgvPartList.DataSource = tempResult;
                IsServerDrawingFile();
            }
        }

        private void dgvPartListFormated()
        {
            this.dgvPartList.Columns["KEY_ID"].Visible = false;
            this.dgvPartList.Columns["BEFORE_NOTE"].Visible = false;

            this.dgvPartList.Columns["생성일자"].Width = 130;
            this.dgvPartList.Columns["생성자"].Width = 80;
            this.dgvPartList.Columns["수정자"].Width = 80;
            this.dgvPartList.Columns["眞도번"].Width = -100;
            this.dgvPartList.Columns["REV"].Width = 50;
            this.dgvPartList.Columns["假도번"].Width = -100;
            this.dgvPartList.Columns["T도번"].Width = 90;
            this.dgvPartList.Columns["모델"].Width = 70;
            this.dgvPartList.Columns["파트"].Width = 110;
            this.dgvPartList.Columns["종류"].Width = 70;
            this.dgvPartList.Columns["품명"].Width = 120;
            this.dgvPartList.Columns["규격"].Width = 100;
            this.dgvPartList.Columns["재질"].Width = 100;
            this.dgvPartList.Columns["후처리"].Width = 100;
            this.dgvPartList.Columns["수량"].Width = 60;
            this.dgvPartList.Columns["업체"].Width = 120;
            this.dgvPartList.Columns["단가"].Width = 70;
            this.dgvPartList.Columns["금액"].Width = 90;
            this.dgvPartList.Columns["Note"].Width = 350;
            this.dgvPartList.Columns["공용"].Width = 0;

            this.dgvPartList.Columns["생성일자"].ReadOnly = true;
            this.dgvPartList.Columns["생성자"].ReadOnly = true;
            this.dgvPartList.Columns["수정자"].ReadOnly = true;
            this.dgvPartList.Columns["眞도번"].ReadOnly = true;
            this.dgvPartList.Columns["REV"].ReadOnly = true;
            this.dgvPartList.Columns["假도번"].ReadOnly = true;
            this.dgvPartList.Columns["T도번"].ReadOnly = true;
            this.dgvPartList.Columns["모델"].ReadOnly = true;
            this.dgvPartList.Columns["파트"].ReadOnly = true;
            this.dgvPartList.Columns["종류"].ReadOnly = true;
            this.dgvPartList.Columns["품명"].ReadOnly = true;
            this.dgvPartList.Columns["규격"].ReadOnly = true;
            this.dgvPartList.Columns["재질"].ReadOnly = true;
            this.dgvPartList.Columns["후처리"].ReadOnly = true;
            this.dgvPartList.Columns["수량"].ReadOnly = true;
            this.dgvPartList.Columns["수량"].DefaultCellStyle.Format = "#,###";
            this.dgvPartList.Columns["업체"].ReadOnly = true;
            this.dgvPartList.Columns["단가"].ReadOnly = true;
            this.dgvPartList.Columns["단가"].DefaultCellStyle.Format = "#,###";
            this.dgvPartList.Columns["금액"].ReadOnly = true;
            this.dgvPartList.Columns["금액"].DefaultCellStyle.Format = "#,###";
            this.dgvPartList.Columns["Note"].ReadOnly = true;
            this.dgvPartList.Columns["공용"].ReadOnly = true;

            if (columnAdded)
                return;

            DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn();
            checkBoxColumn.Name = "DownLoad";
            checkBoxColumn.HeaderText = "다운로드";
            checkBoxColumn.TrueValue = true;
            checkBoxColumn.FalseValue = false;
            dgvPartList.AutoGenerateColumns = false;
            dgvPartList.Columns.Insert(0, checkBoxColumn);
            dgvPartList.Columns[0].Width = 50;            
            columnAdded = true;
        }

        private void dgvPartListEdit(DataGridView dgv, bool isModify)
        {
            if (dgvPartList.Rows.Count == 0)
                return;

            int index = 0;
            DataRow dr = null;

            if (dgvPartList.Rows[dgvPartList.CurrentRow.Index].Cells["공용"].Value.ToString() == "2")
            {
                if (isModify == true)
                    MessageBox.Show("공용품목은 한번 추가되면 수정 할 수 없습니다.\r\n 권한이 있는 분에게 요청하십시요.", "경고");
                else
                    MessageBox.Show("공용품목은 추가는 도번이 있는 최상위 품목을 선택해야 합니다.", "경고");
                return;
            }
            else
            {
                index = dgvPartList.SelectedRows[0].Index;
                
                string strTDN = dgvPartList.Rows[index].Cells["假도번"].Value.ToString();
                string strODN = dgvPartList.Rows[index].Cells["眞도번"].Value.ToString();
                string strODN_Rev = dgvPartList.Rows[index].Cells["REV"].Value.ToString();

                if (strODN == "" && strTDN != "") //가도번
                {
                    int iTDNKey = GetDNKey(true, strTDN);
                    char lastRevision = GetDNLastRevisionCode(true, iTDNKey, "");

                    if (strTDN.Substring(7, 1) != lastRevision.ToString())
                    {
                        string strMessage = "";
                        strMessage += "선택된 (假)도번은 최신이 아닙니다.\r";
                        strMessage += "최신 도번으로 이동을 할려면 '예(Y)', 그렇지 않으면 '아니오(N)' 버튼을 누르세요";

                        if (MessageBox.Show(strMessage, "Warning", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            //최종 위치로 이동을 한다.
                            string strDeleteReviseCdTDN = "";
                            string strLastReviseTDN = "";

                            strDeleteReviseCdTDN = strTDN.Remove(7, 1);
                            strLastReviseTDN = strDeleteReviseCdTDN + lastRevision;

                            System.Data.DataTable table = (System.Data.DataTable)dgvPartList.DataSource;
                            DataRow[] rows = table.Select(string.Format("假도번 = '{0}'", strLastReviseTDN));
                            int selectedIndex = table.Rows.IndexOf(rows[0]);

                            dgvPartList.Rows[index].Selected = false;
                            dgvPartList.Rows[selectedIndex].Selected = true;

                            contextMenuStrip.Visible = false;
                            dr = (dgvPartList.Rows[selectedIndex].DataBoundItem as DataRowView).Row;
                        }
                        else
                            return;
                    }
                    else
                        dr = (dgvPartList.Rows[dgvPartList.CurrentRow.Index].DataBoundItem as DataRowView).Row;
                }
                else if (strODN != "")             //진도번
                {
                    int iODNKey = GetDNKey(false, strODN);
                    char lastRevision = GetDNLastRevisionCode(false, iODNKey, strODN);

                    if (strODN_Rev != lastRevision.ToString())
                    {
                        string strMessage = "";
                        strMessage += "선택된 (眞)도번은 최신이 아닙니다.\r";
                        strMessage += "최신 도번으로 이동을 할려면 '예(Y)', 그렇지 않으면 '아니오(N)' 버튼을 누르세요";

                        if (MessageBox.Show(strMessage, "Warning", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            //최종 위치로 이동을 한다.
                            System.Data.DataTable table = (System.Data.DataTable)dgvPartList.DataSource;
                            DataRow[] rows = table.Select(string.Format("眞도번 = '{0}' AND REV = '{1}'", strODN, lastRevision.ToString()));

                            int selectedIndex = -1;
                            if (rows.Length > 0)
                                selectedIndex = table.Rows.IndexOf(rows[0]);
                            else
                            {
                                MessageBox.Show("공용부품의 원도번(진도번, 가도번) 최종리비전을 찾으세요.", "경고");
                                return;
                            }

                            dgvPartList.Rows[index].Selected = false;
                            dgvPartList.Rows[selectedIndex].Selected = true;

                            contextMenuStrip.Visible = false;
                            dr = (dgvPartList.Rows[selectedIndex].DataBoundItem as DataRowView).Row;
                        }
                        else
                            return;
                    }
                    else
                        dr = (dgvPartList.Rows[dgvPartList.CurrentRow.Index].DataBoundItem as DataRowView).Row;
                }
            }
            
            FormCommonPart frmCommPart = new FormCommonPart(dr, isModify);
            if (frmCommPart.ShowDialog() == DialogResult.OK)
            {
                ServerSearch = true;
                BindDgvPartList();
            }
        }

        private void dgvPartList_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.ColumnIndex == 0 && e.RowIndex == -1)
            {
                e.PaintBackground(e.ClipBounds, false);

                System.Drawing.Point pt = e.CellBounds.Location;  // where you want the bitmap in the cell

                int nChkBoxWidth = 15;
                int nChkBoxHeight = 15;
                int offsetx = (e.CellBounds.Width - nChkBoxWidth) / 2;
                int offsety = (e.CellBounds.Height - nChkBoxHeight) / 2;

                pt.X += offsetx;
                pt.Y += offsety;

                System.Windows.Forms.CheckBox cb = new System.Windows.Forms.CheckBox();
                cb.Size = new Size(nChkBoxWidth, nChkBoxHeight);
                cb.Location = pt;
                cb.CheckedChanged += new EventHandler(dgvPartListCheckBox_CheckedChanged);

                ((DataGridView)sender).Controls.Add(cb);

                e.Handled = true;
            }
        }

        private void dgvPartListCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow r in dgvPartList.Rows)
            {
                r.Cells["DownLoad"].Value = ((System.Windows.Forms.CheckBox)sender).Checked;
            }
        }
        
        private string DgvCheckNull(object value)
        {
            string strResult = "";
            if (value != null)
                strResult = value.ToString();

            return strResult;
        }

        private void dgvPartList_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
                contextMenuStrip.Show(Cursor.Position.X, Cursor.Position.Y);
        }

        private void dgvPartList_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                try
                {
                    dgvPartList.CurrentCell = dgvPartList.Rows[e.RowIndex].Cells[e.ColumnIndex];
                    dgvPartList.Rows[e.RowIndex].Selected = true;
                    dgvPartList.Focus();
                }
                catch (Exception)
                {
                }
            }
        }

        private void dgvPartList_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == dgvPartList.Columns["공용"].Index)
            {
                string value = e.Value.ToString();
                if (value != null)
                {
                    if (value == "2")
                        dgvPartList.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightGray;
                }
            }
        }

        private int GetDNKey(bool isTDN, string strDN)
        {
            int resultDNKey = 0;
            SqlCommand cmd = null;
            if(isTDN)
                cmd = new SqlCommand(String.Format("EXEC drawing_key_of '{0}'", strDN), new SqlConnection(StaticData.db_connetion_string));
            else
                cmd = new SqlCommand(String.Format("EXEC official_drawing_key_of '{0}'", strDN), new SqlConnection(StaticData.db_connetion_string));

            cmd.Connection.Open();
            resultDNKey = (int)cmd.ExecuteScalar();
            cmd.Connection.Close();
            return resultDNKey;
        }

        private char GetDNLastRevisionCode(bool isTDN, int currentDNKey, string strODN)
        {
            string resultDNLastRevisionCode = "";
            SqlCommand query = null;
            if (isTDN)
                query = new SqlCommand(String.Format("EXEC last_revision_code {0}, '{1}'", currentDNKey, DrawingCategory.code), new SqlConnection(StaticData.db_connetion_string));
            else
            {
                string ProjectCode = strODN.Substring(0, 6);
                string OrderType = strODN.Substring(9, 1);

                query = new SqlCommand(String.Format("EXEC official_last_revision_code {0}, '{1}', '{2}'", currentDNKey, ProjectCode, OrderType), new SqlConnection(StaticData.db_connetion_string));
            }
            query.Connection.Open();
            resultDNLastRevisionCode = (string)query.ExecuteScalar();
            query.Connection.Close();
            return resultDNLastRevisionCode[0];
        }

        private void IsServerDrawingFile()      //도면 유무 검색
        {
            al_Drwaing = new ArrayList();
            for (int i = 0; i < dgvPartList.Rows.Count; i++)
                CreateDrawingName(i);
            ArrayList al_result = dwg.SearchFile(al_Drwaing);
            for (int i = 0; i < dgvPartList.Rows.Count; i++)
            {
                for (int j = 0; j < al_result.Count; j++)
                {
                    if (i == int.Parse(al_result[j].ToString()))
                        dgvPartList.Rows[i].Cells["T도번"].Style.BackColor = Color.Pink;
                }
            }
        }

        private void CreateDrawingName(int dgvTotalCount) // 도면파일이름 만들기
        {
            if (dgvPartList.Rows[dgvTotalCount].Cells["眞도번"].Value.ToString().Trim() != "")
                al_Drwaing.Add(string.Format("{0}_{1}", dgvPartList.Rows[dgvTotalCount].Cells["眞도번"].Value.ToString().Trim(), dgvPartList.Rows[dgvTotalCount].Cells["REV"].Value.ToString().Trim()));
            else if (dgvPartList.Rows[dgvTotalCount].Cells["眞도번"].Value.ToString().Trim() == "" && dgvPartList.Rows[dgvTotalCount].Cells["假도번"].Value.ToString().Trim() != "")
                al_Drwaing.Add(dgvPartList.Rows[dgvTotalCount].Cells["假도번"].Value.ToString().Trim());
            else if (dgvPartList.Rows[dgvTotalCount].Cells["眞도번"].Value.ToString().Trim() == "" && dgvPartList.Rows[dgvTotalCount].Cells["假도번"].Value.ToString().Trim() == "" && dgvPartList.Rows[dgvTotalCount].Cells["T도번"].Value.ToString().Trim() != "")
            {
                if (dgvPartList.Rows[dgvTotalCount].Cells["T도번"].Value.ToString().Trim().Length == 10)
                    al_Drwaing.Add(string.Format("{0}_{1}", dgvPartList.Rows[dgvTotalCount].Cells["T도번"].Value.ToString().Trim(), dgvPartList.Rows[dgvTotalCount].Cells["REV"].Value.ToString().Trim()));
                else
                    al_Drwaing.Add(dgvPartList.Rows[dgvTotalCount].Cells["T도번"].Value.ToString().Trim());
            }
        }

    }
}
