﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DrawingNumberManager
{
    public partial class FormManagerProject : Form
    {
        private short  ProjectType;
        private string edit_mode;

        public FormManagerProject(short strProjectType)
        {
            ProjectType = strProjectType;

            InitializeComponent();
        }

        private void FormManagerProject_Load(object sender, EventArgs e)
        {
            SelectProjectType();
            BindDgvProject();
            Init_InputValue(Const.EditMode_None);
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            Init_InputValue(Const.EditMode_Insert);
        }

        private void btnModify_Click(object sender, EventArgs e)
        {
            Init_InputValue(Const.EditMode_Modify);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            Init_InputValue(Const.EditMode_Delete);
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            switch (edit_mode)
            {
                case Const.EditMode_Insert:
                    ProjectCodeUID('I');
                    break;
                case Const.EditMode_Modify:
                    ProjectCodeUID('U');
                    break;
                case Const.EditMode_Delete:
                    ProjectCodeUID('D');
                    break;
            }
            Init_InputValue(Const.EditMode_None);
            BindDgvProject();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Init_InputValue(Const.EditMode_None);
            BindDgvProject();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgvProjectTypeOfContent_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < -1)
                return;

            txtCode.Text = this.dgvProjectTypeOfContent.Rows[e.RowIndex].Cells["CODE_NAME"].Value.ToString();
            txtCodeValue.Text = this.dgvProjectTypeOfContent.Rows[e.RowIndex].Cells["CODE_VALUE"].Value.ToString();
        }

        private void SelectProjectType()
        {
            switch (ProjectType)
            {
                case Const.CODE_MODEL:
                    cmbProjectType.SelectedIndex = 0;
                    break;
                case Const.CODE_PART:
                    cmbProjectType.SelectedIndex = 1;
                    break;
                case Const.CODE_PROCESS:
                    cmbProjectType.SelectedIndex = 2;
                    break;
            }
        }

        private void BindDgvProject()
        {
            DataTable result = new DataTable();
            SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC code_info '{0}'", ProjectType), StaticData.db_connetion_string);
            query.Fill(result);

            dgvProjectTypeOfContent.DataSource = result;
        }

        private void ProjectCodeUID(char flagUID)
        {
            SqlConnection connection = new SqlConnection(StaticData.db_connetion_string);
            connection.Open();

            try
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "code_info_IUD";

                cmd.Parameters.Add("@PROJECT_TYPE", SqlDbType.Int, 1).Value = ProjectType;
                cmd.Parameters.Add("@CODE", SqlDbType.VarChar, 3).Value = txtCode.Text;
                cmd.Parameters.Add("@VALUE", SqlDbType.VarChar, 100).Value = txtCodeValue.Text;
                cmd.Parameters.Add("@IUD_Flag", SqlDbType.Char, 1).Value = flagUID;

                int excuteCount = cmd.ExecuteNonQuery();
                if (excuteCount != 1)
                {
                    MessageBox.Show(string.Format("ProjectCodeUID가 [{0}]를 하지 못하였습니다. ", flagUID));
                }
                connection.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show("code_info_IUD \r" + ex.ToString());
            }
            connection.Close();
        }


        private void Init_InputValue(string EditMode)
        {
            switch (EditMode)
            {
                case Const.EditMode_Insert:
                    SetClear();
                    this.txtCode.Enabled = true;
                    this.txtCodeValue.Enabled = true;
                    this.dgvProjectTypeOfContent.Enabled = false;
                    this.btnModify.Visible = false;
                    this.btnDelete.Visible = false;
                    this.btnInsert.Enabled = false;
                    this.btnOK.Visible = true;
                    this.btnCancel.Visible = true;
                    this.btnClose.Visible = false;
                    break;
                case Const.EditMode_Modify:
                    this.txtCode.Enabled = false;
                    this.txtCodeValue.Enabled = true;
                    this.dgvProjectTypeOfContent.Enabled = false;
                    this.btnInsert.Visible = false;
                    this.btnDelete.Visible = false;
                    this.btnModify.Enabled = false;
                    this.btnOK.Visible = true;
                    this.btnCancel.Visible = true;
                    this.btnClose.Visible = false;
                    break;
                case Const.EditMode_Delete:
                    this.txtCode.Enabled = false;
                    this.txtCodeValue.Enabled = false;
                    this.dgvProjectTypeOfContent.Enabled = false;
                    this.btnInsert.Visible = false;
                    this.btnModify.Visible = false;
                    this.btnDelete.Enabled = false;
                    this.btnOK.Visible = true;
                    this.btnCancel.Visible = true;
                    this.btnClose.Visible = false;
                    break;
                case Const.EditMode_None:
                    this.txtCode.Enabled = false;
                    this.txtCodeValue.Enabled = false;
                    this.dgvProjectTypeOfContent.Enabled = true;
                    this.btnInsert.Visible = true;
                    this.btnModify.Visible = true;
                    this.btnDelete.Visible = true;
                    this.btnInsert.Enabled = true;
                    this.btnModify.Enabled = true;
                    this.btnDelete.Enabled = true;
                    this.btnOK.Visible = false;
                    this.btnCancel.Visible = false;
                    this.btnClose.Visible = true;
                    break;
                default:
                    break;
            }
            edit_mode = EditMode;
        }

        private void SetClear()
        {
            this.txtCode.Text = "";
            this.txtCode.Text = "";
            this.txtCodeValue.Text = "";
        }





    }
}
