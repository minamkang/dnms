﻿namespace DrawingNumberManager
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.msMain = new System.Windows.Forms.MenuStrip();
            this.tsmiSetup = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiModifyPassword = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiConfiguration = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiManagement = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiModel = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiPart = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiMaker = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiPartList = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiReport = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDrawingNumberHistory = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiOfficialDrawingNumberHistory = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiNewDrawingNumber = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiReviseDrawinNumber = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiNewOfficialDrawingNumber = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiReviseOfficialDrawinNumber = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiNewOrderType = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiOrder = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiExit = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlNewDrawingNumber = new System.Windows.Forms.Panel();
            this.gbODNDetail = new System.Windows.Forms.GroupBox();
            this.btnMaker = new System.Windows.Forms.Button();
            this.cmbModel = new System.Windows.Forms.ComboBox();
            this.lblModel = new System.Windows.Forms.Label();
            this.cmbPart = new System.Windows.Forms.ComboBox();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.lblPart = new System.Windows.Forms.Label();
            this.lblPrice = new System.Windows.Forms.Label();
            this.txtUnitPrice = new System.Windows.Forms.TextBox();
            this.lblUnitPrice = new System.Windows.Forms.Label();
            this.txtQty = new System.Windows.Forms.TextBox();
            this.lblQty = new System.Windows.Forms.Label();
            this.txtMaker = new System.Windows.Forms.TextBox();
            this.lblMaker = new System.Windows.Forms.Label();
            this.txtAfterProcess = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMaterial = new System.Windows.Forms.TextBox();
            this.lblMaterial = new System.Windows.Forms.Label();
            this.txtSpecfication = new System.Windows.Forms.TextBox();
            this.lblSpecfication = new System.Windows.Forms.Label();
            this.txtPartName = new System.Windows.Forms.TextBox();
            this.lblPartName = new System.Windows.Forms.Label();
            this.txtType = new System.Windows.Forms.TextBox();
            this.lblType = new System.Windows.Forms.Label();
            this.DrawingNumberDisplayPanel = new System.Windows.Forms.Panel();
            this.lblNewDrawNumber = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.txtNewDrawNumber = new System.Windows.Forms.TextBox();
            this.lblDrawingCategory = new System.Windows.Forms.Label();
            this.cmbDrawingCategory = new System.Windows.Forms.ComboBox();
            this.lblByte = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.lblNote = new System.Windows.Forms.Label();
            this.btnPublish = new System.Windows.Forms.Button();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.lblWarringContent = new System.Windows.Forms.Label();
            this.btnReflesh = new System.Windows.Forms.Button();
            this.lblWarning = new System.Windows.Forms.Label();
            this.pnlReviseDrawingNumber = new System.Windows.Forms.Panel();
            this.lblReviseDrawingNumber = new System.Windows.Forms.Label();
            this.btnCheckVaild = new System.Windows.Forms.Button();
            this.txtReviseDrawingNumber = new System.Windows.Forms.TextBox();
            this.msMain.SuspendLayout();
            this.contextMenuStrip.SuspendLayout();
            this.pnlNewDrawingNumber.SuspendLayout();
            this.gbODNDetail.SuspendLayout();
            this.DrawingNumberDisplayPanel.SuspendLayout();
            this.pnlReviseDrawingNumber.SuspendLayout();
            this.SuspendLayout();
            // 
            // msMain
            // 
            this.msMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiSetup,
            this.tsmiManagement,
            this.tsmiPartList,
            this.tsmiReport});
            this.msMain.Location = new System.Drawing.Point(0, 0);
            this.msMain.Name = "msMain";
            this.msMain.Size = new System.Drawing.Size(701, 24);
            this.msMain.TabIndex = 0;
            this.msMain.Text = "menuStrip1";
            // 
            // tsmiSetup
            // 
            this.tsmiSetup.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiModifyPassword,
            this.tsmiConfiguration});
            this.tsmiSetup.Name = "tsmiSetup";
            this.tsmiSetup.Size = new System.Drawing.Size(50, 20);
            this.tsmiSetup.Text = "Setup";
            // 
            // tsmiModifyPassword
            // 
            this.tsmiModifyPassword.Name = "tsmiModifyPassword";
            this.tsmiModifyPassword.Size = new System.Drawing.Size(166, 22);
            this.tsmiModifyPassword.Text = "Modify Password";
            this.tsmiModifyPassword.Click += new System.EventHandler(this.tsmiModifyPassword_Click);
            // 
            // tsmiConfiguration
            // 
            this.tsmiConfiguration.Name = "tsmiConfiguration";
            this.tsmiConfiguration.Size = new System.Drawing.Size(166, 22);
            this.tsmiConfiguration.Text = "Configuration";
            this.tsmiConfiguration.Click += new System.EventHandler(this.tsmiConfiguration_Click);
            // 
            // tsmiManagement
            // 
            this.tsmiManagement.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiModel,
            this.tsmiPart,
            this.tsmiMaker});
            this.tsmiManagement.Name = "tsmiManagement";
            this.tsmiManagement.Size = new System.Drawing.Size(90, 20);
            this.tsmiManagement.Text = "Management";
            // 
            // tsmiModel
            // 
            this.tsmiModel.Name = "tsmiModel";
            this.tsmiModel.Size = new System.Drawing.Size(108, 22);
            this.tsmiModel.Text = "Model";
            this.tsmiModel.Click += new System.EventHandler(this.tsmiModel_Click);
            // 
            // tsmiPart
            // 
            this.tsmiPart.Name = "tsmiPart";
            this.tsmiPart.Size = new System.Drawing.Size(108, 22);
            this.tsmiPart.Text = "Part";
            this.tsmiPart.Click += new System.EventHandler(this.tsmiPart_Click);
            // 
            // tsmiMaker
            // 
            this.tsmiMaker.Name = "tsmiMaker";
            this.tsmiMaker.Size = new System.Drawing.Size(108, 22);
            this.tsmiMaker.Text = "Maker";
            this.tsmiMaker.Click += new System.EventHandler(this.tsmiMaker_Click);
            // 
            // tsmiPartList
            // 
            this.tsmiPartList.Name = "tsmiPartList";
            this.tsmiPartList.Size = new System.Drawing.Size(62, 20);
            this.tsmiPartList.Text = "Part List";
            this.tsmiPartList.Click += new System.EventHandler(this.tsmiPartList_Click);
            // 
            // tsmiReport
            // 
            this.tsmiReport.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiDrawingNumberHistory,
            this.tsmiOfficialDrawingNumberHistory});
            this.tsmiReport.Name = "tsmiReport";
            this.tsmiReport.Size = new System.Drawing.Size(54, 20);
            this.tsmiReport.Text = "Report";
            // 
            // tsmiDrawingNumberHistory
            // 
            this.tsmiDrawingNumberHistory.Name = "tsmiDrawingNumberHistory";
            this.tsmiDrawingNumberHistory.Size = new System.Drawing.Size(251, 22);
            this.tsmiDrawingNumberHistory.Text = "Drawing Number Histroy";
            this.tsmiDrawingNumberHistory.Click += new System.EventHandler(this.tsmiDrawingNumberHistory_Click);
            // 
            // tsmiOfficialDrawingNumberHistory
            // 
            this.tsmiOfficialDrawingNumberHistory.Name = "tsmiOfficialDrawingNumberHistory";
            this.tsmiOfficialDrawingNumberHistory.Size = new System.Drawing.Size(251, 22);
            this.tsmiOfficialDrawingNumberHistory.Text = "Official Drawing Number History";
            this.tsmiOfficialDrawingNumberHistory.Click += new System.EventHandler(this.tsmiOfficialDrawingNumberHistory_Click);
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiNewDrawingNumber,
            this.tsmiReviseDrawinNumber,
            this.toolStripSeparator1,
            this.tsmiNewOfficialDrawingNumber,
            this.tsmiReviseOfficialDrawinNumber,
            this.tsmiNewOrderType,
            this.toolStripSeparator2,
            this.tsmiOrder,
            this.toolStripSeparator3,
            this.tsmiExit});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(247, 176);
            // 
            // tsmiNewDrawingNumber
            // 
            this.tsmiNewDrawingNumber.Name = "tsmiNewDrawingNumber";
            this.tsmiNewDrawingNumber.Size = new System.Drawing.Size(246, 22);
            this.tsmiNewDrawingNumber.Text = "New Drawing Number";
            this.tsmiNewDrawingNumber.Click += new System.EventHandler(this.tsmiNewDrawingNumber_Click);
            // 
            // tsmiReviseDrawinNumber
            // 
            this.tsmiReviseDrawinNumber.Name = "tsmiReviseDrawinNumber";
            this.tsmiReviseDrawinNumber.Size = new System.Drawing.Size(246, 22);
            this.tsmiReviseDrawinNumber.Text = "Revise Drawing Number";
            this.tsmiReviseDrawinNumber.Click += new System.EventHandler(this.tsmiReviseDrawinNumber_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(243, 6);
            // 
            // tsmiNewOfficialDrawingNumber
            // 
            this.tsmiNewOfficialDrawingNumber.Name = "tsmiNewOfficialDrawingNumber";
            this.tsmiNewOfficialDrawingNumber.Size = new System.Drawing.Size(246, 22);
            this.tsmiNewOfficialDrawingNumber.Text = "New Official Drawing Number";
            this.tsmiNewOfficialDrawingNumber.Click += new System.EventHandler(this.tsmiNewOfficialDrawingNumber_Click);
            // 
            // tsmiReviseOfficialDrawinNumber
            // 
            this.tsmiReviseOfficialDrawinNumber.Name = "tsmiReviseOfficialDrawinNumber";
            this.tsmiReviseOfficialDrawinNumber.Size = new System.Drawing.Size(246, 22);
            this.tsmiReviseOfficialDrawinNumber.Text = "Revise Official Drawing Number";
            this.tsmiReviseOfficialDrawinNumber.Click += new System.EventHandler(this.tsmiReviseOfficialDrawinNumber_Click);
            // 
            // tsmiNewOrderType
            // 
            this.tsmiNewOrderType.Name = "tsmiNewOrderType";
            this.tsmiNewOrderType.Size = new System.Drawing.Size(246, 22);
            this.tsmiNewOrderType.Text = "New Order Type";
            this.tsmiNewOrderType.Click += new System.EventHandler(this.tsmiNewOrderType_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(243, 6);
            // 
            // tsmiOrder
            // 
            this.tsmiOrder.Name = "tsmiOrder";
            this.tsmiOrder.Size = new System.Drawing.Size(246, 22);
            this.tsmiOrder.Text = "Publish Order";
            this.tsmiOrder.Click += new System.EventHandler(this.tsmiOrder_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(243, 6);
            // 
            // tsmiExit
            // 
            this.tsmiExit.Name = "tsmiExit";
            this.tsmiExit.Size = new System.Drawing.Size(246, 22);
            this.tsmiExit.Text = "Exit";
            this.tsmiExit.Click += new System.EventHandler(this.tsmiExit_Click);
            // 
            // pnlNewDrawingNumber
            // 
            this.pnlNewDrawingNumber.BackColor = System.Drawing.Color.Honeydew;
            this.pnlNewDrawingNumber.Controls.Add(this.gbODNDetail);
            this.pnlNewDrawingNumber.Controls.Add(this.DrawingNumberDisplayPanel);
            this.pnlNewDrawingNumber.Controls.Add(this.lblDrawingCategory);
            this.pnlNewDrawingNumber.Controls.Add(this.cmbDrawingCategory);
            this.pnlNewDrawingNumber.Controls.Add(this.lblByte);
            this.pnlNewDrawingNumber.Controls.Add(this.btnClear);
            this.pnlNewDrawingNumber.Controls.Add(this.lblNote);
            this.pnlNewDrawingNumber.Controls.Add(this.btnPublish);
            this.pnlNewDrawingNumber.Controls.Add(this.txtNote);
            this.pnlNewDrawingNumber.Controls.Add(this.lblWarringContent);
            this.pnlNewDrawingNumber.Controls.Add(this.btnReflesh);
            this.pnlNewDrawingNumber.Controls.Add(this.lblWarning);
            this.pnlNewDrawingNumber.Location = new System.Drawing.Point(0, 27);
            this.pnlNewDrawingNumber.Name = "pnlNewDrawingNumber";
            this.pnlNewDrawingNumber.Size = new System.Drawing.Size(345, 453);
            this.pnlNewDrawingNumber.TabIndex = 16;
            this.pnlNewDrawingNumber.TabStop = true;
            // 
            // gbODNDetail
            // 
            this.gbODNDetail.Controls.Add(this.btnMaker);
            this.gbODNDetail.Controls.Add(this.cmbModel);
            this.gbODNDetail.Controls.Add(this.lblModel);
            this.gbODNDetail.Controls.Add(this.cmbPart);
            this.gbODNDetail.Controls.Add(this.txtPrice);
            this.gbODNDetail.Controls.Add(this.lblPart);
            this.gbODNDetail.Controls.Add(this.lblPrice);
            this.gbODNDetail.Controls.Add(this.txtUnitPrice);
            this.gbODNDetail.Controls.Add(this.lblUnitPrice);
            this.gbODNDetail.Controls.Add(this.txtQty);
            this.gbODNDetail.Controls.Add(this.lblQty);
            this.gbODNDetail.Controls.Add(this.txtMaker);
            this.gbODNDetail.Controls.Add(this.lblMaker);
            this.gbODNDetail.Controls.Add(this.txtAfterProcess);
            this.gbODNDetail.Controls.Add(this.label2);
            this.gbODNDetail.Controls.Add(this.txtMaterial);
            this.gbODNDetail.Controls.Add(this.lblMaterial);
            this.gbODNDetail.Controls.Add(this.txtSpecfication);
            this.gbODNDetail.Controls.Add(this.lblSpecfication);
            this.gbODNDetail.Controls.Add(this.txtPartName);
            this.gbODNDetail.Controls.Add(this.lblPartName);
            this.gbODNDetail.Controls.Add(this.txtType);
            this.gbODNDetail.Controls.Add(this.lblType);
            this.gbODNDetail.Location = new System.Drawing.Point(13, 123);
            this.gbODNDetail.Name = "gbODNDetail";
            this.gbODNDetail.Size = new System.Drawing.Size(325, 285);
            this.gbODNDetail.TabIndex = 27;
            this.gbODNDetail.TabStop = false;
            this.gbODNDetail.Text = "Detail Content";
            // 
            // btnMaker
            // 
            this.btnMaker.Location = new System.Drawing.Point(274, 203);
            this.btnMaker.Name = "btnMaker";
            this.btnMaker.Size = new System.Drawing.Size(47, 22);
            this.btnMaker.TabIndex = 10;
            this.btnMaker.Text = "...";
            this.btnMaker.UseVisualStyleBackColor = true;
            this.btnMaker.Click += new System.EventHandler(this.btnMaker_Click);
            // 
            // cmbModel
            // 
            this.cmbModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbModel.FormattingEnabled = true;
            this.cmbModel.Location = new System.Drawing.Point(91, 17);
            this.cmbModel.Name = "cmbModel";
            this.cmbModel.Size = new System.Drawing.Size(228, 20);
            this.cmbModel.TabIndex = 2;
            // 
            // lblModel
            // 
            this.lblModel.AutoSize = true;
            this.lblModel.Location = new System.Drawing.Point(11, 20);
            this.lblModel.Name = "lblModel";
            this.lblModel.Size = new System.Drawing.Size(40, 12);
            this.lblModel.TabIndex = 18;
            this.lblModel.Text = "Model";
            // 
            // cmbPart
            // 
            this.cmbPart.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPart.FormattingEnabled = true;
            this.cmbPart.Location = new System.Drawing.Point(92, 43);
            this.cmbPart.Name = "cmbPart";
            this.cmbPart.Size = new System.Drawing.Size(227, 20);
            this.cmbPart.TabIndex = 3;
            // 
            // txtPrice
            // 
            this.txtPrice.Location = new System.Drawing.Point(92, 258);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(229, 21);
            this.txtPrice.TabIndex = 13;
            this.txtPrice.Text = "0";
            // 
            // lblPart
            // 
            this.lblPart.AutoSize = true;
            this.lblPart.Location = new System.Drawing.Point(11, 46);
            this.lblPart.Name = "lblPart";
            this.lblPart.Size = new System.Drawing.Size(27, 12);
            this.lblPart.TabIndex = 20;
            this.lblPart.Text = "Part";
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Location = new System.Drawing.Point(12, 261);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(34, 12);
            this.lblPrice.TabIndex = 16;
            this.lblPrice.Text = "Price";
            // 
            // txtUnitPrice
            // 
            this.txtUnitPrice.Location = new System.Drawing.Point(241, 230);
            this.txtUnitPrice.Name = "txtUnitPrice";
            this.txtUnitPrice.Size = new System.Drawing.Size(80, 21);
            this.txtUnitPrice.TabIndex = 12;
            this.txtUnitPrice.Text = "0";
            this.txtUnitPrice.TextChanged += new System.EventHandler(this.txtUnitPrice_TextChanged);
            // 
            // lblUnitPrice
            // 
            this.lblUnitPrice.AutoSize = true;
            this.lblUnitPrice.Location = new System.Drawing.Point(184, 234);
            this.lblUnitPrice.Name = "lblUnitPrice";
            this.lblUnitPrice.Size = new System.Drawing.Size(46, 12);
            this.lblUnitPrice.TabIndex = 14;
            this.lblUnitPrice.Text = "U Price";
            // 
            // txtQty
            // 
            this.txtQty.Location = new System.Drawing.Point(91, 231);
            this.txtQty.Name = "txtQty";
            this.txtQty.Size = new System.Drawing.Size(80, 21);
            this.txtQty.TabIndex = 11;
            this.txtQty.Text = "0";
            this.txtQty.TextChanged += new System.EventHandler(this.txtQty_TextChanged);
            // 
            // lblQty
            // 
            this.lblQty.AutoSize = true;
            this.lblQty.Location = new System.Drawing.Point(11, 234);
            this.lblQty.Name = "lblQty";
            this.lblQty.Size = new System.Drawing.Size(24, 12);
            this.lblQty.TabIndex = 12;
            this.lblQty.Text = "Qty";
            // 
            // txtMaker
            // 
            this.txtMaker.Enabled = false;
            this.txtMaker.Location = new System.Drawing.Point(91, 204);
            this.txtMaker.Name = "txtMaker";
            this.txtMaker.Size = new System.Drawing.Size(177, 21);
            this.txtMaker.TabIndex = 9;
            // 
            // lblMaker
            // 
            this.lblMaker.AutoSize = true;
            this.lblMaker.Location = new System.Drawing.Point(11, 207);
            this.lblMaker.Name = "lblMaker";
            this.lblMaker.Size = new System.Drawing.Size(40, 12);
            this.lblMaker.TabIndex = 10;
            this.lblMaker.Text = "Maker";
            // 
            // txtAfterProcess
            // 
            this.txtAfterProcess.Location = new System.Drawing.Point(91, 177);
            this.txtAfterProcess.Name = "txtAfterProcess";
            this.txtAfterProcess.Size = new System.Drawing.Size(230, 21);
            this.txtAfterProcess.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 180);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 12);
            this.label2.TabIndex = 8;
            this.label2.Text = "After Process";
            // 
            // txtMaterial
            // 
            this.txtMaterial.Location = new System.Drawing.Point(91, 150);
            this.txtMaterial.Name = "txtMaterial";
            this.txtMaterial.Size = new System.Drawing.Size(230, 21);
            this.txtMaterial.TabIndex = 7;
            // 
            // lblMaterial
            // 
            this.lblMaterial.AutoSize = true;
            this.lblMaterial.Location = new System.Drawing.Point(11, 153);
            this.lblMaterial.Name = "lblMaterial";
            this.lblMaterial.Size = new System.Drawing.Size(50, 12);
            this.lblMaterial.TabIndex = 6;
            this.lblMaterial.Text = "Material";
            // 
            // txtSpecfication
            // 
            this.txtSpecfication.Location = new System.Drawing.Point(91, 122);
            this.txtSpecfication.Name = "txtSpecfication";
            this.txtSpecfication.Size = new System.Drawing.Size(230, 21);
            this.txtSpecfication.TabIndex = 6;
            // 
            // lblSpecfication
            // 
            this.lblSpecfication.AutoSize = true;
            this.lblSpecfication.Location = new System.Drawing.Point(11, 125);
            this.lblSpecfication.Name = "lblSpecfication";
            this.lblSpecfication.Size = new System.Drawing.Size(74, 12);
            this.lblSpecfication.TabIndex = 4;
            this.lblSpecfication.Text = "Specfication";
            // 
            // txtPartName
            // 
            this.txtPartName.Location = new System.Drawing.Point(91, 68);
            this.txtPartName.Name = "txtPartName";
            this.txtPartName.Size = new System.Drawing.Size(230, 21);
            this.txtPartName.TabIndex = 4;
            // 
            // lblPartName
            // 
            this.lblPartName.AutoSize = true;
            this.lblPartName.Location = new System.Drawing.Point(11, 71);
            this.lblPartName.Name = "lblPartName";
            this.lblPartName.Size = new System.Drawing.Size(61, 12);
            this.lblPartName.TabIndex = 2;
            this.lblPartName.Text = "PartName";
            // 
            // txtType
            // 
            this.txtType.Location = new System.Drawing.Point(91, 95);
            this.txtType.Name = "txtType";
            this.txtType.Size = new System.Drawing.Size(230, 21);
            this.txtType.TabIndex = 5;
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(11, 98);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(34, 12);
            this.lblType.TabIndex = 0;
            this.lblType.Text = "Type";
            // 
            // DrawingNumberDisplayPanel
            // 
            this.DrawingNumberDisplayPanel.BackColor = System.Drawing.Color.Gold;
            this.DrawingNumberDisplayPanel.Controls.Add(this.lblNewDrawNumber);
            this.DrawingNumberDisplayPanel.Controls.Add(this.btnClose);
            this.DrawingNumberDisplayPanel.Controls.Add(this.txtNewDrawNumber);
            this.DrawingNumberDisplayPanel.Location = new System.Drawing.Point(9, 454);
            this.DrawingNumberDisplayPanel.Name = "DrawingNumberDisplayPanel";
            this.DrawingNumberDisplayPanel.Size = new System.Drawing.Size(331, 117);
            this.DrawingNumberDisplayPanel.TabIndex = 6;
            // 
            // lblNewDrawNumber
            // 
            this.lblNewDrawNumber.AutoSize = true;
            this.lblNewDrawNumber.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblNewDrawNumber.Location = new System.Drawing.Point(7, 14);
            this.lblNewDrawNumber.Name = "lblNewDrawNumber";
            this.lblNewDrawNumber.Size = new System.Drawing.Size(177, 16);
            this.lblNewDrawNumber.TabIndex = 0;
            this.lblNewDrawNumber.Text = "New Drawing Number";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(251, 72);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(74, 34);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // txtNewDrawNumber
            // 
            this.txtNewDrawNumber.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtNewDrawNumber.Location = new System.Drawing.Point(7, 33);
            this.txtNewDrawNumber.Name = "txtNewDrawNumber";
            this.txtNewDrawNumber.ReadOnly = true;
            this.txtNewDrawNumber.Size = new System.Drawing.Size(318, 32);
            this.txtNewDrawNumber.TabIndex = 1;
            this.txtNewDrawNumber.TextChanged += new System.EventHandler(this.txtNewDrawNumber_TextChanged);
            // 
            // lblDrawingCategory
            // 
            this.lblDrawingCategory.AutoSize = true;
            this.lblDrawingCategory.Location = new System.Drawing.Point(11, 8);
            this.lblDrawingCategory.Name = "lblDrawingCategory";
            this.lblDrawingCategory.Size = new System.Drawing.Size(56, 12);
            this.lblDrawingCategory.TabIndex = 0;
            this.lblDrawingCategory.Text = "Category";
            // 
            // cmbDrawingCategory
            // 
            this.cmbDrawingCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDrawingCategory.FormattingEnabled = true;
            this.cmbDrawingCategory.Location = new System.Drawing.Point(16, 27);
            this.cmbDrawingCategory.Name = "cmbDrawingCategory";
            this.cmbDrawingCategory.Size = new System.Drawing.Size(69, 20);
            this.cmbDrawingCategory.TabIndex = 13;
            // 
            // lblByte
            // 
            this.lblByte.AutoSize = true;
            this.lblByte.ForeColor = System.Drawing.Color.OrangeRed;
            this.lblByte.Location = new System.Drawing.Point(213, 8);
            this.lblByte.Name = "lblByte";
            this.lblByte.Size = new System.Drawing.Size(52, 12);
            this.lblByte.TabIndex = 0;
            this.lblByte.Text = "0/0 Byte";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(291, 3);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(47, 22);
            this.btnClear.TabIndex = 12;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // lblNote
            // 
            this.lblNote.AutoSize = true;
            this.lblNote.Location = new System.Drawing.Point(89, 8);
            this.lblNote.Name = "lblNote";
            this.lblNote.Size = new System.Drawing.Size(31, 12);
            this.lblNote.TabIndex = 0;
            this.lblNote.Text = "Note";
            // 
            // btnPublish
            // 
            this.btnPublish.Enabled = false;
            this.btnPublish.Location = new System.Drawing.Point(282, 414);
            this.btnPublish.Name = "btnPublish";
            this.btnPublish.Size = new System.Drawing.Size(56, 34);
            this.btnPublish.TabIndex = 14;
            this.btnPublish.Text = "Publish";
            this.btnPublish.UseVisualStyleBackColor = true;
            this.btnPublish.Click += new System.EventHandler(this.btnPublish_Click);
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(91, 26);
            this.txtNote.Multiline = true;
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(247, 51);
            this.txtNote.TabIndex = 1;
            this.txtNote.TextChanged += new System.EventHandler(this.txtNote_TextChanged);
            // 
            // lblWarringContent
            // 
            this.lblWarringContent.AutoSize = true;
            this.lblWarringContent.ForeColor = System.Drawing.Color.OrangeRed;
            this.lblWarringContent.Location = new System.Drawing.Point(11, 97);
            this.lblWarringContent.Name = "lblWarringContent";
            this.lblWarringContent.Size = new System.Drawing.Size(11, 12);
            this.lblWarringContent.TabIndex = 0;
            this.lblWarringContent.Text = "?";
            // 
            // btnReflesh
            // 
            this.btnReflesh.Location = new System.Drawing.Point(14, 54);
            this.btnReflesh.Name = "btnReflesh";
            this.btnReflesh.Size = new System.Drawing.Size(73, 23);
            this.btnReflesh.TabIndex = 14;
            this.btnReflesh.Text = "Refresh";
            this.btnReflesh.UseVisualStyleBackColor = true;
            this.btnReflesh.Click += new System.EventHandler(this.btnReflesh_Click);
            // 
            // lblWarning
            // 
            this.lblWarning.AutoSize = true;
            this.lblWarning.ForeColor = System.Drawing.Color.OrangeRed;
            this.lblWarning.Location = new System.Drawing.Point(7, 83);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(62, 12);
            this.lblWarning.TabIndex = 0;
            this.lblWarning.Text = "※Warning";
            // 
            // pnlReviseDrawingNumber
            // 
            this.pnlReviseDrawingNumber.BackColor = System.Drawing.Color.Honeydew;
            this.pnlReviseDrawingNumber.Controls.Add(this.lblReviseDrawingNumber);
            this.pnlReviseDrawingNumber.Controls.Add(this.btnCheckVaild);
            this.pnlReviseDrawingNumber.Controls.Add(this.txtReviseDrawingNumber);
            this.pnlReviseDrawingNumber.Location = new System.Drawing.Point(351, 27);
            this.pnlReviseDrawingNumber.Name = "pnlReviseDrawingNumber";
            this.pnlReviseDrawingNumber.Size = new System.Drawing.Size(345, 119);
            this.pnlReviseDrawingNumber.TabIndex = 17;
            this.pnlReviseDrawingNumber.TabStop = true;
            // 
            // lblReviseDrawingNumber
            // 
            this.lblReviseDrawingNumber.AutoSize = true;
            this.lblReviseDrawingNumber.Location = new System.Drawing.Point(19, 10);
            this.lblReviseDrawingNumber.Name = "lblReviseDrawingNumber";
            this.lblReviseDrawingNumber.Size = new System.Drawing.Size(142, 12);
            this.lblReviseDrawingNumber.TabIndex = 0;
            this.lblReviseDrawingNumber.Text = "Revise Drawing Number";
            // 
            // btnCheckVaild
            // 
            this.btnCheckVaild.Location = new System.Drawing.Point(241, 25);
            this.btnCheckVaild.Name = "btnCheckVaild";
            this.btnCheckVaild.Size = new System.Drawing.Size(90, 34);
            this.btnCheckVaild.TabIndex = 16;
            this.btnCheckVaild.Text = "Check Vaild";
            this.btnCheckVaild.UseVisualStyleBackColor = true;
            this.btnCheckVaild.Click += new System.EventHandler(this.btnCheckVaild_Click);
            // 
            // txtReviseDrawingNumber
            // 
            this.txtReviseDrawingNumber.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtReviseDrawingNumber.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtReviseDrawingNumber.Location = new System.Drawing.Point(15, 27);
            this.txtReviseDrawingNumber.Name = "txtReviseDrawingNumber";
            this.txtReviseDrawingNumber.Size = new System.Drawing.Size(181, 32);
            this.txtReviseDrawingNumber.TabIndex = 15;
            this.txtReviseDrawingNumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtReviseDrawingNumber_KeyDown);
            // 
            // FormMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.ClientSize = new System.Drawing.Size(701, 483);
            this.Controls.Add(this.msMain);
            this.Controls.Add(this.pnlReviseDrawingNumber);
            this.Controls.Add(this.pnlNewDrawingNumber);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.msMain;
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Publish Drawing Number";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.Load += new System.EventHandler(this.FormNewDrawingNumber_Load);
            this.Shown += new System.EventHandler(this.FormNewDrawingNumber_Shown);
            this.Resize += new System.EventHandler(this.FormNewDrawingNumber_Resize);
            this.msMain.ResumeLayout(false);
            this.msMain.PerformLayout();
            this.contextMenuStrip.ResumeLayout(false);
            this.pnlNewDrawingNumber.ResumeLayout(false);
            this.pnlNewDrawingNumber.PerformLayout();
            this.gbODNDetail.ResumeLayout(false);
            this.gbODNDetail.PerformLayout();
            this.DrawingNumberDisplayPanel.ResumeLayout(false);
            this.DrawingNumberDisplayPanel.PerformLayout();
            this.pnlReviseDrawingNumber.ResumeLayout(false);
            this.pnlReviseDrawingNumber.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip msMain;
        private System.Windows.Forms.ToolStripMenuItem tsmiReport;
        private System.Windows.Forms.ToolStripMenuItem tsmiDrawingNumberHistory;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem tsmiNewDrawingNumber;
        private System.Windows.Forms.ToolStripMenuItem tsmiReviseDrawinNumber;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem tsmiExit;
        private System.Windows.Forms.Panel pnlNewDrawingNumber;
        private System.Windows.Forms.Panel DrawingNumberDisplayPanel;
        private System.Windows.Forms.Label lblNewDrawNumber;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TextBox txtNewDrawNumber;
        private System.Windows.Forms.Label lblDrawingCategory;
        private System.Windows.Forms.ComboBox cmbDrawingCategory;
        private System.Windows.Forms.Label lblByte;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label lblNote;
        private System.Windows.Forms.Button btnPublish;
        private System.Windows.Forms.TextBox txtNote;
        private System.Windows.Forms.Label lblWarringContent;
        private System.Windows.Forms.Button btnReflesh;
        private System.Windows.Forms.Label lblWarning;
        private System.Windows.Forms.Panel pnlReviseDrawingNumber;
        private System.Windows.Forms.Label lblReviseDrawingNumber;
        private System.Windows.Forms.Button btnCheckVaild;
		private System.Windows.Forms.TextBox txtReviseDrawingNumber;
		private System.Windows.Forms.ToolStripMenuItem tsmiSetup;
        private System.Windows.Forms.ToolStripMenuItem tsmiModifyPassword;
        private System.Windows.Forms.ToolStripMenuItem tsmiOfficialDrawingNumberHistory;
        private System.Windows.Forms.ToolStripMenuItem tsmiNewOfficialDrawingNumber;
        private System.Windows.Forms.ToolStripMenuItem tsmiReviseOfficialDrawinNumber;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem tsmiNewOrderType;
        private System.Windows.Forms.ToolStripMenuItem tsmiOrder;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.GroupBox gbODNDetail;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.TextBox txtUnitPrice;
        private System.Windows.Forms.Label lblUnitPrice;
        private System.Windows.Forms.TextBox txtQty;
        private System.Windows.Forms.Label lblQty;
        private System.Windows.Forms.TextBox txtMaker;
        private System.Windows.Forms.Label lblMaker;
        private System.Windows.Forms.TextBox txtAfterProcess;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtMaterial;
        private System.Windows.Forms.Label lblMaterial;
        private System.Windows.Forms.TextBox txtSpecfication;
        private System.Windows.Forms.Label lblSpecfication;
        private System.Windows.Forms.TextBox txtPartName;
        private System.Windows.Forms.Label lblPartName;
        private System.Windows.Forms.TextBox txtType;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.ToolStripMenuItem tsmiPartList;
        private System.Windows.Forms.ToolStripMenuItem tsmiConfiguration;
        private System.Windows.Forms.ComboBox cmbModel;
        private System.Windows.Forms.Label lblModel;
        private System.Windows.Forms.ComboBox cmbPart;
        private System.Windows.Forms.Label lblPart;
        private System.Windows.Forms.ToolStripMenuItem tsmiManagement;
        private System.Windows.Forms.ToolStripMenuItem tsmiModel;
        private System.Windows.Forms.ToolStripMenuItem tsmiPart;
        private System.Windows.Forms.Button btnMaker;
        private System.Windows.Forms.ToolStripMenuItem tsmiMaker;
    }
}