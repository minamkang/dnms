using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;

namespace DrawingNumberManager
{
	public class Authority 
    {
        //추후에는 권한따른 화면, 메뉴 등을 관리하는 부분이 필요하다.
		private bool login_Yn;
		private bool note_write;
        private bool part_management;

		public bool LoginYn
		{
			get { return (login_Yn); }
		}

		public bool NoteWrite
		{
			get { return (note_write); }
		}

        public bool PartManagement
        {
            get { return (part_management); } 
        }

		//생성자
		public Authority(string strID)
		{
			DataTable result = new DataTable();
			SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC authority_info '{0}' ", strID), StaticData.db_connetion_string);
			query.Fill(result);
			AuthorityParsing(result.Rows[0]);
		}

		private void AuthorityParsing(DataRow result)
		{
			string bitA = Convert.ToString(int.Parse(result["ACCESS_LEVEL"].ToString()), 2);
			int[] bits = bitA.PadLeft(8, '0').Select(c => int.Parse(c.ToString())).ToArray();
			
			if (bits[7] == 0)
				login_Yn = false;
			else
				login_Yn = true;

			if (bits[6] == 0)
				note_write = false;
			else
				note_write = true;

            if (bits[5] == 0)
                part_management = false;
            else
                part_management = true;

		}

	}
}
