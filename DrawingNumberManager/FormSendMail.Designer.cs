﻿namespace DrawingNumberManager
{
    partial class FormSendMail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtReceiverAddress = new System.Windows.Forms.TextBox();
            this.txtSubject = new System.Windows.Forms.TextBox();
            this.lblMaker = new System.Windows.Forms.Label();
            this.lblSubject = new System.Windows.Forms.Label();
            this.txtBodyContent = new System.Windows.Forms.TextBox();
            this.btnSend = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.lblBodyContent = new System.Windows.Forms.Label();
            this.lblAttachmentFile = new System.Windows.Forms.Label();
            this.btnAddFile = new System.Windows.Forms.Button();
            this.btnDeleteFile = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtReferenceAddress = new System.Windows.Forms.TextBox();
            this.dgvAppendFile = new System.Windows.Forms.DataGridView();
            this.File = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Extenstion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FullName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblWarring = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAppendFile)).BeginInit();
            this.SuspendLayout();
            // 
            // txtReceiverAddress
            // 
            this.txtReceiverAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReceiverAddress.Enabled = false;
            this.txtReceiverAddress.Location = new System.Drawing.Point(51, 12);
            this.txtReceiverAddress.Name = "txtReceiverAddress";
            this.txtReceiverAddress.Size = new System.Drawing.Size(694, 21);
            this.txtReceiverAddress.TabIndex = 1;
            // 
            // txtSubject
            // 
            this.txtSubject.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSubject.Enabled = false;
            this.txtSubject.Location = new System.Drawing.Point(51, 82);
            this.txtSubject.Name = "txtSubject";
            this.txtSubject.Size = new System.Drawing.Size(694, 21);
            this.txtSubject.TabIndex = 3;
            // 
            // lblMaker
            // 
            this.lblMaker.AutoSize = true;
            this.lblMaker.Location = new System.Drawing.Point(12, 15);
            this.lblMaker.Name = "lblMaker";
            this.lblMaker.Size = new System.Drawing.Size(33, 12);
            this.lblMaker.TabIndex = 3;
            this.lblMaker.Text = "수신:";
            // 
            // lblSubject
            // 
            this.lblSubject.AutoSize = true;
            this.lblSubject.Location = new System.Drawing.Point(12, 85);
            this.lblSubject.Name = "lblSubject";
            this.lblSubject.Size = new System.Drawing.Size(33, 12);
            this.lblSubject.TabIndex = 4;
            this.lblSubject.Text = "제목:";
            // 
            // txtBodyContent
            // 
            this.txtBodyContent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBodyContent.Location = new System.Drawing.Point(14, 126);
            this.txtBodyContent.Multiline = true;
            this.txtBodyContent.Name = "txtBodyContent";
            this.txtBodyContent.Size = new System.Drawing.Size(731, 243);
            this.txtBodyContent.TabIndex = 4;
            // 
            // btnSend
            // 
            this.btnSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSend.Location = new System.Drawing.Point(670, 596);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(75, 50);
            this.btnSend.TabIndex = 8;
            this.btnSend.Text = "보내기";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 12);
            this.label3.TabIndex = 7;
            // 
            // lblBodyContent
            // 
            this.lblBodyContent.AutoSize = true;
            this.lblBodyContent.Location = new System.Drawing.Point(12, 111);
            this.lblBodyContent.Name = "lblBodyContent";
            this.lblBodyContent.Size = new System.Drawing.Size(33, 12);
            this.lblBodyContent.TabIndex = 8;
            this.lblBodyContent.Text = "본문:";
            // 
            // lblAttachmentFile
            // 
            this.lblAttachmentFile.AutoSize = true;
            this.lblAttachmentFile.Location = new System.Drawing.Point(12, 384);
            this.lblAttachmentFile.Name = "lblAttachmentFile";
            this.lblAttachmentFile.Size = new System.Drawing.Size(53, 12);
            this.lblAttachmentFile.TabIndex = 9;
            this.lblAttachmentFile.Text = "첨부파일";
            // 
            // btnAddFile
            // 
            this.btnAddFile.Location = new System.Drawing.Point(629, 373);
            this.btnAddFile.Name = "btnAddFile";
            this.btnAddFile.Size = new System.Drawing.Size(55, 23);
            this.btnAddFile.TabIndex = 5;
            this.btnAddFile.Text = "추가(+)";
            this.btnAddFile.UseVisualStyleBackColor = true;
            this.btnAddFile.Click += new System.EventHandler(this.btnAddFile_Click);
            // 
            // btnDeleteFile
            // 
            this.btnDeleteFile.Location = new System.Drawing.Point(690, 373);
            this.btnDeleteFile.Name = "btnDeleteFile";
            this.btnDeleteFile.Size = new System.Drawing.Size(55, 23);
            this.btnDeleteFile.TabIndex = 6;
            this.btnDeleteFile.Text = "삭제(-)";
            this.btnDeleteFile.UseVisualStyleBackColor = true;
            this.btnDeleteFile.Click += new System.EventHandler(this.btnDeleteFile_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 12);
            this.label1.TabIndex = 14;
            this.label1.Text = "참조:";
            // 
            // txtReferenceAddress
            // 
            this.txtReferenceAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtReferenceAddress.Location = new System.Drawing.Point(51, 39);
            this.txtReferenceAddress.Name = "txtReferenceAddress";
            this.txtReferenceAddress.Size = new System.Drawing.Size(694, 21);
            this.txtReferenceAddress.TabIndex = 2;
            // 
            // dgvAppendFile
            // 
            this.dgvAppendFile.AllowUserToAddRows = false;
            this.dgvAppendFile.AllowUserToDeleteRows = false;
            this.dgvAppendFile.AllowUserToResizeRows = false;
            this.dgvAppendFile.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAppendFile.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.File,
            this.Extenstion,
            this.FullName});
            this.dgvAppendFile.Location = new System.Drawing.Point(14, 402);
            this.dgvAppendFile.MultiSelect = false;
            this.dgvAppendFile.Name = "dgvAppendFile";
            this.dgvAppendFile.ReadOnly = true;
            this.dgvAppendFile.RowHeadersVisible = false;
            this.dgvAppendFile.RowTemplate.Height = 23;
            this.dgvAppendFile.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAppendFile.Size = new System.Drawing.Size(731, 188);
            this.dgvAppendFile.TabIndex = 7;
            // 
            // File
            // 
            this.File.HeaderText = "File";
            this.File.Name = "File";
            this.File.ReadOnly = true;
            // 
            // Extenstion
            // 
            this.Extenstion.HeaderText = "Extenstion";
            this.Extenstion.Name = "Extenstion";
            this.Extenstion.ReadOnly = true;
            // 
            // FullName
            // 
            this.FullName.HeaderText = "FullName";
            this.FullName.Name = "FullName";
            this.FullName.ReadOnly = true;
            this.FullName.Visible = false;
            // 
            // lblWarring
            // 
            this.lblWarring.AutoSize = true;
            this.lblWarring.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblWarring.ForeColor = System.Drawing.Color.OrangeRed;
            this.lblWarring.Location = new System.Drawing.Point(71, 384);
            this.lblWarring.Name = "lblWarring";
            this.lblWarring.Size = new System.Drawing.Size(195, 12);
            this.lblWarring.TabIndex = 16;
            this.lblWarring.Text = "[※도면, 발주서는 숨겨진 파일]";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label2.Location = new System.Drawing.Point(49, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(178, 12);
            this.label2.TabIndex = 17;
            this.label2.Text = "[※주소 구분은 ( , )로 진행]";
            // 
            // FormSendMail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(757, 652);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblWarring);
            this.Controls.Add(this.dgvAppendFile);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtReferenceAddress);
            this.Controls.Add(this.btnDeleteFile);
            this.Controls.Add(this.btnAddFile);
            this.Controls.Add(this.lblAttachmentFile);
            this.Controls.Add(this.lblBodyContent);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.txtBodyContent);
            this.Controls.Add(this.lblSubject);
            this.Controls.Add(this.lblMaker);
            this.Controls.Add(this.txtSubject);
            this.Controls.Add(this.txtReceiverAddress);
            this.MaximizeBox = false;
            this.Name = "FormSendMail";
            this.Text = "Mail 보내기";
            this.Load += new System.EventHandler(this.FormSendMail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAppendFile)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtReceiverAddress;
        private System.Windows.Forms.TextBox txtSubject;
        private System.Windows.Forms.Label lblMaker;
        private System.Windows.Forms.Label lblSubject;
        private System.Windows.Forms.TextBox txtBodyContent;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblBodyContent;
        private System.Windows.Forms.Label lblAttachmentFile;
        private System.Windows.Forms.Button btnAddFile;
        private System.Windows.Forms.Button btnDeleteFile;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtReferenceAddress;
        private System.Windows.Forms.DataGridView dgvAppendFile;
        private System.Windows.Forms.Label lblWarring;
        private System.Windows.Forms.DataGridViewTextBoxColumn File;
        private System.Windows.Forms.DataGridViewTextBoxColumn Extenstion;
        private System.Windows.Forms.DataGridViewTextBoxColumn FullName;
        private System.Windows.Forms.Label label2;
    }
}