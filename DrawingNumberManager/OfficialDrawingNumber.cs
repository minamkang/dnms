using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Collections;

namespace DrawingNumberManager
{
	public class OfficialDrawingNumber : DrawingNumber
	{
        private string Source = "";
        private string OrderType = "";
        private string official_drawing_number = "";
        private string temporary_drawing_number = "";
        private string old_drawing_number = "";
        private string Note = "";
        private string flag = "";

        private ArrayList detail_content;

        //swkang 20160908 add
        private string new_order_type = "";

        //Create Official DrawingNumber 생성자
        public OfficialDrawingNumber(string source, string order_type, string strTDN, string strNote, string strOldDN, ArrayList alDetail)
        {
            Source = source;
            OrderType = order_type;
            temporary_drawing_number = strTDN;
            old_drawing_number = strOldDN;
            Note = strNote;
            flag = Const.FLAG_CREATE;
            detail_content = alDetail;

            official_drawing_number = DrawingNumber().ToString();
        }
        
        //Revise Official DrawingNumber 생성자
        public OfficialDrawingNumber(string source, string strTDN, string strNote, ArrayList alDetail)
        {
            Source = source;
            temporary_drawing_number = strTDN;
            Note = strNote;
            flag = Const.FLAG_REVISE;

            detail_content = alDetail;

            if (temporary_drawing_number.Length > 0)
                official_drawing_number = DrawingNumber().ToString();
            else
            {
                if(Note.Length > 0)
                    official_drawing_number = DrawingNumber().ToString();
            }                
        }

        //swkang add 20160908
        public OfficialDrawingNumber(string source, string strNote, string strNewOrderType, string EmptyString)
        {
            Source = source;
            new_order_type = strNewOrderType;
            Note = strNote;
            flag = Const.FLAG_ORDERTYPE;

            if (Note.Length > 0)
                official_drawing_number = DrawingNumber().ToString();
        }

        new public bool Issue()
        {
            if (CheckVaild(Source) != true)
                return false;
            return true;
        }

		//Drawing Number 재정의
		public override string ToString()
		{
            return official_drawing_number;
		}

        protected string DrawingNumber()
        {
            string resultDN = "";
            SqlCommand query = null;

            try
            {
                string temp = "";
                if (flag == Const.FLAG_CREATE)
                {
                    temp = String.Format("EXEC dbo.create_official_drawing_number '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', {11}, {12}, {13}, {14}",
                        StaticData.user_id, Source, OrderType, Note, temporary_drawing_number, old_drawing_number,
                        detail_content[0],      //Type
                        detail_content[1],      //Part Name
                        detail_content[2],      //Specfication
                        detail_content[3],      //Material 
                        detail_content[4],      //After Process
                        detail_content[5],      //Quantity 
                        detail_content[6],      //Maker
                        detail_content[7],      //Unit Price 
                        detail_content[8]       //Price
                        );

                    query = new SqlCommand(String.Format("EXEC dbo.create_official_drawing_number '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', {11}, {12}, {13}, {14}",
                        StaticData.user_id, Source, OrderType, Note, temporary_drawing_number, old_drawing_number,
                        detail_content[0],      //Type
                        detail_content[1],      //Part Name
                        detail_content[2],      //Specfication
                        detail_content[3],      //Material 
                        detail_content[4],      //After Process
                        detail_content[5],      //Quantity 
                        detail_content[6],      //Maker
                        detail_content[7],      //Unit Price 
                        detail_content[8]       //Price
                        ), new SqlConnection(StaticData.db_connetion_string));
                }
                else if (flag == Const.FLAG_REVISE)
                {
                    temp = String.Format("EXEC dbo.revise_official_drawing_number '{0}', '{1}', '{2}', '{3}', '{4}','{5}', '{6}', '{7}', '{8}', '{9}', {10}, {11}, {12}, {13}",
                        StaticData.user_id, 
                        Source, 
                        temporary_drawing_number, 
                        Note, 
                        GetOldDrawingNumber(Source),
                        detail_content[0],      //Type
                        detail_content[1],      //Part Name
                        detail_content[2],      //Specfication
                        detail_content[3],      //Material 
                        detail_content[4],      //After Process
                        detail_content[5],      //Quantity 
                        detail_content[6],      //Maker
                        detail_content[7],      //Unit Price 
                        detail_content[8]       //Price
                        );
                    query = new SqlCommand(String.Format("EXEC dbo.revise_official_drawing_number '{0}', '{1}', '{2}', '{3}', '{4}','{5}', '{6}', '{7}', '{8}', '{9}', {10}, {11}, {12}, {13}",
                        StaticData.user_id, Source, temporary_drawing_number, Note, GetOldDrawingNumber(Source),
                        detail_content[0],      //Type
                        detail_content[1],      //Part Name
                        detail_content[2],      //Specfication
                        detail_content[3],      //Material 
                        detail_content[4],      //After Process
                        detail_content[5],      //Quantity 
                        detail_content[6],      //Maker
                        detail_content[7],      //Unit Price 
                        detail_content[8]       //Price
                        ), new SqlConnection(StaticData.db_connetion_string));
                }
                else if (flag == Const.FLAG_ORDERTYPE)  //swkang 20160908 ADD
                {
                    temp = String.Format("EXEC dbo.new_order_type_official_drawing_number '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', {9}, {10}, {11}, {12}",
                        StaticData.user_id, Source, new_order_type, Note,
                        detail_content[0],      //Type
                        detail_content[1],      //Part Name
                        detail_content[2],      //Specfication
                        detail_content[3],      //Material 
                        detail_content[4],      //After Process
                        detail_content[5],      //Quantity 
                        detail_content[6],      //Maker
                        detail_content[7],      //Unit Price 
                        detail_content[8]       //Price
                        );
                    query = new SqlCommand(String.Format("EXEC dbo.new_order_type_official_drawing_number '{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', {9}, {10}, {11}, {12}",
                        StaticData.user_id, Source, new_order_type, Note,
                        detail_content[0],      //Type
                        detail_content[1],      //Part Name
                        detail_content[2],      //Specfication
                        detail_content[3],      //Material 
                        detail_content[4],      //After Process
                        detail_content[5],      //Quantity 
                        detail_content[6],      //Maker
                        detail_content[7],      //Unit Price 
                        detail_content[8]       //Price
                        ), new SqlConnection(StaticData.db_connetion_string));
                }

                query.Connection.Open();
                resultDN = (string)query.ExecuteScalar();
                query.Connection.Close();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return resultDN;
        }

        //DrawingNumber 정규성을 체크한다.
        protected bool CheckVaild(string source)
        {
            try
            {
                //리비젼 정보가 있는지 체크한다.
                char source_revision = '0';
                if (source.Length == 11)
                {
                    source_revision = Convert.ToChar(source.Substring(10, 1));
                    source = source.Substring(0, 10);
                }

                //Check Length
                if (source.Length != Const.OFFICIAL_DRAWING_NO_LENGTH)
                {
                    MessageBox.Show(string.Format("(眞)도면번호는 {0}자리 입니다.\r 확인하시고 다시 입력하세요.", Const.OFFICIAL_DRAWING_NO_LENGTH), "Warning");
                    return false;
                }
                
                //Check Project Code
                if (ProjectCodeCheck(Const.CODE_MODEL, source.Substring(0, 3)) == false 
                    || ProjectCodeCheck(Const.CODE_PART, source.Substring(3, 2)) == false
                    || ProjectCodeCheck(Const.CODE_PROCESS, source.Substring(5, 1)) == false
                    || SerialNumberCheck(source.Substring(0, 6), source.Substring(6, 3)) == false
                    || (Regex.IsMatch(source.Substring(9, 1), @"^[A-Z]$") != true))
                {
                    MessageBox.Show("유효하지 않는 도면번호입니다.", "Warning");
                    return false;
                }

                DrawingCategory.project_code = source.Substring(0, 6);
                serial_number = UInt16.Parse(source.Substring(6, 3));
                if (source_revision != '0')
                    revision_code = source_revision;
            }
            catch (Exception ex)
            {
                MessageBox.Show("VaildCheck \r" + ex.ToString());
                return false;
            }
            return true;
        }

        protected bool SerialNumberCheck(string strPorjectCode, string strSerialNumber)
        {
            string result = "";
            SqlCommand query = new SqlCommand(String.Format("EXEC check_invalid_official_serial_number '{0}', {1} ", strPorjectCode, int.Parse(strSerialNumber), result), new SqlConnection(StaticData.db_connetion_string));

            query.Connection.Open();
            result = (string)query.ExecuteScalar();
            query.Connection.Close();

            if (result[0] == 'N')
                return false;

            return true;
        }

        protected bool ProjectCodeCheck(short iCode, string strCodeName)
        {
            string result = "";
            SqlCommand query = new SqlCommand(String.Format("EXEC check_invalid_code_management {0}, '{1}'", iCode, strCodeName, result), new SqlConnection(StaticData.db_connetion_string));

            query.Connection.Open();
            result = (string)query.ExecuteScalar();
            query.Connection.Close();

            if (result[0] == 'N')
                return false;

            return true;
        }

        private string GetOldDrawingNumber(string strRevisionDrawingNumber)
        {
            string result = "";

            string strDrawingNumber = strRevisionDrawingNumber.Substring(0, 10);
            SqlCommand query = new SqlCommand(String.Format("EXEC get_old_drawing_number '{0}'", strDrawingNumber), new SqlConnection(StaticData.db_connetion_string));

            query.Connection.Open();
            result = (string)query.ExecuteScalar();
            query.Connection.Close();
            return result;
        }

        private int SearchUserKey()
        {
            int iresult = 0;
            return iresult;
        }

	}
}
