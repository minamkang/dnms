﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using Microsoft.Office.Interop.Excel;

namespace DrawingNumberManager
{
    public partial class FormDNHistory : Form
    {
        private char department_code;
		private bool note_write;

		bool formLoadStartDate = false;
		bool columnAdded = false;		//최초 컬럼 표현할때 쓰인다.
		bool ServerSearch = false;		//날짜관련 Data가 변경 되면 Server에서 Search여부
		
		double checkedCount = 0;		//체크박스 체크 Count

        ArrayList alContent = null;

        public FormDNHistory(char departmentCode, bool bNoteWrite)
        {
			department_code = departmentCode;
			note_write = bNoteWrite;


            InitializeComponent();
        }

        private void FormDrawingNumberHistory_Load(object sender, EventArgs e)
        {
			this.Text = Const.FORM_DRAWING_NUMBER_HISTORY;
			ServerSearch = true;

			//Excel 프로그램이 설치 되었는지 여부를 판단하여 Excel버튼을 보여질지 여부 판단
			Type officeType = Type.GetTypeFromProgID("Excel.Application");
			if (officeType == null)
				btnExcel.Visible = false;

			BindCmbDepartment();
			BindUserID();
			
			if (note_write == false)
			{
				btnExcel.Location = new System.Drawing.Point(812, 27);
				btnSaveNote.Visible = false;
			}

			formLoadStartDate = true;
			DateTime Now = DateTime.Now.Date;
			dtpickerStartDate.Value = Now;
			dtpickerEndDate.Value = Now.AddDays(1).AddSeconds(-1);
			formLoadStartDate = false;

			btnSaveNote.Enabled = false;
			BindDgvDrawingNumberHistory();
			ExcelButtonEnable();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
			BindDgvDrawingNumberHistory();
			ExcelButtonEnable();
			checkedCount = 0;
			btnSaveNote.Enabled = false;
        }

        private void btnNowDay_Click(object sender, EventArgs e)
        {
            DisplayDate("Day", 0);
        }

        private void btn1Week_Click(object sender, EventArgs e)
        {
            DisplayDate("Day", -7);
        }

        private void btn15Day_Click(object sender, EventArgs e)
        {
            DisplayDate("Day", -15);
        }

        private void btn1Month_Click(object sender, EventArgs e)
        {
            DisplayDate("Month", -1);
        }

        private void btn3Month_Click(object sender, EventArgs e)
        {
            DisplayDate("Month", -3);
        }

        private void btn6Month_Click(object sender, EventArgs e)
        {
            DisplayDate("Month", -6);
        }

        private void btn9Month_Click(object sender, EventArgs e)
        {
            DisplayDate("Month", -9);
        }

        private void btn1Year_Click(object sender, EventArgs e)
        {
            DisplayDate("Year", -1);
        }

        private void DisplayDate(string strDateType, short iDateValue)
        {
            DateTime Now = DateTime.Now.Date;
            switch (strDateType)
            {
                case "Day":
                    dtpickerStartDate.Value = Now.AddDays(iDateValue);
                    break;
                case "Month":
                    dtpickerStartDate.Value = Now.AddMonths(iDateValue);
                    break;
                case "Year":
                    dtpickerStartDate.Value = Now.AddYears(iDateValue);
                    break;
            }

            dtpickerEndDate.Value = Now.AddDays(1).AddSeconds(-1);
            ServerSearch = true;
            txtTDN.Text = "";
        }

		private void dtpickerEndDate_ValueChanged(object sender, EventArgs e)
		{
			if (dtpickerEndDate.Value < dtpickerStartDate.Value)
			{
				MessageBox.Show("End Date는 Start Date보다 작을 수 없습니다.");
				DateTime Now = DateTime.Now.Date;
				dtpickerEndDate.Value = Now.AddDays(1).AddSeconds(-1);
			}
			ServerSearch = true;
            txtTDN.Text = "";
		}

		private void dtpickerStartDate_ValueChanged(object sender, EventArgs e)
		{
			if (formLoadStartDate == true)
				return;

			if (dtpickerStartDate.Value > dtpickerEndDate.Value)
			{
				MessageBox.Show("Start Date는 End Date보다 클 수 없습니다.");
				DateTime Now = DateTime.Now.Date;
				dtpickerStartDate.Value = Now;
			}
			ServerSearch = true;
            txtTDN.Text = "";
		}

		private void btnSaveNote_Click(object sender, EventArgs e)
		{
			for (int i = 0; i < this.dgvDNHistory.RowCount; i++)
			{
				string Check = DgvCheckNull(dgvDNHistory.Rows[i].Cells[4].Value);
				bool bResult = false;
				if (!Check.Equals(""))
					bResult = true;

				if (bResult == true)
				{
					System.Data.DataTable dt = new System.Data.DataTable();

					string userId = DgvCheckNull(dgvDNHistory.Rows[i].Cells["사용자ID"].Value);
					string revise_code = DgvCheckNull(dgvDNHistory.Rows[i].Cells["도면번호"].Value).Substring(7, 1);
					string key_id = DgvCheckNull(dgvDNHistory.Rows[i].Cells["KeyID"].Value);
					string note = DgvCheckNull(dgvDNHistory.Rows[i].Cells["Note"].Value);  //Modify 내용

					//강성환 추가 2014년 01월 21일
					note = note.Replace("'", "");
					note = note.Replace("%", "");
					SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC modify_note '{0}', '{1}', {2}, '{3}'",
					userId, revise_code, key_id, note), StaticData.db_connetion_string);
					query.Fill(dt);
				}
				else
					continue;
			}

			//화면에 재 Display
			ServerSearch = true;
			checkedCount = 0;
			btnSaveNote.Enabled = false;
			BindDgvDrawingNumberHistory();
		}

		private void btnExcel_Click(object sender, EventArgs e)
		{
			Microsoft.Office.Interop.Excel._Application app = null;
			Workbook wkbook = null;
			Sheets sheetCollection = null;
			Worksheet wksheet = null;
			Range range = null;

			try
			{
				Object oMissing = System.Reflection.Missing.Value;
				app = new Microsoft.Office.Interop.Excel.Application();
				app.Visible = false;
				app.UserControl = true;
				System.Globalization.CultureInfo ci = System.Globalization.CultureInfo.InvariantCulture;
				wkbook = app.Workbooks.Add(oMissing);
				sheetCollection = app.Worksheets;
				wksheet = (Worksheet)sheetCollection.get_Item(1);
				wksheet.Name = "Drawing Number History";

				// 제목 추출
				for (int j = 0; j < dgvDNHistory.Columns.Count; j++)
				{
					if (dgvDNHistory.Columns[j].Visible.Equals(true))
					{
						if (j == 5)
						{
							range = (Range)wksheet.Cells[1, j - 1];
							range.Value2 = dgvDNHistory.Columns[j].HeaderText;
							break;
						}

						range = (Range)wksheet.Cells[1, j];
						range.Value2 = dgvDNHistory.Columns[j].HeaderText;
					}
				}

				// 데이터 추출
				for (int i = 0; i < dgvDNHistory.Rows.Count; i++)
				{
					for (int j = 0; j < dgvDNHistory.Columns.Count; j++)
					{
						if (dgvDNHistory.Columns[j].Visible.Equals(true))
						{
							if (j == 5)
							{
								range = (Range)wksheet.Cells[i + 2, j - 1];
								range.Value2 = dgvDNHistory[j, i].Value.ToString();
								break;
							}

							//값이 존재를 하지 않음
							if (j == 4 && note_write == true)
								continue;

							range = (Range)wksheet.Cells[i + 2, j];
							range.Value2 = dgvDNHistory[j, i].Value.ToString();
						}
					}
				}

				//사이즈 조절
				wksheet.Columns.AutoFit();
				wksheet.Rows.AutoFit();
				app.Visible = true;

				btnExcel.Enabled = false;
			}
			catch (Exception ex)
			{
				ex.ToString();
			}
		}

		private void FormDNHistory_Resize(object sender, EventArgs e)
		{
			dgvDNHistoryCellFormating();
		}

		//체크박스가 true인 경우에 UnCheck를 하면 Event처리
		private void dgvDNHistory_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
			if (e.RowIndex == -1)
				return;

			if (note_write == true)
			{
				if (e.ColumnIndex == 4)
				{
					if ((Boolean)dgvDNHistory.Rows[e.RowIndex].Cells[4].EditedFormattedValue == true)
					{
						dgvDNHistory.Rows[e.RowIndex].Cells["Note"].Value = DgvCheckNull(dgvDNHistory.Rows[e.RowIndex].Cells["BeforeNote"].Value);
						dgvDNHistory.Rows[e.RowIndex].Cells[4].Value = false;
						checkedCount--;
					}
					
					dgvDNHistory.Rows[e.RowIndex].Cells[6].Style.ForeColor = Color.Black;
					SaveNoteButtonEnable();
				}
			}
		}

		//Note Cell을 변경이 완료 되었을때 Event
		private void dgvDNHistory_CellEndEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (note_write == true)
			{
				string beforeNote = DgvCheckNull(dgvDNHistory.Rows[e.RowIndex].Cells["BeforeNote"].Value);	// 전에 내용
				string afterNote = DgvCheckNull(dgvDNHistory.Rows[e.RowIndex].Cells["Note"].Value);			// 변경 내용

				if (e.ColumnIndex == 6)
				{
					if (beforeNote != afterNote)
					{
						dgvDNHistory.Rows[e.RowIndex].Cells[6].Style.ForeColor = Color.Red;
						dgvDNHistory.Rows[e.RowIndex].Cells[4].Value = true;
						dgvDNHistory.Rows[e.RowIndex].Cells["Note"].Value = afterNote;
						checkedCount++;
					}
					else
					{
						dgvDNHistory.Rows[e.RowIndex].Cells["Note"].Value = beforeNote;
						dgvDNHistory.Rows[e.RowIndex].Cells[6].Style.ForeColor = Color.Black;
					}
				}
				SaveNoteButtonEnable();
			}
		}

		private void cmbUserID_SelectedValueChanged(object sender, EventArgs e)
		{
			BindDgvDrawingNumberHistory();
		}

		private void cmbDrawingCategory_SelectedValueChanged(object sender, EventArgs e)
		{
			BindDgvDrawingNumberHistory();
		}

        private void txtTDN_TextChanged(object sender, EventArgs e)
        {
            if (txtTDN.Text.Length > 0)
                ServerSearch = true;
        }

		System.Data.DataTable result = null;
		System.Data.DataTable tempResult = null;
		private void BindDgvDrawingNumberHistory()
		{
			if (ServerSearch == true)
			{
				result = new System.Data.DataTable();
				tempResult = new System.Data.DataTable();

				//Data History정보를 모두 읽어온다.
                SqlDataAdapter query = null;
                if (txtTDN.Text.Length == 0)
                {
                    query = new SqlDataAdapter(String.Format("EXEC drawing_number_history '{0}', '{1}', ' '",
                        dtpickerStartDate.Value.ToString(Const.DATE_TIME_FORMAT), dtpickerEndDate.Value.ToString(Const.DATE_TIME_FORMAT)), StaticData.db_connetion_string);
                }
                else if (txtTDN.Text.Length >= 1 && txtTDN.Text != "")
                {
                    query = new SqlDataAdapter(String.Format("EXEC drawing_number_history  ' ', ' ', '{0}%'", txtTDN.Text), StaticData.db_connetion_string);
                }

				query.Fill(result);
				ServerSearch = false;

				result.Columns[0].ColumnName = "KeyID";
				result.Columns[1].ColumnName = "생성일자";
				result.Columns[2].ColumnName = "사용자ID";
				result.Columns[3].ColumnName = "도면번호";
                result.Columns[4].ColumnName = "(眞)도면번호";
                result.Columns[5].ColumnName = "Note";
				result.Columns[6].ColumnName = "BeforeNote";
				result.Columns[7].ColumnName = "Category";
                
				dgvDNHistory.DataSource = result;
				dgvDNHistoryCellFormating();
			}

			tempResult = result.Copy();
			if (tempResult != null && tempResult.Rows.Count > 0)
			{
				DataRow[] dtrow = null;
				string category = cmbDrawingCategory.Text;
				string id = cmbUserID.SelectedValue.ToString();

				//Enter값 제거
				txtNote.Text = txtNote.Text.Replace("\r\n", "");
				if (txtNote.Text.Length == 0)
				{
					if (category != "ALL" && id != "ALL")
						dtrow = tempResult.Select(string.Format(" 사용자ID = '{0}' AND Category = '{1}'", id, category));
					else if (category != "ALL" && id == "ALL")
						dtrow = tempResult.Select(string.Format(" Category = '{0}'", category));
					else if (category == "ALL" && id != "ALL")
						dtrow = tempResult.Select(string.Format(" 사용자ID = '{0}'", id));
					else if (category == "ALL" && id == "ALL")
					{
						dgvDNHistory.DataSource = tempResult;
						return;
					}

					if (dtrow == null)
						return;
	 
					if (dtrow.Length > 0)
						tempResult = dtrow.CopyToDataTable();
					else
						tempResult.Rows.Clear();	
				}
				else if (txtNote.Text.Length > 0)
				{
					string[] strSearchNote = txtNote.Text.Split(' ');
					if (strSearchNote.Length > 0)
					{
						for (int i = 0; i < strSearchNote.Length; i++)
						{
							if (category != "ALL" && id != "ALL")
								dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND Category = '{1}' AND NOTE LIKE '%{2}%'", id, category, strSearchNote[i]));
							else if (category != "ALL" && id == "ALL")
								dtrow = tempResult.Select(string.Format(" Category = '{0}' AND NOTE LIKE '%{1}%'", category, strSearchNote[i]));
							else if (category == "ALL" && id != "ALL")
								dtrow = tempResult.Select(string.Format(" 사용자ID = '{0}' AND NOTE LIKE '%{1}%'", id, strSearchNote[i]));
							else if (category == "ALL" && id == "ALL")
								dtrow = tempResult.Select(string.Format("NOTE LIKE '%{0}%'", strSearchNote[i]));

							if (dtrow == null)
								return;
							if (dtrow.Length > 0)
								tempResult = dtrow.CopyToDataTable();
							else
								tempResult.Rows.Clear();
						}
					}
				}
				dgvDNHistory.DataSource = tempResult;
			}
		}

		private void dgvDNHistoryCellFormating()
		{
			dgvDNHistory.Columns["KeyID"].Visible = false;
			dgvDNHistory.Columns["생성일자"].Width = 150;
			dgvDNHistory.Columns["생성일자"].DefaultCellStyle.Format = "yyyy-MM-dd HH:mm:ss.fff";
			dgvDNHistory.Columns["사용자ID"].Width = 80;
			dgvDNHistory.Columns["도면번호"].Width = 80;
            dgvDNHistory.Columns["(眞)도면번호"].Width = 100;
			dgvDNHistory.Columns["BeforeNote"].Visible = false;
			dgvDNHistory.Columns["Category"].Visible = false;

            dgvDNHistory.Columns["TYPE"].Visible = false;
            dgvDNHistory.Columns["PROJECT_CODE"].Visible = false;
            dgvDNHistory.Columns["PART_NAME"].Visible = false;
            dgvDNHistory.Columns["SPECFICATION"].Visible = false;
            dgvDNHistory.Columns["MATERIAL"].Visible = false;
            dgvDNHistory.Columns["APROCESS"].Visible = false;
            dgvDNHistory.Columns["QUANTITY"].Visible = false;
            dgvDNHistory.Columns["MAKER"].Visible = false;
            dgvDNHistory.Columns["UNIT_PRICE"].Visible = false;
            dgvDNHistory.Columns["PRICE"].Visible = false;

			if (note_write == false)
				dgvDNHistory.ReadOnly = true;
			else
			{
				//최초 생성시 한번만 쓰인다.
				if (columnAdded == true)
				{
					dgvDNHistory.Columns[0].ReadOnly = true;
					dgvDNHistory.Columns[1].ReadOnly = true;
					dgvDNHistory.Columns[2].ReadOnly = true;
					dgvDNHistory.Columns[3].ReadOnly = true;
					dgvDNHistory.Columns[4].ReadOnly = true;
					dgvDNHistory.Columns[4].Width = 80;
                    dgvDNHistory.Columns[5].ReadOnly = true;
                    dgvDNHistory.Columns[5].Width = 100;
					dgvDNHistory.Columns[6].ReadOnly = false;
					dgvDNHistory.Columns[7].ReadOnly = true;
					return;
				}

				DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn();
				checkBoxColumn.Name = "Modify_Note";
				checkBoxColumn.HeaderText = "Note변경";
				checkBoxColumn.TrueValue = true;
				checkBoxColumn.FalseValue = false;
				dgvDNHistory.AutoGenerateColumns = false;
				dgvDNHistory.Columns.Insert(4, checkBoxColumn);
				dgvDNHistory.Columns[4].Width = 80;
				columnAdded = true;

				dgvDNHistory.Columns[0].ReadOnly = true;
				dgvDNHistory.Columns[1].ReadOnly = true;
				dgvDNHistory.Columns[2].ReadOnly = true;
				dgvDNHistory.Columns[3].ReadOnly = true;
				dgvDNHistory.Columns[4].ReadOnly = true;
				dgvDNHistory.Columns[5].ReadOnly = true;
				dgvDNHistory.Columns[6].ReadOnly = false;
                dgvDNHistory.Columns[7].ReadOnly = true;
			}
		}


        private void BindCmbDepartment()
        {
			System.Data.DataTable result = new System.Data.DataTable();
			SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC department_code_info"), StaticData.db_connetion_string);
			query.Fill(result);
			if (result != null)
			{
				result.Rows.Add("ALL");

				cmbDrawingCategory.ValueMember = "VALUE";
				cmbDrawingCategory.DisplayMember = "CODE";
				cmbDrawingCategory.DataSource = result;

				cmbDrawingCategory.Text = department_code.ToString();
			}
        }

		private void BindUserID()
		{
			System.Data.DataTable result = new System.Data.DataTable();
			SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC user_infomation 0"), StaticData.db_connetion_string);
			query.Fill(result);

			if (result != null)
			{
                result.DefaultView.Sort = "NAME ASC";
                result.Rows.Add("ALL", "ALL", "ALL", "ALL", "ALL");

				cmbUserID.ValueMember = "ID";
				cmbUserID.DisplayMember = "NAME";
				cmbUserID.DataSource = result;
				cmbUserID.SelectedValue = StaticData.user_id;
			}
		}

		private void SaveNoteButtonEnable()
		{
			if (checkedCount > 0)
				btnSaveNote.Enabled = true;
			else
				btnSaveNote.Enabled = false;
		}

		private void ExcelButtonEnable()
		{
			if (dgvDNHistory.Rows.Count > 0)
				btnExcel.Enabled = true;
			else
				btnExcel.Enabled = false;
		}

		private string DgvCheckNull(object value)
		{
			string strResult = "";
			if (value != null)
			{
				strResult = value.ToString();
			}

			return strResult;
		}

        private void dgvDNHistory_MouseDown(object sender, MouseEventArgs e)
        {
            int index = 0;
            if (e.Button == MouseButtons.Right)
            {
                if (dgvDNHistory.SelectedRows.Count > 0)
                {
                    index = dgvDNHistory.SelectedRows[0].Index;
                    string strTDN = dgvDNHistory.Rows[index].Cells["도면번호"].Value.ToString();

                    int iTDNKey = GetDNKey(strTDN);
                    char lastRevision = GetDNLastRevisionCode(iTDNKey);

                    if (strTDN.Substring(7, 1) != lastRevision.ToString())
                    {
                        string strMessage = "";
                        strMessage += "선택된 가도번은 최신이 아닙니다.\r";
                        strMessage += "최신 도번으로 이동을 할려면 '예(Y)', 그렇지 않으면 '아니오(N)' 버튼을 누르세요";

                        if (MessageBox.Show(strMessage, "Warning", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            //최종 위치로 이동을 한다.
                            string strDeleteReviseCdTDN = "";
                            string strLastReviseTDN = "";

                            strDeleteReviseCdTDN = strTDN.Remove(7, 1);
                            strLastReviseTDN = strDeleteReviseCdTDN + lastRevision;

                            System.Data.DataTable table = (System.Data.DataTable)dgvDNHistory.DataSource;
                            DataRow[] rows = table.Select(string.Format(" 도면번호 = '{0}'", strLastReviseTDN));
                            int selectedIndex = table.Rows.IndexOf(rows[0]);

                            dgvDNHistory.Rows[selectedIndex].Selected = true;
                            tsmiReviseODN.Visible = false;
                        }
                    }
                    else
                    {
                        string strODN = dgvDNHistory.Rows[index].Cells["(眞)도면번호"].Value.ToString();
                        if (strODN == "")
                        {
                            tsmiCreateODN.Visible = true;
                            tsmiReviseODN.Visible = false;
                        }
                        else if (strODN.Length > 0)
                        {
                            tsmiReviseODN.Visible = true;
                            tsmiCreateODN.Visible = false;
                        }
                    }
                }
            }
        }

        private void tsmiCreateODN_Click(object sender, EventArgs e)
        {
            int index = 0;
            if (dgvDNHistory.SelectedRows.Count > 0)
            {
                index = dgvDNHistory.SelectedRows[0].Index;
                string strTDN = dgvDNHistory.Rows[index].Cells["도면번호"].Value.ToString();

                //20200417 추가
                alContent = new ArrayList();
                alContent.Add(dgvDNHistory.Rows[index].Cells["TYPE"].Value);
                if (dgvDNHistory.Rows[index].Cells["PROJECT_CODE"].Value != null && dgvDNHistory.Rows[index].Cells["PROJECT_CODE"].Value.ToString() != "")
                {
                    alContent.Add(dgvDNHistory.Rows[index].Cells["PROJECT_CODE"].Value.ToString().Substring(0, 3)); //MODEL
                    alContent.Add(dgvDNHistory.Rows[index].Cells["PROJECT_CODE"].Value.ToString().Substring(3, 2)); //PART 
                }
                else
                {
                    alContent.Add(""); //MODEL
                    alContent.Add(""); //PART 
                }

                alContent.Add(dgvDNHistory.Rows[index].Cells["PART_NAME"].Value.ToString());
                alContent.Add(dgvDNHistory.Rows[index].Cells["SPECFICATION"].Value.ToString());
                alContent.Add(dgvDNHistory.Rows[index].Cells["MATERIAL"].Value.ToString());
                alContent.Add(dgvDNHistory.Rows[index].Cells["APROCESS"].Value.ToString());
                alContent.Add(dgvDNHistory.Rows[index].Cells["QUANTITY"].Value.ToString());
                alContent.Add(dgvDNHistory.Rows[index].Cells["MAKER"].Value.ToString());
                alContent.Add(dgvDNHistory.Rows[index].Cells["UNIT_PRICE"].Value.ToString());
                alContent.Add(dgvDNHistory.Rows[index].Cells["PRICE"].Value.ToString());
                //20200417 추가

                FormPublishOfficialDN frmPublishODN = new FormPublishOfficialDN(strTDN, "", 0, Const.FLAG_TDN_TO_CREATE_ODN, alContent);
                if (frmPublishODN.ShowDialog() == DialogResult.OK)
                {
                    //진도번 Histroy에 저장을 한다.
                    System.Data.DataTable dt = new System.Data.DataTable();
                    string userId = DgvCheckNull(dgvDNHistory.Rows[index].Cells["사용자ID"].Value);
                    string revise_code = DgvCheckNull(dgvDNHistory.Rows[index].Cells["도면번호"].Value).Substring(7, 1);
                    string key_id = DgvCheckNull(dgvDNHistory.Rows[index].Cells["KeyID"].Value);

                    SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC modify_ODN '{0}', '{1}', {2}, '{3}'",
                    userId, revise_code, key_id, frmPublishODN.OfficialDrawingNumber), StaticData.db_connetion_string);
                    query.Fill(dt);

                    ServerSearch = true;
                    BindDgvDrawingNumberHistory();
                    ServerSearch = false;
                }
            }
        }

        private void tsmiReviseODN_Click(object sender, EventArgs e)
        {
            int index = 0;

            if (dgvDNHistory.SelectedRows.Count > 0)
            {
                index = dgvDNHistory.SelectedRows[0].Index;
                string strODN = dgvDNHistory.Rows[index].Cells["(眞)도면번호"].Value.ToString();
                string strTDN = dgvDNHistory.Rows[index].Cells["도면번호"].Value.ToString();

                //20200417 추가
                alContent = new ArrayList();
                alContent.Add(dgvDNHistory.Rows[index].Cells["TYPE"].Value);
                if (dgvDNHistory.Rows[index].Cells["PROJECT_CODE"].Value != null)
                {
                    alContent.Add(dgvDNHistory.Rows[index].Cells["PROJECT_CODE"].Value.ToString().Substring(0, 3)); //MODEL
                    alContent.Add(dgvDNHistory.Rows[index].Cells["PROJECT_CODE"].Value.ToString().Substring(3, 2)); //PART 
                }
                alContent.Add(dgvDNHistory.Rows[index].Cells["PART_NAME"].Value);
                alContent.Add(dgvDNHistory.Rows[index].Cells["SPECFICATION"].Value);
                alContent.Add(dgvDNHistory.Rows[index].Cells["MATERIAL"].Value);
                alContent.Add(dgvDNHistory.Rows[index].Cells["APROCESS"].Value);
                alContent.Add(dgvDNHistory.Rows[index].Cells["QUANTITY"].Value);
                alContent.Add(dgvDNHistory.Rows[index].Cells["MAKER"].Value);
                alContent.Add(dgvDNHistory.Rows[index].Cells["UNIT_PRICE"].Value);
                alContent.Add(dgvDNHistory.Rows[index].Cells["PRICE"].Value);
                //20200417 추가

                FormPublishOfficialDN frmPublishReviseODN = new FormPublishOfficialDN(strTDN, strODN, 0, Const.FLAG_TDN_TO_REVISE_ODN, alContent);
                if (frmPublishReviseODN.ShowDialog() == DialogResult.OK)
                {
                    //진도번 Histroy에 저장을 한다.
                    System.Data.DataTable dt = new System.Data.DataTable();
                    string userId = DgvCheckNull(dgvDNHistory.Rows[index].Cells["사용자ID"].Value);
                    string revise_code = DgvCheckNull(dgvDNHistory.Rows[index].Cells["도면번호"].Value).Substring(7, 1);
                    string key_id = DgvCheckNull(dgvDNHistory.Rows[index].Cells["KeyID"].Value);

                    SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC modify_ODN '{0}', '{1}', {2}, '{3}'",
                    userId, revise_code, key_id, frmPublishReviseODN.OfficialDrawingNumber), StaticData.db_connetion_string);
                    query.Fill(dt);

                    ServerSearch = true;
                    BindDgvDrawingNumberHistory();
                    ServerSearch = false;
                }
            }
        }

        private int GetDNKey(string strTDN)
        {
            int resultDNKey = 0;
            SqlCommand cmd = new SqlCommand(String.Format("EXEC drawing_key_of '{0}'", strTDN), new SqlConnection(StaticData.db_connetion_string));
            cmd.Connection.Open();
            resultDNKey = (int)cmd.ExecuteScalar();
            cmd.Connection.Close();
            return resultDNKey;
        }

        private char GetDNLastRevisionCode(int currentDNKey)
        {
            string resultDNLastRevisionCode = "";
            SqlCommand query = new SqlCommand(String.Format("EXEC last_revision_code {0}, '{1}'", currentDNKey, DrawingCategory.code), new SqlConnection(StaticData.db_connetion_string));
            query.Connection.Open();
            resultDNLastRevisionCode = (string)query.ExecuteScalar();
            query.Connection.Close();
            return resultDNLastRevisionCode[0];
        }
    }
}
