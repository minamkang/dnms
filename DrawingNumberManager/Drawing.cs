using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Drawing;

namespace DrawingNumberManager
{
	public class Drawing 
    {
		public DateTime create_date ;
		public User author ;
		public DrawingNumber drawing_number ;
		public DrawingCategory category ;

        public Drawing()
        {
            if (StaticData.server_path == "")
            {
                MessageBox.Show("서버 경로가 지정되지 않았습니다. \r\nConfiguration에서 설정하여 주십시요.");
                return;
            }

            if (StaticData.local_path == "")
            {
                MessageBox.Show("로컬 경로가 지정되지 않았습니다. \r\nConfiguration에서 설정하여 주십시요.");
                return;
            }
        }

        public bool Progress_Downloads(ArrayList SourcefileName, ucProgressBar ucPb, ref int searchedCount)
        {
            bool isResult = false;

            try
            {
                ucPb.StatusText = "도면 다운로드";
                ucPb.StatusFontColor = Color.Red;

                DirectoryInfo di = new DirectoryInfo(StaticData.server_path);
                foreach (FileInfo f in di.GetFiles())
                {
                    string[] strInFileName = f.Name.Split('.'); //확장자 제거

                    for (int i = 0; i < SourcefileName.Count; i++)
                    {
                        if (strInFileName[0] == SourcefileName[i].ToString())
                        {
                            string source_path = Path.Combine(StaticData.server_path, f.Name);
                            string target_path = Path.Combine(StaticData.local_path, f.Name);

                            File.Copy(source_path, target_path, true);

                            searchedCount++;
                            ucPb.Message = string.Format("{0}/{1}을 로컬로 저장.", searchedCount, SourcefileName.Count);
                            ucPb.Progress = (int)(searchedCount * 100 / SourcefileName.Count);
                            Application.DoEvents();

                            //break;
                        }
                    }
                }

                isResult = true;
            }
            catch (Exception)
            {
                isResult = false;
            }
            finally
            {
                ucPb.Visible = false;
            }

            return isResult;
        }

        public bool Downloads(ArrayList SourcefileName, ref ArrayList al_Result)
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(StaticData.server_path);
                foreach (FileInfo f in di.GetFiles())
                {
                    string[] strInFileName = f.Name.Split('.'); //확장자 제거

                    for (int i = 0; i < SourcefileName.Count; i++)
                    {
                        if (strInFileName[0] == SourcefileName[i].ToString())
                        {
                            string source_path = Path.Combine(StaticData.server_path, f.Name);
                            string strMailPath = "C:\\KM_ORDER\\MAIL";
                            string attatchment_path = Path.Combine(strMailPath, f.Name);

                            File.Copy(source_path, attatchment_path, true);
                            al_Result.Add(string.Format("{0},{1}", SourcefileName[i].ToString(), attatchment_path));
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("메일전송 실패(File 경로 확인)" + ex.ToString());
                return false;
            }
        }

        public string Download(string SourcefileName)
        {
            string Result = "";
            try
            {
                string[] delte_files = Directory.GetFiles(@Const.LOCAL_VIEW_PATH);
                foreach (string dfilePath in delte_files)
                {
                    FileInfo file = new FileInfo(dfilePath);
                    file.IsReadOnly = false;
                    File.Delete(dfilePath);
                }
            }
            catch (Exception ex)
            {
                Result = ex.ToString();
                return Result;
            }

            try
            {
                int fileCopyCount = 0;
                DirectoryInfo di = new DirectoryInfo(StaticData.server_path);
                foreach (FileInfo f in di.GetFiles())
                {
                    string[] strInFileName = f.Name.Split('.'); //확장자 제거
                    if (strInFileName[0] == SourcefileName)
                    {
                        if (strInFileName[1] != "bak")
                        {
                            string source_path = Path.Combine(StaticData.server_path, f.Name);
                            string target_path = Path.Combine(Const.LOCAL_VIEW_PATH, f.Name);

                            File.Copy(source_path, target_path, true);

                            FileInfo file = new FileInfo(target_path);
                            file.IsReadOnly = false;

                            fileCopyCount++;
                            break;
                        }
                    }
                }

                if (fileCopyCount == 0)
                    Result = string.Format("도번:[{0}] 파일이 없습니다.", SourcefileName);
            }
            catch (Exception ex)
            {
                Result = ex.ToString();
                return Result;
            }

            return Result;
        }

        public ArrayList SearchFile(ArrayList SourcefileName)
        {
            ArrayList al_Result = new ArrayList();
            try
            {
                DirectoryInfo di = new DirectoryInfo(StaticData.server_path);
                foreach (FileInfo f in di.GetFiles())
                {
                    string[] strInFileName = f.Name.Split('.'); //확장자 제거

                    for (int i = 0; i < SourcefileName.Count; i++)
                    {
                        if (strInFileName[0] == SourcefileName[i].ToString())
                        {
                            al_Result.Add(i);
                            break;
                        }
                    }
                }
            }
            catch (Exception)
            {
                
            }

            return al_Result;
        }

	}
}
