﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using Microsoft.Office.Interop.Excel;

namespace DrawingNumberManager
{
    public partial class FormODNHistory : Form
    {
        public delegate void FormSendDataHandler(object obj);
        public event FormSendDataHandler FormSendEvent;

        private bool note_write;

        bool formLoadStartDate = false;
        bool columnAdded = false;		//최초 컬럼 표현할때 쓰인다.
        bool ServerSearch = false;		//날짜관련 Data가 변경 되면 Server에서 Search여부

        double checkedCount = 0;		//체크박스 체크 Count

        System.Data.DataTable result = null;
        System.Data.DataTable tempResult = null;
 
        public FormODNHistory(bool bNoteWrite)
        {
            note_write = bNoteWrite;
            InitializeComponent();
        }

        private void FormODNHistory_Load(object sender, EventArgs e)
        {
            this.Text = Const.FORM_OFFICIAL_DRAWING_NUMBER_HISTORY;
            ServerSearch = true;

            //Excel 프로그램이 설치 되었는지 여부를 판단하여 Excel버튼을 보여질지 여부 판단
            Type officeType = Type.GetTypeFromProgID("Excel.Application");
            if (officeType == null)
                btnExcel.Visible = false;

            BindUserID();

            //콤보박스 초기화
            for (short i = 1; i <= 3; i++)
                BindCmbBox(i);

            if (note_write == false)
            {
                btnExcel.Location = new System.Drawing.Point(944, 17);
                btnSaveNote.Visible = false;
            }

            formLoadStartDate = true;
            DateTime Now = DateTime.Now.Date;
            dtpickerStartDate.Value = Now;
            dtpickerEndDate.Value = Now.AddDays(1).AddSeconds(-1);
            formLoadStartDate = false;

            btnSaveNote.Enabled = false;
            BindDgvOfficialDrawingNumberHistory();
            ExcelButtonEnable();
        }

        private void dtpickerStartDate_ValueChanged(object sender, EventArgs e)
        {
            if (formLoadStartDate == true)
                return;

            if (dtpickerStartDate.Value > dtpickerEndDate.Value)
            {
                MessageBox.Show("Start Date는 End Date보다 클 수 없습니다.");
                DateTime Now = DateTime.Now.Date;
                dtpickerStartDate.Value = Now;
            }
            ServerSearch = true;
        }

        private void dtpickerEndDate_ValueChanged(object sender, EventArgs e)
        {
            if (dtpickerEndDate.Value < dtpickerStartDate.Value)
            {
                MessageBox.Show("End Date는 Start Date보다 작을 수 없습니다.");
                DateTime Now = DateTime.Now.Date;
                dtpickerEndDate.Value = Now.AddDays(1).AddSeconds(-1);
            }
            ServerSearch = true;
        }

        private void btnNowDay_Click(object sender, EventArgs e)
        {
            DisplayDate("Day", 0);
        }

        private void btn1Week_Click(object sender, EventArgs e)
        {
            DisplayDate("Day", -7);
        }

        private void btn15Day_Click(object sender, EventArgs e)
        {
            DisplayDate("Day", -15);
        }

        private void btn1Month_Click(object sender, EventArgs e)
        {
            DisplayDate("Month", -1);
        }

        private void btn3Month_Click(object sender, EventArgs e)
        {
            DisplayDate("Month", -3);
        }

        private void btn6Month_Click(object sender, EventArgs e)
        {
            DisplayDate("Month", -6);
        }

        private void btn9Month_Click(object sender, EventArgs e)
        {
            DisplayDate("Month", -9);
        }

        private void btn1Year_Click(object sender, EventArgs e)
        {
            DisplayDate("Year", -1);
        }

        private void DisplayDate(string strDateType, short iDateValue)
        {
            DateTime Now = DateTime.Now.Date;
            switch (strDateType)
            {
                case "Day":
                    dtpickerStartDate.Value = Now.AddDays(iDateValue);
                    break;
                case "Month":
                    dtpickerStartDate.Value = Now.AddMonths(iDateValue);
                    break;
                case "Year":
                    dtpickerStartDate.Value = Now.AddYears(iDateValue);
                    break;
            }

            dtpickerEndDate.Value = Now.AddDays(1).AddSeconds(-1);
            ServerSearch = true;
        }

        private void cmbModel_SelectedValueChanged(object sender, EventArgs e)
        {
            BindDgvOfficialDrawingNumberHistory();
        }

        private void cmbPart_SelectedValueChanged(object sender, EventArgs e)
        {
            BindDgvOfficialDrawingNumberHistory();
        }

        private void cmbProcess_SelectedValueChanged(object sender, EventArgs e)
        {
            BindDgvOfficialDrawingNumberHistory();
        }

        private void cmbUserID_SelectedValueChanged(object sender, EventArgs e)
        {
            BindDgvOfficialDrawingNumberHistory();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            BindDgvOfficialDrawingNumberHistory();
            ExcelButtonEnable();
            checkedCount = 0;
            btnSaveNote.Enabled = false;
        }

        private void btnSaveNote_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < this.dgvODNHistory.RowCount; i++)
            {
                string Check = DgvCheckNull(dgvODNHistory.Rows[i].Cells[5].Value);
                bool bResult = false;
                
                if (!Check.Equals(""))
                    bResult = true;

                if (bResult == true)
                {
                    System.Data.DataTable dt = new System.Data.DataTable();

                    string userId = DgvCheckNull(dgvODNHistory.Rows[i].Cells["사용자ID"].Value);
                    string revise_code = DgvCheckNull(dgvODNHistory.Rows[i].Cells["Revision"].Value);
                    string key_id = DgvCheckNull(dgvODNHistory.Rows[i].Cells["KeyID"].Value);
                    string note = DgvCheckNull(dgvODNHistory.Rows[i].Cells["Note"].Value);  //Modify 내용
                    
                    //swkang 20160912 add
                    string order_type = DgvCheckNull(dgvODNHistory.Rows[i].Cells["진도면번호"].Value).Substring(9, 1);


                    note = note.Replace("'", "");
                    note = note.Replace("%", "");
                    SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC modify_odn_note '{0}', '{1}', {2}, '{3}', {4}", userId, revise_code, key_id, note, order_type), StaticData.db_connetion_string);
                    query.Fill(dt);
                }
                else
                    continue;
            }

            //화면에 재 Display
            ServerSearch = true;
            checkedCount = 0;
            btnSaveNote.Enabled = false;
            BindDgvOfficialDrawingNumberHistory();
        }

        private void btnExcel_Click(object sender, EventArgs e)
        {
            Microsoft.Office.Interop.Excel._Application app = null;
            Workbook wkbook = null;
            Sheets sheetCollection = null;
            Worksheet wksheet = null;
            Range range = null;

            try
            {
                Object oMissing = System.Reflection.Missing.Value;
                app = new Microsoft.Office.Interop.Excel.Application();
                app.UserControl = true;
                System.Globalization.CultureInfo ci = System.Globalization.CultureInfo.InvariantCulture;
                wkbook = app.Workbooks.Add(oMissing);
                sheetCollection = app.Worksheets;
                wksheet = (Worksheet)sheetCollection.get_Item(1);
                wksheet.Name = "Official Drawing Number History";

                // 제목 추출
                for (int j = 0; j < dgvODNHistory.Columns.Count; j++)
                {
                    if (dgvODNHistory.Columns[j].Visible.Equals(true))
                    {
                        if (j > 5)
                        {
                            range = (Range)wksheet.Cells[1, j - 1];
                            range.Value2 = dgvODNHistory.Columns[j].HeaderText;
                            continue;
                        } 
                       
                        range = (Range)wksheet.Cells[1, j];
                        range.Value2 = dgvODNHistory.Columns[j].HeaderText;
                    }
                }

                // 데이터 추출
                for (int i = 0; i < dgvODNHistory.Rows.Count; i++)
                {
                    for (int j = 0; j < dgvODNHistory.Columns.Count; j++)
                    {
                        if (dgvODNHistory.Columns[j].Visible.Equals(true))
                        {
                            if (j > 5)
                            {
                                range = (Range)wksheet.Cells[i + 2, j - 1];
                                range.Value2 = dgvODNHistory[j, i].Value.ToString();
                                continue;
                            }

                            //값이 존재를 하지 않음
                            if (j == 5 && note_write == true)
                                continue;

                            range = (Range)wksheet.Cells[i + 2, j];
                            range.Value2 = dgvODNHistory[j, i].Value.ToString();
                        }
                    }
                }

                //사이즈 조절
                wksheet.Columns.AutoFit();
                wksheet.Rows.AutoFit();
                app.Visible = true;

                btnExcel.Enabled = false;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        //체크박스가 true인 경우에 UnCheck를 하면 Event처리
        private void dgvODNHistory_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1)
                return;

            if (note_write == true)
            {
                if (e.ColumnIndex == 5)
                {
                    //CheckBox
                    if ((Boolean)dgvODNHistory.Rows[e.RowIndex].Cells[5].EditedFormattedValue == true)
                    {
                        dgvODNHistory.Rows[e.RowIndex].Cells["Note"].Value = DgvCheckNull(dgvODNHistory.Rows[e.RowIndex].Cells["BeforeNote"].Value);
                        dgvODNHistory.Rows[e.RowIndex].Cells[5].Value = false;
                        checkedCount--;
                    }

                    dgvODNHistory.Rows[e.RowIndex].Cells[6].Style.ForeColor = Color.Black;
                    SaveNoteButtonEnable();
                }
            }
        }

        //Note Cell을 변경이 완료 되었을때 Event
        private void dgvODNHistory_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (note_write == true)
            {
                string beforeNote = DgvCheckNull(dgvODNHistory.Rows[e.RowIndex].Cells["BeforeNote"].Value);	    // 전에 내용
                string afterNote = DgvCheckNull(dgvODNHistory.Rows[e.RowIndex].Cells["Note"].Value);			// 변경 내용

                if (e.ColumnIndex == 6)
                {
                    if (beforeNote != afterNote)
                    {
                        dgvODNHistory.Rows[e.RowIndex].Cells[6].Style.ForeColor = Color.Red;
                        dgvODNHistory.Rows[e.RowIndex].Cells[5].Value = true;
                        dgvODNHistory.Rows[e.RowIndex].Cells["Note"].Value = afterNote;
                        checkedCount++;
                    }
                    else
                    {
                        dgvODNHistory.Rows[e.RowIndex].Cells["Note"].Value = beforeNote;
                        dgvODNHistory.Rows[e.RowIndex].Cells[6].Style.ForeColor = Color.Black;
                    }
                }
                SaveNoteButtonEnable();
            }
        }

        private void txtOldDrawingNumber_TextChanged(object sender, EventArgs e)
        {
            if (txtOldDrawingNumber.Text.Trim().Length > 0)
                ServerSearch = true;
        }

        private void BindDgvOfficialDrawingNumberHistory()
        {
            if (ServerSearch == true)
            {
                result = new System.Data.DataTable();
                tempResult = new System.Data.DataTable();

                //Data History정보를 모두 읽어온다.
                SqlDataAdapter query = null;
                if(txtOldDrawingNumber.Text.Length == 0 && txtOldDrawingNumber.Text == "")
                    query = new SqlDataAdapter(String.Format("EXEC official_drawing_number_history '{0}', '{1}', ' '",
                        dtpickerStartDate.Value.ToString(Const.DATE_TIME_FORMAT), dtpickerEndDate.Value.ToString(Const.DATE_TIME_FORMAT)), StaticData.db_connetion_string);
                else if (txtOldDrawingNumber.Text.Length > 0 && txtOldDrawingNumber.Text != "")
                    query = new SqlDataAdapter(String.Format("EXEC official_drawing_number_history '', '', '{0}%'", txtOldDrawingNumber.Text), StaticData.db_connetion_string);

                query.Fill(result);
                ServerSearch = false;

                result.Columns[0].ColumnName = "KeyID";
                result.Columns[1].ColumnName = "생성일자";
                result.Columns[2].ColumnName = "사용자ID";
                result.Columns[3].ColumnName = "진도면번호";
                result.Columns[4].ColumnName = "Revision";
                result.Columns[5].ColumnName = "Note";
                result.Columns[6].ColumnName = "가도면번호";
                result.Columns[7].ColumnName = "BeforeNote";
                result.Columns[8].ColumnName = "ModelCode";
                result.Columns[9].ColumnName = "PartCode";
                result.Columns[10].ColumnName = "ProcessCode";
                result.Columns[11].ColumnName = "SerialNumber";
                result.Columns[12].ColumnName = "구도면번호";

                dgvODNHistory.DataSource = result;
                dgvODNHistoryCellFormating();
            }

            tempResult = result.Copy();
            if (tempResult != null && tempResult.Rows.Count > 0)
            {
                DataRow[] dtrow = null;
                string strModel = cmbModel.SelectedValue.ToString();
                string strPart = cmbPart.SelectedValue.ToString();
                string strProcess = cmbProcess.SelectedValue.ToString();
                string strSerialNumber = txtSerialNumber.Text;
                string id = cmbUserID.SelectedValue.ToString();

                //Enter값 제거
                txtNote.Text = txtNote.Text.Replace("\r\n", "");
                if (txtNote.Text.Length == 0)
                {
                    if (strModel != "" && strPart == "" && strProcess == "" && strSerialNumber == "" && id != "ALL")
                        dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND ModelCode = '{1}'", id, strModel));
                    else if (strModel != "" && strPart == "" && strProcess == "" && strSerialNumber != "" && id != "ALL")
                        dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND ModelCode = '{1}' AND SerialNumber = '{2}'", id, strModel, strSerialNumber));
                    else if (strModel != "" && strPart == "" && strProcess == "" && strSerialNumber == "" && id == "ALL")
                        dtrow = tempResult.Select(string.Format("ModelCode = '{0}'", strModel));
                    else if (strModel != "" && strPart == "" && strProcess == "" && strSerialNumber != "" && id == "ALL")
                        dtrow = tempResult.Select(string.Format("ModelCode = {0}' AND SerialNumber = '{1}'", strModel, strSerialNumber));
                    else if (strModel == "" && strPart != "" && strProcess == "" && strSerialNumber == ""  && id != "ALL")
                        dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND PartCode = '{1}'", id, strPart));
                    else if (strModel == "" && strPart != "" && strProcess == "" && strSerialNumber != ""  && id != "ALL")
                        dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND PartCode = '{1}' AND SerialNumber = '{2}'", id, strPart, strSerialNumber));
                    else if (strModel == "" && strPart != "" && strProcess == "" && strSerialNumber == "" && id == "ALL")
                        dtrow = tempResult.Select(string.Format("PartCode = '{0}'", strPart));
                    else if (strModel == "" && strPart != "" && strProcess == "" && strSerialNumber != "" && id == "ALL")
                        dtrow = tempResult.Select(string.Format("PartCode = '{0}' AND SerialNumber = '{1}'", strPart, strSerialNumber));
                    else if (strModel == "" && strPart == "" && strProcess != "" && strSerialNumber == "" && id != "ALL")
                        dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND ProcessCode = '{1}'", id, strProcess));
                    else if (strModel == "" && strPart == "" && strProcess != "" && strSerialNumber != "" && id != "ALL")
                        dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND ProcessCode = '{1}' AND SerialNumber = '{2}'", id, strProcess, strSerialNumber));
                    else if (strModel == "" && strPart == "" && strProcess != "" && strSerialNumber == "" && id == "ALL")
                        dtrow = tempResult.Select(string.Format("ProcessCode = '{0}'", strProcess));
                    else if (strModel == "" && strPart == "" && strProcess != "" && strSerialNumber != "" && id == "ALL")
                        dtrow = tempResult.Select(string.Format("ProcessCode = '{0}' AND SerialNumber = '{1}'", strProcess, strSerialNumber));
                    else if (strModel != "" && strPart != "" && strProcess == "" && strSerialNumber == "" && id != "ALL")
                        dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND ModelCode = '{1}' AND PartCode = '{2}'", id, strModel, strPart));
                    else if (strModel != "" && strPart != "" && strProcess == "" && strSerialNumber != "" && id != "ALL")
                        dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND ModelCode = '{1}'  AND PartCode = '{2}' AND SerialNumber = '{3}'", id, strModel, strPart, strSerialNumber));
                    else if (strModel != "" && strPart != "" && strProcess == "" && strSerialNumber == "" && id == "ALL")
                        dtrow = tempResult.Select(string.Format("ModelCode = '{0}' AND PartCode = '{1}'", strModel, strPart));
                    else if (strModel != "" && strPart != "" && strProcess == "" && strSerialNumber != "" && id == "ALL")
                        dtrow = tempResult.Select(string.Format("ModelCode = '{0}' AND PartCode = '{1}' AND SerialNumber = '{2}'", strModel, strPart, strSerialNumber));
                    else if (strModel != "" && strPart == "" && strProcess != "" && strSerialNumber == "" && id != "ALL")
                        dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND ModelCode = '{1}' AND ProcessCode = '{2}'", id, strModel, strProcess));
                    else if (strModel != "" && strPart == "" && strProcess != "" && strSerialNumber != "" && id != "ALL")
                        dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND ModelCode = '{1}' AND ProcessCode = '{2}' AND SerialNumber = '{3}'", id, strModel, strProcess, strSerialNumber));
                    else if (strModel != "" && strPart == "" && strProcess != "" && strSerialNumber == "" && id == "ALL")
                        dtrow = tempResult.Select(string.Format("ModelCode = '{0}' AND ProcessCode = '{1}'", strModel, strProcess));
                    else if (strModel != "" && strPart == "" && strProcess != "" && strSerialNumber != "" && id == "ALL")
                        dtrow = tempResult.Select(string.Format("ModelCode = '{0}' AND ProcessCode = '{1}' AND SerialNumber = '{2}'", strModel, strProcess, strSerialNumber));
                    else if (strModel == "" && strPart != "" && strProcess != "" && strSerialNumber == "" && id != "ALL")
                        dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND PartCode = '{1}' AND ProcessCode = '{2}'", id, strPart, strProcess));
                    else if (strModel == "" && strPart != "" && strProcess != "" && strSerialNumber != "" && id != "ALL")
                        dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND PartCode = '{1}' AND ProcessCode = '{2}' AND SerialNumber = '{3}'", id, strPart, strProcess, strSerialNumber));
                    else if (strModel == "" && strPart != "" && strProcess != "" && strSerialNumber == "" && id == "ALL")
                        dtrow = tempResult.Select(string.Format("PartCode = '{0}' AND ProcessCode = '{1}'",strPart, strProcess));
                    else if (strModel == "" && strPart != "" && strProcess != "" && strSerialNumber != "" && id == "ALL")
                        dtrow = tempResult.Select(string.Format("PartCode = '{0}' AND ProcessCode = '{1}' AND SerialNumber = '{2}'", strPart, strProcess, strSerialNumber));
                    else if (strModel != "" && strPart != "" && strProcess != "" && strSerialNumber != "" && strSerialNumber == "" && id != "ALL")
                        dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND ModelCode = '{1}' AND PartCode = '{2}' AND ProcessCode = '{3}'", id, strModel, strPart, strProcess));
                    else if (strModel != "" && strPart != "" && strProcess != "" && strSerialNumber != "" && id != "ALL")
                        dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND ModelCode = '{1}' AND PartCode = '{2}' AND ProcessCode = '{3}',  SerialNumber = '{4}'", id, strModel, strPart, strProcess, strSerialNumber));
                    else if (strModel != "" && strPart != "" && strProcess != "" && strSerialNumber != "" && strSerialNumber == "" && id == "ALL")
                        dtrow = tempResult.Select(string.Format("ModelCode = '{0}' AND PartCode = '{1}' AND ProcessCode = '{2}'", strModel, strPart, strProcess));
                    else if (strModel != "" && strPart != "" && strProcess != "" && strSerialNumber != "" && id == "ALL")
                        dtrow = tempResult.Select(string.Format("ModelCode = '{0}' AND PartCode = '{1}' AND ProcessCode = '{2}',  SerialNumber = '{3}'", strModel, strPart, strProcess, strSerialNumber));
                    else if (strModel == "" && strPart == "" && strProcess == "" && strSerialNumber == "" && id != "ALL")
                        dtrow = tempResult.Select(string.Format("사용자ID = '{0}'", id));
                    else if (strModel == "" && strPart == "" && strProcess == "" && strSerialNumber != "" && id != "ALL")
                        dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND SerialNumber = '{1}'", id, strSerialNumber));
                    else if (strModel == "" && strPart == "" && strProcess == "" && strSerialNumber != "" && id == "ALL")
                        dtrow = tempResult.Select(string.Format("SerialNumber = '{0}'", strSerialNumber));
                    else if (strModel == "" && strPart == "" && strProcess == "" && id == "ALL")
                    {
                        dgvODNHistory.DataSource = tempResult;
                        return;
                    }

                    if (dtrow == null)
                        return;

                    if (dtrow.Length > 0)
                        tempResult = dtrow.CopyToDataTable();
                    else
                        tempResult.Rows.Clear();

                }
                else if (txtNote.Text.Length > 0)
                {
                    string[] strSearchNote = txtNote.Text.Split(' ');
                    if (strSearchNote.Length > 0)
                    {
                        for (int i = 0; i < strSearchNote.Length; i++)
                        {
                            if (strModel != "" && strPart == "" && strProcess == "" && strSerialNumber == "" && id != "ALL")
                                dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND ModelCode = '{1}' AND NOTE LIKE '%{2}%' ", id, strModel, strSearchNote[i]));
                            else if (strModel != "" && strPart == "" && strProcess == "" && strSerialNumber != "" && id != "ALL")
                                dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND ModelCode = '{1}' AND SerialNumber = '{2}' AND NOTE LIKE '%{3}%' ", id, strModel, strSerialNumber, strSearchNote[i]));
                            else if (strModel != "" && strPart == "" && strProcess == "" && strSerialNumber == "" && id == "ALL")
                                dtrow = tempResult.Select(string.Format("ModelCode = '{0}' AND NOTE LIKE '%{1}%' ", strModel, strSearchNote[i]));
                            else if (strModel != "" && strPart == "" && strProcess == "" && strSerialNumber != "" && id == "ALL")
                                dtrow = tempResult.Select(string.Format("ModelCode = '{0}' AND SerialNumber = '{1}' AND NOTE LIKE '%{2}%' ", strModel, strSerialNumber, strSearchNote[i]));
                            else if (strModel == "" && strPart != "" && strProcess == "" && strSerialNumber == "" && id != "ALL")
                                dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND PartCode = '{1}' AND NOTE LIKE '%{2}%'", id, strPart, strSearchNote[i]));
                            else if (strModel == "" && strPart != "" && strProcess == "" && strSerialNumber != "" && id != "ALL")
                                dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND PartCode = '{1}' AND SerialNumber = '{2}' AND NOTE LIKE '%{3}%'", id, strPart, strSerialNumber, strSearchNote[i]));
                            else if (strModel == "" && strPart != "" && strProcess == "" && strSerialNumber == "" && id == "ALL")
                                dtrow = tempResult.Select(string.Format("PartCode = '{0}' AND NOTE LIKE '%{1}%'", strPart, strSearchNote[i]));
                            else if (strModel == "" && strPart != "" && strProcess == "" && strSerialNumber != "" && id == "ALL")
                                dtrow = tempResult.Select(string.Format("PartCode = '{0}' AND SerialNumber = '{1}' AND NOTE LIKE '%{2}%'", strPart, strSerialNumber, strSearchNote[i]));
                            else if (strModel == "" && strPart == "" && strProcess != "" && strSerialNumber == "" && id != "ALL")
                                dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND ProcessCode = '{1}' AND NOTE LIKE '%{2}%'", id, strProcess, strSearchNote[i]));
                            else if (strModel == "" && strPart == "" && strProcess != "" && strSerialNumber != "" && id != "ALL")
                                dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND ProcessCode = '{1}' AND SerialNumber = '{2}' AND NOTE LIKE '%{3}%'", id, strProcess, strSerialNumber, strSearchNote[i]));
                            else if (strModel == "" && strPart == "" && strProcess != "" && strSerialNumber == "" && id == "ALL")
                                dtrow = tempResult.Select(string.Format("ProcessCode = '{0}' AND NOTE LIKE '%{1}%'", strProcess, strSearchNote[i]));
                            else if (strModel == "" && strPart == "" && strProcess != "" && strSerialNumber != "" && id == "ALL")
                                dtrow = tempResult.Select(string.Format("ProcessCode = '{0}' AND SerialNumber = '{1}' AND NOTE LIKE '%{2}%'", strProcess, strSerialNumber, strSearchNote[i]));
                            else if (strModel != "" && strPart != "" && strProcess == "" && strSerialNumber == "" && id != "ALL")
                                dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND ModelCode = '{1}' AND PartCode = '{2}' AND NOTE LIKE '%{3}%'", id, strModel, strPart, strSearchNote[i]));
                            else if (strModel != "" && strPart != "" && strProcess == "" && strSerialNumber != "" && id != "ALL")
                                dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND ModelCode = '{1}' AND PartCode = '{2}' AND SerialNumber = '{3}' AND NOTE LIKE '%{4}%'", id, strModel, strPart, strSerialNumber, strSearchNote[i]));
                            else if (strModel != "" && strPart != "" && strProcess == "" && strSerialNumber == "" && id == "ALL")
                                dtrow = tempResult.Select(string.Format("ModelCode = '{0}' AND PartCode = '{1}' AND NOTE LIKE '%{2}%'", strModel, strPart, strSearchNote[i]));
                            else if (strModel != "" && strPart != "" && strProcess == "" && strSerialNumber != "" && id == "ALL")
                                dtrow = tempResult.Select(string.Format("ModelCode = '{0}' AND PartCode = '{1}' AND SerialNumber = '{2}' AND NOTE LIKE '%{3}%'", strModel, strPart, strSerialNumber, strSearchNote[i]));
                            else if (strModel != "" && strPart == "" && strProcess != "" && strSerialNumber == "" && id != "ALL")
                                dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND ModelCode = '{1}' AND ProcessCode = '{2}' AND NOTE LIKE '%{3}%'", id, strModel, strProcess, strSearchNote[i]));
                            else if (strModel != "" && strPart == "" && strProcess != "" && strSerialNumber != "" && id != "ALL")
                                dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND ModelCode = '{1}' AND ProcessCode = '{2}' AND SerialNumber = '{3}' AND NOTE LIKE '%{4}%'", id, strModel, strProcess, strSerialNumber, strSearchNote[i]));
                            else if (strModel != "" && strPart == "" && strProcess != "" && strSerialNumber == "" && id == "ALL")
                                dtrow = tempResult.Select(string.Format("ModelCode = '{0}' AND ProcessCode = '{1}' AND NOTE LIKE '%{2}%'", strModel, strProcess, strSearchNote[i]));
                            else if (strModel != "" && strPart == "" && strProcess != "" && strSerialNumber != "" && id == "ALL")
                                dtrow = tempResult.Select(string.Format("ModelCode = '{0}' AND ProcessCode = '{1}' AND SerialNumber = '{2}' AND NOTE LIKE '%{3}%'", strModel, strProcess, strSerialNumber, strSearchNote[i]));
                            else if (strModel == "" && strPart != "" && strProcess != "" && strSerialNumber == "" && id != "ALL")
                                dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND PartCode = '{1}' AND ProcessCode = '{2}' AND NOTE LIKE '%{3}%'", id, strPart, strProcess, strSearchNote[i]));
                            else if (strModel == "" && strPart != "" && strProcess != "" && strSerialNumber != "" && id != "ALL")
                                dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND PartCode = '{1}' AND ProcessCode = '{2}' AND SerialNumber = '{3}' AND NOTE LIKE '%{4}%'", id, strPart, strProcess, strSerialNumber, strSearchNote[i]));
                            else if (strModel == "" && strPart != "" && strProcess != "" && strSerialNumber == "" && id == "ALL")
                                dtrow = tempResult.Select(string.Format("PartCode = '{0}' AND ProcessCode = '{1}' AND NOTE LIKE '%{2}%'", strPart, strProcess, strSearchNote[i]));
                            else if (strModel == "" && strPart != "" && strProcess != "" && strSerialNumber != "" && id == "ALL")
                                dtrow = tempResult.Select(string.Format("PartCode = '{0}' AND ProcessCode = '{1}' AND SerialNumber = '{2}' AND NOTE LIKE '%{3}%'", strPart, strProcess, strSerialNumber, strSearchNote[i]));
                            else if (strModel != "" && strPart != "" && strProcess != "" && strSerialNumber == "" && id != "ALL")
                                dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND ModelCode = '{1}' AND PartCode = '{2}' AND ProcessCode = '{3}' AND NOTE LIKE '%{4}%'", id, strModel, strPart, strProcess, strSearchNote[i]));
                            else if (strModel != "" && strPart != "" && strProcess != "" && strSerialNumber != "" && id != "ALL")
                                dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND ModelCode = '{1}' AND PartCode = '{2}' AND ProcessCode = '{3}' AND SerialNumber = '{4}' AND NOTE LIKE '%{5}%'", id, strModel, strPart, strProcess, strSerialNumber, strSearchNote[i]));
                            else if (strModel != "" && strPart != "" && strProcess != "" && strSerialNumber == "" && id == "ALL")
                                dtrow = tempResult.Select(string.Format("ModelCode = '{0}' AND PartCode = '{1}' AND ProcessCode = '{2}' AND NOTE LIKE '%{3}%'", strModel, strPart, strProcess, strSearchNote[i]));
                            else if (strModel != "" && strPart != "" && strProcess != "" && strSerialNumber != "" && id == "ALL")
                                dtrow = tempResult.Select(string.Format("ModelCode = '{0}' AND PartCode = '{1}' AND ProcessCode = '{2}' AND SerialNumber = '{3}' AND NOTE LIKE '%{4}%'", id, strModel, strPart, strProcess, strSerialNumber, strSearchNote[i]));
                            else if (strModel == "" && strPart == "" && strProcess == "" && strSerialNumber == "" && id != "ALL")
                                dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND NOTE LIKE '%{1}%'", id, strSearchNote[i]));
                            else if (strModel == "" && strPart == "" && strProcess == "" && strSerialNumber != "" && id != "ALL")
                                dtrow = tempResult.Select(string.Format("사용자ID = '{0}' AND SerialNumber = '{1}' AND NOTE LIKE '%{2}%'", id, strSerialNumber, strSearchNote[i]));
                            else if (strModel == "" && strPart == "" && strProcess == "" && strSerialNumber == "" && id == "ALL")
                                dtrow = tempResult.Select(string.Format("NOTE LIKE '%{0}%'", strSearchNote[i]));
                            else if (strModel == "" && strPart == "" && strProcess == "" && strSerialNumber != "" && id == "ALL")
                                dtrow = tempResult.Select(string.Format("SerialNumber = '{0}' AND NOTE LIKE '%{1}%'", strSerialNumber, strSearchNote[i]));

                            if (dtrow == null)
                                return;
                            if (dtrow.Length > 0)
                                tempResult = dtrow.CopyToDataTable();
                            else
                                tempResult.Rows.Clear();
                        }
                    }
                }

                dgvODNHistory.DataSource = tempResult;
            }
        }

        private void dgvODNHistoryCellFormating()
        {
            dgvODNHistory.Columns["KeyID"].Visible = false;
            dgvODNHistory.Columns["생성일자"].Width = 150;
            dgvODNHistory.Columns["생성일자"].DefaultCellStyle.Format = "yyyy-MM-dd HH:mm:ss.fff";
            dgvODNHistory.Columns["사용자ID"].Width = 80;
            dgvODNHistory.Columns["진도면번호"].Width = 80;
            dgvODNHistory.Columns["Revision"].Width = 50;
            dgvODNHistory.Columns["Note"].Width = 300;
            dgvODNHistory.Columns["가도면번호"].Width = 100;
            dgvODNHistory.Columns["BeforeNote"].Visible = false;
            dgvODNHistory.Columns["ModelCode"].Visible = false;
            dgvODNHistory.Columns["PartCode"].Visible = false;
            dgvODNHistory.Columns["ProcessCode"].Visible = false;
            dgvODNHistory.Columns["SerialNumber"].Visible = false;
            dgvODNHistory.Columns["구도면번호"].Width = 100;
            
            if (note_write == false)
                dgvODNHistory.ReadOnly = true;
            else
            {
                //최초 생성시 한번만 쓰인다.
                if (columnAdded == true)
                {
                    dgvODNHistory.Columns[0].ReadOnly = true;
                    dgvODNHistory.Columns[1].ReadOnly = true;
                    dgvODNHistory.Columns[2].ReadOnly = true;
                    dgvODNHistory.Columns[3].ReadOnly = true;
                    dgvODNHistory.Columns[4].ReadOnly = true;
                    dgvODNHistory.Columns[5].ReadOnly = true;
                    dgvODNHistory.Columns[5].Width = 80;
                    dgvODNHistory.Columns[6].ReadOnly = false;
                    dgvODNHistory.Columns[6].Width = 400;
                    dgvODNHistory.Columns[7].ReadOnly = true;
                    dgvODNHistory.Columns[8].ReadOnly = true;
                    dgvODNHistory.Columns[9].ReadOnly = true;
                    dgvODNHistory.Columns[10].ReadOnly = true;
                    dgvODNHistory.Columns[11].ReadOnly = true;
                    dgvODNHistory.Columns[12].ReadOnly = true;
                    dgvODNHistory.Columns[12].Width = 80;
                    return;
                }

                DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn();
                checkBoxColumn.Name = "Modify_Note";
                checkBoxColumn.HeaderText = "Note변경";
                checkBoxColumn.TrueValue = true;
                checkBoxColumn.FalseValue = false;
                dgvODNHistory.AutoGenerateColumns = false;
                dgvODNHistory.Columns.Insert(5, checkBoxColumn);
                dgvODNHistory.Columns[5].Width = 80;
                columnAdded = true;

                dgvODNHistory.Columns[0].ReadOnly = true;
                dgvODNHistory.Columns[1].ReadOnly = true;
                dgvODNHistory.Columns[2].ReadOnly = true;
                dgvODNHistory.Columns[3].ReadOnly = true;
                dgvODNHistory.Columns[4].ReadOnly = true;
                dgvODNHistory.Columns[5].ReadOnly = true;
                dgvODNHistory.Columns[6].ReadOnly = false;
                dgvODNHistory.Columns[7].ReadOnly = true;
                dgvODNHistory.Columns[8].ReadOnly = true;
                dgvODNHistory.Columns[9].ReadOnly = true;
                dgvODNHistory.Columns[10].ReadOnly = true;
                dgvODNHistory.Columns[11].ReadOnly = true;
                dgvODNHistory.Columns[12].ReadOnly = true;
                dgvODNHistory.Columns[13].ReadOnly = true;
            }
        }


        private void BindUserID()
        {
            System.Data.DataTable result = new System.Data.DataTable();
            SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC user_infomation 0"), StaticData.db_connetion_string);
            query.Fill(result);

            if (result != null)
            {
                result.DefaultView.Sort = "NAME ASC";
                result.Rows.Add("ALL", "ALL", "ALL", "ALL", "ALL");
                cmbUserID.ValueMember = "ID";
                cmbUserID.DisplayMember = "NAME";
                cmbUserID.DataSource = result;
                cmbUserID.SelectedValue = StaticData.user_id;
            }
        }

        private void BindCmbBox(short sType)
        {
            System.Data.DataTable result = new System.Data.DataTable();
            SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC code_info {0}", sType), StaticData.db_connetion_string);
            query.Fill(result);
            result.Rows.Add("", "");

            if (result != null)
            {
                if (sType == 1)
                {
                    cmbModel.ValueMember = "CODE_NAME";
                    cmbModel.DisplayMember = "CODE_VALUE";
                    cmbModel.DataSource = result;
                    cmbModel.SelectedIndex = cmbModel.Items.Count - 1;
                }
                else if (sType == 2)
                {
                    cmbPart.ValueMember = "CODE_NAME";
                    cmbPart.DisplayMember = "CODE_VALUE";
                    cmbPart.DataSource = result;
                    cmbPart.SelectedIndex = cmbPart.Items.Count - 1;
                }
                else if (sType == 3)
                {
                    cmbProcess.ValueMember = "CODE_NAME";
                    cmbProcess.DisplayMember = "CODE_VALUE";
                    cmbProcess.DataSource = result;
                    cmbProcess.SelectedIndex = cmbProcess.Items.Count - 1;
                }
            }
        }

        private void ExcelButtonEnable()
        {
            if (dgvODNHistory.Rows.Count > 0)
                btnExcel.Enabled = true;
            else
                btnExcel.Enabled = false;
        }

        private void SaveNoteButtonEnable()
        {
            if (checkedCount > 0)
                btnSaveNote.Enabled = true;
            else
                btnSaveNote.Enabled = false;
        }


        private string DgvCheckNull(object value)
        {
            string strResult = "";
            if (value != null)
            {
                strResult = value.ToString();
            }

            return strResult;
        }

        private void tsmiSendODN_Click(object sender, EventArgs e)
        {
            int index = 0;

            try
            {
                if (dgvODNHistory.SelectedRows.Count > 0)
                {
                    index = dgvODNHistory.SelectedRows[0].Index;
                    string strODN = dgvODNHistory.Rows[index].Cells["진도면번호"].Value.ToString();

                    this.FormSendEvent(strODN);
                    this.Dispose();

                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                MessageBox.Show("New Order Type에서만 지정이 가능합니다.");
            }
        }

    }
}
