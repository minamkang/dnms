﻿namespace DrawingNumberManager
{
    partial class FormPublishRODN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblInputDrawingNumber = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.lblNote = new System.Windows.Forms.Label();
            this.btnRevise = new System.Windows.Forms.Button();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.ReviseDrawingNumberDisplayPanel = new System.Windows.Forms.Panel();
            this.txtRevisionCode = new System.Windows.Forms.TextBox();
            this.lblReviseOfficialDrawNumber = new System.Windows.Forms.Label();
            this.btnMain = new System.Windows.Forms.Button();
            this.txtReviseOfficialDrawNumber = new System.Windows.Forms.TextBox();
            this.gbODNDetail = new System.Windows.Forms.GroupBox();
            this.btnMaker = new System.Windows.Forms.Button();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.lblPrice = new System.Windows.Forms.Label();
            this.txtUnitPrice = new System.Windows.Forms.TextBox();
            this.lblUnitPrice = new System.Windows.Forms.Label();
            this.txtQty = new System.Windows.Forms.TextBox();
            this.lblQty = new System.Windows.Forms.Label();
            this.txtMaker = new System.Windows.Forms.TextBox();
            this.lblCustomer = new System.Windows.Forms.Label();
            this.txtAfterProcess = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMaterial = new System.Windows.Forms.TextBox();
            this.lblMaterial = new System.Windows.Forms.Label();
            this.txtSpecfication = new System.Windows.Forms.TextBox();
            this.lblSpecfication = new System.Windows.Forms.Label();
            this.txtPartName = new System.Windows.Forms.TextBox();
            this.lblPartName = new System.Windows.Forms.Label();
            this.txtType = new System.Windows.Forms.TextBox();
            this.lblType = new System.Windows.Forms.Label();
            this.ReviseDrawingNumberDisplayPanel.SuspendLayout();
            this.gbODNDetail.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblInputDrawingNumber
            // 
            this.lblInputDrawingNumber.AutoSize = true;
            this.lblInputDrawingNumber.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblInputDrawingNumber.ForeColor = System.Drawing.Color.Navy;
            this.lblInputDrawingNumber.Location = new System.Drawing.Point(11, 7);
            this.lblInputDrawingNumber.Name = "lblInputDrawingNumber";
            this.lblInputDrawingNumber.Size = new System.Drawing.Size(12, 12);
            this.lblInputDrawingNumber.TabIndex = 30;
            this.lblInputDrawingNumber.Text = "?";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(290, 30);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(47, 22);
            this.btnClear.TabIndex = 25;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // lblNote
            // 
            this.lblNote.AutoSize = true;
            this.lblNote.Location = new System.Drawing.Point(11, 35);
            this.lblNote.Name = "lblNote";
            this.lblNote.Size = new System.Drawing.Size(31, 12);
            this.lblNote.TabIndex = 26;
            this.lblNote.Text = "Note";
            // 
            // btnRevise
            // 
            this.btnRevise.Location = new System.Drawing.Point(281, 382);
            this.btnRevise.Name = "btnRevise";
            this.btnRevise.Size = new System.Drawing.Size(56, 34);
            this.btnRevise.TabIndex = 12;
            this.btnRevise.Text = "Revise";
            this.btnRevise.UseVisualStyleBackColor = true;
            this.btnRevise.Click += new System.EventHandler(this.btnRevise_Click);
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(12, 52);
            this.txtNote.Multiline = true;
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(325, 51);
            this.txtNote.TabIndex = 1;
            // 
            // ReviseDrawingNumberDisplayPanel
            // 
            this.ReviseDrawingNumberDisplayPanel.BackColor = System.Drawing.Color.LightBlue;
            this.ReviseDrawingNumberDisplayPanel.Controls.Add(this.txtRevisionCode);
            this.ReviseDrawingNumberDisplayPanel.Controls.Add(this.lblReviseOfficialDrawNumber);
            this.ReviseDrawingNumberDisplayPanel.Controls.Add(this.btnMain);
            this.ReviseDrawingNumberDisplayPanel.Controls.Add(this.txtReviseOfficialDrawNumber);
            this.ReviseDrawingNumberDisplayPanel.Location = new System.Drawing.Point(6, 422);
            this.ReviseDrawingNumberDisplayPanel.Name = "ReviseDrawingNumberDisplayPanel";
            this.ReviseDrawingNumberDisplayPanel.Size = new System.Drawing.Size(331, 133);
            this.ReviseDrawingNumberDisplayPanel.TabIndex = 31;
            this.ReviseDrawingNumberDisplayPanel.Visible = false;
            // 
            // txtRevisionCode
            // 
            this.txtRevisionCode.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtRevisionCode.Location = new System.Drawing.Point(250, 49);
            this.txtRevisionCode.Name = "txtRevisionCode";
            this.txtRevisionCode.ReadOnly = true;
            this.txtRevisionCode.Size = new System.Drawing.Size(75, 32);
            this.txtRevisionCode.TabIndex = 15;
            // 
            // lblReviseOfficialDrawNumber
            // 
            this.lblReviseOfficialDrawNumber.AutoSize = true;
            this.lblReviseOfficialDrawNumber.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblReviseOfficialDrawNumber.Location = new System.Drawing.Point(7, 14);
            this.lblReviseOfficialDrawNumber.Name = "lblReviseOfficialDrawNumber";
            this.lblReviseOfficialDrawNumber.Size = new System.Drawing.Size(261, 16);
            this.lblReviseOfficialDrawNumber.TabIndex = 14;
            this.lblReviseOfficialDrawNumber.Text = "Revise Official Drawing Number";
            // 
            // btnMain
            // 
            this.btnMain.Location = new System.Drawing.Point(250, 87);
            this.btnMain.Name = "btnMain";
            this.btnMain.Size = new System.Drawing.Size(75, 38);
            this.btnMain.TabIndex = 13;
            this.btnMain.Text = "Close";
            this.btnMain.UseVisualStyleBackColor = true;
            this.btnMain.Click += new System.EventHandler(this.btnMain_Click);
            // 
            // txtReviseOfficialDrawNumber
            // 
            this.txtReviseOfficialDrawNumber.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtReviseOfficialDrawNumber.Location = new System.Drawing.Point(7, 49);
            this.txtReviseOfficialDrawNumber.Name = "txtReviseOfficialDrawNumber";
            this.txtReviseOfficialDrawNumber.ReadOnly = true;
            this.txtReviseOfficialDrawNumber.Size = new System.Drawing.Size(242, 32);
            this.txtReviseOfficialDrawNumber.TabIndex = 0;
            this.txtReviseOfficialDrawNumber.TextChanged += new System.EventHandler(this.txtReviseOfficialDrawNumber_TextChanged);
            // 
            // gbODNDetail
            // 
            this.gbODNDetail.Controls.Add(this.btnMaker);
            this.gbODNDetail.Controls.Add(this.txtPrice);
            this.gbODNDetail.Controls.Add(this.lblPrice);
            this.gbODNDetail.Controls.Add(this.txtUnitPrice);
            this.gbODNDetail.Controls.Add(this.lblUnitPrice);
            this.gbODNDetail.Controls.Add(this.txtQty);
            this.gbODNDetail.Controls.Add(this.lblQty);
            this.gbODNDetail.Controls.Add(this.txtMaker);
            this.gbODNDetail.Controls.Add(this.lblCustomer);
            this.gbODNDetail.Controls.Add(this.txtAfterProcess);
            this.gbODNDetail.Controls.Add(this.label2);
            this.gbODNDetail.Controls.Add(this.txtMaterial);
            this.gbODNDetail.Controls.Add(this.lblMaterial);
            this.gbODNDetail.Controls.Add(this.txtSpecfication);
            this.gbODNDetail.Controls.Add(this.lblSpecfication);
            this.gbODNDetail.Controls.Add(this.txtPartName);
            this.gbODNDetail.Controls.Add(this.lblPartName);
            this.gbODNDetail.Controls.Add(this.txtType);
            this.gbODNDetail.Controls.Add(this.lblType);
            this.gbODNDetail.Location = new System.Drawing.Point(6, 127);
            this.gbODNDetail.Name = "gbODNDetail";
            this.gbODNDetail.Size = new System.Drawing.Size(331, 251);
            this.gbODNDetail.TabIndex = 32;
            this.gbODNDetail.TabStop = false;
            this.gbODNDetail.Text = "Detail Content";
            // 
            // btnMaker
            // 
            this.btnMaker.Location = new System.Drawing.Point(278, 159);
            this.btnMaker.Name = "btnMaker";
            this.btnMaker.Size = new System.Drawing.Size(47, 22);
            this.btnMaker.TabIndex = 8;
            this.btnMaker.Text = "...";
            this.btnMaker.UseVisualStyleBackColor = true;
            this.btnMaker.Click += new System.EventHandler(this.btnMaker_Click);
            // 
            // txtPrice
            // 
            this.txtPrice.Location = new System.Drawing.Point(91, 215);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(91, 21);
            this.txtPrice.TabIndex = 11;
            this.txtPrice.Text = "0";
            this.txtPrice.TextChanged += new System.EventHandler(this.txtPrice_TextChanged);
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Location = new System.Drawing.Point(11, 218);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(34, 12);
            this.lblPrice.TabIndex = 16;
            this.lblPrice.Text = "Price";
            // 
            // txtUnitPrice
            // 
            this.txtUnitPrice.Location = new System.Drawing.Point(240, 187);
            this.txtUnitPrice.Name = "txtUnitPrice";
            this.txtUnitPrice.Size = new System.Drawing.Size(85, 21);
            this.txtUnitPrice.TabIndex = 10;
            this.txtUnitPrice.Text = "0";
            this.txtUnitPrice.TextChanged += new System.EventHandler(this.txtUnitPrice_TextChanged);
            // 
            // lblUnitPrice
            // 
            this.lblUnitPrice.AutoSize = true;
            this.lblUnitPrice.Location = new System.Drawing.Point(188, 190);
            this.lblUnitPrice.Name = "lblUnitPrice";
            this.lblUnitPrice.Size = new System.Drawing.Size(46, 12);
            this.lblUnitPrice.TabIndex = 14;
            this.lblUnitPrice.Text = "U Price";
            // 
            // txtQty
            // 
            this.txtQty.Location = new System.Drawing.Point(90, 188);
            this.txtQty.Name = "txtQty";
            this.txtQty.Size = new System.Drawing.Size(92, 21);
            this.txtQty.TabIndex = 9;
            this.txtQty.Text = "0";
            this.txtQty.TextChanged += new System.EventHandler(this.txtQty_TextChanged);
            // 
            // lblQty
            // 
            this.lblQty.AutoSize = true;
            this.lblQty.Location = new System.Drawing.Point(10, 191);
            this.lblQty.Name = "lblQty";
            this.lblQty.Size = new System.Drawing.Size(24, 12);
            this.lblQty.TabIndex = 12;
            this.lblQty.Text = "Qty";
            // 
            // txtMaker
            // 
            this.txtMaker.Enabled = false;
            this.txtMaker.Location = new System.Drawing.Point(90, 161);
            this.txtMaker.Name = "txtMaker";
            this.txtMaker.Size = new System.Drawing.Size(182, 21);
            this.txtMaker.TabIndex = 7;
            // 
            // lblCustomer
            // 
            this.lblCustomer.AutoSize = true;
            this.lblCustomer.Location = new System.Drawing.Point(10, 164);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(40, 12);
            this.lblCustomer.TabIndex = 10;
            this.lblCustomer.Text = "Maker";
            // 
            // txtAfterProcess
            // 
            this.txtAfterProcess.Location = new System.Drawing.Point(90, 134);
            this.txtAfterProcess.Name = "txtAfterProcess";
            this.txtAfterProcess.Size = new System.Drawing.Size(235, 21);
            this.txtAfterProcess.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 137);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 12);
            this.label2.TabIndex = 8;
            this.label2.Text = "After Process";
            // 
            // txtMaterial
            // 
            this.txtMaterial.Location = new System.Drawing.Point(90, 107);
            this.txtMaterial.Name = "txtMaterial";
            this.txtMaterial.Size = new System.Drawing.Size(235, 21);
            this.txtMaterial.TabIndex = 5;
            // 
            // lblMaterial
            // 
            this.lblMaterial.AutoSize = true;
            this.lblMaterial.Location = new System.Drawing.Point(10, 110);
            this.lblMaterial.Name = "lblMaterial";
            this.lblMaterial.Size = new System.Drawing.Size(50, 12);
            this.lblMaterial.TabIndex = 6;
            this.lblMaterial.Text = "Material";
            // 
            // txtSpecfication
            // 
            this.txtSpecfication.Location = new System.Drawing.Point(90, 79);
            this.txtSpecfication.Name = "txtSpecfication";
            this.txtSpecfication.Size = new System.Drawing.Size(235, 21);
            this.txtSpecfication.TabIndex = 4;
            // 
            // lblSpecfication
            // 
            this.lblSpecfication.AutoSize = true;
            this.lblSpecfication.Location = new System.Drawing.Point(10, 82);
            this.lblSpecfication.Name = "lblSpecfication";
            this.lblSpecfication.Size = new System.Drawing.Size(74, 12);
            this.lblSpecfication.TabIndex = 4;
            this.lblSpecfication.Text = "Specfication";
            // 
            // txtPartName
            // 
            this.txtPartName.Location = new System.Drawing.Point(90, 25);
            this.txtPartName.Name = "txtPartName";
            this.txtPartName.Size = new System.Drawing.Size(235, 21);
            this.txtPartName.TabIndex = 2;
            // 
            // lblPartName
            // 
            this.lblPartName.AutoSize = true;
            this.lblPartName.Location = new System.Drawing.Point(10, 28);
            this.lblPartName.Name = "lblPartName";
            this.lblPartName.Size = new System.Drawing.Size(61, 12);
            this.lblPartName.TabIndex = 2;
            this.lblPartName.Text = "PartName";
            // 
            // txtType
            // 
            this.txtType.Location = new System.Drawing.Point(90, 52);
            this.txtType.Name = "txtType";
            this.txtType.Size = new System.Drawing.Size(235, 21);
            this.txtType.TabIndex = 3;
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(10, 55);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(34, 12);
            this.lblType.TabIndex = 0;
            this.lblType.Text = "Type";
            // 
            // FormPublishRODN
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.ClientSize = new System.Drawing.Size(342, 420);
            this.Controls.Add(this.ReviseDrawingNumberDisplayPanel);
            this.Controls.Add(this.lblInputDrawingNumber);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.lblNote);
            this.Controls.Add(this.btnRevise);
            this.Controls.Add(this.txtNote);
            this.Controls.Add(this.gbODNDetail);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FormPublishRODN";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Revise Official Drawing Number";
            this.Load += new System.EventHandler(this.FormPublishRODN_Load);
            this.ReviseDrawingNumberDisplayPanel.ResumeLayout(false);
            this.ReviseDrawingNumberDisplayPanel.PerformLayout();
            this.gbODNDetail.ResumeLayout(false);
            this.gbODNDetail.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblInputDrawingNumber;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label lblNote;
        private System.Windows.Forms.Button btnRevise;
        private System.Windows.Forms.TextBox txtNote;
        private System.Windows.Forms.Panel ReviseDrawingNumberDisplayPanel;
        private System.Windows.Forms.Label lblReviseOfficialDrawNumber;
        private System.Windows.Forms.Button btnMain;
        private System.Windows.Forms.TextBox txtReviseOfficialDrawNumber;
        private System.Windows.Forms.TextBox txtRevisionCode;
        private System.Windows.Forms.GroupBox gbODNDetail;
        private System.Windows.Forms.Button btnMaker;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.TextBox txtUnitPrice;
        private System.Windows.Forms.Label lblUnitPrice;
        private System.Windows.Forms.TextBox txtQty;
        private System.Windows.Forms.Label lblQty;
        private System.Windows.Forms.TextBox txtMaker;
        private System.Windows.Forms.Label lblCustomer;
        private System.Windows.Forms.TextBox txtAfterProcess;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtMaterial;
        private System.Windows.Forms.Label lblMaterial;
        private System.Windows.Forms.TextBox txtSpecfication;
        private System.Windows.Forms.Label lblSpecfication;
        private System.Windows.Forms.TextBox txtPartName;
        private System.Windows.Forms.Label lblPartName;
        private System.Windows.Forms.TextBox txtType;
        private System.Windows.Forms.Label lblType;
    }
}