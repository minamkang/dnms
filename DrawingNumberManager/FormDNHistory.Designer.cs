﻿namespace DrawingNumberManager
{
    partial class FormDNHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlSearch = new System.Windows.Forms.Panel();
            this.txtTDN = new System.Windows.Forms.TextBox();
            this.lblTDN = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.cmbUserID = new System.Windows.Forms.ComboBox();
            this.btnSaveNote = new System.Windows.Forms.Button();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.lblNote = new System.Windows.Forms.Label();
            this.btnExcel = new System.Windows.Forms.Button();
            this.lblDrawingCategory = new System.Windows.Forms.Label();
            this.cmbDrawingCategory = new System.Windows.Forms.ComboBox();
            this.btn1Year = new System.Windows.Forms.Button();
            this.btn9Month = new System.Windows.Forms.Button();
            this.btn6Month = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btn3Month = new System.Windows.Forms.Button();
            this.btn1Month = new System.Windows.Forms.Button();
            this.btn15Day = new System.Windows.Forms.Button();
            this.btn1Week = new System.Windows.Forms.Button();
            this.btnNowDay = new System.Windows.Forms.Button();
            this.lblFrom = new System.Windows.Forms.Label();
            this.lblEndDate = new System.Windows.Forms.Label();
            this.dtpickerEndDate = new System.Windows.Forms.DateTimePicker();
            this.lblStartDate = new System.Windows.Forms.Label();
            this.dtpickerStartDate = new System.Windows.Forms.DateTimePicker();
            this.pnlDataGrideView = new System.Windows.Forms.Panel();
            this.dgvDNHistory = new System.Windows.Forms.DataGridView();
            this.cmsOfficialDrawingNumber = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiCreateODN = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiReviseODN = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlSearch.SuspendLayout();
            this.pnlDataGrideView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDNHistory)).BeginInit();
            this.cmsOfficialDrawingNumber.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSearch
            // 
            this.pnlSearch.BackColor = System.Drawing.Color.Honeydew;
            this.pnlSearch.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlSearch.Controls.Add(this.txtTDN);
            this.pnlSearch.Controls.Add(this.lblTDN);
            this.pnlSearch.Controls.Add(this.lblName);
            this.pnlSearch.Controls.Add(this.cmbUserID);
            this.pnlSearch.Controls.Add(this.btnSaveNote);
            this.pnlSearch.Controls.Add(this.txtNote);
            this.pnlSearch.Controls.Add(this.lblNote);
            this.pnlSearch.Controls.Add(this.btnExcel);
            this.pnlSearch.Controls.Add(this.lblDrawingCategory);
            this.pnlSearch.Controls.Add(this.cmbDrawingCategory);
            this.pnlSearch.Controls.Add(this.btn1Year);
            this.pnlSearch.Controls.Add(this.btn9Month);
            this.pnlSearch.Controls.Add(this.btn6Month);
            this.pnlSearch.Controls.Add(this.btnSearch);
            this.pnlSearch.Controls.Add(this.btn3Month);
            this.pnlSearch.Controls.Add(this.btn1Month);
            this.pnlSearch.Controls.Add(this.btn15Day);
            this.pnlSearch.Controls.Add(this.btn1Week);
            this.pnlSearch.Controls.Add(this.btnNowDay);
            this.pnlSearch.Controls.Add(this.lblFrom);
            this.pnlSearch.Controls.Add(this.lblEndDate);
            this.pnlSearch.Controls.Add(this.dtpickerEndDate);
            this.pnlSearch.Controls.Add(this.lblStartDate);
            this.pnlSearch.Controls.Add(this.dtpickerStartDate);
            this.pnlSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSearch.Location = new System.Drawing.Point(0, 0);
            this.pnlSearch.Name = "pnlSearch";
            this.pnlSearch.Size = new System.Drawing.Size(988, 134);
            this.pnlSearch.TabIndex = 7;
            // 
            // txtTDN
            // 
            this.txtTDN.Location = new System.Drawing.Point(56, 100);
            this.txtTDN.MaxLength = 8;
            this.txtTDN.Name = "txtTDN";
            this.txtTDN.Size = new System.Drawing.Size(112, 21);
            this.txtTDN.TabIndex = 31;
            this.txtTDN.TextChanged += new System.EventHandler(this.txtTDN_TextChanged);
            // 
            // lblTDN
            // 
            this.lblTDN.AutoSize = true;
            this.lblTDN.Location = new System.Drawing.Point(5, 104);
            this.lblTDN.Name = "lblTDN";
            this.lblTDN.Size = new System.Drawing.Size(41, 12);
            this.lblTDN.TabIndex = 30;
            this.lblTDN.Text = "가도번";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(412, 56);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(39, 12);
            this.lblName.TabIndex = 29;
            this.lblName.Text = "Name";
            // 
            // cmbUserID
            // 
            this.cmbUserID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUserID.FormattingEnabled = true;
            this.cmbUserID.Location = new System.Drawing.Point(414, 71);
            this.cmbUserID.Name = "cmbUserID";
            this.cmbUserID.Size = new System.Drawing.Size(121, 20);
            this.cmbUserID.TabIndex = 28;
            this.cmbUserID.SelectedValueChanged += new System.EventHandler(this.cmbUserID_SelectedValueChanged);
            // 
            // btnSaveNote
            // 
            this.btnSaveNote.Location = new System.Drawing.Point(812, 27);
            this.btnSaveNote.Name = "btnSaveNote";
            this.btnSaveNote.Size = new System.Drawing.Size(80, 49);
            this.btnSaveNote.TabIndex = 26;
            this.btnSaveNote.Text = "Save Note";
            this.btnSaveNote.UseVisualStyleBackColor = true;
            this.btnSaveNote.Click += new System.EventHandler(this.btnSaveNote_Click);
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(549, 27);
            this.txtNote.Multiline = true;
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(156, 64);
            this.txtNote.TabIndex = 25;
            // 
            // lblNote
            // 
            this.lblNote.AutoSize = true;
            this.lblNote.Location = new System.Drawing.Point(547, 12);
            this.lblNote.Name = "lblNote";
            this.lblNote.Size = new System.Drawing.Size(31, 12);
            this.lblNote.TabIndex = 24;
            this.lblNote.Text = "Note";
            // 
            // btnExcel
            // 
            this.btnExcel.Location = new System.Drawing.Point(898, 27);
            this.btnExcel.Name = "btnExcel";
            this.btnExcel.Size = new System.Drawing.Size(80, 49);
            this.btnExcel.TabIndex = 21;
            this.btnExcel.Text = "Excel";
            this.btnExcel.UseVisualStyleBackColor = true;
            this.btnExcel.Click += new System.EventHandler(this.btnExcel_Click);
            // 
            // lblDrawingCategory
            // 
            this.lblDrawingCategory.AutoSize = true;
            this.lblDrawingCategory.Location = new System.Drawing.Point(412, 12);
            this.lblDrawingCategory.Name = "lblDrawingCategory";
            this.lblDrawingCategory.Size = new System.Drawing.Size(106, 12);
            this.lblDrawingCategory.TabIndex = 20;
            this.lblDrawingCategory.Text = "Drawing Category";
            // 
            // cmbDrawingCategory
            // 
            this.cmbDrawingCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDrawingCategory.FormattingEnabled = true;
            this.cmbDrawingCategory.Location = new System.Drawing.Point(414, 27);
            this.cmbDrawingCategory.Name = "cmbDrawingCategory";
            this.cmbDrawingCategory.Size = new System.Drawing.Size(121, 20);
            this.cmbDrawingCategory.TabIndex = 3;
            this.cmbDrawingCategory.SelectedValueChanged += new System.EventHandler(this.cmbDrawingCategory_SelectedValueChanged);
            // 
            // btn1Year
            // 
            this.btn1Year.BackColor = System.Drawing.Color.LightBlue;
            this.btn1Year.Location = new System.Drawing.Point(355, 67);
            this.btn1Year.Name = "btn1Year";
            this.btn1Year.Size = new System.Drawing.Size(44, 27);
            this.btn1Year.TabIndex = 13;
            this.btn1Year.Text = "1년";
            this.btn1Year.UseVisualStyleBackColor = false;
            this.btn1Year.Click += new System.EventHandler(this.btn1Year_Click);
            // 
            // btn9Month
            // 
            this.btn9Month.BackColor = System.Drawing.Color.LightBlue;
            this.btn9Month.Location = new System.Drawing.Point(305, 67);
            this.btn9Month.Name = "btn9Month";
            this.btn9Month.Size = new System.Drawing.Size(44, 27);
            this.btn9Month.TabIndex = 12;
            this.btn9Month.Text = "9개월";
            this.btn9Month.UseVisualStyleBackColor = false;
            this.btn9Month.Click += new System.EventHandler(this.btn9Month_Click);
            // 
            // btn6Month
            // 
            this.btn6Month.BackColor = System.Drawing.Color.LightBlue;
            this.btn6Month.Location = new System.Drawing.Point(255, 67);
            this.btn6Month.Name = "btn6Month";
            this.btn6Month.Size = new System.Drawing.Size(44, 27);
            this.btn6Month.TabIndex = 11;
            this.btn6Month.Text = "6개월";
            this.btn6Month.UseVisualStyleBackColor = false;
            this.btn6Month.Click += new System.EventHandler(this.btn6Month_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(726, 27);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(80, 49);
            this.btnSearch.TabIndex = 4;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btn3Month
            // 
            this.btn3Month.BackColor = System.Drawing.Color.LightBlue;
            this.btn3Month.Location = new System.Drawing.Point(205, 67);
            this.btn3Month.Name = "btn3Month";
            this.btn3Month.Size = new System.Drawing.Size(44, 27);
            this.btn3Month.TabIndex = 10;
            this.btn3Month.Text = "3개월";
            this.btn3Month.UseVisualStyleBackColor = false;
            this.btn3Month.Click += new System.EventHandler(this.btn3Month_Click);
            // 
            // btn1Month
            // 
            this.btn1Month.BackColor = System.Drawing.Color.LightBlue;
            this.btn1Month.Location = new System.Drawing.Point(156, 67);
            this.btn1Month.Name = "btn1Month";
            this.btn1Month.Size = new System.Drawing.Size(44, 27);
            this.btn1Month.TabIndex = 9;
            this.btn1Month.Text = "1개월";
            this.btn1Month.UseVisualStyleBackColor = false;
            this.btn1Month.Click += new System.EventHandler(this.btn1Month_Click);
            // 
            // btn15Day
            // 
            this.btn15Day.BackColor = System.Drawing.Color.LightBlue;
            this.btn15Day.Location = new System.Drawing.Point(106, 67);
            this.btn15Day.Name = "btn15Day";
            this.btn15Day.Size = new System.Drawing.Size(44, 27);
            this.btn15Day.TabIndex = 8;
            this.btn15Day.Text = "15일";
            this.btn15Day.UseVisualStyleBackColor = false;
            this.btn15Day.Click += new System.EventHandler(this.btn15Day_Click);
            // 
            // btn1Week
            // 
            this.btn1Week.BackColor = System.Drawing.Color.LightBlue;
            this.btn1Week.Location = new System.Drawing.Point(56, 67);
            this.btn1Week.Name = "btn1Week";
            this.btn1Week.Size = new System.Drawing.Size(44, 27);
            this.btn1Week.TabIndex = 7;
            this.btn1Week.Text = "1주일";
            this.btn1Week.UseVisualStyleBackColor = false;
            this.btn1Week.Click += new System.EventHandler(this.btn1Week_Click);
            // 
            // btnNowDay
            // 
            this.btnNowDay.BackColor = System.Drawing.Color.LightBlue;
            this.btnNowDay.Location = new System.Drawing.Point(6, 67);
            this.btnNowDay.Name = "btnNowDay";
            this.btnNowDay.Size = new System.Drawing.Size(44, 27);
            this.btnNowDay.TabIndex = 6;
            this.btnNowDay.Text = "당일";
            this.btnNowDay.UseVisualStyleBackColor = false;
            this.btnNowDay.Click += new System.EventHandler(this.btnNowDay_Click);
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(198, 31);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(14, 12);
            this.lblFrom.TabIndex = 10;
            this.lblFrom.Text = "~";
            // 
            // lblEndDate
            // 
            this.lblEndDate.AutoSize = true;
            this.lblEndDate.Location = new System.Drawing.Point(234, 12);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(56, 12);
            this.lblEndDate.TabIndex = 9;
            this.lblEndDate.Text = "End Date";
            // 
            // dtpickerEndDate
            // 
            this.dtpickerEndDate.CustomFormat = "";
            this.dtpickerEndDate.Location = new System.Drawing.Point(236, 27);
            this.dtpickerEndDate.MaxDate = new System.DateTime(9998, 12, 1, 0, 0, 0, 0);
            this.dtpickerEndDate.Name = "dtpickerEndDate";
            this.dtpickerEndDate.Size = new System.Drawing.Size(163, 21);
            this.dtpickerEndDate.TabIndex = 2;
            this.dtpickerEndDate.Value = new System.DateTime(2013, 11, 19, 0, 0, 0, 0);
            this.dtpickerEndDate.ValueChanged += new System.EventHandler(this.dtpickerEndDate_ValueChanged);
            // 
            // lblStartDate
            // 
            this.lblStartDate.AutoSize = true;
            this.lblStartDate.Location = new System.Drawing.Point(5, 12);
            this.lblStartDate.Name = "lblStartDate";
            this.lblStartDate.Size = new System.Drawing.Size(59, 12);
            this.lblStartDate.TabIndex = 7;
            this.lblStartDate.Text = "Start Date";
            // 
            // dtpickerStartDate
            // 
            this.dtpickerStartDate.CustomFormat = "";
            this.dtpickerStartDate.Location = new System.Drawing.Point(7, 27);
            this.dtpickerStartDate.MaxDate = new System.DateTime(9998, 12, 1, 0, 0, 0, 0);
            this.dtpickerStartDate.Name = "dtpickerStartDate";
            this.dtpickerStartDate.Size = new System.Drawing.Size(163, 21);
            this.dtpickerStartDate.TabIndex = 1;
            this.dtpickerStartDate.Value = new System.DateTime(2013, 11, 11, 0, 0, 0, 0);
            this.dtpickerStartDate.ValueChanged += new System.EventHandler(this.dtpickerStartDate_ValueChanged);
            // 
            // pnlDataGrideView
            // 
            this.pnlDataGrideView.Controls.Add(this.dgvDNHistory);
            this.pnlDataGrideView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDataGrideView.Location = new System.Drawing.Point(0, 134);
            this.pnlDataGrideView.Name = "pnlDataGrideView";
            this.pnlDataGrideView.Size = new System.Drawing.Size(988, 488);
            this.pnlDataGrideView.TabIndex = 8;
            this.pnlDataGrideView.TabStop = true;
            // 
            // dgvDNHistory
            // 
            this.dgvDNHistory.AllowUserToAddRows = false;
            this.dgvDNHistory.AllowUserToDeleteRows = false;
            this.dgvDNHistory.AllowUserToResizeRows = false;
            this.dgvDNHistory.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDNHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvDNHistory.ContextMenuStrip = this.cmsOfficialDrawingNumber;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("굴림체", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDNHistory.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgvDNHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDNHistory.Location = new System.Drawing.Point(0, 0);
            this.dgvDNHistory.MultiSelect = false;
            this.dgvDNHistory.Name = "dgvDNHistory";
            this.dgvDNHistory.RowHeadersVisible = false;
            this.dgvDNHistory.RowTemplate.Height = 23;
            this.dgvDNHistory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDNHistory.Size = new System.Drawing.Size(988, 488);
            this.dgvDNHistory.TabIndex = 0;
            this.dgvDNHistory.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDNHistory_CellContentClick);
            this.dgvDNHistory.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDNHistory_CellEndEdit);
            this.dgvDNHistory.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgvDNHistory_MouseDown);
            // 
            // cmsOfficialDrawingNumber
            // 
            this.cmsOfficialDrawingNumber.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiCreateODN,
            this.tsmiReviseODN});
            this.cmsOfficialDrawingNumber.Name = "cmsRealDrawingNumber";
            this.cmsOfficialDrawingNumber.Size = new System.Drawing.Size(247, 48);
            // 
            // tsmiCreateODN
            // 
            this.tsmiCreateODN.Name = "tsmiCreateODN";
            this.tsmiCreateODN.Size = new System.Drawing.Size(246, 22);
            this.tsmiCreateODN.Text = "Create Official DrawingNumber";
            this.tsmiCreateODN.Visible = false;
            this.tsmiCreateODN.Click += new System.EventHandler(this.tsmiCreateODN_Click);
            // 
            // tsmiReviseODN
            // 
            this.tsmiReviseODN.Name = "tsmiReviseODN";
            this.tsmiReviseODN.Size = new System.Drawing.Size(246, 22);
            this.tsmiReviseODN.Text = "Revise Official Drawing Number";
            this.tsmiReviseODN.Visible = false;
            this.tsmiReviseODN.Click += new System.EventHandler(this.tsmiReviseODN_Click);
            // 
            // FormDNHistory
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(988, 622);
            this.Controls.Add(this.pnlDataGrideView);
            this.Controls.Add(this.pnlSearch);
            this.Name = "FormDNHistory";
            this.Text = "Drawing Number History";
            this.Load += new System.EventHandler(this.FormDrawingNumberHistory_Load);
            this.Resize += new System.EventHandler(this.FormDNHistory_Resize);
            this.pnlSearch.ResumeLayout(false);
            this.pnlSearch.PerformLayout();
            this.pnlDataGrideView.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDNHistory)).EndInit();
            this.cmsOfficialDrawingNumber.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlSearch;
        private System.Windows.Forms.Button btnSearch;
		private System.Windows.Forms.Panel pnlDataGrideView;
        private System.Windows.Forms.DataGridView dgvDNHistory;
        private System.Windows.Forms.Label lblStartDate;
        private System.Windows.Forms.DateTimePicker dtpickerStartDate;
        private System.Windows.Forms.Button btn1Year;
        private System.Windows.Forms.Button btn9Month;
        private System.Windows.Forms.Button btn6Month;
        private System.Windows.Forms.Button btn3Month;
        private System.Windows.Forms.Button btn1Month;
        private System.Windows.Forms.Button btn15Day;
        private System.Windows.Forms.Button btn1Week;
        private System.Windows.Forms.Button btnNowDay;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.Label lblEndDate;
        private System.Windows.Forms.DateTimePicker dtpickerEndDate;
        private System.Windows.Forms.Label lblDrawingCategory;
		private System.Windows.Forms.ComboBox cmbDrawingCategory;
		private System.Windows.Forms.Button btnExcel;
		private System.Windows.Forms.Label lblNote;
		private System.Windows.Forms.TextBox txtNote;
		private System.Windows.Forms.Button btnSaveNote;
		private System.Windows.Forms.ComboBox cmbUserID;
		private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.ContextMenuStrip cmsOfficialDrawingNumber;
        private System.Windows.Forms.ToolStripMenuItem tsmiCreateODN;
        private System.Windows.Forms.ToolStripMenuItem tsmiReviseODN;
        private System.Windows.Forms.TextBox txtTDN;
        private System.Windows.Forms.Label lblTDN;

    }
}