﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace DrawingNumberManager
{
    public partial class FormMaker : Form
    {
        private bool is_Manement;
        
        private string maker_name;
        public string MakerName
        {
            get { return maker_name; }
            set { maker_name = value; }
        }

        private int maker_code;
        public int MakerCode
        {
            get { return maker_code; }
            set { maker_code = value; }
        }

        private string edit_mode;

        public FormMaker(bool isMangement)
        {
            is_Manement = isMangement;
            InitializeComponent();
        }

        private void FormManagementCompany_Load(object sender, EventArgs e)
        {
            BindMaker("");
            BindCmbPaymentType();
            InitializeDgvCompany();
            InitializeInputValue(Const.EditMode_None);
        }

        private void dgvCompany_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            this.txtAccountCode.Text = dgvCompany.Rows[e.RowIndex].Cells["CODE"].Value.ToString().Trim();
            this.txtCompanyName.Text = dgvCompany.Rows[e.RowIndex].Cells["업체명"].Value.ToString().Trim();
            this.txtAddress.Text = dgvCompany.Rows[e.RowIndex].Cells["주소"].Value.ToString().Trim();
            this.txtBusinessNumber.Text = dgvCompany.Rows[e.RowIndex].Cells["사업자번호"].Value.ToString().Trim();
            this.txtRepresentative.Text = dgvCompany.Rows[e.RowIndex].Cells["대표자"].Value.ToString().Trim();
            this.txtIndustryType.Text = dgvCompany.Rows[e.RowIndex].Cells["업태"].Value.ToString().Trim();
            this.txtBusinessType.Text = dgvCompany.Rows[e.RowIndex].Cells["종목"].Value.ToString().Trim();
            this.txtManager.Text = dgvCompany.Rows[e.RowIndex].Cells["담당자"].Value.ToString().Trim();
            this.txtTel.Text = dgvCompany.Rows[e.RowIndex].Cells["전화"].Value.ToString().Trim();
            this.txtManagerMail.Text = dgvCompany.Rows[e.RowIndex].Cells["E메일"].Value.ToString().Trim();
            this.txtFax.Text = dgvCompany.Rows[e.RowIndex].Cells["팩스"].Value.ToString().Trim();
            this.cmbPaymentType.SelectedValue = dgvCompany.Rows[e.RowIndex].Cells["지불방법"].Value.ToString().Trim();
            this.txtNote.Text = dgvCompany.Rows[e.RowIndex].Cells["NOTE"].Value.ToString().Trim();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            BindMaker(txtSMaker.Text);
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            InitializeInputValue(Const.EditMode_Insert);
            SetClear();
        }

        private void btnModify_Click(object sender, EventArgs e)
        {
            InitializeInputValue(Const.EditMode_Modify);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            InitializeInputValue(Const.EditMode_Delete);
        }

        private void btnComfirm_Click(object sender, EventArgs e)
        {
            if (is_Manement == true)
            {
                StringBuilder sbData = new StringBuilder();
                switch (edit_mode)
                {
                    case Const.EditMode_Insert:
                    case Const.EditMode_Modify:
                        sbData.Append("'" + txtAccountCode.Text + "',");
                        sbData.Append("'" + txtCompanyName.Text + "',");
                        sbData.Append("'" + txtAddress.Text + "',");
                        sbData.Append("'" + txtBusinessNumber.Text + "',");
                        sbData.Append("'" + txtRepresentative.Text + "',");
                        sbData.Append("'" + txtIndustryType.Text + "',");
                        sbData.Append("'" + txtBusinessType.Text + "',");
                        sbData.Append("'" + txtManager.Text + "',");
                        sbData.Append("'" + txtTel.Text + "',");
                        sbData.Append("'" + txtManagerMail.Text + "',");
                        sbData.Append("'" + txtFax.Text + "',");
                        sbData.Append("'" + cmbPaymentType.SelectedValue + "',");
                        sbData.Append("'" + txtNote.Text + "'");
                        if (edit_mode == Const.EditMode_Insert)
                            Edit_Make("I", sbData.ToString());
                        else
                            Edit_Make("U", sbData.ToString());
                        break;
                    case Const.EditMode_Delete:
                        sbData.Append("'" + txtAccountCode.Text + "',");
                        sbData.Append("'',");
                        sbData.Append("'',");
                        sbData.Append("'',");
                        sbData.Append("'',");
                        sbData.Append("'',");
                        sbData.Append("'',");
                        sbData.Append("'',");
                        sbData.Append("'',");
                        sbData.Append("'',");
                        sbData.Append("'',");
                        sbData.Append("'',");
                        sbData.Append("''");
                        Edit_Make("D", sbData.ToString());
                        break;
                }

                BindMaker("");
                InitializeInputValue(Const.EditMode_None);
            }
            else
            {
                maker_name = txtCompanyName.Text;
                maker_code = int.Parse(txtAccountCode.Text);
                DialogResult = DialogResult.OK;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (is_Manement == true)
            {
                BindMaker("");
                InitializeInputValue(Const.EditMode_None);
            }
            else
                this.Close();
        }

        private void InitializeInputValue(string EditMode)
        {
            switch (EditMode)
            {
                case Const.EditMode_None:
                case Const.EditMode_Delete:
                    if (EditMode == Const.EditMode_None)
                    {
                        txtSMaker.Enabled = true;
                        btnSearch.Enabled = true;
                        dgvCompany.Enabled = true;
                    }
                    else if (EditMode == Const.EditMode_Delete)
                    {
                        txtSMaker.Enabled = false;
                        btnSearch.Enabled = false;
                        dgvCompany.Enabled = false;
                    }

                    if (is_Manement == true && EditMode == Const.EditMode_None)
                    {
                        btnInsert.Enabled = true;
                        btnModify.Enabled = true;
                        btnDelete.Enabled = true;
                        btnComfirm.Enabled = false;
                        btnCancel.Enabled = false;
                    }
                    else if (is_Manement == false || EditMode == Const.EditMode_Delete)
                    {
                        btnInsert.Enabled = false;
                        btnModify.Enabled = false;
                        btnDelete.Enabled = false;
                        btnComfirm.Enabled = true;
                        btnCancel.Enabled = true;
                    }

                    txtAccountCode.Enabled = false;
                    txtCompanyName.Enabled = false;
                    txtAddress.Enabled = false;
                    txtBusinessNumber.Enabled = false;
                    txtRepresentative.Enabled = false;
                    txtIndustryType.Enabled = false;
                    txtBusinessType.Enabled = false;
                    txtManager.Enabled = false;
                    txtTel.Enabled = false;
                    txtManagerMail.Enabled = false;
                    txtFax.Enabled = false;
                    cmbPaymentType.Enabled = false;
                    txtNote.Enabled = false;
                    break;
                case Const.EditMode_Insert:
                case Const.EditMode_Modify:
                    txtSMaker.Enabled = false;
                    btnSearch.Enabled = false;
                    dgvCompany.Enabled = false;
                    btnInsert.Enabled = false;
                    btnModify.Enabled = false;
                    btnDelete.Enabled = false;
                    btnComfirm.Enabled = true;
                    btnCancel.Enabled = true;
                    txtAccountCode.Enabled = false;
                    txtCompanyName.Enabled = true;
                    txtAddress.Enabled = true;
                    txtBusinessNumber.Enabled = true;
                    txtRepresentative.Enabled = true;
                    txtIndustryType.Enabled = true;
                    txtBusinessType.Enabled = true;
                    txtManager.Enabled = true;
                    txtTel.Enabled = true;
                    txtManagerMail.Enabled = true;
                    txtFax.Enabled = true;
                    cmbPaymentType.Enabled = true;
                    txtNote.Enabled = true;
                    break;
            }

            edit_mode = EditMode;
        }

        private void BindMaker(string sMaker)
        {
            DataTable result = new DataTable();
            SqlDataAdapter query = null;
            try
            {
                if(sMaker == "")
                    query = new SqlDataAdapter(String.Format("EXEC management_maker 'N', '' "), StaticData.db_connetion_string);
                else
                    query = new SqlDataAdapter(String.Format("EXEC management_maker 'Y', '%{0}%' ", sMaker), StaticData.db_connetion_string);

                query.Fill(result);

                if (result != null)
                {
                    dgvCompany.DataSource = result;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        private void Edit_Make(string flag, string data)
        {
            DataTable dt = new DataTable();
            SqlDataAdapter query = null;

            try
            {
                query = new SqlDataAdapter(String.Format("EXEC maker_info_IUD '{0}', {1}", flag,  data), StaticData.db_connetion_string);
                query.Fill(dt);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }


        private void BindCmbPaymentType()
        {
            DataTable result = new DataTable();
            try
            {
                SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC code_info '4'"), StaticData.db_connetion_string);
                query.Fill(result);

                if (result != null)
                {
                    result.Rows.Add("");
                    cmbPaymentType.ValueMember = "CODE_NAME";
                    cmbPaymentType.DisplayMember = "CODE_VALUE";
                    cmbPaymentType.DataSource = result;
                    cmbPaymentType.SelectedValue = "";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        private void SetClear()
        {
            txtSMaker.Text = "";
            txtAccountCode.Text = "";
            txtCompanyName.Text = "";
            txtAddress.Text = "";
            txtBusinessNumber.Text = "";
            txtRepresentative.Text = "";
            txtIndustryType.Text = "";
            txtBusinessType.Text = "";
            txtManager.Text = "";
            txtTel.Text = "";
            txtManagerMail.Text = "";
            txtFax.Text = "";
            cmbPaymentType.SelectedValue = "";
            txtNote.Text = "";
        }

        private void InitializeDgvCompany()
        {
            this.dgvCompany.Columns["주소"].Visible = false;
            this.dgvCompany.Columns["사업자번호"].Visible = false;
            this.dgvCompany.Columns["대표자"].Visible = false;
            this.dgvCompany.Columns["업태"].Visible = false;
            this.dgvCompany.Columns["종목"].Visible = false;
            this.dgvCompany.Columns["담당자"].Visible = false;
            this.dgvCompany.Columns["전화"].Visible = false;
            this.dgvCompany.Columns["E메일"].Visible = false;
            this.dgvCompany.Columns["팩스"].Visible = false;
            this.dgvCompany.Columns["지불방법"].Visible = false;
            this.dgvCompany.Columns["NOTE"].Visible = false;

            this.dgvCompany.Columns["CODE"].Width = 90;
            this.dgvCompany.Columns["업체명"].Width = 300;
        }

        private void btnMakerImport_Click(object sender, EventArgs e)
        {
            DataTable result = new DataTable();
            SqlDataAdapter query = null;

            OpenFileDialog ofdlg = new OpenFileDialog();
            if (ofdlg.ShowDialog() == DialogResult.OK)
            {
                var sr = new StreamReader(ofdlg.FileName);

                string line = "";
                StringBuilder sb = null; 
                while((line = sr.ReadLine()) != null)
                {
                    try
                    {
                        sb = new StringBuilder();
                        string[] split_maker = line.Split(',');

                        string mail = "";
                        string note = "";
                        if (split_maker[5].ToString().Contains("@") == true) //비고
                            mail = split_maker[5].ToString().Trim();
                        else
                            note = split_maker[5].ToString().Trim();

                        sb.Append("INSERT INTO dbo.MAKER_MANAGEMENT ");
                        sb.Append("(NAME, FAX, MANAGER, TEL, MAIL, ADDRESS, NOTE)");
                        sb.Append(" VALUES ");
                        sb.AppendFormat("('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')",
                            split_maker[0].ToString().Trim(),   //업체명
                            split_maker[2].ToString().Trim(),   //FAX번호
                            split_maker[3].ToString().Trim(),   //담당자
                            split_maker[4].ToString().Trim(),   //핸드폰
                            mail,                               //메일
                            split_maker[8].ToString().Trim(),   //주소
                            note);

                        query = new SqlDataAdapter(sb.ToString(), StaticData.db_connetion_string);
                        query.Fill(result);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                }
            }

            BindMaker("");
        }



    }
}
