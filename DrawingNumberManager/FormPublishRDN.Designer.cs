﻿namespace DrawingNumberManager
{
    partial class FormPublishRDN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblByte = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.lblNote = new System.Windows.Forms.Label();
            this.btnRevise = new System.Windows.Forms.Button();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.lblWarringContent = new System.Windows.Forms.Label();
            this.lblWarning = new System.Windows.Forms.Label();
            this.ReviseDrawingNumberDisplayPanel = new System.Windows.Forms.Panel();
            this.lblReviseDrawNumber = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.txtReviseDrawNumber = new System.Windows.Forms.TextBox();
            this.lblInputDrawingNumber = new System.Windows.Forms.Label();
            this.gbODNDetail = new System.Windows.Forms.GroupBox();
            this.btnMaker = new System.Windows.Forms.Button();
            this.cmbModel = new System.Windows.Forms.ComboBox();
            this.lblModel = new System.Windows.Forms.Label();
            this.cmbPart = new System.Windows.Forms.ComboBox();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.lblPart = new System.Windows.Forms.Label();
            this.lblPrice = new System.Windows.Forms.Label();
            this.txtUnitPrice = new System.Windows.Forms.TextBox();
            this.lblUnitPrice = new System.Windows.Forms.Label();
            this.txtQty = new System.Windows.Forms.TextBox();
            this.lblQty = new System.Windows.Forms.Label();
            this.txtMaker = new System.Windows.Forms.TextBox();
            this.lblMaker = new System.Windows.Forms.Label();
            this.txtAfterProcess = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMaterial = new System.Windows.Forms.TextBox();
            this.lblMaterial = new System.Windows.Forms.Label();
            this.txtSpecfication = new System.Windows.Forms.TextBox();
            this.lblSpecfication = new System.Windows.Forms.Label();
            this.txtPartName = new System.Windows.Forms.TextBox();
            this.lblPartName = new System.Windows.Forms.Label();
            this.txtType = new System.Windows.Forms.TextBox();
            this.lblType = new System.Windows.Forms.Label();
            this.ReviseDrawingNumberDisplayPanel.SuspendLayout();
            this.gbODNDetail.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblByte
            // 
            this.lblByte.AutoSize = true;
            this.lblByte.ForeColor = System.Drawing.Color.OrangeRed;
            this.lblByte.Location = new System.Drawing.Point(200, 37);
            this.lblByte.Name = "lblByte";
            this.lblByte.Size = new System.Drawing.Size(48, 12);
            this.lblByte.TabIndex = 15;
            this.lblByte.Text = "0/?Byte";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(283, 32);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(47, 22);
            this.btnClear.TabIndex = 3;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // lblNote
            // 
            this.lblNote.AutoSize = true;
            this.lblNote.Location = new System.Drawing.Point(4, 37);
            this.lblNote.Name = "lblNote";
            this.lblNote.Size = new System.Drawing.Size(31, 12);
            this.lblNote.TabIndex = 14;
            this.lblNote.Text = "Note";
            // 
            // btnRevise
            // 
            this.btnRevise.Enabled = false;
            this.btnRevise.Location = new System.Drawing.Point(274, 440);
            this.btnRevise.Name = "btnRevise";
            this.btnRevise.Size = new System.Drawing.Size(56, 34);
            this.btnRevise.TabIndex = 14;
            this.btnRevise.Text = "Revise";
            this.btnRevise.UseVisualStyleBackColor = true;
            this.btnRevise.Click += new System.EventHandler(this.btnRevise_Click);
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(5, 54);
            this.txtNote.Multiline = true;
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(325, 51);
            this.txtNote.TabIndex = 1;
            this.txtNote.TextChanged += new System.EventHandler(this.txtNote_TextChanged);
            // 
            // lblWarringContent
            // 
            this.lblWarringContent.AutoSize = true;
            this.lblWarringContent.ForeColor = System.Drawing.Color.OrangeRed;
            this.lblWarringContent.Location = new System.Drawing.Point(1, 127);
            this.lblWarringContent.Name = "lblWarringContent";
            this.lblWarringContent.Size = new System.Drawing.Size(11, 12);
            this.lblWarringContent.TabIndex = 18;
            this.lblWarringContent.Text = "?";
            // 
            // lblWarning
            // 
            this.lblWarning.AutoSize = true;
            this.lblWarning.ForeColor = System.Drawing.Color.OrangeRed;
            this.lblWarning.Location = new System.Drawing.Point(0, 110);
            this.lblWarning.Name = "lblWarning";
            this.lblWarning.Size = new System.Drawing.Size(62, 12);
            this.lblWarning.TabIndex = 17;
            this.lblWarning.Text = "※Warning";
            // 
            // ReviseDrawingNumberDisplayPanel
            // 
            this.ReviseDrawingNumberDisplayPanel.BackColor = System.Drawing.Color.Honeydew;
            this.ReviseDrawingNumberDisplayPanel.Controls.Add(this.lblReviseDrawNumber);
            this.ReviseDrawingNumberDisplayPanel.Controls.Add(this.btnClose);
            this.ReviseDrawingNumberDisplayPanel.Controls.Add(this.txtReviseDrawNumber);
            this.ReviseDrawingNumberDisplayPanel.Location = new System.Drawing.Point(6, 480);
            this.ReviseDrawingNumberDisplayPanel.Name = "ReviseDrawingNumberDisplayPanel";
            this.ReviseDrawingNumberDisplayPanel.Size = new System.Drawing.Size(331, 133);
            this.ReviseDrawingNumberDisplayPanel.TabIndex = 21;
            this.ReviseDrawingNumberDisplayPanel.Visible = false;
            // 
            // lblReviseDrawNumber
            // 
            this.lblReviseDrawNumber.AutoSize = true;
            this.lblReviseDrawNumber.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblReviseDrawNumber.Location = new System.Drawing.Point(7, 14);
            this.lblReviseDrawNumber.Name = "lblReviseDrawNumber";
            this.lblReviseDrawNumber.Size = new System.Drawing.Size(198, 16);
            this.lblReviseDrawNumber.TabIndex = 14;
            this.lblReviseDrawNumber.Text = "Revise Drawing Number";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(250, 87);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 38);
            this.btnClose.TabIndex = 13;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnMain_Click);
            // 
            // txtReviseDrawNumber
            // 
            this.txtReviseDrawNumber.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtReviseDrawNumber.Location = new System.Drawing.Point(7, 49);
            this.txtReviseDrawNumber.Name = "txtReviseDrawNumber";
            this.txtReviseDrawNumber.ReadOnly = true;
            this.txtReviseDrawNumber.Size = new System.Drawing.Size(318, 32);
            this.txtReviseDrawNumber.TabIndex = 0;
            this.txtReviseDrawNumber.TextChanged += new System.EventHandler(this.txtReviseDrawNumber_TextChanged);
            // 
            // lblInputDrawingNumber
            // 
            this.lblInputDrawingNumber.AutoSize = true;
            this.lblInputDrawingNumber.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblInputDrawingNumber.ForeColor = System.Drawing.Color.Navy;
            this.lblInputDrawingNumber.Location = new System.Drawing.Point(4, 9);
            this.lblInputDrawingNumber.Name = "lblInputDrawingNumber";
            this.lblInputDrawingNumber.Size = new System.Drawing.Size(12, 12);
            this.lblInputDrawingNumber.TabIndex = 22;
            this.lblInputDrawingNumber.Text = "?";
            // 
            // gbODNDetail
            // 
            this.gbODNDetail.Controls.Add(this.btnMaker);
            this.gbODNDetail.Controls.Add(this.cmbModel);
            this.gbODNDetail.Controls.Add(this.lblModel);
            this.gbODNDetail.Controls.Add(this.cmbPart);
            this.gbODNDetail.Controls.Add(this.txtPrice);
            this.gbODNDetail.Controls.Add(this.lblPart);
            this.gbODNDetail.Controls.Add(this.lblPrice);
            this.gbODNDetail.Controls.Add(this.txtUnitPrice);
            this.gbODNDetail.Controls.Add(this.lblUnitPrice);
            this.gbODNDetail.Controls.Add(this.txtQty);
            this.gbODNDetail.Controls.Add(this.lblQty);
            this.gbODNDetail.Controls.Add(this.txtMaker);
            this.gbODNDetail.Controls.Add(this.lblMaker);
            this.gbODNDetail.Controls.Add(this.txtAfterProcess);
            this.gbODNDetail.Controls.Add(this.label2);
            this.gbODNDetail.Controls.Add(this.txtMaterial);
            this.gbODNDetail.Controls.Add(this.lblMaterial);
            this.gbODNDetail.Controls.Add(this.txtSpecfication);
            this.gbODNDetail.Controls.Add(this.lblSpecfication);
            this.gbODNDetail.Controls.Add(this.txtPartName);
            this.gbODNDetail.Controls.Add(this.lblPartName);
            this.gbODNDetail.Controls.Add(this.txtType);
            this.gbODNDetail.Controls.Add(this.lblType);
            this.gbODNDetail.Location = new System.Drawing.Point(5, 149);
            this.gbODNDetail.Name = "gbODNDetail";
            this.gbODNDetail.Size = new System.Drawing.Size(325, 285);
            this.gbODNDetail.TabIndex = 28;
            this.gbODNDetail.TabStop = false;
            this.gbODNDetail.Text = "Detail Content";
            // 
            // btnMaker
            // 
            this.btnMaker.Location = new System.Drawing.Point(274, 202);
            this.btnMaker.Name = "btnMaker";
            this.btnMaker.Size = new System.Drawing.Size(47, 22);
            this.btnMaker.TabIndex = 10;
            this.btnMaker.Text = "...";
            this.btnMaker.UseVisualStyleBackColor = true;
            this.btnMaker.Click += new System.EventHandler(this.btnMaker_Click);
            // 
            // cmbModel
            // 
            this.cmbModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbModel.FormattingEnabled = true;
            this.cmbModel.Location = new System.Drawing.Point(91, 17);
            this.cmbModel.Name = "cmbModel";
            this.cmbModel.Size = new System.Drawing.Size(228, 20);
            this.cmbModel.TabIndex = 2;
            // 
            // lblModel
            // 
            this.lblModel.AutoSize = true;
            this.lblModel.Location = new System.Drawing.Point(11, 20);
            this.lblModel.Name = "lblModel";
            this.lblModel.Size = new System.Drawing.Size(40, 12);
            this.lblModel.TabIndex = 18;
            this.lblModel.Text = "Model";
            // 
            // cmbPart
            // 
            this.cmbPart.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPart.FormattingEnabled = true;
            this.cmbPart.Location = new System.Drawing.Point(92, 43);
            this.cmbPart.Name = "cmbPart";
            this.cmbPart.Size = new System.Drawing.Size(227, 20);
            this.cmbPart.TabIndex = 3;
            // 
            // txtPrice
            // 
            this.txtPrice.Location = new System.Drawing.Point(92, 258);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(80, 21);
            this.txtPrice.TabIndex = 13;
            this.txtPrice.Text = "0";
            // 
            // lblPart
            // 
            this.lblPart.AutoSize = true;
            this.lblPart.Location = new System.Drawing.Point(11, 46);
            this.lblPart.Name = "lblPart";
            this.lblPart.Size = new System.Drawing.Size(27, 12);
            this.lblPart.TabIndex = 20;
            this.lblPart.Text = "Part";
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Location = new System.Drawing.Point(12, 261);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(34, 12);
            this.lblPrice.TabIndex = 16;
            this.lblPrice.Text = "Price";
            // 
            // txtUnitPrice
            // 
            this.txtUnitPrice.Location = new System.Drawing.Point(241, 230);
            this.txtUnitPrice.Name = "txtUnitPrice";
            this.txtUnitPrice.Size = new System.Drawing.Size(80, 21);
            this.txtUnitPrice.TabIndex = 12;
            this.txtUnitPrice.Text = "0";
            this.txtUnitPrice.TextChanged += new System.EventHandler(this.txtUnitPrice_TextChanged);
            // 
            // lblUnitPrice
            // 
            this.lblUnitPrice.AutoSize = true;
            this.lblUnitPrice.Location = new System.Drawing.Point(184, 234);
            this.lblUnitPrice.Name = "lblUnitPrice";
            this.lblUnitPrice.Size = new System.Drawing.Size(46, 12);
            this.lblUnitPrice.TabIndex = 14;
            this.lblUnitPrice.Text = "U Price";
            // 
            // txtQty
            // 
            this.txtQty.Location = new System.Drawing.Point(91, 231);
            this.txtQty.Name = "txtQty";
            this.txtQty.Size = new System.Drawing.Size(80, 21);
            this.txtQty.TabIndex = 11;
            this.txtQty.Text = "0";
            this.txtQty.TextChanged += new System.EventHandler(this.txtQty_TextChanged);
            // 
            // lblQty
            // 
            this.lblQty.AutoSize = true;
            this.lblQty.Location = new System.Drawing.Point(11, 234);
            this.lblQty.Name = "lblQty";
            this.lblQty.Size = new System.Drawing.Size(24, 12);
            this.lblQty.TabIndex = 12;
            this.lblQty.Text = "Qty";
            // 
            // txtMaker
            // 
            this.txtMaker.Enabled = false;
            this.txtMaker.Location = new System.Drawing.Point(91, 204);
            this.txtMaker.Name = "txtMaker";
            this.txtMaker.Size = new System.Drawing.Size(180, 21);
            this.txtMaker.TabIndex = 9;
            // 
            // lblMaker
            // 
            this.lblMaker.AutoSize = true;
            this.lblMaker.Location = new System.Drawing.Point(11, 207);
            this.lblMaker.Name = "lblMaker";
            this.lblMaker.Size = new System.Drawing.Size(40, 12);
            this.lblMaker.TabIndex = 10;
            this.lblMaker.Text = "Maker";
            // 
            // txtAfterProcess
            // 
            this.txtAfterProcess.Location = new System.Drawing.Point(91, 177);
            this.txtAfterProcess.Name = "txtAfterProcess";
            this.txtAfterProcess.Size = new System.Drawing.Size(230, 21);
            this.txtAfterProcess.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 180);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 12);
            this.label2.TabIndex = 8;
            this.label2.Text = "After Process";
            // 
            // txtMaterial
            // 
            this.txtMaterial.Location = new System.Drawing.Point(91, 150);
            this.txtMaterial.Name = "txtMaterial";
            this.txtMaterial.Size = new System.Drawing.Size(230, 21);
            this.txtMaterial.TabIndex = 7;
            // 
            // lblMaterial
            // 
            this.lblMaterial.AutoSize = true;
            this.lblMaterial.Location = new System.Drawing.Point(11, 153);
            this.lblMaterial.Name = "lblMaterial";
            this.lblMaterial.Size = new System.Drawing.Size(50, 12);
            this.lblMaterial.TabIndex = 6;
            this.lblMaterial.Text = "Material";
            // 
            // txtSpecfication
            // 
            this.txtSpecfication.Location = new System.Drawing.Point(91, 122);
            this.txtSpecfication.Name = "txtSpecfication";
            this.txtSpecfication.Size = new System.Drawing.Size(230, 21);
            this.txtSpecfication.TabIndex = 6;
            // 
            // lblSpecfication
            // 
            this.lblSpecfication.AutoSize = true;
            this.lblSpecfication.Location = new System.Drawing.Point(11, 125);
            this.lblSpecfication.Name = "lblSpecfication";
            this.lblSpecfication.Size = new System.Drawing.Size(74, 12);
            this.lblSpecfication.TabIndex = 4;
            this.lblSpecfication.Text = "Specfication";
            // 
            // txtPartName
            // 
            this.txtPartName.Location = new System.Drawing.Point(91, 68);
            this.txtPartName.Name = "txtPartName";
            this.txtPartName.Size = new System.Drawing.Size(230, 21);
            this.txtPartName.TabIndex = 4;
            // 
            // lblPartName
            // 
            this.lblPartName.AutoSize = true;
            this.lblPartName.Location = new System.Drawing.Point(11, 71);
            this.lblPartName.Name = "lblPartName";
            this.lblPartName.Size = new System.Drawing.Size(61, 12);
            this.lblPartName.TabIndex = 2;
            this.lblPartName.Text = "PartName";
            // 
            // txtType
            // 
            this.txtType.Location = new System.Drawing.Point(91, 95);
            this.txtType.Name = "txtType";
            this.txtType.Size = new System.Drawing.Size(230, 21);
            this.txtType.TabIndex = 5;
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(11, 98);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(34, 12);
            this.lblType.TabIndex = 0;
            this.lblType.Text = "Type";
            // 
            // FormPublishRDN
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.ClientSize = new System.Drawing.Size(341, 622);
            this.Controls.Add(this.ReviseDrawingNumberDisplayPanel);
            this.Controls.Add(this.lblInputDrawingNumber);
            this.Controls.Add(this.lblByte);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.lblNote);
            this.Controls.Add(this.btnRevise);
            this.Controls.Add(this.txtNote);
            this.Controls.Add(this.lblWarringContent);
            this.Controls.Add(this.lblWarning);
            this.Controls.Add(this.gbODNDetail);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FormPublishRDN";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Revise Drawing Number";
            this.Load += new System.EventHandler(this.FormReviseDrawingNumber_Load);
            this.ReviseDrawingNumberDisplayPanel.ResumeLayout(false);
            this.ReviseDrawingNumberDisplayPanel.PerformLayout();
            this.gbODNDetail.ResumeLayout(false);
            this.gbODNDetail.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblByte;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label lblNote;
        private System.Windows.Forms.Button btnRevise;
        private System.Windows.Forms.TextBox txtNote;
        private System.Windows.Forms.Label lblWarringContent;
        private System.Windows.Forms.Label lblWarning;
        private System.Windows.Forms.Panel ReviseDrawingNumberDisplayPanel;
        private System.Windows.Forms.Label lblReviseDrawNumber;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TextBox txtReviseDrawNumber;
        private System.Windows.Forms.Label lblInputDrawingNumber;
        private System.Windows.Forms.GroupBox gbODNDetail;
        private System.Windows.Forms.ComboBox cmbModel;
        private System.Windows.Forms.Label lblModel;
        private System.Windows.Forms.ComboBox cmbPart;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.Label lblPart;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.TextBox txtUnitPrice;
        private System.Windows.Forms.Label lblUnitPrice;
        private System.Windows.Forms.TextBox txtQty;
        private System.Windows.Forms.Label lblQty;
        private System.Windows.Forms.TextBox txtMaker;
        private System.Windows.Forms.Label lblMaker;
        private System.Windows.Forms.TextBox txtAfterProcess;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtMaterial;
        private System.Windows.Forms.Label lblMaterial;
        private System.Windows.Forms.TextBox txtSpecfication;
        private System.Windows.Forms.Label lblSpecfication;
        private System.Windows.Forms.TextBox txtPartName;
        private System.Windows.Forms.Label lblPartName;
        private System.Windows.Forms.TextBox txtType;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.Button btnMaker;
    }
}