﻿namespace DrawingNumberManager
{
    partial class UcDetailContent
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbODNDetail = new System.Windows.Forms.GroupBox();
            this.btnMaker = new System.Windows.Forms.Button();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.lblPrice = new System.Windows.Forms.Label();
            this.txtUnitPrice = new System.Windows.Forms.TextBox();
            this.lblUnitPrice = new System.Windows.Forms.Label();
            this.txtQty = new System.Windows.Forms.TextBox();
            this.lblQty = new System.Windows.Forms.Label();
            this.txtMaker = new System.Windows.Forms.TextBox();
            this.lblCustomer = new System.Windows.Forms.Label();
            this.txtAfterProcess = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMaterial = new System.Windows.Forms.TextBox();
            this.lblMaterial = new System.Windows.Forms.Label();
            this.txtSpecfication = new System.Windows.Forms.TextBox();
            this.lblSpecfication = new System.Windows.Forms.Label();
            this.txtPartName = new System.Windows.Forms.TextBox();
            this.lblPartName = new System.Windows.Forms.Label();
            this.txtType = new System.Windows.Forms.TextBox();
            this.lblType = new System.Windows.Forms.Label();
            this.gbODNDetail.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbODNDetail
            // 
            this.gbODNDetail.Controls.Add(this.btnMaker);
            this.gbODNDetail.Controls.Add(this.txtPrice);
            this.gbODNDetail.Controls.Add(this.lblPrice);
            this.gbODNDetail.Controls.Add(this.txtUnitPrice);
            this.gbODNDetail.Controls.Add(this.lblUnitPrice);
            this.gbODNDetail.Controls.Add(this.txtQty);
            this.gbODNDetail.Controls.Add(this.lblQty);
            this.gbODNDetail.Controls.Add(this.txtMaker);
            this.gbODNDetail.Controls.Add(this.lblCustomer);
            this.gbODNDetail.Controls.Add(this.txtAfterProcess);
            this.gbODNDetail.Controls.Add(this.label2);
            this.gbODNDetail.Controls.Add(this.txtMaterial);
            this.gbODNDetail.Controls.Add(this.lblMaterial);
            this.gbODNDetail.Controls.Add(this.txtSpecfication);
            this.gbODNDetail.Controls.Add(this.lblSpecfication);
            this.gbODNDetail.Controls.Add(this.txtPartName);
            this.gbODNDetail.Controls.Add(this.lblPartName);
            this.gbODNDetail.Controls.Add(this.txtType);
            this.gbODNDetail.Controls.Add(this.lblType);
            this.gbODNDetail.Location = new System.Drawing.Point(5, 8);
            this.gbODNDetail.Name = "gbODNDetail";
            this.gbODNDetail.Size = new System.Drawing.Size(331, 251);
            this.gbODNDetail.TabIndex = 33;
            this.gbODNDetail.TabStop = false;
            this.gbODNDetail.Text = "Detail Content";
            // 
            // btnMaker
            // 
            this.btnMaker.Location = new System.Drawing.Point(278, 159);
            this.btnMaker.Name = "btnMaker";
            this.btnMaker.Size = new System.Drawing.Size(47, 22);
            this.btnMaker.TabIndex = 22;
            this.btnMaker.Text = "...";
            this.btnMaker.UseVisualStyleBackColor = true;
            this.btnMaker.Click += new System.EventHandler(this.btnMaker_Click);
            // 
            // txtPrice
            // 
            this.txtPrice.Location = new System.Drawing.Point(91, 215);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(91, 21);
            this.txtPrice.TabIndex = 17;
            this.txtPrice.Text = "0";
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Location = new System.Drawing.Point(11, 218);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(34, 12);
            this.lblPrice.TabIndex = 16;
            this.lblPrice.Text = "Price";
            // 
            // txtUnitPrice
            // 
            this.txtUnitPrice.Location = new System.Drawing.Point(240, 187);
            this.txtUnitPrice.Name = "txtUnitPrice";
            this.txtUnitPrice.Size = new System.Drawing.Size(85, 21);
            this.txtUnitPrice.TabIndex = 15;
            this.txtUnitPrice.Text = "0";
            this.txtUnitPrice.TextChanged += new System.EventHandler(this.txtUnitPrice_TextChanged);
            // 
            // lblUnitPrice
            // 
            this.lblUnitPrice.AutoSize = true;
            this.lblUnitPrice.Location = new System.Drawing.Point(188, 190);
            this.lblUnitPrice.Name = "lblUnitPrice";
            this.lblUnitPrice.Size = new System.Drawing.Size(46, 12);
            this.lblUnitPrice.TabIndex = 14;
            this.lblUnitPrice.Text = "U Price";
            // 
            // txtQty
            // 
            this.txtQty.Location = new System.Drawing.Point(90, 188);
            this.txtQty.Name = "txtQty";
            this.txtQty.Size = new System.Drawing.Size(92, 21);
            this.txtQty.TabIndex = 13;
            this.txtQty.Text = "0";
            this.txtQty.TextChanged += new System.EventHandler(this.txtQty_TextChanged);
            // 
            // lblQty
            // 
            this.lblQty.AutoSize = true;
            this.lblQty.Location = new System.Drawing.Point(10, 191);
            this.lblQty.Name = "lblQty";
            this.lblQty.Size = new System.Drawing.Size(24, 12);
            this.lblQty.TabIndex = 12;
            this.lblQty.Text = "Qty";
            // 
            // txtMaker
            // 
            this.txtMaker.Enabled = false;
            this.txtMaker.Location = new System.Drawing.Point(90, 161);
            this.txtMaker.Name = "txtMaker";
            this.txtMaker.Size = new System.Drawing.Size(182, 21);
            this.txtMaker.TabIndex = 11;
            // 
            // lblCustomer
            // 
            this.lblCustomer.AutoSize = true;
            this.lblCustomer.Location = new System.Drawing.Point(10, 164);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(40, 12);
            this.lblCustomer.TabIndex = 10;
            this.lblCustomer.Text = "Maker";
            // 
            // txtAfterProcess
            // 
            this.txtAfterProcess.Location = new System.Drawing.Point(90, 134);
            this.txtAfterProcess.Name = "txtAfterProcess";
            this.txtAfterProcess.Size = new System.Drawing.Size(235, 21);
            this.txtAfterProcess.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 137);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 12);
            this.label2.TabIndex = 8;
            this.label2.Text = "After Process";
            // 
            // txtMaterial
            // 
            this.txtMaterial.Location = new System.Drawing.Point(90, 107);
            this.txtMaterial.Name = "txtMaterial";
            this.txtMaterial.Size = new System.Drawing.Size(235, 21);
            this.txtMaterial.TabIndex = 7;
            // 
            // lblMaterial
            // 
            this.lblMaterial.AutoSize = true;
            this.lblMaterial.Location = new System.Drawing.Point(10, 110);
            this.lblMaterial.Name = "lblMaterial";
            this.lblMaterial.Size = new System.Drawing.Size(50, 12);
            this.lblMaterial.TabIndex = 6;
            this.lblMaterial.Text = "Material";
            // 
            // txtSpecfication
            // 
            this.txtSpecfication.Location = new System.Drawing.Point(90, 79);
            this.txtSpecfication.Name = "txtSpecfication";
            this.txtSpecfication.Size = new System.Drawing.Size(235, 21);
            this.txtSpecfication.TabIndex = 5;
            // 
            // lblSpecfication
            // 
            this.lblSpecfication.AutoSize = true;
            this.lblSpecfication.Location = new System.Drawing.Point(10, 82);
            this.lblSpecfication.Name = "lblSpecfication";
            this.lblSpecfication.Size = new System.Drawing.Size(74, 12);
            this.lblSpecfication.TabIndex = 4;
            this.lblSpecfication.Text = "Specfication";
            // 
            // txtPartName
            // 
            this.txtPartName.Location = new System.Drawing.Point(90, 25);
            this.txtPartName.Name = "txtPartName";
            this.txtPartName.Size = new System.Drawing.Size(235, 21);
            this.txtPartName.TabIndex = 3;
            // 
            // lblPartName
            // 
            this.lblPartName.AutoSize = true;
            this.lblPartName.Location = new System.Drawing.Point(10, 28);
            this.lblPartName.Name = "lblPartName";
            this.lblPartName.Size = new System.Drawing.Size(61, 12);
            this.lblPartName.TabIndex = 2;
            this.lblPartName.Text = "PartName";
            // 
            // txtType
            // 
            this.txtType.Location = new System.Drawing.Point(90, 52);
            this.txtType.Name = "txtType";
            this.txtType.Size = new System.Drawing.Size(235, 21);
            this.txtType.TabIndex = 1;
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(10, 55);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(34, 12);
            this.lblType.TabIndex = 0;
            this.lblType.Text = "Type";
            // 
            // UcDetailContent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gbODNDetail);
            this.Name = "UcDetailContent";
            this.Size = new System.Drawing.Size(343, 265);
            this.gbODNDetail.ResumeLayout(false);
            this.gbODNDetail.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbODNDetail;
        private System.Windows.Forms.Button btnMaker;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.TextBox txtUnitPrice;
        private System.Windows.Forms.Label lblUnitPrice;
        private System.Windows.Forms.TextBox txtQty;
        private System.Windows.Forms.Label lblQty;
        private System.Windows.Forms.TextBox txtMaker;
        private System.Windows.Forms.Label lblCustomer;
        private System.Windows.Forms.TextBox txtAfterProcess;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtMaterial;
        private System.Windows.Forms.Label lblMaterial;
        private System.Windows.Forms.TextBox txtSpecfication;
        private System.Windows.Forms.Label lblSpecfication;
        private System.Windows.Forms.TextBox txtPartName;
        private System.Windows.Forms.Label lblPartName;
        private System.Windows.Forms.TextBox txtType;
        private System.Windows.Forms.Label lblType;
    }
}
