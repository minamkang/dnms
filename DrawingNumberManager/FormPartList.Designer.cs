﻿namespace DrawingNumberManager
{
    partial class FormPartList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlSearch = new System.Windows.Forms.Panel();
            this.btnDownload = new System.Windows.Forms.Button();
            this.gbCondition = new System.Windows.Forms.GroupBox();
            this.lblPartName = new System.Windows.Forms.Label();
            this.txtPartName = new System.Windows.Forms.TextBox();
            this.cmbUserID = new System.Windows.Forms.ComboBox();
            this.cmbModel = new System.Windows.Forms.ComboBox();
            this.lblNote = new System.Windows.Forms.Label();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.lblModel = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblPart = new System.Windows.Forms.Label();
            this.cmbPart = new System.Windows.Forms.ComboBox();
            this.btnExcel = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.gbBasicSearch = new System.Windows.Forms.GroupBox();
            this.txtDrawingNumber = new System.Windows.Forms.TextBox();
            this.lblDrawingNumber = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.lblEndDate = new System.Windows.Forms.Label();
            this.dtpickerEndDate = new System.Windows.Forms.DateTimePicker();
            this.lblStartDate = new System.Windows.Forms.Label();
            this.dtpickerStartDate = new System.Windows.Forms.DateTimePicker();
            this.pnlBody = new System.Windows.Forms.Panel();
            this.ucPbPartList = new DrawingNumberManager.ucProgressBar();
            this.dgvPartList = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiAdd = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiModify = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlSearch.SuspendLayout();
            this.gbCondition.SuspendLayout();
            this.gbBasicSearch.SuspendLayout();
            this.pnlBody.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPartList)).BeginInit();
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSearch
            // 
            this.pnlSearch.Controls.Add(this.btnDownload);
            this.pnlSearch.Controls.Add(this.gbCondition);
            this.pnlSearch.Controls.Add(this.btnExcel);
            this.pnlSearch.Controls.Add(this.btnSearch);
            this.pnlSearch.Controls.Add(this.gbBasicSearch);
            this.pnlSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSearch.Location = new System.Drawing.Point(0, 0);
            this.pnlSearch.Name = "pnlSearch";
            this.pnlSearch.Size = new System.Drawing.Size(1072, 127);
            this.pnlSearch.TabIndex = 0;
            // 
            // btnDownload
            // 
            this.btnDownload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDownload.Location = new System.Drawing.Point(976, 19);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(80, 49);
            this.btnDownload.TabIndex = 50;
            this.btnDownload.Text = "Download";
            this.btnDownload.UseVisualStyleBackColor = true;
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // gbCondition
            // 
            this.gbCondition.Controls.Add(this.lblPartName);
            this.gbCondition.Controls.Add(this.txtPartName);
            this.gbCondition.Controls.Add(this.cmbUserID);
            this.gbCondition.Controls.Add(this.cmbModel);
            this.gbCondition.Controls.Add(this.lblNote);
            this.gbCondition.Controls.Add(this.txtNote);
            this.gbCondition.Controls.Add(this.lblModel);
            this.gbCondition.Controls.Add(this.lblName);
            this.gbCondition.Controls.Add(this.lblPart);
            this.gbCondition.Controls.Add(this.cmbPart);
            this.gbCondition.Location = new System.Drawing.Point(387, 12);
            this.gbCondition.Name = "gbCondition";
            this.gbCondition.Size = new System.Drawing.Size(402, 110);
            this.gbCondition.TabIndex = 49;
            this.gbCondition.TabStop = false;
            this.gbCondition.Text = "조건검색";
            // 
            // lblPartName
            // 
            this.lblPartName.AutoSize = true;
            this.lblPartName.Location = new System.Drawing.Point(240, 21);
            this.lblPartName.Name = "lblPartName";
            this.lblPartName.Size = new System.Drawing.Size(65, 12);
            this.lblPartName.TabIndex = 32;
            this.lblPartName.Text = "Part Name";
            // 
            // txtPartName
            // 
            this.txtPartName.Location = new System.Drawing.Point(242, 36);
            this.txtPartName.MaxLength = 500;
            this.txtPartName.Name = "txtPartName";
            this.txtPartName.Size = new System.Drawing.Size(153, 21);
            this.txtPartName.TabIndex = 33;
            this.txtPartName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPartName_KeyDown);
            // 
            // cmbUserID
            // 
            this.cmbUserID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUserID.FormattingEnabled = true;
            this.cmbUserID.Location = new System.Drawing.Point(7, 81);
            this.cmbUserID.Name = "cmbUserID";
            this.cmbUserID.Size = new System.Drawing.Size(92, 20);
            this.cmbUserID.TabIndex = 28;
            this.cmbUserID.SelectedIndexChanged += new System.EventHandler(this.cmbUserID_SelectedIndexChanged);
            // 
            // cmbModel
            // 
            this.cmbModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbModel.FormattingEnabled = true;
            this.cmbModel.Location = new System.Drawing.Point(7, 36);
            this.cmbModel.Name = "cmbModel";
            this.cmbModel.Size = new System.Drawing.Size(92, 20);
            this.cmbModel.TabIndex = 3;
            this.cmbModel.SelectedIndexChanged += new System.EventHandler(this.cmbModel_SelectedIndexChanged);
            // 
            // lblNote
            // 
            this.lblNote.AutoSize = true;
            this.lblNote.Location = new System.Drawing.Point(103, 66);
            this.lblNote.Name = "lblNote";
            this.lblNote.Size = new System.Drawing.Size(31, 12);
            this.lblNote.TabIndex = 24;
            this.lblNote.Text = "Note";
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(105, 81);
            this.txtNote.MaxLength = 500;
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(290, 21);
            this.txtNote.TabIndex = 25;
            this.txtNote.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNote_KeyDown);
            // 
            // lblModel
            // 
            this.lblModel.AutoSize = true;
            this.lblModel.Location = new System.Drawing.Point(5, 21);
            this.lblModel.Name = "lblModel";
            this.lblModel.Size = new System.Drawing.Size(40, 12);
            this.lblModel.TabIndex = 20;
            this.lblModel.Text = "Model";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(5, 66);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(39, 12);
            this.lblName.TabIndex = 29;
            this.lblName.Text = "Name";
            // 
            // lblPart
            // 
            this.lblPart.AutoSize = true;
            this.lblPart.Location = new System.Drawing.Point(103, 21);
            this.lblPart.Name = "lblPart";
            this.lblPart.Size = new System.Drawing.Size(27, 12);
            this.lblPart.TabIndex = 31;
            this.lblPart.Text = "Part";
            // 
            // cmbPart
            // 
            this.cmbPart.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPart.FormattingEnabled = true;
            this.cmbPart.Location = new System.Drawing.Point(105, 36);
            this.cmbPart.Name = "cmbPart";
            this.cmbPart.Size = new System.Drawing.Size(131, 20);
            this.cmbPart.TabIndex = 30;
            this.cmbPart.SelectedIndexChanged += new System.EventHandler(this.cmbPart_SelectedIndexChanged);
            // 
            // btnExcel
            // 
            this.btnExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExcel.Location = new System.Drawing.Point(890, 19);
            this.btnExcel.Name = "btnExcel";
            this.btnExcel.Size = new System.Drawing.Size(80, 49);
            this.btnExcel.TabIndex = 28;
            this.btnExcel.Text = "Excel";
            this.btnExcel.UseVisualStyleBackColor = true;
            this.btnExcel.Click += new System.EventHandler(this.btnExcel_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Location = new System.Drawing.Point(804, 19);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(80, 49);
            this.btnSearch.TabIndex = 27;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // gbBasicSearch
            // 
            this.gbBasicSearch.Controls.Add(this.txtDrawingNumber);
            this.gbBasicSearch.Controls.Add(this.lblDrawingNumber);
            this.gbBasicSearch.Controls.Add(this.lblFrom);
            this.gbBasicSearch.Controls.Add(this.lblEndDate);
            this.gbBasicSearch.Controls.Add(this.dtpickerEndDate);
            this.gbBasicSearch.Controls.Add(this.lblStartDate);
            this.gbBasicSearch.Controls.Add(this.dtpickerStartDate);
            this.gbBasicSearch.Location = new System.Drawing.Point(12, 12);
            this.gbBasicSearch.Name = "gbBasicSearch";
            this.gbBasicSearch.Size = new System.Drawing.Size(369, 110);
            this.gbBasicSearch.TabIndex = 0;
            this.gbBasicSearch.TabStop = false;
            this.gbBasicSearch.Text = "기본검색";
            // 
            // txtDrawingNumber
            // 
            this.txtDrawingNumber.Location = new System.Drawing.Point(8, 80);
            this.txtDrawingNumber.Name = "txtDrawingNumber";
            this.txtDrawingNumber.Size = new System.Drawing.Size(352, 21);
            this.txtDrawingNumber.TabIndex = 17;
            // 
            // lblDrawingNumber
            // 
            this.lblDrawingNumber.AutoSize = true;
            this.lblDrawingNumber.Location = new System.Drawing.Point(6, 66);
            this.lblDrawingNumber.Name = "lblDrawingNumber";
            this.lblDrawingNumber.Size = new System.Drawing.Size(29, 12);
            this.lblDrawingNumber.TabIndex = 16;
            this.lblDrawingNumber.Text = "도번";
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(177, 38);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(14, 12);
            this.lblFrom.TabIndex = 15;
            this.lblFrom.Text = "~";
            // 
            // lblEndDate
            // 
            this.lblEndDate.AutoSize = true;
            this.lblEndDate.Location = new System.Drawing.Point(195, 17);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(56, 12);
            this.lblEndDate.TabIndex = 14;
            this.lblEndDate.Text = "End Date";
            // 
            // dtpickerEndDate
            // 
            this.dtpickerEndDate.CustomFormat = "";
            this.dtpickerEndDate.Location = new System.Drawing.Point(197, 32);
            this.dtpickerEndDate.MaxDate = new System.DateTime(9998, 12, 1, 0, 0, 0, 0);
            this.dtpickerEndDate.Name = "dtpickerEndDate";
            this.dtpickerEndDate.Size = new System.Drawing.Size(163, 21);
            this.dtpickerEndDate.TabIndex = 12;
            this.dtpickerEndDate.Value = new System.DateTime(2013, 11, 19, 0, 0, 0, 0);
            // 
            // lblStartDate
            // 
            this.lblStartDate.AutoSize = true;
            this.lblStartDate.Location = new System.Drawing.Point(6, 17);
            this.lblStartDate.Name = "lblStartDate";
            this.lblStartDate.Size = new System.Drawing.Size(59, 12);
            this.lblStartDate.TabIndex = 13;
            this.lblStartDate.Text = "Start Date";
            // 
            // dtpickerStartDate
            // 
            this.dtpickerStartDate.CustomFormat = "";
            this.dtpickerStartDate.Location = new System.Drawing.Point(8, 32);
            this.dtpickerStartDate.MaxDate = new System.DateTime(9998, 12, 1, 0, 0, 0, 0);
            this.dtpickerStartDate.Name = "dtpickerStartDate";
            this.dtpickerStartDate.Size = new System.Drawing.Size(163, 21);
            this.dtpickerStartDate.TabIndex = 11;
            this.dtpickerStartDate.Value = new System.DateTime(2013, 11, 11, 0, 0, 0, 0);
            // 
            // pnlBody
            // 
            this.pnlBody.Controls.Add(this.ucPbPartList);
            this.pnlBody.Controls.Add(this.dgvPartList);
            this.pnlBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBody.Location = new System.Drawing.Point(0, 127);
            this.pnlBody.Name = "pnlBody";
            this.pnlBody.Size = new System.Drawing.Size(1072, 495);
            this.pnlBody.TabIndex = 1;
            // 
            // ucPbPartList
            // 
            this.ucPbPartList.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ucPbPartList.Location = new System.Drawing.Point(422, 226);
            this.ucPbPartList.Message = "Message";
            this.ucPbPartList.Name = "ucPbPartList";
            this.ucPbPartList.Progress = 0;
            this.ucPbPartList.Size = new System.Drawing.Size(270, 84);
            this.ucPbPartList.StatusFontColor = System.Drawing.SystemColors.ControlText;
            this.ucPbPartList.StatusText = "Title";
            this.ucPbPartList.TabIndex = 2;
            this.ucPbPartList.Visible = false;
            // 
            // dgvPartList
            // 
            this.dgvPartList.AllowUserToAddRows = false;
            this.dgvPartList.AllowUserToResizeRows = false;
            this.dgvPartList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPartList.ContextMenuStrip = this.contextMenuStrip;
            this.dgvPartList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPartList.Location = new System.Drawing.Point(0, 0);
            this.dgvPartList.Name = "dgvPartList";
            this.dgvPartList.RowHeadersVisible = false;
            this.dgvPartList.RowTemplate.Height = 23;
            this.dgvPartList.Size = new System.Drawing.Size(1072, 495);
            this.dgvPartList.TabIndex = 1;
            this.dgvPartList.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvPartList_CellFormatting);
            this.dgvPartList.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvPartList_CellMouseDown);
            this.dgvPartList.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.dgvPartList_CellPainting);
            this.dgvPartList.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvPartList_ColumnHeaderMouseClick);
            this.dgvPartList.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dgvPartList_MouseClick);
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiAdd,
            this.tsmiDelete,
            this.tsmiModify});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(155, 70);
            // 
            // tsmiAdd
            // 
            this.tsmiAdd.Name = "tsmiAdd";
            this.tsmiAdd.Size = new System.Drawing.Size(154, 22);
            this.tsmiAdd.Text = "공용 부품 추가";
            this.tsmiAdd.Click += new System.EventHandler(this.tsmiAdd_Click);
            // 
            // tsmiDelete
            // 
            this.tsmiDelete.Name = "tsmiDelete";
            this.tsmiDelete.Size = new System.Drawing.Size(154, 22);
            this.tsmiDelete.Text = "공용 부품 삭제";
            this.tsmiDelete.Click += new System.EventHandler(this.tsmiDelete_Click);
            // 
            // tsmiModify
            // 
            this.tsmiModify.Name = "tsmiModify";
            this.tsmiModify.Size = new System.Drawing.Size(154, 22);
            this.tsmiModify.Text = "수정";
            this.tsmiModify.Click += new System.EventHandler(this.tsmiModify_Click);
            // 
            // FormPartList
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.ClientSize = new System.Drawing.Size(1072, 622);
            this.Controls.Add(this.pnlBody);
            this.Controls.Add(this.pnlSearch);
            this.Name = "FormPartList";
            this.Text = "Part List";
            this.Load += new System.EventHandler(this.FormPartList_Load);
            this.pnlSearch.ResumeLayout(false);
            this.gbCondition.ResumeLayout(false);
            this.gbCondition.PerformLayout();
            this.gbBasicSearch.ResumeLayout(false);
            this.gbBasicSearch.PerformLayout();
            this.pnlBody.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPartList)).EndInit();
            this.contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlSearch;
        private System.Windows.Forms.Panel pnlBody;
        private System.Windows.Forms.GroupBox gbBasicSearch;
        private System.Windows.Forms.Label lblDrawingNumber;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.Label lblEndDate;
        private System.Windows.Forms.DateTimePicker dtpickerEndDate;
        private System.Windows.Forms.Label lblStartDate;
        private System.Windows.Forms.DateTimePicker dtpickerStartDate;
        private System.Windows.Forms.Button btnExcel;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtDrawingNumber;
        private System.Windows.Forms.GroupBox gbCondition;
        private System.Windows.Forms.ComboBox cmbUserID;
        private System.Windows.Forms.ComboBox cmbModel;
        private System.Windows.Forms.Label lblNote;
        private System.Windows.Forms.TextBox txtNote;
        private System.Windows.Forms.Label lblModel;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblPart;
        private System.Windows.Forms.ComboBox cmbPart;
        private System.Windows.Forms.DataGridView dgvPartList;
        private System.Windows.Forms.Label lblPartName;
        private System.Windows.Forms.TextBox txtPartName;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem tsmiAdd;
        private System.Windows.Forms.ToolStripMenuItem tsmiModify;
        private System.Windows.Forms.ToolStripMenuItem tsmiDelete;
        private System.Windows.Forms.Button btnDownload;
        private ucProgressBar ucPbPartList;
    }
}