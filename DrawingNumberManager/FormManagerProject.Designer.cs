﻿namespace DrawingNumberManager
{
    partial class FormManagerProject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblValue = new System.Windows.Forms.Label();
            this.txtCodeValue = new System.Windows.Forms.TextBox();
            this.lblCode = new System.Windows.Forms.Label();
            this.txtCode = new System.Windows.Forms.TextBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnModify = new System.Windows.Forms.Button();
            this.btnInsert = new System.Windows.Forms.Button();
            this.dgvProjectTypeOfContent = new System.Windows.Forms.DataGridView();
            this.lblOfficialProjectType = new System.Windows.Forms.Label();
            this.cmbProjectType = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProjectTypeOfContent)).BeginInit();
            this.SuspendLayout();
            // 
            // lblValue
            // 
            this.lblValue.AutoSize = true;
            this.lblValue.Location = new System.Drawing.Point(278, 98);
            this.lblValue.Name = "lblValue";
            this.lblValue.Size = new System.Drawing.Size(37, 12);
            this.lblValue.TabIndex = 34;
            this.lblValue.Text = "Value";
            // 
            // txtCodeValue
            // 
            this.txtCodeValue.Location = new System.Drawing.Point(336, 93);
            this.txtCodeValue.MaxLength = 20;
            this.txtCodeValue.Name = "txtCodeValue";
            this.txtCodeValue.Size = new System.Drawing.Size(140, 21);
            this.txtCodeValue.TabIndex = 33;
            // 
            // lblCode
            // 
            this.lblCode.AutoSize = true;
            this.lblCode.Location = new System.Drawing.Point(278, 71);
            this.lblCode.Name = "lblCode";
            this.lblCode.Size = new System.Drawing.Size(35, 12);
            this.lblCode.TabIndex = 32;
            this.lblCode.Text = "Code";
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(337, 66);
            this.txtCode.MaxLength = 10;
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(139, 21);
            this.txtCode.TabIndex = 31;
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(442, 528);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 35);
            this.btnClose.TabIndex = 28;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(361, 528);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 35);
            this.btnCancel.TabIndex = 27;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(280, 528);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 35);
            this.btnOK.TabIndex = 26;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(175, 528);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 35);
            this.btnDelete.TabIndex = 25;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnModify
            // 
            this.btnModify.Location = new System.Drawing.Point(94, 528);
            this.btnModify.Name = "btnModify";
            this.btnModify.Size = new System.Drawing.Size(75, 35);
            this.btnModify.TabIndex = 24;
            this.btnModify.Text = "Modify";
            this.btnModify.UseVisualStyleBackColor = true;
            this.btnModify.Click += new System.EventHandler(this.btnModify_Click);
            // 
            // btnInsert
            // 
            this.btnInsert.Location = new System.Drawing.Point(13, 528);
            this.btnInsert.Name = "btnInsert";
            this.btnInsert.Size = new System.Drawing.Size(75, 35);
            this.btnInsert.TabIndex = 23;
            this.btnInsert.Text = "Insert";
            this.btnInsert.UseVisualStyleBackColor = true;
            this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
            // 
            // dgvProjectTypeOfContent
            // 
            this.dgvProjectTypeOfContent.AllowUserToAddRows = false;
            this.dgvProjectTypeOfContent.AllowUserToDeleteRows = false;
            this.dgvProjectTypeOfContent.AllowUserToResizeRows = false;
            this.dgvProjectTypeOfContent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvProjectTypeOfContent.Location = new System.Drawing.Point(12, 65);
            this.dgvProjectTypeOfContent.MultiSelect = false;
            this.dgvProjectTypeOfContent.Name = "dgvProjectTypeOfContent";
            this.dgvProjectTypeOfContent.ReadOnly = true;
            this.dgvProjectTypeOfContent.RowHeadersVisible = false;
            this.dgvProjectTypeOfContent.RowTemplate.Height = 23;
            this.dgvProjectTypeOfContent.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProjectTypeOfContent.Size = new System.Drawing.Size(238, 457);
            this.dgvProjectTypeOfContent.TabIndex = 22;
            this.dgvProjectTypeOfContent.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProjectTypeOfContent_RowEnter);
            // 
            // lblOfficialProjectType
            // 
            this.lblOfficialProjectType.AutoSize = true;
            this.lblOfficialProjectType.Location = new System.Drawing.Point(12, 28);
            this.lblOfficialProjectType.Name = "lblOfficialProjectType";
            this.lblOfficialProjectType.Size = new System.Drawing.Size(77, 12);
            this.lblOfficialProjectType.TabIndex = 35;
            this.lblOfficialProjectType.Text = "Project Type";
            // 
            // cmbProjectType
            // 
            this.cmbProjectType.Enabled = false;
            this.cmbProjectType.FormattingEnabled = true;
            this.cmbProjectType.Items.AddRange(new object[] {
            "Model",
            "Part",
            "Process"});
            this.cmbProjectType.Location = new System.Drawing.Point(105, 25);
            this.cmbProjectType.Name = "cmbProjectType";
            this.cmbProjectType.Size = new System.Drawing.Size(145, 20);
            this.cmbProjectType.TabIndex = 36;
            // 
            // FormManagerProject
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.ClientSize = new System.Drawing.Size(527, 568);
            this.Controls.Add(this.cmbProjectType);
            this.Controls.Add(this.lblOfficialProjectType);
            this.Controls.Add(this.lblValue);
            this.Controls.Add(this.txtCodeValue);
            this.Controls.Add(this.lblCode);
            this.Controls.Add(this.txtCode);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnModify);
            this.Controls.Add(this.btnInsert);
            this.Controls.Add(this.dgvProjectTypeOfContent);
            this.Name = "FormManagerProject";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Official Project Manager";
            this.Load += new System.EventHandler(this.FormManagerProject_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProjectTypeOfContent)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblValue;
        private System.Windows.Forms.TextBox txtCodeValue;
        private System.Windows.Forms.Label lblCode;
        private System.Windows.Forms.TextBox txtCode;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnModify;
        private System.Windows.Forms.Button btnInsert;
        private System.Windows.Forms.DataGridView dgvProjectTypeOfContent;
        private System.Windows.Forms.Label lblOfficialProjectType;
        private System.Windows.Forms.ComboBox cmbProjectType;
    }
}