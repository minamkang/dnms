﻿namespace DrawingNumberManager
{
    partial class FormOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlOrder = new System.Windows.Forms.Panel();
            this.tabOrderMagement = new System.Windows.Forms.TabControl();
            this.tpRegistryOrder = new System.Windows.Forms.TabPage();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.pnlROrderDetail = new System.Windows.Forms.Panel();
            this.txtRTotalVAT = new System.Windows.Forms.TextBox();
            this.lblTotalVAT = new System.Windows.Forms.Label();
            this.lblGrandTotal = new System.Windows.Forms.Label();
            this.txtRGrandTotal = new System.Windows.Forms.TextBox();
            this.txtRTotal = new System.Windows.Forms.TextBox();
            this.lblTotal = new System.Windows.Forms.Label();
            this.dgvROrderDetail = new System.Windows.Forms.DataGridView();
            this.lblOrderDetail = new System.Windows.Forms.Label();
            this.pnlROrder = new System.Windows.Forms.Panel();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnAddOrder = new System.Windows.Forms.Button();
            this.btnRequestApprove = new System.Windows.Forms.Button();
            this.dgvROrder = new System.Windows.Forms.DataGridView();
            this.btnUpdateOrder = new System.Windows.Forms.Button();
            this.tpApproveOrder = new System.Windows.Forms.TabPage();
            this.pnlAOrderBody = new System.Windows.Forms.Panel();
            this.pnlAOrderDetail = new System.Windows.Forms.Panel();
            this.txtRQTotalVAT = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtRQGrandTotal = new System.Windows.Forms.TextBox();
            this.txtRQTotal = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dgvRQOrderDetail = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.pnlAOrder = new System.Windows.Forms.Panel();
            this.btnProcApprove = new System.Windows.Forms.Button();
            this.dgvRQOrder = new System.Windows.Forms.DataGridView();
            this.tpHistory = new System.Windows.Forms.TabPage();
            this.pnlVOrderBody = new System.Windows.Forms.Panel();
            this.pnlVOrderDetail = new System.Windows.Forms.Panel();
            this.txtVTotalVAT = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtVGrandTotal = new System.Windows.Forms.TextBox();
            this.txtVTotal = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.dgvVOrderDetail = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.pnlVOrder = new System.Windows.Forms.Panel();
            this.btnCancelApprove = new System.Windows.Forms.Button();
            this.btnExcel = new System.Windows.Forms.Button();
            this.btnReorder = new System.Windows.Forms.Button();
            this.btnSendMail = new System.Windows.Forms.Button();
            this.dgvVOrder = new System.Windows.Forms.DataGridView();
            this.pnlManagementTop = new System.Windows.Forms.Panel();
            this.btnPartList = new System.Windows.Forms.Button();
            this.btnInit = new System.Windows.Forms.Button();
            this.gbBasicSearch = new System.Windows.Forms.GroupBox();
            this.txtSCategorizationContent = new System.Windows.Forms.TextBox();
            this.lblReverseSearch = new System.Windows.Forms.Label();
            this.btnMakerClear = new System.Windows.Forms.Button();
            this.cmbSCategorization = new System.Windows.Forms.ComboBox();
            this.lblStartDate = new System.Windows.Forms.Label();
            this.dtpickerStartDate = new System.Windows.Forms.DateTimePicker();
            this.dtpickerEndDate = new System.Windows.Forms.DateTimePicker();
            this.lblEndDate = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.txtMaker = new System.Windows.Forms.TextBox();
            this.btnMaker = new System.Windows.Forms.Button();
            this.lblMaker = new System.Windows.Forms.Label();
            this.dtpickerDuedate = new System.Windows.Forms.DateTimePicker();
            this.gbCondition = new System.Windows.Forms.GroupBox();
            this.cmbUserID = new System.Windows.Forms.ComboBox();
            this.lblSEditer = new System.Windows.Forms.Label();
            this.cmbSStatus = new System.Windows.Forms.ComboBox();
            this.lblSStatus = new System.Windows.Forms.Label();
            this.txtSOrderNumber = new System.Windows.Forms.TextBox();
            this.lblSOrderNumber = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.pnlOrder.SuspendLayout();
            this.tabOrderMagement.SuspendLayout();
            this.tpRegistryOrder.SuspendLayout();
            this.pnlROrderDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvROrderDetail)).BeginInit();
            this.pnlROrder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvROrder)).BeginInit();
            this.tpApproveOrder.SuspendLayout();
            this.pnlAOrderBody.SuspendLayout();
            this.pnlAOrderDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRQOrderDetail)).BeginInit();
            this.pnlAOrder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRQOrder)).BeginInit();
            this.tpHistory.SuspendLayout();
            this.pnlVOrderBody.SuspendLayout();
            this.pnlVOrderDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVOrderDetail)).BeginInit();
            this.pnlVOrder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVOrder)).BeginInit();
            this.pnlManagementTop.SuspendLayout();
            this.gbBasicSearch.SuspendLayout();
            this.gbCondition.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlOrder
            // 
            this.pnlOrder.Controls.Add(this.tabOrderMagement);
            this.pnlOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlOrder.Location = new System.Drawing.Point(0, 118);
            this.pnlOrder.Name = "pnlOrder";
            this.pnlOrder.Size = new System.Drawing.Size(1178, 510);
            this.pnlOrder.TabIndex = 6;
            // 
            // tabOrderMagement
            // 
            this.tabOrderMagement.Controls.Add(this.tpRegistryOrder);
            this.tabOrderMagement.Controls.Add(this.tpApproveOrder);
            this.tabOrderMagement.Controls.Add(this.tpHistory);
            this.tabOrderMagement.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabOrderMagement.Location = new System.Drawing.Point(0, 0);
            this.tabOrderMagement.Name = "tabOrderMagement";
            this.tabOrderMagement.SelectedIndex = 0;
            this.tabOrderMagement.Size = new System.Drawing.Size(1178, 510);
            this.tabOrderMagement.TabIndex = 2;
            this.tabOrderMagement.SelectedIndexChanged += new System.EventHandler(this.tabOrderMagement_SelectedIndexChanged);
            // 
            // tpRegistryOrder
            // 
            this.tpRegistryOrder.Controls.Add(this.splitter1);
            this.tpRegistryOrder.Controls.Add(this.pnlROrderDetail);
            this.tpRegistryOrder.Controls.Add(this.pnlROrder);
            this.tpRegistryOrder.Location = new System.Drawing.Point(4, 22);
            this.tpRegistryOrder.Name = "tpRegistryOrder";
            this.tpRegistryOrder.Padding = new System.Windows.Forms.Padding(3);
            this.tpRegistryOrder.Size = new System.Drawing.Size(1170, 484);
            this.tpRegistryOrder.TabIndex = 1;
            this.tpRegistryOrder.Text = "등록";
            this.tpRegistryOrder.UseVisualStyleBackColor = true;
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.Gray;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(3, 211);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(1164, 3);
            this.splitter1.TabIndex = 9;
            this.splitter1.TabStop = false;
            // 
            // pnlROrderDetail
            // 
            this.pnlROrderDetail.Controls.Add(this.txtRTotalVAT);
            this.pnlROrderDetail.Controls.Add(this.lblTotalVAT);
            this.pnlROrderDetail.Controls.Add(this.lblGrandTotal);
            this.pnlROrderDetail.Controls.Add(this.txtRGrandTotal);
            this.pnlROrderDetail.Controls.Add(this.txtRTotal);
            this.pnlROrderDetail.Controls.Add(this.lblTotal);
            this.pnlROrderDetail.Controls.Add(this.dgvROrderDetail);
            this.pnlROrderDetail.Controls.Add(this.lblOrderDetail);
            this.pnlROrderDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlROrderDetail.Location = new System.Drawing.Point(3, 211);
            this.pnlROrderDetail.Name = "pnlROrderDetail";
            this.pnlROrderDetail.Size = new System.Drawing.Size(1164, 270);
            this.pnlROrderDetail.TabIndex = 6;
            // 
            // txtRTotalVAT
            // 
            this.txtRTotalVAT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRTotalVAT.Enabled = false;
            this.txtRTotalVAT.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtRTotalVAT.Location = new System.Drawing.Point(924, 244);
            this.txtRTotalVAT.Name = "txtRTotalVAT";
            this.txtRTotalVAT.Size = new System.Drawing.Size(85, 21);
            this.txtRTotalVAT.TabIndex = 52;
            this.txtRTotalVAT.Text = "0";
            this.txtRTotalVAT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblTotalVAT
            // 
            this.lblTotalVAT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotalVAT.AutoSize = true;
            this.lblTotalVAT.Location = new System.Drawing.Point(865, 247);
            this.lblTotalVAT.Name = "lblTotalVAT";
            this.lblTotalVAT.Size = new System.Drawing.Size(53, 12);
            this.lblTotalVAT.TabIndex = 51;
            this.lblTotalVAT.Text = "VAT합계";
            // 
            // lblGrandTotal
            // 
            this.lblGrandTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGrandTotal.AutoSize = true;
            this.lblGrandTotal.Location = new System.Drawing.Point(1015, 247);
            this.lblGrandTotal.Name = "lblGrandTotal";
            this.lblGrandTotal.Size = new System.Drawing.Size(53, 12);
            this.lblGrandTotal.TabIndex = 50;
            this.lblGrandTotal.Text = "지급총액";
            // 
            // txtRGrandTotal
            // 
            this.txtRGrandTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRGrandTotal.Enabled = false;
            this.txtRGrandTotal.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtRGrandTotal.Location = new System.Drawing.Point(1074, 244);
            this.txtRGrandTotal.Name = "txtRGrandTotal";
            this.txtRGrandTotal.Size = new System.Drawing.Size(85, 21);
            this.txtRGrandTotal.TabIndex = 49;
            this.txtRGrandTotal.Text = "0";
            this.txtRGrandTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtRTotal
            // 
            this.txtRTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRTotal.Enabled = false;
            this.txtRTotal.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtRTotal.Location = new System.Drawing.Point(774, 244);
            this.txtRTotal.Name = "txtRTotal";
            this.txtRTotal.Size = new System.Drawing.Size(85, 21);
            this.txtRTotal.TabIndex = 48;
            this.txtRTotal.Text = "0";
            this.txtRTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblTotal
            // 
            this.lblTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotal.AutoSize = true;
            this.lblTotal.Location = new System.Drawing.Point(739, 247);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(29, 12);
            this.lblTotal.TabIndex = 47;
            this.lblTotal.Text = "합계";
            // 
            // dgvROrderDetail
            // 
            this.dgvROrderDetail.AllowUserToAddRows = false;
            this.dgvROrderDetail.AllowUserToDeleteRows = false;
            this.dgvROrderDetail.AllowUserToResizeRows = false;
            this.dgvROrderDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvROrderDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvROrderDetail.Location = new System.Drawing.Point(3, 21);
            this.dgvROrderDetail.MultiSelect = false;
            this.dgvROrderDetail.Name = "dgvROrderDetail";
            this.dgvROrderDetail.ReadOnly = true;
            this.dgvROrderDetail.RowHeadersVisible = false;
            this.dgvROrderDetail.RowTemplate.Height = 23;
            this.dgvROrderDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvROrderDetail.Size = new System.Drawing.Size(1158, 217);
            this.dgvROrderDetail.TabIndex = 20;
            this.dgvROrderDetail.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvROrderDetail_CellClick);
            // 
            // lblOrderDetail
            // 
            this.lblOrderDetail.AutoSize = true;
            this.lblOrderDetail.Location = new System.Drawing.Point(8, 6);
            this.lblOrderDetail.Name = "lblOrderDetail";
            this.lblOrderDetail.Size = new System.Drawing.Size(53, 12);
            this.lblOrderDetail.TabIndex = 19;
            this.lblOrderDetail.Text = "상세내역";
            // 
            // pnlROrder
            // 
            this.pnlROrder.Controls.Add(this.btnDelete);
            this.pnlROrder.Controls.Add(this.btnAddOrder);
            this.pnlROrder.Controls.Add(this.btnRequestApprove);
            this.pnlROrder.Controls.Add(this.dgvROrder);
            this.pnlROrder.Controls.Add(this.btnUpdateOrder);
            this.pnlROrder.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlROrder.Location = new System.Drawing.Point(3, 3);
            this.pnlROrder.Name = "pnlROrder";
            this.pnlROrder.Size = new System.Drawing.Size(1164, 208);
            this.pnlROrder.TabIndex = 8;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(153, 3);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(70, 23);
            this.btnDelete.TabIndex = 8;
            this.btnDelete.Text = "삭제";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAddOrder
            // 
            this.btnAddOrder.Location = new System.Drawing.Point(3, 3);
            this.btnAddOrder.Name = "btnAddOrder";
            this.btnAddOrder.Size = new System.Drawing.Size(70, 23);
            this.btnAddOrder.TabIndex = 0;
            this.btnAddOrder.Text = "추가";
            this.btnAddOrder.UseVisualStyleBackColor = true;
            this.btnAddOrder.Click += new System.EventHandler(this.btnAddOrder_Click);
            // 
            // btnRequestApprove
            // 
            this.btnRequestApprove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRequestApprove.Location = new System.Drawing.Point(1091, 3);
            this.btnRequestApprove.Name = "btnRequestApprove";
            this.btnRequestApprove.Size = new System.Drawing.Size(70, 23);
            this.btnRequestApprove.TabIndex = 7;
            this.btnRequestApprove.Text = "승인요청";
            this.btnRequestApprove.UseVisualStyleBackColor = true;
            this.btnRequestApprove.Click += new System.EventHandler(this.btnRequestApprove_Click);
            // 
            // dgvROrder
            // 
            this.dgvROrder.AllowUserToAddRows = false;
            this.dgvROrder.AllowUserToDeleteRows = false;
            this.dgvROrder.AllowUserToResizeRows = false;
            this.dgvROrder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvROrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvROrder.Location = new System.Drawing.Point(3, 32);
            this.dgvROrder.MultiSelect = false;
            this.dgvROrder.Name = "dgvROrder";
            this.dgvROrder.RowHeadersVisible = false;
            this.dgvROrder.RowTemplate.Height = 23;
            this.dgvROrder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvROrder.Size = new System.Drawing.Size(1158, 173);
            this.dgvROrder.TabIndex = 6;
            this.dgvROrder.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvROrder_CellEndEdit);
            this.dgvROrder.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvROrder_RowEnter);
            // 
            // btnUpdateOrder
            // 
            this.btnUpdateOrder.Location = new System.Drawing.Point(78, 3);
            this.btnUpdateOrder.Name = "btnUpdateOrder";
            this.btnUpdateOrder.Size = new System.Drawing.Size(70, 23);
            this.btnUpdateOrder.TabIndex = 1;
            this.btnUpdateOrder.Text = "수정";
            this.btnUpdateOrder.UseVisualStyleBackColor = true;
            this.btnUpdateOrder.Click += new System.EventHandler(this.btnUpdateOrder_Click);
            // 
            // tpApproveOrder
            // 
            this.tpApproveOrder.Controls.Add(this.pnlAOrderBody);
            this.tpApproveOrder.Location = new System.Drawing.Point(4, 22);
            this.tpApproveOrder.Name = "tpApproveOrder";
            this.tpApproveOrder.Padding = new System.Windows.Forms.Padding(3);
            this.tpApproveOrder.Size = new System.Drawing.Size(1170, 484);
            this.tpApproveOrder.TabIndex = 0;
            this.tpApproveOrder.Text = "결재";
            this.tpApproveOrder.UseVisualStyleBackColor = true;
            // 
            // pnlAOrderBody
            // 
            this.pnlAOrderBody.Controls.Add(this.pnlAOrderDetail);
            this.pnlAOrderBody.Controls.Add(this.splitter2);
            this.pnlAOrderBody.Controls.Add(this.pnlAOrder);
            this.pnlAOrderBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlAOrderBody.Location = new System.Drawing.Point(3, 3);
            this.pnlAOrderBody.Name = "pnlAOrderBody";
            this.pnlAOrderBody.Size = new System.Drawing.Size(1164, 478);
            this.pnlAOrderBody.TabIndex = 11;
            // 
            // pnlAOrderDetail
            // 
            this.pnlAOrderDetail.Controls.Add(this.txtRQTotalVAT);
            this.pnlAOrderDetail.Controls.Add(this.label2);
            this.pnlAOrderDetail.Controls.Add(this.label4);
            this.pnlAOrderDetail.Controls.Add(this.txtRQGrandTotal);
            this.pnlAOrderDetail.Controls.Add(this.txtRQTotal);
            this.pnlAOrderDetail.Controls.Add(this.label5);
            this.pnlAOrderDetail.Controls.Add(this.dgvRQOrderDetail);
            this.pnlAOrderDetail.Controls.Add(this.label3);
            this.pnlAOrderDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlAOrderDetail.Location = new System.Drawing.Point(0, 214);
            this.pnlAOrderDetail.Name = "pnlAOrderDetail";
            this.pnlAOrderDetail.Size = new System.Drawing.Size(1164, 264);
            this.pnlAOrderDetail.TabIndex = 12;
            // 
            // txtRQTotalVAT
            // 
            this.txtRQTotalVAT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRQTotalVAT.Enabled = false;
            this.txtRQTotalVAT.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtRQTotalVAT.Location = new System.Drawing.Point(924, 238);
            this.txtRQTotalVAT.Name = "txtRQTotalVAT";
            this.txtRQTotalVAT.Size = new System.Drawing.Size(85, 21);
            this.txtRQTotalVAT.TabIndex = 58;
            this.txtRQTotalVAT.Text = "0";
            this.txtRQTotalVAT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(865, 241);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 57;
            this.label2.Text = "VAT합계";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1015, 241);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 56;
            this.label4.Text = "지급총액";
            // 
            // txtRQGrandTotal
            // 
            this.txtRQGrandTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRQGrandTotal.Enabled = false;
            this.txtRQGrandTotal.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtRQGrandTotal.Location = new System.Drawing.Point(1074, 238);
            this.txtRQGrandTotal.Name = "txtRQGrandTotal";
            this.txtRQGrandTotal.Size = new System.Drawing.Size(85, 21);
            this.txtRQGrandTotal.TabIndex = 55;
            this.txtRQGrandTotal.Text = "0";
            this.txtRQGrandTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtRQTotal
            // 
            this.txtRQTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRQTotal.Enabled = false;
            this.txtRQTotal.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtRQTotal.Location = new System.Drawing.Point(774, 238);
            this.txtRQTotal.Name = "txtRQTotal";
            this.txtRQTotal.Size = new System.Drawing.Size(85, 21);
            this.txtRQTotal.TabIndex = 54;
            this.txtRQTotal.Text = "0";
            this.txtRQTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(739, 241);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 12);
            this.label5.TabIndex = 53;
            this.label5.Text = "합계";
            // 
            // dgvRQOrderDetail
            // 
            this.dgvRQOrderDetail.AllowUserToAddRows = false;
            this.dgvRQOrderDetail.AllowUserToDeleteRows = false;
            this.dgvRQOrderDetail.AllowUserToResizeRows = false;
            this.dgvRQOrderDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvRQOrderDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRQOrderDetail.Location = new System.Drawing.Point(3, 21);
            this.dgvRQOrderDetail.MultiSelect = false;
            this.dgvRQOrderDetail.Name = "dgvRQOrderDetail";
            this.dgvRQOrderDetail.ReadOnly = true;
            this.dgvRQOrderDetail.RowHeadersVisible = false;
            this.dgvRQOrderDetail.RowTemplate.Height = 23;
            this.dgvRQOrderDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvRQOrderDetail.Size = new System.Drawing.Size(1158, 211);
            this.dgvRQOrderDetail.TabIndex = 20;
            this.dgvRQOrderDetail.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRQOrderDetail_CellClick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 19;
            this.label3.Text = "상세내역";
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.Color.Gray;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter2.Location = new System.Drawing.Point(0, 211);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(1164, 3);
            this.splitter2.TabIndex = 11;
            this.splitter2.TabStop = false;
            // 
            // pnlAOrder
            // 
            this.pnlAOrder.Controls.Add(this.btnProcApprove);
            this.pnlAOrder.Controls.Add(this.dgvRQOrder);
            this.pnlAOrder.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlAOrder.Location = new System.Drawing.Point(0, 0);
            this.pnlAOrder.Name = "pnlAOrder";
            this.pnlAOrder.Size = new System.Drawing.Size(1164, 211);
            this.pnlAOrder.TabIndex = 10;
            // 
            // btnProcApprove
            // 
            this.btnProcApprove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnProcApprove.Location = new System.Drawing.Point(1091, 3);
            this.btnProcApprove.Name = "btnProcApprove";
            this.btnProcApprove.Size = new System.Drawing.Size(70, 23);
            this.btnProcApprove.TabIndex = 7;
            this.btnProcApprove.Text = "승인처리";
            this.btnProcApprove.UseVisualStyleBackColor = true;
            this.btnProcApprove.Click += new System.EventHandler(this.btnProcApprove_Click);
            // 
            // dgvRQOrder
            // 
            this.dgvRQOrder.AllowUserToAddRows = false;
            this.dgvRQOrder.AllowUserToDeleteRows = false;
            this.dgvRQOrder.AllowUserToResizeRows = false;
            this.dgvRQOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvRQOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRQOrder.Location = new System.Drawing.Point(3, 32);
            this.dgvRQOrder.MultiSelect = false;
            this.dgvRQOrder.Name = "dgvRQOrder";
            this.dgvRQOrder.RowHeadersVisible = false;
            this.dgvRQOrder.RowTemplate.Height = 23;
            this.dgvRQOrder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvRQOrder.Size = new System.Drawing.Size(1158, 176);
            this.dgvRQOrder.TabIndex = 6;
            this.dgvRQOrder.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRQOrder_CellEndEdit);
            this.dgvRQOrder.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRQOrder_RowEnter);
            // 
            // tpHistory
            // 
            this.tpHistory.Controls.Add(this.pnlVOrderBody);
            this.tpHistory.Location = new System.Drawing.Point(4, 22);
            this.tpHistory.Name = "tpHistory";
            this.tpHistory.Size = new System.Drawing.Size(1170, 484);
            this.tpHistory.TabIndex = 2;
            this.tpHistory.Text = "열람";
            this.tpHistory.UseVisualStyleBackColor = true;
            // 
            // pnlVOrderBody
            // 
            this.pnlVOrderBody.Controls.Add(this.pnlVOrderDetail);
            this.pnlVOrderBody.Controls.Add(this.splitter3);
            this.pnlVOrderBody.Controls.Add(this.pnlVOrder);
            this.pnlVOrderBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlVOrderBody.Location = new System.Drawing.Point(0, 0);
            this.pnlVOrderBody.Name = "pnlVOrderBody";
            this.pnlVOrderBody.Size = new System.Drawing.Size(1170, 484);
            this.pnlVOrderBody.TabIndex = 12;
            // 
            // pnlVOrderDetail
            // 
            this.pnlVOrderDetail.Controls.Add(this.txtVTotalVAT);
            this.pnlVOrderDetail.Controls.Add(this.label6);
            this.pnlVOrderDetail.Controls.Add(this.label7);
            this.pnlVOrderDetail.Controls.Add(this.txtVGrandTotal);
            this.pnlVOrderDetail.Controls.Add(this.txtVTotal);
            this.pnlVOrderDetail.Controls.Add(this.label8);
            this.pnlVOrderDetail.Controls.Add(this.dgvVOrderDetail);
            this.pnlVOrderDetail.Controls.Add(this.label1);
            this.pnlVOrderDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlVOrderDetail.Location = new System.Drawing.Point(0, 214);
            this.pnlVOrderDetail.Name = "pnlVOrderDetail";
            this.pnlVOrderDetail.Size = new System.Drawing.Size(1170, 270);
            this.pnlVOrderDetail.TabIndex = 12;
            // 
            // txtVTotalVAT
            // 
            this.txtVTotalVAT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVTotalVAT.Enabled = false;
            this.txtVTotalVAT.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtVTotalVAT.Location = new System.Drawing.Point(927, 241);
            this.txtVTotalVAT.Name = "txtVTotalVAT";
            this.txtVTotalVAT.Size = new System.Drawing.Size(85, 21);
            this.txtVTotalVAT.TabIndex = 58;
            this.txtVTotalVAT.Text = "0";
            this.txtVTotalVAT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(868, 244);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 12);
            this.label6.TabIndex = 57;
            this.label6.Text = "VAT합계";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(1018, 244);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 56;
            this.label7.Text = "지급총액";
            // 
            // txtVGrandTotal
            // 
            this.txtVGrandTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVGrandTotal.Enabled = false;
            this.txtVGrandTotal.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtVGrandTotal.Location = new System.Drawing.Point(1077, 241);
            this.txtVGrandTotal.Name = "txtVGrandTotal";
            this.txtVGrandTotal.Size = new System.Drawing.Size(85, 21);
            this.txtVGrandTotal.TabIndex = 55;
            this.txtVGrandTotal.Text = "0";
            this.txtVGrandTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtVTotal
            // 
            this.txtVTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVTotal.Enabled = false;
            this.txtVTotal.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtVTotal.Location = new System.Drawing.Point(777, 241);
            this.txtVTotal.Name = "txtVTotal";
            this.txtVTotal.Size = new System.Drawing.Size(85, 21);
            this.txtVTotal.TabIndex = 54;
            this.txtVTotal.Text = "0";
            this.txtVTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(742, 244);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 53;
            this.label8.Text = "합계";
            // 
            // dgvVOrderDetail
            // 
            this.dgvVOrderDetail.AllowUserToAddRows = false;
            this.dgvVOrderDetail.AllowUserToDeleteRows = false;
            this.dgvVOrderDetail.AllowUserToResizeRows = false;
            this.dgvVOrderDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvVOrderDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVOrderDetail.Location = new System.Drawing.Point(3, 21);
            this.dgvVOrderDetail.MultiSelect = false;
            this.dgvVOrderDetail.Name = "dgvVOrderDetail";
            this.dgvVOrderDetail.RowHeadersVisible = false;
            this.dgvVOrderDetail.RowTemplate.Height = 23;
            this.dgvVOrderDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvVOrderDetail.Size = new System.Drawing.Size(1164, 214);
            this.dgvVOrderDetail.TabIndex = 20;
            this.dgvVOrderDetail.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvVOrderDetail_CellClick);
            this.dgvVOrderDetail.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvVOrderDetail_CellEndEdit);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 12);
            this.label1.TabIndex = 19;
            this.label1.Text = "상세내역";
            // 
            // splitter3
            // 
            this.splitter3.BackColor = System.Drawing.Color.Gray;
            this.splitter3.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter3.Location = new System.Drawing.Point(0, 211);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(1170, 3);
            this.splitter3.TabIndex = 11;
            this.splitter3.TabStop = false;
            // 
            // pnlVOrder
            // 
            this.pnlVOrder.Controls.Add(this.btnCancelApprove);
            this.pnlVOrder.Controls.Add(this.btnExcel);
            this.pnlVOrder.Controls.Add(this.btnReorder);
            this.pnlVOrder.Controls.Add(this.btnSendMail);
            this.pnlVOrder.Controls.Add(this.dgvVOrder);
            this.pnlVOrder.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlVOrder.Location = new System.Drawing.Point(0, 0);
            this.pnlVOrder.Name = "pnlVOrder";
            this.pnlVOrder.Size = new System.Drawing.Size(1170, 211);
            this.pnlVOrder.TabIndex = 10;
            // 
            // btnCancelApprove
            // 
            this.btnCancelApprove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelApprove.BackColor = System.Drawing.Color.Red;
            this.btnCancelApprove.ForeColor = System.Drawing.Color.White;
            this.btnCancelApprove.Location = new System.Drawing.Point(803, 3);
            this.btnCancelApprove.Name = "btnCancelApprove";
            this.btnCancelApprove.Size = new System.Drawing.Size(101, 23);
            this.btnCancelApprove.TabIndex = 11;
            this.btnCancelApprove.Text = "승인요청 취소";
            this.btnCancelApprove.UseVisualStyleBackColor = false;
            this.btnCancelApprove.Click += new System.EventHandler(this.btnCancelApprove_Click);
            // 
            // btnExcel
            // 
            this.btnExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExcel.Location = new System.Drawing.Point(936, 3);
            this.btnExcel.Name = "btnExcel";
            this.btnExcel.Size = new System.Drawing.Size(79, 23);
            this.btnExcel.TabIndex = 10;
            this.btnExcel.Text = "발주서 확인";
            this.btnExcel.UseVisualStyleBackColor = true;
            this.btnExcel.Click += new System.EventHandler(this.btnExcel_Click);
            // 
            // btnReorder
            // 
            this.btnReorder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReorder.Location = new System.Drawing.Point(1097, 3);
            this.btnReorder.Name = "btnReorder";
            this.btnReorder.Size = new System.Drawing.Size(70, 23);
            this.btnReorder.TabIndex = 9;
            this.btnReorder.Text = "재발주";
            this.btnReorder.UseVisualStyleBackColor = true;
            this.btnReorder.Click += new System.EventHandler(this.btnReorder_Click);
            // 
            // btnSendMail
            // 
            this.btnSendMail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSendMail.Location = new System.Drawing.Point(1021, 3);
            this.btnSendMail.Name = "btnSendMail";
            this.btnSendMail.Size = new System.Drawing.Size(70, 23);
            this.btnSendMail.TabIndex = 8;
            this.btnSendMail.Text = "메일전송";
            this.btnSendMail.UseVisualStyleBackColor = true;
            this.btnSendMail.Click += new System.EventHandler(this.btnSendMail_Click);
            // 
            // dgvVOrder
            // 
            this.dgvVOrder.AllowUserToAddRows = false;
            this.dgvVOrder.AllowUserToDeleteRows = false;
            this.dgvVOrder.AllowUserToResizeRows = false;
            this.dgvVOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvVOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVOrder.Location = new System.Drawing.Point(3, 32);
            this.dgvVOrder.MultiSelect = false;
            this.dgvVOrder.Name = "dgvVOrder";
            this.dgvVOrder.RowHeadersVisible = false;
            this.dgvVOrder.RowTemplate.Height = 23;
            this.dgvVOrder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVOrder.Size = new System.Drawing.Size(1164, 176);
            this.dgvVOrder.TabIndex = 6;
            this.dgvVOrder.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvVOrder_CellEndEdit);
            this.dgvVOrder.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvVOrder_RowEnter);
            // 
            // pnlManagementTop
            // 
            this.pnlManagementTop.Controls.Add(this.btnPartList);
            this.pnlManagementTop.Controls.Add(this.btnInit);
            this.pnlManagementTop.Controls.Add(this.gbBasicSearch);
            this.pnlManagementTop.Controls.Add(this.gbCondition);
            this.pnlManagementTop.Controls.Add(this.btnSearch);
            this.pnlManagementTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlManagementTop.Location = new System.Drawing.Point(0, 0);
            this.pnlManagementTop.Name = "pnlManagementTop";
            this.pnlManagementTop.Size = new System.Drawing.Size(1178, 118);
            this.pnlManagementTop.TabIndex = 5;
            // 
            // btnPartList
            // 
            this.btnPartList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPartList.BackColor = System.Drawing.Color.YellowGreen;
            this.btnPartList.Location = new System.Drawing.Point(1101, 89);
            this.btnPartList.Name = "btnPartList";
            this.btnPartList.Size = new System.Drawing.Size(70, 23);
            this.btnPartList.TabIndex = 39;
            this.btnPartList.Text = "품목 조회";
            this.btnPartList.UseVisualStyleBackColor = false;
            this.btnPartList.Click += new System.EventHandler(this.btnPartList_Click);
            // 
            // btnInit
            // 
            this.btnInit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnInit.Location = new System.Drawing.Point(988, 12);
            this.btnInit.Name = "btnInit";
            this.btnInit.Size = new System.Drawing.Size(87, 50);
            this.btnInit.TabIndex = 38;
            this.btnInit.Text = "검색 초기화";
            this.btnInit.UseVisualStyleBackColor = true;
            this.btnInit.Click += new System.EventHandler(this.btnInit_Click);
            // 
            // gbBasicSearch
            // 
            this.gbBasicSearch.Controls.Add(this.txtSCategorizationContent);
            this.gbBasicSearch.Controls.Add(this.lblReverseSearch);
            this.gbBasicSearch.Controls.Add(this.btnMakerClear);
            this.gbBasicSearch.Controls.Add(this.cmbSCategorization);
            this.gbBasicSearch.Controls.Add(this.lblStartDate);
            this.gbBasicSearch.Controls.Add(this.dtpickerStartDate);
            this.gbBasicSearch.Controls.Add(this.dtpickerEndDate);
            this.gbBasicSearch.Controls.Add(this.lblEndDate);
            this.gbBasicSearch.Controls.Add(this.lblFrom);
            this.gbBasicSearch.Controls.Add(this.txtMaker);
            this.gbBasicSearch.Controls.Add(this.btnMaker);
            this.gbBasicSearch.Controls.Add(this.lblMaker);
            this.gbBasicSearch.Controls.Add(this.dtpickerDuedate);
            this.gbBasicSearch.Location = new System.Drawing.Point(7, 9);
            this.gbBasicSearch.Name = "gbBasicSearch";
            this.gbBasicSearch.Size = new System.Drawing.Size(610, 103);
            this.gbBasicSearch.TabIndex = 36;
            this.gbBasicSearch.TabStop = false;
            this.gbBasicSearch.Text = "기본 검색";
            // 
            // txtSCategorizationContent
            // 
            this.txtSCategorizationContent.Location = new System.Drawing.Point(416, 32);
            this.txtSCategorizationContent.Name = "txtSCategorizationContent";
            this.txtSCategorizationContent.Size = new System.Drawing.Size(174, 21);
            this.txtSCategorizationContent.TabIndex = 57;
            this.txtSCategorizationContent.Visible = false;
            // 
            // lblReverseSearch
            // 
            this.lblReverseSearch.AutoSize = true;
            this.lblReverseSearch.Location = new System.Drawing.Point(340, 17);
            this.lblReverseSearch.Name = "lblReverseSearch";
            this.lblReverseSearch.Size = new System.Drawing.Size(41, 12);
            this.lblReverseSearch.TabIndex = 56;
            this.lblReverseSearch.Text = "역조사";
            this.lblReverseSearch.Visible = false;
            // 
            // btnMakerClear
            // 
            this.btnMakerClear.Location = new System.Drawing.Point(277, 71);
            this.btnMakerClear.Name = "btnMakerClear";
            this.btnMakerClear.Size = new System.Drawing.Size(59, 23);
            this.btnMakerClear.TabIndex = 39;
            this.btnMakerClear.Text = "Clear";
            this.btnMakerClear.UseVisualStyleBackColor = true;
            this.btnMakerClear.Click += new System.EventHandler(this.btnMakerClear_Click);
            // 
            // cmbSCategorization
            // 
            this.cmbSCategorization.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSCategorization.FormattingEnabled = true;
            this.cmbSCategorization.Location = new System.Drawing.Point(342, 32);
            this.cmbSCategorization.Name = "cmbSCategorization";
            this.cmbSCategorization.Size = new System.Drawing.Size(68, 20);
            this.cmbSCategorization.TabIndex = 48;
            this.cmbSCategorization.Visible = false;
            this.cmbSCategorization.SelectedIndexChanged += new System.EventHandler(this.cmbReverse_SelectedIndexChanged);
            // 
            // lblStartDate
            // 
            this.lblStartDate.AutoSize = true;
            this.lblStartDate.Location = new System.Drawing.Point(8, 17);
            this.lblStartDate.Name = "lblStartDate";
            this.lblStartDate.Size = new System.Drawing.Size(53, 12);
            this.lblStartDate.TabIndex = 18;
            this.lblStartDate.Text = "시작일자";
            // 
            // dtpickerStartDate
            // 
            this.dtpickerStartDate.CustomFormat = "";
            this.dtpickerStartDate.Location = new System.Drawing.Point(10, 32);
            this.dtpickerStartDate.MaxDate = new System.DateTime(9998, 12, 1, 0, 0, 0, 0);
            this.dtpickerStartDate.Name = "dtpickerStartDate";
            this.dtpickerStartDate.Size = new System.Drawing.Size(150, 21);
            this.dtpickerStartDate.TabIndex = 16;
            this.dtpickerStartDate.Value = new System.DateTime(2013, 11, 11, 0, 0, 0, 0);
            // 
            // dtpickerEndDate
            // 
            this.dtpickerEndDate.CustomFormat = "";
            this.dtpickerEndDate.Location = new System.Drawing.Point(186, 32);
            this.dtpickerEndDate.MaxDate = new System.DateTime(9998, 12, 1, 0, 0, 0, 0);
            this.dtpickerEndDate.Name = "dtpickerEndDate";
            this.dtpickerEndDate.Size = new System.Drawing.Size(150, 21);
            this.dtpickerEndDate.TabIndex = 17;
            this.dtpickerEndDate.Value = new System.DateTime(2013, 11, 19, 0, 0, 0, 0);
            // 
            // lblEndDate
            // 
            this.lblEndDate.AutoSize = true;
            this.lblEndDate.Location = new System.Drawing.Point(184, 17);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(53, 12);
            this.lblEndDate.TabIndex = 19;
            this.lblEndDate.Text = "종료일자";
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(166, 38);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(14, 12);
            this.lblFrom.TabIndex = 20;
            this.lblFrom.Text = "~";
            // 
            // txtMaker
            // 
            this.txtMaker.Enabled = false;
            this.txtMaker.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMaker.Location = new System.Drawing.Point(10, 71);
            this.txtMaker.Name = "txtMaker";
            this.txtMaker.Size = new System.Drawing.Size(213, 21);
            this.txtMaker.TabIndex = 37;
            // 
            // btnMaker
            // 
            this.btnMaker.Location = new System.Drawing.Point(229, 71);
            this.btnMaker.Name = "btnMaker";
            this.btnMaker.Size = new System.Drawing.Size(42, 23);
            this.btnMaker.TabIndex = 38;
            this.btnMaker.Text = "..";
            this.btnMaker.UseVisualStyleBackColor = true;
            this.btnMaker.Click += new System.EventHandler(this.btnMaker_Click);
            // 
            // lblMaker
            // 
            this.lblMaker.AutoSize = true;
            this.lblMaker.Location = new System.Drawing.Point(8, 56);
            this.lblMaker.Name = "lblMaker";
            this.lblMaker.Size = new System.Drawing.Size(41, 12);
            this.lblMaker.TabIndex = 36;
            this.lblMaker.Text = "거래처";
            // 
            // dtpickerDuedate
            // 
            this.dtpickerDuedate.CustomFormat = "";
            this.dtpickerDuedate.Location = new System.Drawing.Point(416, 32);
            this.dtpickerDuedate.MaxDate = new System.DateTime(9998, 12, 1, 0, 0, 0, 0);
            this.dtpickerDuedate.Name = "dtpickerDuedate";
            this.dtpickerDuedate.Size = new System.Drawing.Size(174, 21);
            this.dtpickerDuedate.TabIndex = 55;
            this.dtpickerDuedate.Value = new System.DateTime(2013, 11, 11, 0, 0, 0, 0);
            this.dtpickerDuedate.Visible = false;
            // 
            // gbCondition
            // 
            this.gbCondition.Controls.Add(this.cmbUserID);
            this.gbCondition.Controls.Add(this.lblSEditer);
            this.gbCondition.Controls.Add(this.cmbSStatus);
            this.gbCondition.Controls.Add(this.lblSStatus);
            this.gbCondition.Controls.Add(this.txtSOrderNumber);
            this.gbCondition.Controls.Add(this.lblSOrderNumber);
            this.gbCondition.Location = new System.Drawing.Point(623, 9);
            this.gbCondition.Name = "gbCondition";
            this.gbCondition.Size = new System.Drawing.Size(332, 59);
            this.gbCondition.TabIndex = 37;
            this.gbCondition.TabStop = false;
            this.gbCondition.Text = "조건 검색";
            this.gbCondition.Visible = false;
            // 
            // cmbUserID
            // 
            this.cmbUserID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUserID.FormattingEnabled = true;
            this.cmbUserID.Location = new System.Drawing.Point(220, 30);
            this.cmbUserID.Name = "cmbUserID";
            this.cmbUserID.Size = new System.Drawing.Size(100, 20);
            this.cmbUserID.TabIndex = 46;
            this.cmbUserID.SelectedIndexChanged += new System.EventHandler(this.cmbUserID_SelectedIndexChanged);
            // 
            // lblSEditer
            // 
            this.lblSEditer.AutoSize = true;
            this.lblSEditer.Location = new System.Drawing.Point(218, 14);
            this.lblSEditer.Name = "lblSEditer";
            this.lblSEditer.Size = new System.Drawing.Size(41, 12);
            this.lblSEditer.TabIndex = 45;
            this.lblSEditer.Text = "담당자";
            // 
            // cmbSStatus
            // 
            this.cmbSStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSStatus.FormattingEnabled = true;
            this.cmbSStatus.Location = new System.Drawing.Point(114, 30);
            this.cmbSStatus.Name = "cmbSStatus";
            this.cmbSStatus.Size = new System.Drawing.Size(100, 20);
            this.cmbSStatus.TabIndex = 44;
            this.cmbSStatus.SelectedIndexChanged += new System.EventHandler(this.cmbSStatus_SelectedIndexChanged);
            // 
            // lblSStatus
            // 
            this.lblSStatus.AutoSize = true;
            this.lblSStatus.Location = new System.Drawing.Point(112, 15);
            this.lblSStatus.Name = "lblSStatus";
            this.lblSStatus.Size = new System.Drawing.Size(29, 12);
            this.lblSStatus.TabIndex = 43;
            this.lblSStatus.Text = "상태";
            // 
            // txtSOrderNumber
            // 
            this.txtSOrderNumber.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtSOrderNumber.Location = new System.Drawing.Point(6, 29);
            this.txtSOrderNumber.MaxLength = 10;
            this.txtSOrderNumber.Name = "txtSOrderNumber";
            this.txtSOrderNumber.Size = new System.Drawing.Size(100, 21);
            this.txtSOrderNumber.TabIndex = 40;
            this.txtSOrderNumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSOrderNumber_KeyDown);
            // 
            // lblSOrderNumber
            // 
            this.lblSOrderNumber.AutoSize = true;
            this.lblSOrderNumber.Location = new System.Drawing.Point(6, 17);
            this.lblSOrderNumber.Name = "lblSOrderNumber";
            this.lblSOrderNumber.Size = new System.Drawing.Size(53, 12);
            this.lblSOrderNumber.TabIndex = 39;
            this.lblSOrderNumber.Text = "발주번호";
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.Location = new System.Drawing.Point(1081, 12);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(87, 50);
            this.btnSearch.TabIndex = 29;
            this.btnSearch.Text = "검색";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // FormOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1178, 628);
            this.Controls.Add(this.pnlOrder);
            this.Controls.Add(this.pnlManagementTop);
            this.Name = "FormOrder";
            this.Text = "발주 관리";
            this.Load += new System.EventHandler(this.FormOrder_Load);
            this.SizeChanged += new System.EventHandler(this.FormOrder_SizeChanged);
            this.pnlOrder.ResumeLayout(false);
            this.tabOrderMagement.ResumeLayout(false);
            this.tpRegistryOrder.ResumeLayout(false);
            this.pnlROrderDetail.ResumeLayout(false);
            this.pnlROrderDetail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvROrderDetail)).EndInit();
            this.pnlROrder.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvROrder)).EndInit();
            this.tpApproveOrder.ResumeLayout(false);
            this.pnlAOrderBody.ResumeLayout(false);
            this.pnlAOrderDetail.ResumeLayout(false);
            this.pnlAOrderDetail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRQOrderDetail)).EndInit();
            this.pnlAOrder.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRQOrder)).EndInit();
            this.tpHistory.ResumeLayout(false);
            this.pnlVOrderBody.ResumeLayout(false);
            this.pnlVOrderDetail.ResumeLayout(false);
            this.pnlVOrderDetail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVOrderDetail)).EndInit();
            this.pnlVOrder.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvVOrder)).EndInit();
            this.pnlManagementTop.ResumeLayout(false);
            this.gbBasicSearch.ResumeLayout(false);
            this.gbBasicSearch.PerformLayout();
            this.gbCondition.ResumeLayout(false);
            this.gbCondition.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlOrder;
        private System.Windows.Forms.TabControl tabOrderMagement;
        private System.Windows.Forms.TabPage tpRegistryOrder;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel pnlROrderDetail;
        private System.Windows.Forms.DataGridView dgvROrderDetail;
        private System.Windows.Forms.Label lblOrderDetail;
        private System.Windows.Forms.Panel pnlROrder;
        private System.Windows.Forms.Button btnAddOrder;
        private System.Windows.Forms.Button btnRequestApprove;
        private System.Windows.Forms.DataGridView dgvROrder;
        private System.Windows.Forms.Button btnUpdateOrder;
        private System.Windows.Forms.TabPage tpApproveOrder;
        private System.Windows.Forms.Panel pnlManagementTop;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.Label lblEndDate;
        private System.Windows.Forms.DateTimePicker dtpickerEndDate;
        private System.Windows.Forms.Label lblStartDate;
        private System.Windows.Forms.DateTimePicker dtpickerStartDate;
        private System.Windows.Forms.TabPage tpHistory;
        private System.Windows.Forms.GroupBox gbBasicSearch;
        private System.Windows.Forms.GroupBox gbCondition;
        private System.Windows.Forms.ComboBox cmbUserID;
        private System.Windows.Forms.Label lblSEditer;
        private System.Windows.Forms.ComboBox cmbSStatus;
        private System.Windows.Forms.Label lblSStatus;
        private System.Windows.Forms.TextBox txtSOrderNumber;
        private System.Windows.Forms.Label lblSOrderNumber;
        private System.Windows.Forms.TextBox txtMaker;
        private System.Windows.Forms.Button btnMaker;
        private System.Windows.Forms.Label lblMaker;
        private System.Windows.Forms.ComboBox cmbSCategorization;
        private System.Windows.Forms.Panel pnlAOrderBody;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.Panel pnlAOrder;
        private System.Windows.Forms.Button btnProcApprove;
        private System.Windows.Forms.DataGridView dgvRQOrder;
        private System.Windows.Forms.Panel pnlAOrderDetail;
        private System.Windows.Forms.DataGridView dgvRQOrderDetail;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel pnlVOrderBody;
        private System.Windows.Forms.Panel pnlVOrderDetail;
        private System.Windows.Forms.DataGridView dgvVOrderDetail;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.Panel pnlVOrder;
        private System.Windows.Forms.Button btnSendMail;
        private System.Windows.Forms.DataGridView dgvVOrder;
        private System.Windows.Forms.DateTimePicker dtpickerDuedate;
        private System.Windows.Forms.TextBox txtRTotalVAT;
        private System.Windows.Forms.Label lblTotalVAT;
        private System.Windows.Forms.Label lblGrandTotal;
        private System.Windows.Forms.TextBox txtRGrandTotal;
        private System.Windows.Forms.TextBox txtRTotal;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.TextBox txtRQTotalVAT;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtRQGrandTotal;
        private System.Windows.Forms.TextBox txtRQTotal;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtVTotalVAT;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtVGrandTotal;
        private System.Windows.Forms.TextBox txtVTotal;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnReorder;
        private System.Windows.Forms.Button btnMakerClear;
        private System.Windows.Forms.Label lblReverseSearch;
        private System.Windows.Forms.TextBox txtSCategorizationContent;
        private System.Windows.Forms.Button btnInit;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnExcel;
        private System.Windows.Forms.Button btnPartList;
        private System.Windows.Forms.Button btnCancelApprove;
    }
}