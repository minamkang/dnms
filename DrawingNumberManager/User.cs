using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DrawingNumberManager
{
	public class User 
    {
		private bool logged_in;
        private string id;
		private string password;
		private string name;
		private char department_code;
		private int access_level;
		private int key;
        private string email;
        private string tel;
		
        public string ID
        {
            get { return(id); }
			set { if (!logged_in) id = value; }
        }

		public string PASSWORD
		{
			set { if (!logged_in) password = value; }
		}

		public string Name
		{
			get { return (name); }
			set { if (!logged_in) name = value; }
		}
        
        public char DepartmentCode
        {
            get { return(department_code); }
			set { if (!logged_in) department_code = value; }
        }
       
        public int Key
        {
            get { return (key); }
			set { if (!logged_in) key = value; }
        }

		public int AccessLevel
		{
			get { return (access_level); }
			set { if (!logged_in) access_level = value; }
		}

        public string EMAIL
        {
            get { return (email); }
            set { if (!logged_in) email = value; }
        }

        public string TEL
        {
            get { return (tel); }
            set { if (!logged_in) tel = value; }
        }


		public User()
		{
			logged_in = false;
		}


		private User(DataRow user)
		{
			key = int.Parse(user["USER_KEY"].ToString());
            StaticData.user_key = key;

			id = user["ID"].ToString();
			
            name = user["NAME"].ToString();
            StaticData.user_name = name;

			department_code = user["DEPARTMENT_CODE"].ToString()[0];
			access_level = int.Parse(user["ACCESS_LEVEL"].ToString());

            StaticData.user_mail = user["EMAIL"].ToString();
            StaticData.user_tel = user["TEL"].ToString();

			logged_in = true;
		}

		static public User Login(string strID, string pw)
		{
			DataTable result = new DataTable();
			SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC user_login '{0}', '{1}'", strID, pw), StaticData.db_connetion_string);

			if (query.Fill(result) == 1)
			{
				query.SelectCommand.CommandText = "EXEC user_infomation " + result.Rows[0]["USER_KEY"].ToString();
				result.Clear();
				query.Fill(result);
				return new User(result.Rows[0]);
			}
			else
			{
				return null;
			}
		}

		private static String passwordCRC32(string password)
		{
			Crc32 crc32 = new Crc32();
			String crc32Password = String.Empty;

			byte[] bPassword = Encoding.UTF8.GetBytes(password);
			foreach (byte b in crc32.ComputeHash(bPassword))
			{
				crc32Password += b.ToString("x2").ToUpper();
			}
			return crc32Password;
		}

		static public bool Create(User new_user)
		{
			DataTable result = new DataTable();
			SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC user_create '{0}', '{1}', '{2}', '{3}', '{4}'",
				new_user.id, new_user.name, passwordCRC32(new_user.password), new_user.department_code, new_user.access_level), StaticData.db_connetion_string);
			query.Fill(result);
			return true;
		}

		static public bool Modify(User modify_user, bool revisePW)
		{
			string password = "";
			if (revisePW == false)
				password = modify_user.password;
			else
				password = passwordCRC32(modify_user.password);

			DataTable result = new DataTable();
			SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC user_modify '{0}', '{1}', '{2}', '{3}', '{4}'",
                modify_user.id, modify_user.name, password, modify_user.department_code, modify_user.access_level), StaticData.db_connetion_string);
			query.Fill(result);
			return true;
		}

		static public bool ModifyPassword(string user_id, string ModifyPW)
		{
			DataTable result = new DataTable();
            SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC user_password_modify '{0}', '{1}'", user_id, ModifyPW), StaticData.db_connetion_string);
			query.Fill(result);
			return true;
		}


		static public bool Remove(User remove_user)
		{
			DataTable result = new DataTable();
            SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC user_remove '{0}'", remove_user.id), StaticData.db_connetion_string);
			query.Fill(result);
			return true;
		}
	}

}