﻿namespace DrawingNumberManager
{
    partial class FormPublishOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlLeft = new System.Windows.Forms.Panel();
            this.pnlPartList = new System.Windows.Forms.Panel();
            this.dgvPartList = new System.Windows.Forms.DataGridView();
            this.pnlSearchPartList = new System.Windows.Forms.Panel();
            this.btnSearch = new System.Windows.Forms.Button();
            this.gbCondition = new System.Windows.Forms.GroupBox();
            this.lblPartName = new System.Windows.Forms.Label();
            this.txtPartName = new System.Windows.Forms.TextBox();
            this.cmbUserID = new System.Windows.Forms.ComboBox();
            this.cmbModel = new System.Windows.Forms.ComboBox();
            this.lblNote = new System.Windows.Forms.Label();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.lblModel = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblPart = new System.Windows.Forms.Label();
            this.cmbPart = new System.Windows.Forms.ComboBox();
            this.gbBasicSearch = new System.Windows.Forms.GroupBox();
            this.txtDrawingNumber = new System.Windows.Forms.TextBox();
            this.lblDrawingNumber = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.lblEndDate = new System.Windows.Forms.Label();
            this.dtpickerEndDate = new System.Windows.Forms.DateTimePicker();
            this.lblStartDate = new System.Windows.Forms.Label();
            this.dtpickerStartDate = new System.Windows.Forms.DateTimePicker();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.pnlBody = new System.Windows.Forms.Panel();
            this.pnlBodyOrder = new System.Windows.Forms.Panel();
            this.gbOrderDetail = new System.Windows.Forms.GroupBox();
            this.txtTotalVAT = new System.Windows.Forms.TextBox();
            this.lblTotalVAT = new System.Windows.Forms.Label();
            this.lblGrandTotal = new System.Windows.Forms.Label();
            this.txtGrandTotal = new System.Windows.Forms.TextBox();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.lblTotal = new System.Windows.Forms.Label();
            this.dgvOrderDetail = new System.Windows.Forms.DataGridView();
            this.cmsOrderDetail = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiAddRow = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDeleteRow = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlBodyLeft = new System.Windows.Forms.Panel();
            this.btnPartToOrder = new System.Windows.Forms.Button();
            this.pnlBodyTop = new System.Windows.Forms.Panel();
            this.gpOrder = new System.Windows.Forms.GroupBox();
            this.txtMakerMessage = new System.Windows.Forms.TextBox();
            this.cmbApprover = new System.Windows.Forms.ComboBox();
            this.lblMakerMessage = new System.Windows.Forms.Label();
            this.lblApprover = new System.Windows.Forms.Label();
            this.cmbVAT = new System.Windows.Forms.ComboBox();
            this.lblVAT = new System.Windows.Forms.Label();
            this.btnMaker = new System.Windows.Forms.Button();
            this.txtMaker = new System.Windows.Forms.TextBox();
            this.txtOrderNumber = new System.Windows.Forms.TextBox();
            this.lblOrderNumber = new System.Windows.Forms.Label();
            this.lblMaker = new System.Windows.Forms.Label();
            this.btnCreatedOrder = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.pnlRightBottom = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.pnlLeft.SuspendLayout();
            this.pnlPartList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPartList)).BeginInit();
            this.pnlSearchPartList.SuspendLayout();
            this.gbCondition.SuspendLayout();
            this.gbBasicSearch.SuspendLayout();
            this.pnlBody.SuspendLayout();
            this.pnlBodyOrder.SuspendLayout();
            this.gbOrderDetail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrderDetail)).BeginInit();
            this.cmsOrderDetail.SuspendLayout();
            this.pnlBodyLeft.SuspendLayout();
            this.pnlBodyTop.SuspendLayout();
            this.gpOrder.SuspendLayout();
            this.pnlRightBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlLeft
            // 
            this.pnlLeft.Controls.Add(this.pnlPartList);
            this.pnlLeft.Controls.Add(this.pnlSearchPartList);
            this.pnlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlLeft.Location = new System.Drawing.Point(0, 0);
            this.pnlLeft.Name = "pnlLeft";
            this.pnlLeft.Size = new System.Drawing.Size(787, 628);
            this.pnlLeft.TabIndex = 0;
            // 
            // pnlPartList
            // 
            this.pnlPartList.Controls.Add(this.dgvPartList);
            this.pnlPartList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPartList.Location = new System.Drawing.Point(0, 122);
            this.pnlPartList.Name = "pnlPartList";
            this.pnlPartList.Size = new System.Drawing.Size(787, 506);
            this.pnlPartList.TabIndex = 1;
            // 
            // dgvPartList
            // 
            this.dgvPartList.AllowUserToAddRows = false;
            this.dgvPartList.AllowUserToDeleteRows = false;
            this.dgvPartList.AllowUserToResizeRows = false;
            this.dgvPartList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPartList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPartList.Location = new System.Drawing.Point(0, 0);
            this.dgvPartList.Name = "dgvPartList";
            this.dgvPartList.ReadOnly = true;
            this.dgvPartList.RowHeadersVisible = false;
            this.dgvPartList.RowTemplate.Height = 23;
            this.dgvPartList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPartList.Size = new System.Drawing.Size(787, 506);
            this.dgvPartList.TabIndex = 0;
            // 
            // pnlSearchPartList
            // 
            this.pnlSearchPartList.Controls.Add(this.btnSearch);
            this.pnlSearchPartList.Controls.Add(this.gbCondition);
            this.pnlSearchPartList.Controls.Add(this.gbBasicSearch);
            this.pnlSearchPartList.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSearchPartList.Location = new System.Drawing.Point(0, 0);
            this.pnlSearchPartList.Name = "pnlSearchPartList";
            this.pnlSearchPartList.Size = new System.Drawing.Size(787, 122);
            this.pnlSearchPartList.TabIndex = 0;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(698, 10);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(80, 49);
            this.btnSearch.TabIndex = 53;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // gbCondition
            // 
            this.gbCondition.Controls.Add(this.lblPartName);
            this.gbCondition.Controls.Add(this.txtPartName);
            this.gbCondition.Controls.Add(this.cmbUserID);
            this.gbCondition.Controls.Add(this.cmbModel);
            this.gbCondition.Controls.Add(this.lblNote);
            this.gbCondition.Controls.Add(this.txtNote);
            this.gbCondition.Controls.Add(this.lblModel);
            this.gbCondition.Controls.Add(this.lblName);
            this.gbCondition.Controls.Add(this.lblPart);
            this.gbCondition.Controls.Add(this.cmbPart);
            this.gbCondition.Location = new System.Drawing.Point(350, 3);
            this.gbCondition.Name = "gbCondition";
            this.gbCondition.Size = new System.Drawing.Size(342, 110);
            this.gbCondition.TabIndex = 52;
            this.gbCondition.TabStop = false;
            this.gbCondition.Text = "조건검색";
            // 
            // lblPartName
            // 
            this.lblPartName.AutoSize = true;
            this.lblPartName.Location = new System.Drawing.Point(218, 17);
            this.lblPartName.Name = "lblPartName";
            this.lblPartName.Size = new System.Drawing.Size(65, 12);
            this.lblPartName.TabIndex = 32;
            this.lblPartName.Text = "Part Name";
            // 
            // txtPartName
            // 
            this.txtPartName.Location = new System.Drawing.Point(220, 36);
            this.txtPartName.MaxLength = 500;
            this.txtPartName.Name = "txtPartName";
            this.txtPartName.Size = new System.Drawing.Size(116, 21);
            this.txtPartName.TabIndex = 33;
            this.txtPartName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPartName_KeyDown);
            // 
            // cmbUserID
            // 
            this.cmbUserID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUserID.FormattingEnabled = true;
            this.cmbUserID.Location = new System.Drawing.Point(7, 81);
            this.cmbUserID.Name = "cmbUserID";
            this.cmbUserID.Size = new System.Drawing.Size(92, 20);
            this.cmbUserID.TabIndex = 28;
            this.cmbUserID.SelectedIndexChanged += new System.EventHandler(this.cmbUserID_SelectedIndexChanged);
            // 
            // cmbModel
            // 
            this.cmbModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbModel.FormattingEnabled = true;
            this.cmbModel.Location = new System.Drawing.Point(7, 36);
            this.cmbModel.Name = "cmbModel";
            this.cmbModel.Size = new System.Drawing.Size(92, 20);
            this.cmbModel.TabIndex = 3;
            this.cmbModel.SelectedIndexChanged += new System.EventHandler(this.cmbModel_SelectedIndexChanged);
            // 
            // lblNote
            // 
            this.lblNote.AutoSize = true;
            this.lblNote.Location = new System.Drawing.Point(103, 66);
            this.lblNote.Name = "lblNote";
            this.lblNote.Size = new System.Drawing.Size(31, 12);
            this.lblNote.TabIndex = 24;
            this.lblNote.Text = "Note";
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(105, 81);
            this.txtNote.MaxLength = 500;
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(231, 21);
            this.txtNote.TabIndex = 25;
            this.txtNote.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNote_KeyDown);
            // 
            // lblModel
            // 
            this.lblModel.AutoSize = true;
            this.lblModel.Location = new System.Drawing.Point(5, 21);
            this.lblModel.Name = "lblModel";
            this.lblModel.Size = new System.Drawing.Size(40, 12);
            this.lblModel.TabIndex = 20;
            this.lblModel.Text = "Model";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(5, 66);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(39, 12);
            this.lblName.TabIndex = 29;
            this.lblName.Text = "Name";
            // 
            // lblPart
            // 
            this.lblPart.AutoSize = true;
            this.lblPart.Location = new System.Drawing.Point(103, 21);
            this.lblPart.Name = "lblPart";
            this.lblPart.Size = new System.Drawing.Size(27, 12);
            this.lblPart.TabIndex = 31;
            this.lblPart.Text = "Part";
            // 
            // cmbPart
            // 
            this.cmbPart.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPart.FormattingEnabled = true;
            this.cmbPart.Location = new System.Drawing.Point(105, 36);
            this.cmbPart.Name = "cmbPart";
            this.cmbPart.Size = new System.Drawing.Size(109, 20);
            this.cmbPart.TabIndex = 30;
            this.cmbPart.SelectedIndexChanged += new System.EventHandler(this.cmbPart_SelectedIndexChanged);
            // 
            // gbBasicSearch
            // 
            this.gbBasicSearch.Controls.Add(this.txtDrawingNumber);
            this.gbBasicSearch.Controls.Add(this.lblDrawingNumber);
            this.gbBasicSearch.Controls.Add(this.lblFrom);
            this.gbBasicSearch.Controls.Add(this.lblEndDate);
            this.gbBasicSearch.Controls.Add(this.dtpickerEndDate);
            this.gbBasicSearch.Controls.Add(this.lblStartDate);
            this.gbBasicSearch.Controls.Add(this.dtpickerStartDate);
            this.gbBasicSearch.Location = new System.Drawing.Point(3, 3);
            this.gbBasicSearch.Name = "gbBasicSearch";
            this.gbBasicSearch.Size = new System.Drawing.Size(341, 110);
            this.gbBasicSearch.TabIndex = 50;
            this.gbBasicSearch.TabStop = false;
            this.gbBasicSearch.Text = "기본검색";
            // 
            // txtDrawingNumber
            // 
            this.txtDrawingNumber.Location = new System.Drawing.Point(8, 80);
            this.txtDrawingNumber.Name = "txtDrawingNumber";
            this.txtDrawingNumber.Size = new System.Drawing.Size(326, 21);
            this.txtDrawingNumber.TabIndex = 17;
            // 
            // lblDrawingNumber
            // 
            this.lblDrawingNumber.AutoSize = true;
            this.lblDrawingNumber.Location = new System.Drawing.Point(6, 66);
            this.lblDrawingNumber.Name = "lblDrawingNumber";
            this.lblDrawingNumber.Size = new System.Drawing.Size(29, 12);
            this.lblDrawingNumber.TabIndex = 16;
            this.lblDrawingNumber.Text = "도번";
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(164, 38);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(14, 12);
            this.lblFrom.TabIndex = 15;
            this.lblFrom.Text = "~";
            // 
            // lblEndDate
            // 
            this.lblEndDate.AutoSize = true;
            this.lblEndDate.Location = new System.Drawing.Point(182, 17);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(56, 12);
            this.lblEndDate.TabIndex = 14;
            this.lblEndDate.Text = "End Date";
            // 
            // dtpickerEndDate
            // 
            this.dtpickerEndDate.CustomFormat = "";
            this.dtpickerEndDate.Location = new System.Drawing.Point(184, 32);
            this.dtpickerEndDate.MaxDate = new System.DateTime(9998, 12, 1, 0, 0, 0, 0);
            this.dtpickerEndDate.Name = "dtpickerEndDate";
            this.dtpickerEndDate.Size = new System.Drawing.Size(150, 21);
            this.dtpickerEndDate.TabIndex = 12;
            this.dtpickerEndDate.Value = new System.DateTime(2013, 11, 19, 0, 0, 0, 0);
            // 
            // lblStartDate
            // 
            this.lblStartDate.AutoSize = true;
            this.lblStartDate.Location = new System.Drawing.Point(6, 17);
            this.lblStartDate.Name = "lblStartDate";
            this.lblStartDate.Size = new System.Drawing.Size(59, 12);
            this.lblStartDate.TabIndex = 13;
            this.lblStartDate.Text = "Start Date";
            // 
            // dtpickerStartDate
            // 
            this.dtpickerStartDate.CustomFormat = "";
            this.dtpickerStartDate.Location = new System.Drawing.Point(8, 32);
            this.dtpickerStartDate.MaxDate = new System.DateTime(9998, 12, 1, 0, 0, 0, 0);
            this.dtpickerStartDate.Name = "dtpickerStartDate";
            this.dtpickerStartDate.Size = new System.Drawing.Size(150, 21);
            this.dtpickerStartDate.TabIndex = 11;
            this.dtpickerStartDate.Value = new System.DateTime(2013, 11, 11, 0, 0, 0, 0);
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.Gray;
            this.splitter1.Location = new System.Drawing.Point(787, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 628);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // pnlBody
            // 
            this.pnlBody.Controls.Add(this.pnlBodyOrder);
            this.pnlBody.Controls.Add(this.pnlRightBottom);
            this.pnlBody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBody.Location = new System.Drawing.Point(790, 0);
            this.pnlBody.Name = "pnlBody";
            this.pnlBody.Size = new System.Drawing.Size(327, 628);
            this.pnlBody.TabIndex = 2;
            // 
            // pnlBodyOrder
            // 
            this.pnlBodyOrder.Controls.Add(this.gbOrderDetail);
            this.pnlBodyOrder.Controls.Add(this.pnlBodyLeft);
            this.pnlBodyOrder.Controls.Add(this.pnlBodyTop);
            this.pnlBodyOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBodyOrder.Location = new System.Drawing.Point(0, 0);
            this.pnlBodyOrder.Name = "pnlBodyOrder";
            this.pnlBodyOrder.Size = new System.Drawing.Size(327, 572);
            this.pnlBodyOrder.TabIndex = 2;
            // 
            // gbOrderDetail
            // 
            this.gbOrderDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbOrderDetail.Controls.Add(this.txtTotalVAT);
            this.gbOrderDetail.Controls.Add(this.lblTotalVAT);
            this.gbOrderDetail.Controls.Add(this.lblGrandTotal);
            this.gbOrderDetail.Controls.Add(this.txtGrandTotal);
            this.gbOrderDetail.Controls.Add(this.txtTotal);
            this.gbOrderDetail.Controls.Add(this.lblTotal);
            this.gbOrderDetail.Controls.Add(this.dgvOrderDetail);
            this.gbOrderDetail.Location = new System.Drawing.Point(26, 110);
            this.gbOrderDetail.Name = "gbOrderDetail";
            this.gbOrderDetail.Size = new System.Drawing.Size(294, 456);
            this.gbOrderDetail.TabIndex = 5;
            this.gbOrderDetail.TabStop = false;
            this.gbOrderDetail.Text = "발주상세내역";
            // 
            // txtTotalVAT
            // 
            this.txtTotalVAT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotalVAT.Enabled = false;
            this.txtTotalVAT.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtTotalVAT.Location = new System.Drawing.Point(50, 429);
            this.txtTotalVAT.Name = "txtTotalVAT";
            this.txtTotalVAT.Size = new System.Drawing.Size(85, 21);
            this.txtTotalVAT.TabIndex = 46;
            this.txtTotalVAT.Text = "0";
            this.txtTotalVAT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblTotalVAT
            // 
            this.lblTotalVAT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotalVAT.AutoSize = true;
            this.lblTotalVAT.Location = new System.Drawing.Point(-9, 432);
            this.lblTotalVAT.Name = "lblTotalVAT";
            this.lblTotalVAT.Size = new System.Drawing.Size(53, 12);
            this.lblTotalVAT.TabIndex = 45;
            this.lblTotalVAT.Text = "VAT합계";
            // 
            // lblGrandTotal
            // 
            this.lblGrandTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGrandTotal.AutoSize = true;
            this.lblGrandTotal.Location = new System.Drawing.Point(141, 432);
            this.lblGrandTotal.Name = "lblGrandTotal";
            this.lblGrandTotal.Size = new System.Drawing.Size(53, 12);
            this.lblGrandTotal.TabIndex = 44;
            this.lblGrandTotal.Text = "지급총액";
            // 
            // txtGrandTotal
            // 
            this.txtGrandTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGrandTotal.Enabled = false;
            this.txtGrandTotal.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtGrandTotal.Location = new System.Drawing.Point(200, 429);
            this.txtGrandTotal.Name = "txtGrandTotal";
            this.txtGrandTotal.Size = new System.Drawing.Size(85, 21);
            this.txtGrandTotal.TabIndex = 43;
            this.txtGrandTotal.Text = "0";
            this.txtGrandTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTotal
            // 
            this.txtTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotal.Enabled = false;
            this.txtTotal.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtTotal.Location = new System.Drawing.Point(-100, 429);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(85, 21);
            this.txtTotal.TabIndex = 41;
            this.txtTotal.Text = "0";
            this.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblTotal
            // 
            this.lblTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTotal.AutoSize = true;
            this.lblTotal.Location = new System.Drawing.Point(-135, 432);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(29, 12);
            this.lblTotal.TabIndex = 40;
            this.lblTotal.Text = "합계";
            // 
            // dgvOrderDetail
            // 
            this.dgvOrderDetail.AllowUserToAddRows = false;
            this.dgvOrderDetail.AllowUserToDeleteRows = false;
            this.dgvOrderDetail.AllowUserToResizeRows = false;
            this.dgvOrderDetail.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvOrderDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOrderDetail.ContextMenuStrip = this.cmsOrderDetail;
            this.dgvOrderDetail.Location = new System.Drawing.Point(3, 17);
            this.dgvOrderDetail.MultiSelect = false;
            this.dgvOrderDetail.Name = "dgvOrderDetail";
            this.dgvOrderDetail.RowHeadersVisible = false;
            this.dgvOrderDetail.RowTemplate.Height = 23;
            this.dgvOrderDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvOrderDetail.Size = new System.Drawing.Size(285, 406);
            this.dgvOrderDetail.TabIndex = 5;
            this.dgvOrderDetail.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvOrderDetail_CellClick);
            this.dgvOrderDetail.CellLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvOrderDetail_CellLeave);
            this.dgvOrderDetail.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvOrderDetail_CellValueChanged);
            this.dgvOrderDetail.ColumnWidthChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.dgvOrderDetail_ColumnWidthChanged);
            this.dgvOrderDetail.Scroll += new System.Windows.Forms.ScrollEventHandler(this.dgvOrderDetail_Scroll);
            this.dgvOrderDetail.Click += new System.EventHandler(this.dgvOrderDetail_Click);
            // 
            // cmsOrderDetail
            // 
            this.cmsOrderDetail.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiAddRow,
            this.tsmiDeleteRow});
            this.cmsOrderDetail.Name = "cmsOrderDetail";
            this.cmsOrderDetail.Size = new System.Drawing.Size(115, 48);
            // 
            // tsmiAddRow
            // 
            this.tsmiAddRow.Name = "tsmiAddRow";
            this.tsmiAddRow.Size = new System.Drawing.Size(114, 22);
            this.tsmiAddRow.Text = "행 추가";
            this.tsmiAddRow.Click += new System.EventHandler(this.tsmiAddRow_Click);
            // 
            // tsmiDeleteRow
            // 
            this.tsmiDeleteRow.Name = "tsmiDeleteRow";
            this.tsmiDeleteRow.Size = new System.Drawing.Size(114, 22);
            this.tsmiDeleteRow.Text = "행 삭제";
            this.tsmiDeleteRow.Click += new System.EventHandler(this.tsmiDeleteRow_Click);
            // 
            // pnlBodyLeft
            // 
            this.pnlBodyLeft.Controls.Add(this.btnPartToOrder);
            this.pnlBodyLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlBodyLeft.Location = new System.Drawing.Point(0, 107);
            this.pnlBodyLeft.Name = "pnlBodyLeft";
            this.pnlBodyLeft.Size = new System.Drawing.Size(23, 465);
            this.pnlBodyLeft.TabIndex = 4;
            // 
            // btnPartToOrder
            // 
            this.btnPartToOrder.BackColor = System.Drawing.Color.Ivory;
            this.btnPartToOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPartToOrder.Font = new System.Drawing.Font("굴림", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnPartToOrder.ForeColor = System.Drawing.Color.Red;
            this.btnPartToOrder.Location = new System.Drawing.Point(0, 0);
            this.btnPartToOrder.Name = "btnPartToOrder";
            this.btnPartToOrder.Size = new System.Drawing.Size(23, 465);
            this.btnPartToOrder.TabIndex = 0;
            this.btnPartToOrder.Text = "▶";
            this.btnPartToOrder.UseVisualStyleBackColor = false;
            this.btnPartToOrder.Click += new System.EventHandler(this.btnPartToOrder_Click);
            // 
            // pnlBodyTop
            // 
            this.pnlBodyTop.Controls.Add(this.gpOrder);
            this.pnlBodyTop.Controls.Add(this.btnClear);
            this.pnlBodyTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlBodyTop.Location = new System.Drawing.Point(0, 0);
            this.pnlBodyTop.Name = "pnlBodyTop";
            this.pnlBodyTop.Size = new System.Drawing.Size(327, 107);
            this.pnlBodyTop.TabIndex = 3;
            // 
            // gpOrder
            // 
            this.gpOrder.Controls.Add(this.txtMakerMessage);
            this.gpOrder.Controls.Add(this.cmbApprover);
            this.gpOrder.Controls.Add(this.lblMakerMessage);
            this.gpOrder.Controls.Add(this.lblApprover);
            this.gpOrder.Controls.Add(this.cmbVAT);
            this.gpOrder.Controls.Add(this.lblVAT);
            this.gpOrder.Controls.Add(this.btnMaker);
            this.gpOrder.Controls.Add(this.txtMaker);
            this.gpOrder.Controls.Add(this.txtOrderNumber);
            this.gpOrder.Controls.Add(this.lblOrderNumber);
            this.gpOrder.Controls.Add(this.lblMaker);
            this.gpOrder.Controls.Add(this.btnCreatedOrder);
            this.gpOrder.Location = new System.Drawing.Point(7, 3);
            this.gpOrder.Name = "gpOrder";
            this.gpOrder.Size = new System.Drawing.Size(649, 98);
            this.gpOrder.TabIndex = 0;
            this.gpOrder.TabStop = false;
            this.gpOrder.Text = "발주내역";
            // 
            // txtMakerMessage
            // 
            this.txtMakerMessage.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMakerMessage.Location = new System.Drawing.Point(423, 45);
            this.txtMakerMessage.Multiline = true;
            this.txtMakerMessage.Name = "txtMakerMessage";
            this.txtMakerMessage.Size = new System.Drawing.Size(209, 42);
            this.txtMakerMessage.TabIndex = 9;
            // 
            // cmbApprover
            // 
            this.cmbApprover.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbApprover.FormattingEnabled = true;
            this.cmbApprover.Location = new System.Drawing.Point(423, 14);
            this.cmbApprover.Name = "cmbApprover";
            this.cmbApprover.Size = new System.Drawing.Size(208, 20);
            this.cmbApprover.TabIndex = 50;
            // 
            // lblMakerMessage
            // 
            this.lblMakerMessage.AutoSize = true;
            this.lblMakerMessage.Location = new System.Drawing.Point(325, 48);
            this.lblMakerMessage.Name = "lblMakerMessage";
            this.lblMakerMessage.Size = new System.Drawing.Size(77, 12);
            this.lblMakerMessage.TabIndex = 49;
            this.lblMakerMessage.Text = "업체전달사항";
            // 
            // lblApprover
            // 
            this.lblApprover.AutoSize = true;
            this.lblApprover.Location = new System.Drawing.Point(325, 21);
            this.lblApprover.Name = "lblApprover";
            this.lblApprover.Size = new System.Drawing.Size(29, 12);
            this.lblApprover.TabIndex = 47;
            this.lblApprover.Text = "결재";
            // 
            // cmbVAT
            // 
            this.cmbVAT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbVAT.FormattingEnabled = true;
            this.cmbVAT.ItemHeight = 12;
            this.cmbVAT.Location = new System.Drawing.Point(104, 72);
            this.cmbVAT.Name = "cmbVAT";
            this.cmbVAT.Size = new System.Drawing.Size(209, 20);
            this.cmbVAT.TabIndex = 38;
            this.cmbVAT.SelectedIndexChanged += new System.EventHandler(this.cmbVAT_SelectedIndexChanged);
            // 
            // lblVAT
            // 
            this.lblVAT.AutoSize = true;
            this.lblVAT.Location = new System.Drawing.Point(6, 75);
            this.lblVAT.Name = "lblVAT";
            this.lblVAT.Size = new System.Drawing.Size(41, 12);
            this.lblVAT.TabIndex = 39;
            this.lblVAT.Text = "부가세";
            // 
            // btnMaker
            // 
            this.btnMaker.Location = new System.Drawing.Point(271, 43);
            this.btnMaker.Name = "btnMaker";
            this.btnMaker.Size = new System.Drawing.Size(42, 23);
            this.btnMaker.TabIndex = 6;
            this.btnMaker.Text = "..";
            this.btnMaker.UseVisualStyleBackColor = true;
            this.btnMaker.Click += new System.EventHandler(this.btnMaker_Click);
            // 
            // txtMaker
            // 
            this.txtMaker.Enabled = false;
            this.txtMaker.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMaker.Location = new System.Drawing.Point(104, 45);
            this.txtMaker.Name = "txtMaker";
            this.txtMaker.Size = new System.Drawing.Size(161, 21);
            this.txtMaker.TabIndex = 5;
            // 
            // txtOrderNumber
            // 
            this.txtOrderNumber.BackColor = System.Drawing.Color.NavajoWhite;
            this.txtOrderNumber.Enabled = false;
            this.txtOrderNumber.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtOrderNumber.Location = new System.Drawing.Point(104, 17);
            this.txtOrderNumber.Name = "txtOrderNumber";
            this.txtOrderNumber.Size = new System.Drawing.Size(209, 21);
            this.txtOrderNumber.TabIndex = 3;
            // 
            // lblOrderNumber
            // 
            this.lblOrderNumber.AutoSize = true;
            this.lblOrderNumber.Location = new System.Drawing.Point(6, 21);
            this.lblOrderNumber.Name = "lblOrderNumber";
            this.lblOrderNumber.Size = new System.Drawing.Size(29, 12);
            this.lblOrderNumber.TabIndex = 24;
            this.lblOrderNumber.Text = "번호";
            // 
            // lblMaker
            // 
            this.lblMaker.AutoSize = true;
            this.lblMaker.Location = new System.Drawing.Point(6, 48);
            this.lblMaker.Name = "lblMaker";
            this.lblMaker.Size = new System.Drawing.Size(41, 12);
            this.lblMaker.TabIndex = 25;
            this.lblMaker.Text = "거래처";
            // 
            // btnCreatedOrder
            // 
            this.btnCreatedOrder.Location = new System.Drawing.Point(271, 16);
            this.btnCreatedOrder.Name = "btnCreatedOrder";
            this.btnCreatedOrder.Size = new System.Drawing.Size(42, 23);
            this.btnCreatedOrder.TabIndex = 51;
            this.btnCreatedOrder.Text = "생성";
            this.btnCreatedOrder.UseVisualStyleBackColor = true;
            this.btnCreatedOrder.Visible = false;
            this.btnCreatedOrder.Click += new System.EventHandler(this.btnCreatedOrder_Click);
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.BackColor = System.Drawing.Color.Ivory;
            this.btnClear.ForeColor = System.Drawing.Color.Black;
            this.btnClear.Location = new System.Drawing.Point(233, 13);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(87, 50);
            this.btnClear.TabIndex = 12;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // pnlRightBottom
            // 
            this.pnlRightBottom.Controls.Add(this.btnSave);
            this.pnlRightBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlRightBottom.Location = new System.Drawing.Point(0, 572);
            this.pnlRightBottom.Name = "pnlRightBottom";
            this.pnlRightBottom.Size = new System.Drawing.Size(327, 56);
            this.pnlRightBottom.TabIndex = 0;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(233, 5);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(87, 50);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "저장";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // FormPublishOrder
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1117, 628);
            this.Controls.Add(this.pnlBody);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.pnlLeft);
            this.Name = "FormPublishOrder";
            this.Text = "발주 등록";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormPublishOrder_Load);
            this.Shown += new System.EventHandler(this.FormPublishOrder_Shown);
            this.pnlLeft.ResumeLayout(false);
            this.pnlPartList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPartList)).EndInit();
            this.pnlSearchPartList.ResumeLayout(false);
            this.gbCondition.ResumeLayout(false);
            this.gbCondition.PerformLayout();
            this.gbBasicSearch.ResumeLayout(false);
            this.gbBasicSearch.PerformLayout();
            this.pnlBody.ResumeLayout(false);
            this.pnlBodyOrder.ResumeLayout(false);
            this.gbOrderDetail.ResumeLayout(false);
            this.gbOrderDetail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrderDetail)).EndInit();
            this.cmsOrderDetail.ResumeLayout(false);
            this.pnlBodyLeft.ResumeLayout(false);
            this.pnlBodyTop.ResumeLayout(false);
            this.gpOrder.ResumeLayout(false);
            this.gpOrder.PerformLayout();
            this.pnlRightBottom.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlLeft;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel pnlBody;
        private System.Windows.Forms.Panel pnlBodyOrder;
        private System.Windows.Forms.Button btnPartToOrder;
        private System.Windows.Forms.Panel pnlPartList;
        private System.Windows.Forms.Panel pnlSearchPartList;
        private System.Windows.Forms.DataGridView dgvPartList;
        private System.Windows.Forms.DataGridView dgvOrderDetail;
        private System.Windows.Forms.Panel pnlBodyLeft;
        private System.Windows.Forms.Panel pnlBodyTop;
        private System.Windows.Forms.Panel pnlRightBottom;
        private System.Windows.Forms.GroupBox gbOrderDetail;
        private System.Windows.Forms.GroupBox gpOrder;
        private System.Windows.Forms.Label lblApprover;
        private System.Windows.Forms.ComboBox cmbVAT;
        private System.Windows.Forms.Label lblVAT;
        private System.Windows.Forms.Button btnMaker;
        private System.Windows.Forms.TextBox txtMaker;
        private System.Windows.Forms.TextBox txtOrderNumber;
        private System.Windows.Forms.Label lblOrderNumber;
        private System.Windows.Forms.Label lblMaker;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TextBox txtMakerMessage;
        private System.Windows.Forms.Label lblMakerMessage;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label lblGrandTotal;
        private System.Windows.Forms.TextBox txtGrandTotal;
        private System.Windows.Forms.ContextMenuStrip cmsOrderDetail;
        private System.Windows.Forms.TextBox txtTotalVAT;
        private System.Windows.Forms.Label lblTotalVAT;
        private System.Windows.Forms.ToolStripMenuItem tsmiAddRow;
        private System.Windows.Forms.ToolStripMenuItem tsmiDeleteRow;
        private System.Windows.Forms.GroupBox gbCondition;
        private System.Windows.Forms.Label lblPartName;
        private System.Windows.Forms.TextBox txtPartName;
        private System.Windows.Forms.ComboBox cmbUserID;
        private System.Windows.Forms.ComboBox cmbModel;
        private System.Windows.Forms.Label lblNote;
        private System.Windows.Forms.TextBox txtNote;
        private System.Windows.Forms.Label lblModel;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblPart;
        private System.Windows.Forms.ComboBox cmbPart;
        private System.Windows.Forms.GroupBox gbBasicSearch;
        private System.Windows.Forms.TextBox txtDrawingNumber;
        private System.Windows.Forms.Label lblDrawingNumber;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.Label lblEndDate;
        private System.Windows.Forms.DateTimePicker dtpickerEndDate;
        private System.Windows.Forms.Label lblStartDate;
        private System.Windows.Forms.DateTimePicker dtpickerStartDate;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.ComboBox cmbApprover;
        private System.Windows.Forms.Button btnCreatedOrder;
    }
}