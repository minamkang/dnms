﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrawingNumberManager
{
    public class StaticData
    {
        public static string db_connetion_string;
        public static string category;
        
        public static string user_id;
        public static string user_name;
        public static string user_password;
        public static string remember_id_pw;
        public static string user_tel;
        public static string user_mail;
        public static int user_key;

        public static string server_path;
        public static string local_path;
    }
}
