﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Data.SqlClient;

namespace DrawingNumberManager
{
    public partial class FormNoticeMessage : Form
    {
        Notice notice = null;
        Thread thr_notice = null;

        public FormNoticeMessage()
        {
            InitializeComponent();
            notice = new Notice(this);
        }

        private void FormNoticeMessage_Load(object sender, EventArgs e)
        {
            if (notice != null)
            {
                thr_notice = new Thread(new ThreadStart(notice.KeepAliveNotice));
                thr_notice.IsBackground = true;
                thr_notice.Start();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            DataTable result = new DataTable();

            try
            {
                string query = string.Format("EXEC modify_notify_display '{0}', '{1}'", 
                    lblOrderNumber.Text[0], lblOrderNumber.Text.Substring(1, 10));


                SqlDataAdapter queryAdapter = new SqlDataAdapter(string.Format("EXEC modify_notify_display '{0}', '{1}'", 
                    lblOrderNumber.Text[0], lblOrderNumber.Text.Substring(1, 10)), StaticData.db_connetion_string);
                queryAdapter.Fill(result);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

    }
}
