﻿using System;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Collections;
using ExcelCore = Microsoft.Office.Interop.Excel;
using System.IO.Compression;

namespace DrawingNumberManager
{
    public partial class FormOrder : Form
    {
        bool ServerSearch = false;		//날짜관련 Data가 변경 되면 Server에서 Search여부
        bool is_Reverse_duedate = false;
        bool is_FormLoad = false;

        private int maker_code;
        private string OrderNumber = "";
        
        DateTime Now = DateTime.Now.Date;

        System.Data.DataTable result = null;
        System.Data.DataTable tempResult = null;
        System.Data.DataTable dtMaker = null;
        
        private string strOrderFile = "";
        string strViewPath = "C:\\KM_ORDER\\VIEW";
        string strMailPath = "C:\\KM_ORDER\\MAIL";
        string strMailZipPath = "C:\\KM_ORDER\\MAIL_ZIP";

        private int user_level = 0;

        public FormOrder(int iUserLevel)
        {
            user_level = iUserLevel;
            InitializeComponent();
        }

        private void FormOrder_Load(object sender, EventArgs e)
        {
            ServerSearch = true;
            is_FormLoad = true;
            this.Text = string.Format("{0}[{1}]", Const.FORM_ORDER_MANAGEMENT, StaticData.user_name);
            
            dtpickerStartDate.Value = Now.AddDays(-30);
            dtpickerEndDate.Value = Now.AddDays(1).AddSeconds(-1);
            dtpickerDuedate.Value = Now.AddDays(1).AddSeconds(-1);

            BindUserID();
            BindStatus();
            BindReverseCategorization();

            gbCondition.Visible = false;
            BindOrderList();
            is_FormLoad = false;
        }

        private void FormOrder_SizeChanged(object sender, EventArgs e)
        {
            pnlROrder.Height = pnlOrder.Height / 2;
        }

        private void tabOrderMagement_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (tabOrderMagement.SelectedTab.Text)
            {
                case "등록":
                case "결재":
                    gbCondition.Visible = false;
                    lblReverseSearch.Visible = false;
                    cmbSCategorization.Visible = false;
                    txtSCategorizationContent.Visible = false;
                    dtpickerDuedate.Visible = false;
                    txtSCategorizationContent.Text = "";
                    is_Reverse_duedate = false;
                    cmbSCategorization.SelectedValue = "";

                    if (tabOrderMagement.SelectedTab.Text == "등록")
                        pnlROrder.Height = pnlOrder.Height / 2;
                    else
                        pnlAOrder.Height = pnlAOrderBody.Height / 2;

                    break;
                case "열람":
                    gbCondition.Visible = true;
                    lblReverseSearch.Visible = true;
                    cmbSCategorization.Visible = true;
                    txtSCategorizationContent.Visible = true;
                    dtpickerDuedate.Visible = true;
                    pnlVOrder.Height = pnlVOrderBody.Height / 2;
                    break;
            }

            ServerSearch = true;
            BindOrderList();
        }

        private void cmbReverse_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (is_FormLoad == false)
            {
                switch (cmbSCategorization.Text)
                {
                    case "":
                    case "도번":
                    case "품명":
                        if (tabOrderMagement.SelectedTab.Text == "열람")
                        {
                            txtSCategorizationContent.Visible = true;
                            dtpickerDuedate.Visible = false;
                        }
                        break;
                    case "납기일":
                        if (tabOrderMagement.SelectedTab.Text == "열람")
                        {
                            txtSCategorizationContent.Visible = false;
                            dtpickerDuedate.Visible = true;
                            is_Reverse_duedate = true;
                        }
                        break;
                }
            }
        }

        private void cmbSStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (is_FormLoad == false)
                BindOrderList();
        }

        private void txtSOrderNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
                BindOrderList();
        }

        private void cmbUserID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (is_FormLoad == false)
                BindOrderList();
        }

        private void btnInit_Click(object sender, EventArgs e)
        {
            dtpickerStartDate.Value = Now.AddDays(-30);
            dtpickerEndDate.Value = Now.AddDays(1).AddSeconds(-1);
            dtpickerDuedate.Value = Now.AddDays(1).AddSeconds(-1);
            cmbSCategorization.SelectedValue = "";
            cmbUserID.SelectedValue = "ALL";
            cmbSStatus.SelectedValue = "";
            txtSCategorizationContent.Text = "";
            txtMaker.Text = "";
            txtSOrderNumber.Text = "";
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            ServerSearch = true;
            BindOrderList();
        }

        private void btnMaker_Click(object sender, EventArgs e)
        {
            FormMaker frmCompany = new FormMaker(false);
            if (frmCompany.ShowDialog() == DialogResult.OK)
            {
                txtMaker.Text = frmCompany.MakerName;
                maker_code = frmCompany.MakerCode;
            }
        }

        private void btnMakerClear_Click(object sender, EventArgs e)
        {
            txtMaker.Text = "";
            maker_code = -1;
        }

        private void btnAddOrder_Click(object sender, EventArgs e)
        {
            FormPublishOrder frmPublishOrder = new FormPublishOrder(true, "", -1);
            frmPublishOrder.Show();
        }

        private void btnUpdateOrder_Click(object sender, EventArgs e)
        {
            if (dgvROrder.Rows.Count > 0)
            {
                maker_code = int.Parse(dgvROrder.Rows[dgvROrder.CurrentRow.Index].Cells["MAKER_CODE"].Value.ToString());
                OrderNumber = dgvROrder.Rows[dgvROrder.CurrentRow.Index].Cells["발주번호"].Value.ToString();
                FormPublishOrder frmPublishOrder = new FormPublishOrder(false, OrderNumber, maker_code);
                frmPublishOrder.Show();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgvROrder.Rows.Count > 0)
            {
                if (dgvROrder.Rows[dgvROrder.CurrentRow.Index].Cells["발주일자"].Value.ToString().Length > 0)
                {
                    MessageBox.Show("발송된 발주서는 삭제할수 없습니다.", "경고");
                    return;
                }

                System.Data.DataTable result = new System.Data.DataTable();
                SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC delete_order '{0}'",
                    dgvROrder.Rows[dgvROrder.CurrentRow.Index].Cells["발주번호"].Value.ToString()), StaticData.db_connetion_string);
                query.Fill(result);

                ServerSearch = true;
                BindOrderList();
            }
        }

        private void btnRequestApprove_Click(object sender, EventArgs e)
        {
            if (dgvROrder.Rows.Count > 0)
            {
                System.Data.DataTable result = new System.Data.DataTable();
                SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC order_list_update '{0}', 'S', 'R'",
                    dgvROrder.Rows[dgvROrder.CurrentRow.Index].Cells["발주번호"].Value.ToString()), StaticData.db_connetion_string);

                query.Fill(result);

                ServerSearch = true;
                BindOrderList();
            }
        }

        private void btnProcApprove_Click(object sender, EventArgs e)
        {
            if (dgvRQOrder.Rows.Count > 0)
            {
                System.Data.DataTable result = new System.Data.DataTable();
                SqlDataAdapter query = null;

                string message = string.Format("발주번호[{0}]를 승인하시겠습니까?", dgvRQOrder.Rows[dgvRQOrder.CurrentRow.Index].Cells["발주번호"].Value.ToString());
                FormMessage frmMessage = new FormMessage(message, "승인", "반려");

                DialogResult resultdlg =  frmMessage.ShowDialog();
                if (resultdlg == DialogResult.OK)
                {
                    query = new SqlDataAdapter(String.Format("EXEC order_list_update '{0}', 'S', 'A'",
                    dgvRQOrder.Rows[dgvRQOrder.CurrentRow.Index].Cells["발주번호"].Value.ToString()), StaticData.db_connetion_string);
                }
                else if (resultdlg == DialogResult.No)
                {
                    query = new SqlDataAdapter(String.Format("EXEC order_list_update '{0}', 'S', 'B'",
                    dgvRQOrder.Rows[dgvRQOrder.CurrentRow.Index].Cells["발주번호"].Value.ToString()), StaticData.db_connetion_string);
                }
                else
                    return;

                query.Fill(result);

                ServerSearch = true;
                BindOrderList();
            }
        }

        private void BindUserID()
        {
            System.Data.DataTable result = new System.Data.DataTable();
            SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC user_infomation 0"), StaticData.db_connetion_string);
            query.Fill(result);

            if (result != null)
            {
                result.DefaultView.Sort = "NAME ASC";
                result.Rows.Add("ALL", "ALL", "ALL", "ALL", "ALL");
                cmbUserID.ValueMember = "ID";
                cmbUserID.DisplayMember = "NAME";
                cmbUserID.DataSource = result;
                cmbUserID.SelectedValue = "ALL";
            }
        }

        private void BindOrderList()
        {
            if (ServerSearch == true)
            {
                result = new System.Data.DataTable();
                tempResult = new System.Data.DataTable();

                if (txtSCategorizationContent.Text == "" && is_Reverse_duedate == false)
                {
                    int search_maker_code = -1;
                    if (maker_code > 0)
                    {
                        if(txtMaker.Text == "")
                            search_maker_code = -1;
                        else
                            search_maker_code = maker_code;
                    }

                    SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC order_list '{0}', '{1}', {2}, ''",
                    dtpickerStartDate.Value.ToString(Const.DATE_TIME_FORMAT), dtpickerEndDate.Value.ToString(Const.DATE_TIME_FORMAT),
                    search_maker_code), StaticData.db_connetion_string);
                    query.Fill(result);
                }
                else if (txtSCategorizationContent.Text != "" || is_Reverse_duedate == true)     //역검색이 포함되었으면
                {
                    ArrayList alOrderKey = new ArrayList();
                    System.Data.DataTable dtReverse_Result = new System.Data.DataTable();

                    if (cmbSCategorization.SelectedValue.ToString() != "")
                    {    
                        string strQuery = "";
                        switch (cmbSCategorization.SelectedValue.ToString())
                        {
                            case "DN":  strQuery = String.Format("EXEC order_detail_list 'DN', '{0}', ''", txtSCategorizationContent.Text);     break;
                            case "PN":  strQuery = String.Format("EXEC order_detail_list 'PN', '{0}', ''", txtSCategorizationContent.Text);     break;
                            case "DD":
                                strQuery = String.Format("EXEC order_detail_list 'DD', '{0}', ''", dtpickerDuedate.Value.ToString(Const.DUE_DATE));
                                is_Reverse_duedate = false;
                                break;
                            case "SP":  strQuery = String.Format("EXEC order_detail_list 'SP', '{0}', ''", txtSCategorizationContent.Text);     break;
                        }

                        SqlDataAdapter query = new SqlDataAdapter(strQuery, StaticData.db_connetion_string);
                        query.Fill(dtReverse_Result);

                        for (short rowCount = 0; rowCount < dtReverse_Result.Rows.Count; rowCount++)
                            alOrderKey.Add(dtReverse_Result.Rows[rowCount]["ORDER_KEY"].ToString());
                    }

                    System.Data.DataTable dtTempOrderOfCategorization = null;
                    if (result.Rows.Count == 0)
                    {
                        result.Columns.Add("발주번호");
                        result.Columns.Add("생성일자");
                        result.Columns.Add("업체명");
                        result.Columns.Add("거래구분");
                        result.Columns.Add("VAT");
                        result.Columns.Add("담당자");
                        result.Columns.Add("승인자");
                        result.Columns.Add("승인일자");
                        result.Columns.Add("상태");
                        result.Columns.Add("업체전달사항");
                        result.Columns.Add("발주메모");
                        result.Columns.Add("NOTIFY_DISPLAY");
                        result.Columns.Add("발주일자");
                        result.Columns.Add("MAKER_CODE");
                        result.Columns.Add("TEL");
                    }
                    
                    for (int rows = 0; rows < alOrderKey.Count; rows++)
                    {
                        int search_maker_code = -1;
                        if (maker_code > 0)
                            search_maker_code = maker_code;

                        dtTempOrderOfCategorization = new System.Data.DataTable();
                        SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC order_list '{0}', '{1}', {2}, '{3}'",
                        dtpickerStartDate.Value.ToString(Const.DATE_TIME_FORMAT), dtpickerEndDate.Value.ToString(Const.DATE_TIME_FORMAT),
                        search_maker_code, alOrderKey[rows].ToString()), StaticData.db_connetion_string);

                        query.Fill(dtTempOrderOfCategorization);
                        if (dtTempOrderOfCategorization.Rows.Count == 1)
                            result.Rows.Add(dtTempOrderOfCategorization.Rows[0].ItemArray);
                    }
                }

                ServerSearch = false;
                switch(tabOrderMagement.SelectedTab.Text)
                {
                    case "등록":  dgvROrder.DataSource = result;      break;
                    case "결재":  dgvRQOrder.DataSource = result;     break;
                    case "열람":  dgvVOrder.DataSource = result;      break;
                }
            }

            tempResult = result.Copy();

            if (tempResult != null && tempResult.Rows.Count > 0)
            {
                if (tabOrderMagement.SelectedTab.Text == "등록")
                    dgvTabSearch("R", dgvROrder, tempResult);
                else if (tabOrderMagement.SelectedTab.Text == "결재")
                    dgvTabSearch("A", dgvRQOrder, tempResult);
                else if (tabOrderMagement.SelectedTab.Text == "열람")
                    dgvTabSearch("V", dgvVOrder, tempResult);
            }
            
            if (tempResult != null && tempResult.Rows.Count == 0)
            {
                if (tabOrderMagement.SelectedTab.Text == "등록")
                {
                    dgvOrderFormatting(dgvROrder);
                    dgvROrderDetail.DataSource = null;
                }
                else if (tabOrderMagement.SelectedTab.Text == "결재")
                {
                    dgvOrderFormatting(dgvRQOrder);
                    dgvRQOrderDetail.DataSource = null;
                }
                else if (tabOrderMagement.SelectedTab.Text == "열람")
                {
                    dgvOrderFormatting(dgvVOrder);
                    dgvVOrderDetail.DataSource = null;
                }
            }
        }

        private void dgvTabSearch(string flag, DataGridView dgvSearch, System.Data.DataTable tempResult)
        {
            DataRow[] dtrow = null;

            if (flag == "R")
                dtrow = tempResult.Select(string.Format("담당자='{0}' AND (상태='{1}' OR 상태='{2}')", StaticData.user_name, "W", "B"));
            else if (flag == "A")
                dtrow = tempResult.Select(string.Format("승인자='{0}' AND 상태='{1}' ", StaticData.user_name, "R"));
            else if (flag == "V")
            {
                if (txtSOrderNumber.Text != "" && cmbSStatus.Text == "" && cmbUserID.Text == "ALL")
                    dtrow = tempResult.Select(string.Format("발주번호 LIKE '%{0}%' ", txtSOrderNumber.Text));
                else if (txtSOrderNumber.Text == "" && cmbSStatus.Text != "" && cmbUserID.Text == "ALL")
                    dtrow = tempResult.Select(string.Format("상태='{0}' ", cmbSStatus.SelectedValue,ToString()));
                else if (txtSOrderNumber.Text == "" && cmbSStatus.Text == "" && cmbUserID.Text != "ALL")
                    dtrow = tempResult.Select(string.Format("담당자='{0}' ", cmbUserID.Text));

                else if (txtSOrderNumber.Text != "" && cmbSStatus.Text != "" && cmbUserID.Text == "ALL")
                    dtrow = tempResult.Select(string.Format("발주번호 LIKE '%{0}%' AND 상태='{1}'", txtSOrderNumber.Text, cmbSStatus.SelectedValue.ToString()));
                else if (txtSOrderNumber.Text != "" && cmbSStatus.Text == "" && cmbUserID.Text != "ALL")
                    dtrow = tempResult.Select(string.Format("발주번호 LIKE '%{0}%' AND 담당자='{1}'", txtSOrderNumber.Text, cmbUserID.Text));
                else if (txtSOrderNumber.Text == "" && cmbSStatus.Text != "" && cmbUserID.Text != "ALL")
                    dtrow = tempResult.Select(string.Format("상태='{0}' AND 담당자='{1}'", cmbSStatus.SelectedValue.ToString(), cmbUserID.Text));

                else if (txtSOrderNumber.Text != "" && cmbSStatus.Text != "" && cmbUserID.Text != "ALL")
                    dtrow = tempResult.Select(string.Format("발주번호 LIKE '%{0}%' AND 상태='{1}' AND 담당자='{2}'", txtSOrderNumber.Text, cmbSStatus.SelectedValue.ToString(), cmbUserID.Text));
                else if (txtSOrderNumber.Text == "" && cmbSStatus.Text == "" && cmbUserID.Text == "ALL")
                {
                    dgvVOrder.DataSource = tempResult;
                    dgvOrderFormatting(dgvVOrder);
                }
            }

            if (dtrow == null)
                return;

            if (dtrow.Length > 0)
                tempResult = dtrow.CopyToDataTable();
            else
            {
                tempResult.Rows.Clear();
                if (flag == "R")
                    dgvROrderDetail.DataSource = null;
                else if (flag == "A")
                    dgvRQOrderDetail.DataSource = null;
                else if (flag == "V")
                    dgvVOrderDetail.DataSource = null;
            }

            dgvSearch.DataSource = tempResult;
            dgvOrderFormatting(dgvSearch);
        }

        private void dgvROrder_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvROrder == null)
                return;

            if (dgvROrder.Rows.Count > 0)
            {
                System.Data.DataTable dtR_Detail = new System.Data.DataTable();
                SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC order_detail_list 'S', '', {0}",
                    dgvROrder.Rows[e.RowIndex].Cells["발주번호"].Value.ToString()), StaticData.db_connetion_string);

                query.Fill(dtR_Detail);
                dgvROrderDetail.DataSource = dtR_Detail;
                dgvOrderDetailFormatting(dgvROrderDetail, false);
                ColumnsSortDisable(dgvROrderDetail);

                string strFlagVAT = dgvROrder.Rows[e.RowIndex].Cells["VAT"].Value.ToString();
                PriceCalculartion(strFlagVAT, dgvROrderDetail);
            }
        }

        private void dgvRQOrder_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvRQOrder == null)
                return;

            if (dgvRQOrder.Rows.Count > 0)
            {
                System.Data.DataTable dtRQ_Detail = new System.Data.DataTable();
                SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC order_detail_list 'S', '', '{0}'",
                    dgvRQOrder.Rows[e.RowIndex].Cells["발주번호"].Value.ToString()), StaticData.db_connetion_string);

                query.Fill(dtRQ_Detail);
                dgvRQOrderDetail.DataSource = dtRQ_Detail;
                dgvOrderDetailFormatting(dgvRQOrderDetail, false);
                ColumnsSortDisable(dgvRQOrderDetail);

                string strFlagVAT = dgvRQOrder.Rows[e.RowIndex].Cells["VAT"].Value.ToString();
                PriceCalculartion(strFlagVAT, dgvRQOrderDetail);
            }
        }

        private void dgvVOrder_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvVOrder == null)
                return;


            if (dgvVOrder.Rows.Count > 0)
            {
                System.Data.DataTable dtV_Detail = new System.Data.DataTable();
                SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC order_detail_list 'S', '', '{0}'",
                    dgvVOrder.Rows[e.RowIndex].Cells["발주번호"].Value.ToString()), StaticData.db_connetion_string);

                query.Fill(dtV_Detail);
                dgvVOrderDetail.DataSource = dtV_Detail;

                bool isApprove = (dgvVOrder.Rows[e.RowIndex].Cells["상태"].Value.ToString() == "승인");
                dgvOrderDetailFormatting(dgvVOrderDetail, isApprove);
                ColumnsSortDisable(dgvVOrderDetail);

                string strFlagVAT = dgvVOrder.Rows[e.RowIndex].Cells["VAT"].Value.ToString();
                PriceCalculartion(strFlagVAT, dgvVOrderDetail);
            }
        }

        private void dgvROrder_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if(dgvROrder.Rows.Count > 0 && dgvROrder.Rows[e.RowIndex].Cells["발주메모"].Value.ToString().Length > 0)
            {
                System.Data.DataTable result = new System.Data.DataTable();
                SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC order_list_update '{0}', 'M', '{1}'",
                    dgvROrder.Rows[e.RowIndex].Cells["발주번호"].Value.ToString(),
                    dgvROrder.Rows[e.RowIndex].Cells["발주메모"].Value.ToString()), StaticData.db_connetion_string);

                query.Fill(result);
            }
        }

        private void dgvRQOrder_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvRQOrder.Rows.Count > 0 && dgvRQOrder.Rows[e.RowIndex].Cells["발주메모"].Value.ToString().Length > 0)
            {
                System.Data.DataTable result = new System.Data.DataTable();
                SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC order_list_update '{0}', 'M', '{1}'",
                    dgvRQOrder.Rows[e.RowIndex].Cells["발주번호"].Value.ToString(),
                    dgvRQOrder.Rows[e.RowIndex].Cells["발주메모"].Value.ToString()), StaticData.db_connetion_string);

                query.Fill(result);
            }
        }

        private void dgvVOrder_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvVOrder.Rows.Count > 0 && dgvVOrder.Rows[e.RowIndex].Cells["발주메모"].Value.ToString().Length > 0)
            {
                System.Data.DataTable result = new System.Data.DataTable();
                SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC order_list_update '{0}', 'M', '{1}'",
                    dgvVOrder.Rows[e.RowIndex].Cells["발주번호"].Value.ToString(),
                    dgvVOrder.Rows[e.RowIndex].Cells["발주메모"].Value.ToString()), StaticData.db_connetion_string);

                query.Fill(result);
            }
        }

        private void dgvVOrderDetail_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvVOrderDetail.Rows.Count > 0)
            {
                System.Data.DataTable result = new System.Data.DataTable();
                SqlDataAdapter query = null;

                if (dgvVOrderDetail.Rows[e.RowIndex].Cells["비고"].Value.ToString().Length > 0)
                {
                    
                    query = new SqlDataAdapter(String.Format("EXEC order_detail_update 'N', '{0}', {1}, '{2}'",
                        dgvVOrder.Rows[dgvVOrder.CurrentRow.Index].Cells["발주번호"].Value.ToString(),
                        dgvVOrderDetail.Rows[e.RowIndex].Cells["NO"].Value.ToString(),
                        dgvVOrderDetail.Rows[e.RowIndex].Cells["비고"].Value.ToString()), StaticData.db_connetion_string);

                    query.Fill(result);
                }

                if (dgvVOrderDetail.Rows[e.RowIndex].Cells["수량"].Value.ToString().Length > 0)
                {
                    query = new SqlDataAdapter(String.Format("EXEC order_detail_update 'Q', '{0}', {1}, '{2}'",
                        dgvVOrder.Rows[dgvVOrder.CurrentRow.Index].Cells["발주번호"].Value.ToString(),
                        dgvVOrderDetail.Rows[e.RowIndex].Cells["NO"].Value.ToString(),
                        dgvVOrderDetail.Rows[e.RowIndex].Cells["수량"].Value.ToString()), StaticData.db_connetion_string);

                    query.Fill(result);

                    string strFlagVAT = dgvVOrder.Rows[dgvVOrder.CurrentRow.Index].Cells["VAT"].Value.ToString();
                    PriceCalculartion(strFlagVAT, dgvVOrderDetail);
                }

                if (dgvVOrderDetail.Rows[e.RowIndex].Cells["단가"].Value.ToString().Length > 0)
                {
                    query = new SqlDataAdapter(String.Format("EXEC order_detail_update 'U', '{0}', {1}, '{2}'",
                        dgvVOrder.Rows[dgvVOrder.CurrentRow.Index].Cells["발주번호"].Value.ToString(),
                        dgvVOrderDetail.Rows[e.RowIndex].Cells["NO"].Value.ToString(),
                        dgvVOrderDetail.Rows[e.RowIndex].Cells["단가"].Value.ToString()), StaticData.db_connetion_string);

                    query.Fill(result);

                    string strFlagVAT = dgvVOrder.Rows[dgvVOrder.CurrentRow.Index].Cells["VAT"].Value.ToString();
                    PriceCalculartion(strFlagVAT, dgvVOrderDetail);
                }

                dgvVOrderDetail.Refresh();
            }
        }

        private void dgvROrderDetail_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DrawingButtonOrderDetail(dgvROrderDetail, e);
        }

        private void dgvRQOrderDetail_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DrawingButtonOrderDetail(dgvRQOrderDetail, e);
        }

        private void dgvVOrderDetail_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DrawingButtonOrderDetail(dgvVOrderDetail, e);
        }

        private void btnExcel_Click(object sender, EventArgs e)
        {
            if (dgvVOrder.Rows.Count > 0)
            {
                ExcelFilePath("V");
                dgvToExcel(true);
            }
        }

        private void btnSendMail_Click(object sender, EventArgs e)
        {
            if (dgvVOrder.Rows.Count > 0)
            {
                bool isContinue = true;

                if (dgvVOrder.Rows[dgvVOrder.CurrentRow.Index].Cells["담당자"].Value.ToString() != StaticData.user_name)
                {
                    MessageBox.Show("담당자만이 메일을 전송할 수 있습니다.", "경고");
                    return;
                }

                if (dgvVOrder.Rows[dgvVOrder.CurrentRow.Index].Cells["상태"].Value.ToString() != "승인")
                {
                    string strMessage = "미승인 발주서 입니다.\r\n\r\n그래도 진행 하시겠습니까?";
                    FormMessage frmMessage = new FormMessage(strMessage, "진행", "취소");
                    DialogResult resultdlg = frmMessage.ShowDialog();

                    if (resultdlg == DialogResult.OK)
                        isContinue = true;
                    else
                        isContinue = false;
                }
                
                if (isContinue)
                {
                    if (dgvVOrder.Rows.Count > 0)
                    {
                        ExcelFilePath("M");
                        dgvToExcel(false);

                        ArrayList alSearchFileName = new ArrayList();                  //파일명 쓰기
                        ArrayList result_al_SeverSearchFile = new ArrayList();         //서버에서 읽어온 결과(도번_리비젼:경로)
                        ArrayList result_al_path = new ArrayList();                    //경로 저장

                        for (int i = 0; i < dgvVOrderDetail.Rows.Count; i++)
                        {
                            if (dgvVOrderDetail.Rows[i].Cells["도번"].Value.ToString() != "")
                            {
                                if (dgvVOrderDetail.Rows[i].Cells["도번"].Value.ToString().Length == 10)  //진도번
                                    alSearchFileName.Add(string.Format("{0}_{1}",
                                        dgvVOrderDetail.Rows[i].Cells["도번"].Value.ToString(), dgvVOrderDetail.Rows[i].Cells["REV"].Value.ToString()));
                                else
                                    alSearchFileName.Add(dgvVOrderDetail.Rows[i].Cells["도번"].Value.ToString());
                            }
                        }

                        Drawing dwg = new Drawing();
                        if (dwg.Downloads(alSearchFileName, ref result_al_SeverSearchFile) == false)
                            return;

                        bool is_error = true;
                        string ErrorMessage = "";
                        int ErrorCount = 0;

                        for (int i = 0; i < alSearchFileName.Count; i++)
                        {
                            for (int j = 0; j < result_al_SeverSearchFile.Count; j++)
                            {
                                string[] splitContent = result_al_SeverSearchFile[j].ToString().Split(',');
                                if (splitContent[0] == alSearchFileName[i].ToString())
                                {
                                    result_al_path.Add(splitContent[1]);
                                    is_error = false;
                                    break;
                                }
                            }

                            if (is_error == true)
                            {
                                ErrorCount++;
                                ErrorMessage += string.Format("{0}_[{1}] ", (i + 1).ToString(), alSearchFileName[i].ToString());

                                if (ErrorCount % 2 == 0)
                                    ErrorMessage += Environment.NewLine;
                            }

                            is_error = true;
                        }

                        bool is_mail_send = true;
                        if (ErrorMessage.Length > 0)
                        {
                            StringBuilder sbErrorContent = new StringBuilder();
                            sbErrorContent.Append("도면파일이 없습니다.\r\n\r\n");
                            sbErrorContent.Append(string.Format("{0} \r\n\r\n", ErrorMessage));
                            sbErrorContent.Append("메일전송을 원하시면 '진행'을, 그렇지 않으면 '취소'를 선택.");
                            FormMessage frmMessage = new FormMessage(sbErrorContent.ToString(), "진행", "취소");
                            DialogResult dlgResult = frmMessage.ShowDialog();
                            if (dlgResult == DialogResult.Cancel || dlgResult == DialogResult.No)
                                is_mail_send = false;
                        }

                        if (is_mail_send == true)
                        {
                            //swkang 20200711 zip파일 만들기
                            string zipPath = "";
                            if (result_al_path.Count > 0)
                            {
                                try
                                {
                                    result_al_path.Clear();
                                    string strMailPath = @"C:\KM_ORDER\MAIL";
                                    zipPath = Path.Combine(strMailZipPath, dgvVOrder.Rows[dgvVOrder.CurrentRow.Index].Cells["발주번호"].Value.ToString() + ".zip");
                                    ZipFile.CreateFromDirectory(strMailPath, zipPath);
                                    result_al_path.Add(zipPath);  //발주서 추가
                                }
                                catch(Exception ex)
                                {
                                    ex.ToString();
                                }
                            }
#if false
                            result_al_path.Add(Path.Combine(strMailPath, dgvVOrder.Rows[dgvVOrder.CurrentRow.Index].Cells["발주번호"].Value.ToString() + ".xlsx"));       //발주서 추가
#else
                            //swkang 경로 수정
                            result_al_path.Add(Path.Combine(strMailZipPath, dgvVOrder.Rows[dgvVOrder.CurrentRow.Index].Cells["발주번호"].Value.ToString() + ".xlsx"));     //발주서 추가
#endif

                            FormSendMail frmMail = new FormSendMail(dtMaker, dgvVOrder.Rows[dgvVOrder.CurrentRow.Index].Cells["발주번호"].Value.ToString(), result_al_path);
                            if (frmMail.ShowDialog() == DialogResult.OK)
                            {
                                UpdateOrderDate(dgvVOrder.Rows[dgvVOrder.CurrentRow.Index].Cells["발주번호"].Value.ToString());
                                MessageBox.Show("메일을 전송하였습니다.");
                            }
                            else
                                MessageBox.Show("메일 전송 실패....");
                        }
                    }
                }
            }
        }

        private void btnReorder_Click(object sender, EventArgs e)
        {
            if (dgvVOrder.Rows.Count > 0)
            {
                string ReOrderNumber = "";
                string ReMakerCode = "";
                string ReApproverUserKey = "";

                System.Data.DataTable result = new System.Data.DataTable();
                SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC create_order_number"), StaticData.db_connetion_string);
                query.Fill(result);
                if (result != null && result.Rows.Count == 1)
                    ReOrderNumber = result.Rows[0]["CON"].ToString();

                result = new System.Data.DataTable();
                query = new SqlDataAdapter(String.Format("EXEC management_maker 'N', '{0}'", dgvVOrder.Rows[dgvVOrder.CurrentRow.Index].Cells["업체명"].Value.ToString()), StaticData.db_connetion_string);
                query.Fill(result);
                if (result != null && result.Rows.Count == 1)
                    ReMakerCode = result.Rows[0]["CODE"].ToString();

                result = new System.Data.DataTable();
                query = new SqlDataAdapter(String.Format("EXEC user_infomation 0"), StaticData.db_connetion_string);
                query.Fill(result);
                for (int i = 0; i < result.Rows.Count; i++)
                {
                    if (result.Rows[i]["NAME"].ToString() == dgvVOrder.Rows[dgvVOrder.CurrentRow.Index].Cells["승인자"].Value.ToString())
                    {
                        ReApproverUserKey = result.Rows[i]["USER_KEY"].ToString();
                        break;
                    }
                }

                //ORDER 결과 저장
                result = new System.Data.DataTable();
                query = new SqlDataAdapter(
                String.Format("EXEC order_create '{0}', {1}, '{2}', {3}, {4}, '{5}'",
                    ReOrderNumber,
                    ReMakerCode,
                    dgvVOrder.Rows[dgvVOrder.CurrentRow.Index].Cells["VAT"].Value.ToString(),
                    StaticData.user_key,
                    ReApproverUserKey,
                    dgvVOrder.Rows[dgvVOrder.CurrentRow.Index].Cells["업체전달사항"].Value.ToString()), StaticData.db_connetion_string);
                query.Fill(result);

                //ORDER_DETAIL 저장
                for (int row = 0; row < dgvVOrderDetail.Rows.Count; row++)
                {
                    result = new System.Data.DataTable();
                    query = new SqlDataAdapter(String.Format("EXEC order_create_detail '{0}', {1}, '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', {8}, {9}, {10}, {11}, '{12}', '{13}'",
                        ReOrderNumber,
                        dgvVOrderDetail.Rows[row].Cells["NO"].Value.ToString(),
                        dgvVOrderDetail.Rows[row].Cells["도번"].Value.ToString(),
                        dgvVOrderDetail.Rows[row].Cells["REV"].Value.ToString(),
                        dgvVOrderDetail.Rows[row].Cells["모델"].Value.ToString(),
                        dgvVOrderDetail.Rows[row].Cells["품명"].Value.ToString(),
                        dgvVOrderDetail.Rows[row].Cells["규격"].Value.ToString(),
                        dgvVOrderDetail.Rows[row].Cells["단위"].Value.ToString()[0],
                        dgvVOrderDetail.Rows[row].Cells["단가"].Value.ToString(),
                        dgvVOrderDetail.Rows[row].Cells["수량"].Value.ToString(),
                        dgvVOrderDetail.Rows[row].Cells["소계"].Value.ToString(),
                        dgvVOrderDetail.Rows[row].Cells["부가세"].Value.ToString(),
                        dgvVOrderDetail.Rows[row].Cells["납기일"].Value.ToString(),
                        dgvVOrderDetail.Rows[row].Cells["비고"].Value.ToString()), StaticData.db_connetion_string);
                    query.Fill(result);
                }

                ServerSearch = true;
                BindOrderList();
            }
        }

        private void dgvOrderFormatting(DataGridView dgvOrder)
        {
            dgvOrder.Columns["발주번호"].Width = 90;
            dgvOrder.Columns["생성일자"].Width = 140;
            dgvOrder.Columns["업체명"].Width = 120;
            dgvOrder.Columns["거래구분"].Width = 100;
            dgvOrder.Columns["담당자"].Width = 80;
            dgvOrder.Columns["승인자"].Width = 80;
            dgvOrder.Columns["승인일자"].Width = 140;
            dgvOrder.Columns["상태"].Width = 70;
            dgvOrder.Columns["업체전달사항"].Width = 400;
            dgvOrder.Columns["발주메모"].Width = 400;
            dgvOrder.Columns["VAT"].Width = 0;
            dgvOrder.Columns["발주일자"].Width = 140;
            dgvOrder.Columns["MAKER_CODE"].Width = 0;
            dgvOrder.Columns["TEL"].Width = 0;

            dgvOrder.Columns["발주번호"].ReadOnly = true;
            dgvOrder.Columns["생성일자"].ReadOnly = true;
            dgvOrder.Columns["업체명"].ReadOnly = true;
            dgvOrder.Columns["거래구분"].ReadOnly = true;
            dgvOrder.Columns["담당자"].ReadOnly = true;
            dgvOrder.Columns["승인자"].ReadOnly = true;
            dgvOrder.Columns["승인일자"].ReadOnly = true;
            dgvOrder.Columns["상태"].ReadOnly = true;
            dgvOrder.Columns["업체전달사항"].ReadOnly = true;
            dgvOrder.Columns["발주일자"].ReadOnly = true;

            dgvOrder.Columns["NOTIFY_DISPLAY"].Visible = false;
            dgvOrder.Columns["MAKER_CODE"].Visible = false;
            dgvOrder.Columns["TEL"].Visible = false;

            for (int i = 0; i < dgvOrder.Rows.Count; i++)
            {
                if (dgvOrder.Rows[i].Cells["상태"].Value.ToString() == "A")
                {
                    dgvOrder.Rows[i].Cells["상태"].Value = "승인";
                    dgvOrder.Rows[i].DefaultCellStyle.BackColor = Color.WhiteSmoke;
                }
                else if (dgvOrder.Rows[i].Cells["상태"].Value.ToString() == "B")
                {
                    dgvOrder.Rows[i].Cells["상태"].Value = "반려";
                    dgvOrder.Rows[i].DefaultCellStyle.BackColor = Color.LightPink;
                }
                else if (dgvOrder.Rows[i].Cells["상태"].Value.ToString() == "W")
                    dgvOrder.Rows[i].Cells["상태"].Value = "등록";

                else if (dgvOrder.Rows[i].Cells["상태"].Value.ToString() == "R")
                {
                    dgvOrder.Rows[i].Cells["상태"].Value = "승인요청";
                    dgvOrder.Rows[i].DefaultCellStyle.BackColor = Color.LightYellow;
                }
            }
        }

        private void dgvOrderDetailFormatting(DataGridView dgvOrderDetail, bool isApprove)
        {
            if (dgvOrderDetail != null)
            {
                dgvOrderDetail.Columns["NO"].Width = 65;
                dgvOrderDetail.Columns["모델"].Width = 70;
                dgvOrderDetail.Columns["도면"].Width = 65;
                dgvOrderDetail.Columns["도번"].Width = 90;
                dgvOrderDetail.Columns["REV"].Width = 65;
                dgvOrderDetail.Columns["품명"].Width = 120;
                dgvOrderDetail.Columns["규격"].Width = 100;
                dgvOrderDetail.Columns["단위"].Width = 65;
                dgvOrderDetail.Columns["수량"].Width = 70;
                dgvOrderDetail.Columns["단가"].Width = 100;
                dgvOrderDetail.Columns["소계"].Width = 100;
                dgvOrderDetail.Columns["부가세"].Width = 100;
                dgvOrderDetail.Columns["납기일"].Width = 120;
                dgvOrderDetail.Columns["비고"].Width = 200;
                dgvOrderDetail.Columns["KEY_ID"].Width = 1;

                dgvOrderDetail.Columns["NO"].ReadOnly = true;
                dgvOrderDetail.Columns["모델"].ReadOnly = true;
                dgvOrderDetail.Columns["도면"].ReadOnly = true;
                dgvOrderDetail.Columns["도번"].ReadOnly = true;
                dgvOrderDetail.Columns["REV"].ReadOnly = true;
                dgvOrderDetail.Columns["품명"].ReadOnly = true;
                dgvOrderDetail.Columns["규격"].ReadOnly = true;
                dgvOrderDetail.Columns["단위"].ReadOnly = true;

#if false
                dgvOrderDetail.Columns["수량"].ReadOnly = true;
                dgvOrderDetail.Columns["단가"].ReadOnly = true;
#else
                if(user_level == 7 && isApprove == true)
                {
                    dgvOrderDetail.Columns["수량"].ReadOnly = false;
                    dgvOrderDetail.Columns["단가"].ReadOnly = false;
                }
                else
                {
                    dgvOrderDetail.Columns["수량"].ReadOnly = true;
                    dgvOrderDetail.Columns["단가"].ReadOnly = true;
                }
#endif
                dgvOrderDetail.Columns["소계"].ReadOnly = true;
                dgvOrderDetail.Columns["부가세"].ReadOnly = true;
                dgvOrderDetail.Columns["납기일"].ReadOnly = true;
                dgvOrderDetail.Columns["비고"].ReadOnly = false;
                dgvOrderDetail.Columns["KEY_ID"].ReadOnly = true;
                
                dgvOrderDetail.Columns["수량"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvOrderDetail.Columns["수량"].DefaultCellStyle.Format = "#,###";
                dgvOrderDetail.Columns["단가"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvOrderDetail.Columns["단가"].DefaultCellStyle.Format = "#,###";
                dgvOrderDetail.Columns["소계"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvOrderDetail.Columns["소계"].DefaultCellStyle.Format = "#,###";
                dgvOrderDetail.Columns["부가세"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvOrderDetail.Columns["부가세"].DefaultCellStyle.Format = "#,###";

                for (int i = 0; i < dgvOrderDetail.Rows.Count; i++)
                {
                    switch (dgvOrderDetail.Rows[i].Cells["단위"].Value.ToString())
                    {
                        case "B": dgvOrderDetail.Rows[i].Cells["단위"].Value = "BOX";         break;
                        case "E": dgvOrderDetail.Rows[i].Cells["단위"].Value = "EA";          break;
                        case "K": dgvOrderDetail.Rows[i].Cells["단위"].Value = "KG";          break;
                        case "L": dgvOrderDetail.Rows[i].Cells["단위"].Value = "LITER";       break;
                        case "S": dgvOrderDetail.Rows[i].Cells["단위"].Value = "SET";         break;
                        case "U": dgvOrderDetail.Rows[i].Cells["단위"].Value = "UNDEFINED";   break;
                    }

                    if (dgvOrderDetail.Rows[i].Cells["도번"].Value.ToString() != "")
                    {
                        DataGridViewButtonCell btnDrawingViewColumn = new DataGridViewButtonCell();
                        dgvOrderDetail.Rows[i].Cells[2] = btnDrawingViewColumn;
                    }
                }
            }
        }

        private void DrawingButtonOrderDetail(DataGridView dgvOrderDetail, DataGridViewCellEventArgs e)
        {
            if (dgvOrderDetail.Rows.Count == 0)
                return;

            if (dgvOrderDetail.Columns[e.ColumnIndex].Name == "도면")
            {
                try
                {
                    string dwgDrawingNumber = dgvOrderDetail.Rows[e.RowIndex].Cells["도번"].Value.ToString();
                    string dwgRevice = dgvOrderDetail.Rows[e.RowIndex].Cells["REV"].Value.ToString();

                    if (dwgDrawingNumber == "")
                        return;

                    if (dwgRevice != "")
                    {
                        if (dwgDrawingNumber.Length == 10)
                            dwgDrawingNumber += string.Format("_{0}", dwgRevice);
                    }

                    DirectoryInfo di = new DirectoryInfo(Const.LOCAL_VIEW_PATH);
                    if (di.Exists == false)
                        di.Create();

                    Drawing dwg = new Drawing();
                    string strResult = dwg.Download(dwgDrawingNumber);
                    if (strResult.Length > 0)
                    {
                        MessageBox.Show(string.Format("{0}", strResult), "확인");
                        return;
                    }
                    else
                    {
                        string ViewPath = "";
                        foreach (var file in di.GetFiles())
                        {
                            ViewPath = Path.Combine(Const.LOCAL_VIEW_PATH, file.Name);
                            break;
                        }

                        Process process = new Process();
                        process.StartInfo.FileName = ViewPath;
                        process.StartInfo.UseShellExecute = true;
                        process.StartInfo.ErrorDialog = true;
                        process.Start();
                    }
                }
                catch (Exception ex)
                {
                    ex.ToString();
                }
            }
        }

        private void ColumnsSortDisable(DataGridView dgvOrderDetail)
        {
            foreach (DataGridViewColumn i in dgvOrderDetail.Columns)
                i.SortMode = DataGridViewColumnSortMode.NotSortable;
        }

        private void PriceCalculartion(string flag_vat, DataGridView dgvOrderDetail)
        {
            int iTotal = 0;
            int iVatTotal = 0;
            int iGrandTotal = 0;

            for (int i = 0; i < dgvOrderDetail.Rows.Count; i++)
            {
                int iUnitPrice = int.Parse(dgvOrderDetail.Rows[i].Cells["단가"].Value.ToString());
                int iQty = int.Parse(dgvOrderDetail.Rows[i].Cells["수량"].Value.ToString());

                if (iUnitPrice == 0 || iQty == 0)
                    continue;

                dgvOrderDetail.Rows[i].Cells["소계"].Value = iUnitPrice * iQty;

                if (flag_vat == "E")
                    dgvOrderDetail.Rows[i].Cells["부가세"].Value = (iUnitPrice * iQty) * 0.1;
                else if (flag_vat == "C")
                    dgvOrderDetail.Rows[i].Cells["부가세"].Value = 0;

                iTotal += int.Parse(dgvOrderDetail.Rows[i].Cells["소계"].Value.ToString());
                iVatTotal += int.Parse(dgvOrderDetail.Rows[i].Cells["부가세"].Value.ToString());
            }

            iGrandTotal = iTotal + iVatTotal;
            if (tabOrderMagement.SelectedTab.Text == "등록")
            {           
                txtRTotal.Text = String.Format("{0:#,###}", iTotal);
                txtRTotalVAT.Text = String.Format("{0:#,###}", iVatTotal);
                txtRGrandTotal.Text = String.Format("{0:#,###}", iGrandTotal);
            }
            else if (tabOrderMagement.SelectedTab.Text == "결재")
            {
                txtRQTotal.Text = String.Format("{0:#,###}", iTotal);
                txtRQTotalVAT.Text = String.Format("{0:#,###}", iVatTotal);
                txtRQGrandTotal.Text = String.Format("{0:#,###}", iGrandTotal);
            }
            else if (tabOrderMagement.SelectedTab.Text == "열람")
            {
                txtVTotal.Text = String.Format("{0:#,###}", iTotal);
                txtVTotalVAT.Text = String.Format("{0:#,###}", iVatTotal);
                txtVGrandTotal.Text = String.Format("{0:#,###}", iGrandTotal);
            }
        }

        private void BindStatus()
        {
            System.Data.DataTable result = new System.Data.DataTable();
            SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC code_info 7"), StaticData.db_connetion_string);
            query.Fill(result);

            result.Rows.Add("");
            cmbSStatus.ValueMember = "CODE_NAME";
            cmbSStatus.DisplayMember = "CODE_VALUE";
            cmbSStatus.DataSource = result;
            cmbSStatus.SelectedValue = "";
        }

        private void BindReverseCategorization()
        {
            System.Data.DataTable result = new System.Data.DataTable();
            SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC code_info 8"), StaticData.db_connetion_string);
            query.Fill(result);

            result.Rows.Add("");
            cmbSCategorization.ValueMember = "CODE_NAME";
            cmbSCategorization.DisplayMember = "CODE_VALUE";
            cmbSCategorization.DataSource = result;
            cmbSCategorization.SelectedValue = "";
        }

        private void dgvToExcel(bool isViewExcel)
        {
            try
            {
                ExcelCore.Application app = null;
                ExcelCore.Workbook wb = null;
                ExcelCore.Worksheet wksheet = null;
                app = new ExcelCore.Application();

                string strTargetViewPath = Path.Combine(strViewPath, string.Format("V_{0}.xlsx", dgvVOrder.Rows[dgvVOrder.CurrentRow.Index].Cells["발주번호"].Value.ToString()));
#if false
                string strTargetPath = Path.Combine(strMailZipPath, string.Format("{0}.xlsx", dgvVOrder.Rows[dgvVOrder.CurrentRow.Index].Cells["발주번호"].Value.ToString()));
#else
                //zip파일에 Write하도록 수정
                string strTargetMailPath = Path.Combine(strMailZipPath, string.Format("{0}.xlsx", dgvVOrder.Rows[dgvVOrder.CurrentRow.Index].Cells["발주번호"].Value.ToString()));
#endif
                if (isViewExcel)
                {
                    File.Copy(strOrderFile, strTargetViewPath, true);
                    wb = app.Workbooks.Open(strTargetViewPath);
                }
                else
                {
                    File.Copy(strOrderFile, strTargetMailPath, true);
                    wb = app.Workbooks.Open(strTargetMailPath);
                }
                
                wksheet = wb.Worksheets.get_Item(1) as ExcelCore.Worksheet;

                wksheet.Range["J2"].Value = "'" + Now.ToString("yyyy-MM-dd").Substring(0, 10);
                wksheet.Range["O2"].Value = dgvVOrder.Rows[dgvVOrder.CurrentRow.Index].Cells["발주번호"].Value.ToString();
                wksheet.Range["C3"].Value = dgvVOrder.Rows[dgvVOrder.CurrentRow.Index].Cells["업체명"].Value.ToString();

                System.Data.DataTable dtMaker = ResultMaker(wksheet.Range["C3"].Value);
                if (dtMaker.Rows.Count == 1)
                {
                    wksheet.Range["C4"].Value = dtMaker.Rows[0]["담당자"].ToString();
                    wksheet.Range["C5"].Value = "'" + dtMaker.Rows[0]["전화"].ToString();
                    wksheet.Range["C6"].Value = dtMaker.Rows[0]["팩스"].ToString();
                    wksheet.Range["C7"].Value = dtMaker.Rows[0]["E메일"].ToString();

                    switch (dtMaker.Rows[0]["지불방법"].ToString())
                    {
                        case "E":   wksheet.Range["J9"].Value = "현금결제";         break;
                        case "P":   wksheet.Range["J9"].Value = "선결제";           break;
                        case "R":   wksheet.Range["J9"].Value = "정기결제";         break;
                    }
                }

                wksheet.Range["J6"].Value = dgvVOrder.Rows[dgvVOrder.CurrentRow.Index].Cells["담당자"].Value.ToString();
                wksheet.Range["J7"].Value = dgvVOrder.Rows[dgvVOrder.CurrentRow.Index].Cells["TEL"].Value.ToString();
                wksheet.Range["C9"].Value = txtVTotal.Text;

                if (dgvVOrder.Rows[dgvVOrder.CurrentRow.Index].Cells["VAT"].Value.ToString() == "E")
                    wksheet.Range["C10"].Value = "별도";
                else if (dgvVOrder.Rows[dgvVOrder.CurrentRow.Index].Cells["VAT"].Value.ToString() == "C")
                    wksheet.Range["C10"].Value = "포함";

                //본문 -- 20행 초과시 Excel행 추가
                int AddExcelRowCount = 0;        
                if (dgvVOrderDetail.Rows.Count > 20)
                {
                    AddExcelRowCount = dgvVOrderDetail.Rows.Count - 20; //AddExcelRowCount 만큼 행을 추가 한다.
                    for (int i = 0; i < AddExcelRowCount; i++)
                    {
                        ExcelCore.Range select_range = wksheet.Range["A32", "P32"];
                        select_range.Select();
                        select_range.Copy();
                        ExcelCore.Range add_range = wksheet.Range["A33", "P33"];
                        add_range.Select();
                        add_range.Insert();
                    }

                    ExcelCore.Range add_range_height = wksheet.Range["A33", "P42"];
                    add_range_height.RowHeight = 25.5;
                }

                for (int i = 0; i < dgvVOrderDetail.Rows.Count; i++)    //본문
                {
                    wksheet.Range[string.Format("A{0}", i + 13)].Value = (i + 1).ToString();
                    wksheet.Range[string.Format("B{0}", i + 13)].Value = dgvVOrderDetail.Rows[i].Cells["도번"].Value.ToString();
                    wksheet.Range[string.Format("D{0}", i + 13)].Value = dgvVOrderDetail.Rows[i].Cells["품명"].Value.ToString();
                    wksheet.Range[string.Format("H{0}", i + 13)].Value = dgvVOrderDetail.Rows[i].Cells["규격"].Value.ToString();
                    wksheet.Range[string.Format("J{0}", i + 13)].Value = dgvVOrderDetail.Rows[i].Cells["단위"].Value.ToString();
                    wksheet.Range[string.Format("K{0}", i + 13)].Value = dgvVOrderDetail.Rows[i].Cells["수량"].Value.ToString();
                    wksheet.Range[string.Format("L{0}", i + 13)].Value = dgvVOrderDetail.Rows[i].Cells["단가"].Value.ToString();
                    wksheet.Range[string.Format("N{0}", i + 13)].Value = dgvVOrderDetail.Rows[i].Cells["소계"].Value.ToString();
                    wksheet.Range[string.Format("P{0}", i + 13)].Value = "'" + dgvVOrderDetail.Rows[i].Cells["납기일"].Value.ToString().Substring(0, 10);
                }

                if (dgvVOrder.Rows[dgvVOrder.CurrentRow.Index].Cells["VAT"].Value.ToString() == "E")
                    wksheet.Range[string.Format("M{0}", AddExcelRowCount + 33)].Value = "별도";
                else if (dgvVOrder.Rows[dgvVOrder.CurrentRow.Index].Cells["VAT"].Value.ToString() == "C")
                    wksheet.Range[string.Format("M{0}", AddExcelRowCount + 33)].Value = "포함";

                wksheet.Range[string.Format("N{0}", AddExcelRowCount + 33)].Value = txtVTotal.Text;
                wksheet.Range[string.Format("D{0}", AddExcelRowCount + 34)].Value = dgvVOrder.Rows[dgvVOrder.CurrentRow.Index].Cells["업체전달사항"].Value.ToString();

                wb.Save();
                wb = null;
                app.Quit();

                if (isViewExcel == true)
                {
                    MessageBox.Show("발주서: Excel 작업이 완료 되었습니다.", "결과");

                    wb = app.Workbooks.Open(strTargetViewPath);
                    wksheet = wb.Worksheets.get_Item(1) as ExcelCore.Worksheet;
                    app.Visible = true;
                }    
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            finally
            {
                //if (isViewExcel == false)
                //    KillProcessExcel();
            }
        }

        private void KillProcessExcel()
        {
            try
            {
                Process[] ExcelPros = Process.GetProcessesByName("EXCEL"); // 안죽은 엑셀 프로세스 죽이기
                for (int i = 0; i < ExcelPros.Length; i++)
                    ExcelPros[i].Kill();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }

        private DataTable ResultMaker(string strMaker)
        {
            dtMaker = new System.Data.DataTable();
            SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC management_maker 'N', '{0}'", strMaker), StaticData.db_connetion_string);
            query.Fill(dtMaker);

            return dtMaker;
        }

        private void UpdateOrderDate(string orderNumber)
        {
            DataTable result = new DataTable();
            SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC order_list_update '{0}', 'D', ''", orderNumber), StaticData.db_connetion_string);
            query.Fill(result);

            ServerSearch = true;
            BindOrderList();
        }

        private void ExcelFilePath(string strFlag)
        {
            try
            {
                string strGetDiretory = System.IO.Directory.GetCurrentDirectory().ToString();
                strOrderFile = string.Format("{0}\\{1}", strGetDiretory, Const.ORDER_FILE_NAME);

                DirectoryInfo di = new DirectoryInfo(strViewPath);
                if (di.Exists == false)
                    di.Create();

                di = new DirectoryInfo(strMailPath);
                if (di.Exists == false)
                {
                    di.Create();

                    di.Attributes = FileAttributes.ReadOnly;
                    if ((di.Attributes & FileAttributes.Hidden) != FileAttributes.Hidden)
                        di.Attributes = di.Attributes | FileAttributes.Hidden;
                }

                di = new DirectoryInfo(strMailZipPath);
                if (di.Exists == false)
                {
                    di.Create();

                    di.Attributes = FileAttributes.ReadOnly;
                    if ((di.Attributes & FileAttributes.Hidden) != FileAttributes.Hidden)
                        di.Attributes = di.Attributes | FileAttributes.Hidden;
                }

                if (strFlag == "M")
                {
                    DirectoryInfo dirInfo = new DirectoryInfo(strMailPath);
                    foreach (var file in dirInfo.GetFiles())
                    {
                        FileInfo fileMode = new FileInfo(strMailPath + "\\" + file.ToString());
                        fileMode.IsReadOnly = false;

                        file.Delete();
                    }

                    dirInfo = new DirectoryInfo(strMailZipPath);
                    foreach (var file in dirInfo.GetFiles())
                    {
                        FileInfo fileMode = new FileInfo(strMailZipPath + "\\" + file.ToString());
                        fileMode.IsReadOnly = false;

                        file.Delete();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnPartList_Click(object sender, EventArgs e)
        {
            FormPartList frmPartList = new FormPartList(user_level);
            frmPartList.Show();
        }

        private void btnCancelApprove_Click(object sender, EventArgs e)
        {
            if (dgvVOrder.Rows.Count > 0)
            {
                if (dgvVOrder.Rows[dgvVOrder.CurrentRow.Index].Cells["담당자"].Value.ToString() == StaticData.user_name 
                    && dgvVOrder.Rows[dgvVOrder.CurrentRow.Index].Cells["상태"].Value.ToString() == "승인요청")
                {
                    System.Data.DataTable result = new System.Data.DataTable();
                    SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC order_list_update '{0}', 'S', 'B'",
                        dgvVOrder.Rows[dgvVOrder.CurrentRow.Index].Cells["발주번호"].Value.ToString()), StaticData.db_connetion_string);

                    query.Fill(result);

                    ServerSearch = true;
                    BindOrderList();
                }
                else
                    MessageBox.Show("승인요청 상태에서만 변경이 됩니다.", "경고");
            }
        }

    }
}
