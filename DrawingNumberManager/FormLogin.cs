﻿using System;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

namespace DrawingNumberManager
{
    //버전 수정 내용
    //3.0.0.11 1. 발주서(열람) - 승인상태 에서 단가, 수량 변경 가능
    //         2. 값 관련 포멧 변경(100단위 콤마 추가)
    //         3. 품목리스트 창 공용 할 수 있도록 프로그램 변경
    //         4. 발주서 역조사 중 규격으로 검색 할 수 있도록 프로그램 변경
    //         5. 구매품 발주인 경우 모델을 등록하지 않으면 저장이 안되도록 기능 개선
    //         6. 사용자 이름 가나다 순으로 변경
    //         7. 가도번에 모델, 파트를 등록하지 않으면 도번이 생성이 되지 않도록 기능 개선
    //         8. 열람에서 승인요청 상태에서 담당자가 승인취소를 할수 있도록 기능 개선
    //         9. 각 Form 화면 사이즈 변경
    //3.0.0.10 1. FAX번호 변경
    //3.0.0.9  1. 발주 등록에서 행 추가를 하면 기존 공백이었던 것을 => 현재날짜 + 7일 되도록 개선
    //3.0.0.8  1. 메일 참조 할 수 있도록 기능 개선(구분자는 ,)
    //         2. 발주서 Excel에서 익월말 -> 현금결제 수정
    //         3. 가도번 DetailContent 초기화 부분 삭제
    //         4. 발주서 도면 파일 압축 파일로 지정하도록 기능 개선
    //3.0.0.7  1. 발주 등록 화면에 PartList에 업체 추가
    //         2. PartList에 공용부품 추가 시 Model, Part를 등록하도록 요청 메시지 추가
    //3.0.0.6  1.PartList 수정시 MODEL, PART를 읽지 못하는 증상 관련 수정(SP get_code_name 추가)
    //3.0.0.5: 1.DrawingNumberManger 중복 실행 방지.
    //         2.Maker에서 Import기능 안보이도록 수정.
    //3.0.0.4: 1.도면보기에서 .bak파일을 먼저 읽는 현상 관련해서 수정
    //3.0.0.3: 1.발주서[열람] 비고를 수정 할 수 있도록 기능 개선.
    //         2.발주서 저장 시 소계가 0이면 저장이 안되는 것을 수량이 0이면 안되는 것으로 수정. 
    //3.0.0.2: 1.Part List 컬럼 Sort시 도면 파일이 있는지 여부 미 표시.
    //         2.발주서 도면 클릭 시 중복된 도면 번호 내용 발생 수정.
    //         3.발주서 등록시 수량 단가가 바뀌어서 들어가는 현상 수정.
    //3.0.0.1:최초 배포

    public partial class FormLogin : Form
    {
        public User user = null;
		String crc32Password = null;
		int PWChangeCount = 0;

		Ini clsIni = new Ini();
		public FormLogin()
        {
            InitializeComponent();
        }

        private void initialize()
        {
            this.Text = Const.FORM_LOGIN;
            this.txtID.MaxLength = 10;
            this.txtPW.MaxLength = 10;
            this.txtPW.PasswordChar = '*';
			this.lblVersion.Text = lblVersion.Text + Version();
        }

        private void FormLogin_Load(object sender, EventArgs e)
        {
			Color myRgbColor = new Color();
			myRgbColor = Color.FromArgb(255, 255, 255);

            initialize();
			clsIni.LoadIni();

			if (StaticData.user_id.Length > 0)
            {
                if (StaticData.remember_id_pw == "Y")
                {
                    txtID.Text = StaticData.user_id;
                    txtPW.Text = StaticData.user_password;
                    cbRemermberID_PW.Checked = true;
                }
            }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                bool bCheckedInvaild = CheckInvaild();
                if (bCheckedInvaild == true)
                    DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
				MessageBox.Show(ex.ToString());
            }
        }

        private void txtID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtPW.Focus();
            }
        }

        private void txtPW_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnLogin_Click(null, null);
            }
        }

		private void txtPW_TextChanged(object sender, EventArgs e)
		{
			//변수로 들어간 txtPW_TextChanged 1이지만 타자를 쳤을때는 한자 한자 처리를 하므로 Count는 1이상이다.
			PWChangeCount++;
		}

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

		private void cbRemermberID_PW_CheckedChanged(object sender, EventArgs e)
		{
            if (cbRemermberID_PW.Checked == false)
                StaticData.remember_id_pw = "N";
		}

        private bool CheckInvaild()
        {
            if (txtID.Text.Length == 0)
            {
				MessageBox.Show("사용자 ID를 입력하여 주십시오..", "Warning");
                txtID.Focus();
                return false;
            }

            if (txtPW.Text.Length == 0)
            {
				MessageBox.Show("사용자 Password를 입력하여 주십시오..", "Warning");
                txtPW.Text = "";
                txtPW.Focus();
                return false;
            }

            if (txtID.Text.Length > 0 && txtPW.Text.Length > 0)
            {
				crc32Password = String.Empty;
                if (StaticData.remember_id_pw == "N" || StaticData.remember_id_pw.Length == 0 || PWChangeCount > 1)
                {
                    Crc32 crc32 = new Crc32();
					byte[] bPassword = Encoding.UTF8.GetBytes(txtPW.Text);
                    foreach (byte b in crc32.ComputeHash(bPassword))
						crc32Password += b.ToString("x2").ToUpper();
                }
				else
					crc32Password = StaticData.user_password;

				user = User.Login(txtID.Text, crc32Password);
				if (user == null)
				{
					MessageBox.Show("ID 또는 Password 및 권한(이)가 맞는지 확인을 하십시오.!");
					return false;
				}
                //대문자를 입력하더라도 소문자로 변환해서 확인
                if (txtID.Text != user.ID)
                {
                    if (user.ID != txtID.Text.ToLower() || user.ID != txtID.Text.ToUpper())
                    {
						MessageBox.Show("사용자 정보가 없습니다. \r 관리자에게 문의를 하십시오.", "Warning");
                        return false;
                    }
                }

                SaveIni();
            }
            return true;
        }

        private void SaveIni()
        {
            StaticData.user_id = user.ID;
            StaticData.user_password = crc32Password;
            StaticData.category = user.DepartmentCode.ToString();

			if (cbRemermberID_PW.Checked == true)
                StaticData.remember_id_pw = "Y";
			else
                StaticData.remember_id_pw = "N";

			clsIni.SaveIni();
        }


		private string Version()
		{
			string returnVersion = "";

			try
			{
				//ClickOnce의 버젼 취득
				returnVersion = System.Deployment.Application.ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
			}
			catch (System.Deployment.Application.DeploymentException ex)
			{
				//ClickOnce배포가 아니므로 어셈블리버젼을 취득
				ex.ToString();
				returnVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
			}
			catch (Exception ex)
			{
				ex.ToString();
				returnVersion = "확인불가";
			}

			return returnVersion;
		}


    }
}
