﻿namespace DrawingNumberManager
{
	partial class FormModifyPassword
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.lblBeforePassword = new System.Windows.Forms.Label();
            this.lblModifyPassword = new System.Windows.Forms.Label();
            this.lblComfirmPassword = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.txtBeforePassword = new System.Windows.Forms.TextBox();
            this.txtModifyPassword = new System.Windows.Forms.TextBox();
            this.txtComfirmPassword = new System.Windows.Forms.TextBox();
            this.lblSplit = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblBeforePassword
            // 
            this.lblBeforePassword.AutoSize = true;
            this.lblBeforePassword.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblBeforePassword.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lblBeforePassword.Location = new System.Drawing.Point(2, 12);
            this.lblBeforePassword.Name = "lblBeforePassword";
            this.lblBeforePassword.Size = new System.Drawing.Size(88, 12);
            this.lblBeforePassword.TabIndex = 0;
            this.lblBeforePassword.Text = "현재 비밀번호";
            // 
            // lblModifyPassword
            // 
            this.lblModifyPassword.AutoSize = true;
            this.lblModifyPassword.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblModifyPassword.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lblModifyPassword.Location = new System.Drawing.Point(2, 52);
            this.lblModifyPassword.Name = "lblModifyPassword";
            this.lblModifyPassword.Size = new System.Drawing.Size(75, 12);
            this.lblModifyPassword.TabIndex = 1;
            this.lblModifyPassword.Text = "새 비밀번호";
            // 
            // lblComfirmPassword
            // 
            this.lblComfirmPassword.AutoSize = true;
            this.lblComfirmPassword.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblComfirmPassword.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lblComfirmPassword.Location = new System.Drawing.Point(2, 78);
            this.lblComfirmPassword.Name = "lblComfirmPassword";
            this.lblComfirmPassword.Size = new System.Drawing.Size(106, 12);
            this.lblComfirmPassword.TabIndex = 2;
            this.lblComfirmPassword.Text = "새 비밀번호 확인";
            // 
            // btnOK
            // 
            this.btnOK.BackColor = System.Drawing.Color.LimeGreen;
            this.btnOK.Location = new System.Drawing.Point(224, 102);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(60, 33);
            this.btnOK.TabIndex = 3;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = false;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // txtBeforePassword
            // 
            this.txtBeforePassword.BackColor = System.Drawing.Color.Snow;
            this.txtBeforePassword.Location = new System.Drawing.Point(114, 9);
            this.txtBeforePassword.Name = "txtBeforePassword";
            this.txtBeforePassword.PasswordChar = '*';
            this.txtBeforePassword.Size = new System.Drawing.Size(170, 21);
            this.txtBeforePassword.TabIndex = 5;
            // 
            // txtModifyPassword
            // 
            this.txtModifyPassword.BackColor = System.Drawing.Color.Snow;
            this.txtModifyPassword.Location = new System.Drawing.Point(114, 49);
            this.txtModifyPassword.Name = "txtModifyPassword";
            this.txtModifyPassword.PasswordChar = '*';
            this.txtModifyPassword.Size = new System.Drawing.Size(170, 21);
            this.txtModifyPassword.TabIndex = 6;
            // 
            // txtComfirmPassword
            // 
            this.txtComfirmPassword.BackColor = System.Drawing.Color.Snow;
            this.txtComfirmPassword.Location = new System.Drawing.Point(114, 75);
            this.txtComfirmPassword.Name = "txtComfirmPassword";
            this.txtComfirmPassword.PasswordChar = '*';
            this.txtComfirmPassword.Size = new System.Drawing.Size(170, 21);
            this.txtComfirmPassword.TabIndex = 7;
            // 
            // lblSplit
            // 
            this.lblSplit.AutoSize = true;
            this.lblSplit.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.lblSplit.Location = new System.Drawing.Point(3, 33);
            this.lblSplit.Name = "lblSplit";
            this.lblSplit.Size = new System.Drawing.Size(281, 12);
            this.lblSplit.TabIndex = 8;
            this.lblSplit.Text = "----------------------------------------------";
            // 
            // FormModifyPassword
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.ClientSize = new System.Drawing.Size(290, 138);
            this.Controls.Add(this.lblSplit);
            this.Controls.Add(this.txtComfirmPassword);
            this.Controls.Add(this.txtModifyPassword);
            this.Controls.Add(this.txtBeforePassword);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lblComfirmPassword);
            this.Controls.Add(this.lblModifyPassword);
            this.Controls.Add(this.lblBeforePassword);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "FormModifyPassword";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormModifyPassword";
            this.Load += new System.EventHandler(this.FormModifyPassword_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblBeforePassword;
		private System.Windows.Forms.Label lblModifyPassword;
		private System.Windows.Forms.Label lblComfirmPassword;
		private System.Windows.Forms.Button btnOK;
		private System.Windows.Forms.TextBox txtBeforePassword;
		private System.Windows.Forms.TextBox txtModifyPassword;
		private System.Windows.Forms.TextBox txtComfirmPassword;
		private System.Windows.Forms.Label lblSplit;
	}
}