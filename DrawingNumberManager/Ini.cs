﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;


namespace DrawingNumberManager
{
    public class Ini
    {
        [DllImport("kernel32.dll")]
        private static extern int GetPrivateProfileString(
            String section, 
            String Key, 
            String def, 
            StringBuilder retVal, 
            int Size, 
            String filePath);

        [DllImport("Kernel32.dll")]
        private static extern long WritePrivateProfileString(
            String section, 
            String Key, 
            String val, 
            String filePath);

		// 자체 DB접속 문자열
		string database = "";
		public string Database
		{
			get { return database; }
		}

		string category = "";
		public string Category
		{
			get { return category; }
			set { category = value; }
		}

		string user_id = "";
		public string UserID
		{
			get { return user_id; }
			set { user_id = value; }
		}

		string user_pw = "";
		public string UserPW
		{
			get { return user_pw; }
			set { user_pw = value; }
		}

		string remember_id_pw = "";
		public string RemberID_PW
		{
			get { return remember_id_pw; }
			set { remember_id_pw = value; }
		}

        string save_server_path = "";
        public string SaveServerPath
        {
            get { return save_server_path; }
            set { save_server_path = value; }
        }

        string save_local_path = "";
        public string SaveLocalPath
        {
            get { return save_local_path; }
            set { save_local_path = value; }
        }


		private string strIniFile = "";

        //생성자
		public Ini()
        {
			string strGetDiretory = System.IO.Directory.GetCurrentDirectory().ToString();
			strIniFile = string.Format("{0}\\{1}", strGetDiretory, Const.INI_FILE_NAME);
        }

		public void LoadIni()
		{
            StaticData.db_connetion_string = IniReadValue(Const.INI_SECTION_MAIN, Const.INI_KEY_DB_CONN_STR, strIniFile);
            StaticData.category = IniReadValue(Const.INI_SECTION_MAIN, Const.INI_KEY_DEPARTMENT_STR, strIniFile);
            StaticData.user_id = IniReadValue(Const.INI_SECTION_MAIN, Const.INI_KEY_ID, strIniFile);
            StaticData.user_password = IniReadValue(Const.INI_SECTION_MAIN, Const.INI_KEY_PW, strIniFile);
            StaticData.remember_id_pw = IniReadValue(Const.INI_SECTION_MAIN, Const.INI_KEY_REMEMBER_ID_PW, strIniFile);
            StaticData.server_path = IniReadValue(Const.INI_SECTION_MAIN, Const.INI_SERVER_PATH, strIniFile);
            StaticData.local_path = IniReadValue(Const.INI_SECTION_MAIN, Const.INI_LOCAL_PATH, strIniFile);
		}

		public void SaveIni()
		{
            if (StaticData.category.Length > 0)
                IniWriteValue(Const.INI_SECTION_MAIN, Const.INI_KEY_DEPARTMENT_STR, StaticData.category, strIniFile);
            if (StaticData.user_id.Length > 0)
                IniWriteValue(Const.INI_SECTION_MAIN, Const.INI_KEY_ID, StaticData.user_id, strIniFile);
            if (StaticData.user_password.Length > 0)
                IniWriteValue(Const.INI_SECTION_MAIN, Const.INI_KEY_PW, StaticData.user_password, strIniFile);
			if (StaticData.remember_id_pw.Length > 0)
                IniWriteValue(Const.INI_SECTION_MAIN, Const.INI_KEY_REMEMBER_ID_PW, StaticData.remember_id_pw, strIniFile);
            if (StaticData.server_path.Length > 0)
                IniWriteValue(Const.INI_SECTION_MAIN, Const.INI_SERVER_PATH, StaticData.server_path, strIniFile);
            if (StaticData.local_path.Length > 0)
                IniWriteValue(Const.INI_SECTION_MAIN, Const.INI_LOCAL_PATH, StaticData.local_path, strIniFile);
		}

        private void IniWriteValue(String Section, String Key, String Value, string avaPath)
        {
            WritePrivateProfileString(Section, Key, Value, avaPath);
        }

        private String IniReadValue(String Section, String Key, string avsPath)
        {
            StringBuilder temp = new StringBuilder(2000);
            int i = GetPrivateProfileString(Section, Key, "", temp, 2000, avsPath);
            return temp.ToString();
        }
    }
}
