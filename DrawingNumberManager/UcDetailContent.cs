﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DrawingNumberManager
{
    public partial class UcDetailContent : UserControl
    {
        private int maker_code;

        private string PartName
        {
            set { txtPartName.Text = value; }
        }

        private string Type
        {
            set { txtType.Text = value; }
        }

        private string Specfication
        {
            set { txtSpecfication.Text = value; }
        }

        private string Material
        {
            set { txtMaterial.Text = value; }
        }

        private string AfterProcess
        {
            set { txtAfterProcess.Text = value; }
        }

        private string Maker
        {
            set { txtMaker.Text = value; }
        }

        private string Qty
        {
            set { txtQty.Text = value; }
        }

        private string UnitPrice
        {
            set { txtUnitPrice.Text = value; }
        }


        private string Price
        {
            set { txtPrice.Text = value; }
        }

        public UcDetailContent()
        {
            InitializeComponent();
        }

        private void btnMaker_Click(object sender, EventArgs e)
        {
            FormMaker frmCompany = new FormMaker(false);
            if (frmCompany.ShowDialog() == DialogResult.OK)
            {
                txtMaker.Text = frmCompany.MakerName;
                maker_code = frmCompany.MakerCode;
            }
        }

        private void txtQty_TextChanged(object sender, EventArgs e)
        {
            CalculationPrice();
        }

        private void txtUnitPrice_TextChanged(object sender, EventArgs e)
        {
            CalculationPrice();
        }

        private void CalculationPrice()
        {
            if (txtQty.Text == "" || txtUnitPrice.Text == "")
                return;

            try
            {
                int qty = int.Parse(txtQty.Text);
                int unit_price = int.Parse(txtUnitPrice.Text);
                int price = unit_price * qty;

                txtPrice.Text = price.ToString();
            }
            catch (Exception ex)
            {
                txtQty.Text = "0";
                txtUnitPrice.Text = "0";
                txtPrice.Text = "0";
                ex.ToString();
            }
        }
    }
}
