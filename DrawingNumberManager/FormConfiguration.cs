﻿using System;
using System.Windows.Forms;

namespace DrawingNumberManager
{
    public partial class FormConfiguration : Form
    {
        Ini clsIni = new Ini();

        public FormConfiguration()
        {
            InitializeComponent();
        }

        private void FormConfiguration_Load(object sender, EventArgs e)
        {
            clsIni.LoadIni();
            txtServerPath.Text = StaticData.server_path;
            txtLocalPath.Text = StaticData.local_path;
        }

        private void btnServerPath_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == DialogResult.OK)
                txtServerPath.Text = fbd.SelectedPath;
        }

        private void bthLocalPath_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == DialogResult.OK)
                txtLocalPath.Text = fbd.SelectedPath;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            SaveIni();
            MessageBox.Show("Configuration이 변경 되었습니다.", "확인");
            this.Close();
        }

        private void SaveIni()
        {
            StaticData.server_path = txtServerPath.Text;
            StaticData.local_path = txtLocalPath.Text;
            clsIni.SaveIni();
        }

    }
}
