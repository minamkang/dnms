﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DrawingNumberManager
{
    public partial class FormMessage : Form
    {
        public FormMessage(string message, string btn1Text, string btn2Text)
        {
            InitializeComponent();

            lblContent.Text = message;
            btn1.Text = btn1Text;
            btn2.Text = btn2Text;
        }

        private void FormMessage_Load(object sender, EventArgs e)
        {
            int numLines = lblContent.Text.Split('\n').Length;

            if (numLines > 3)
            {
                lblContent.ForeColor = Color.BlueViolet;
                btnTextCopy.Visible = true;

                for (int i = 2; i < numLines; i++)
                {
                    this.Height += 10;
                    int YCurrentLocation = btn1.Location.Y;

                    btn1.Location = new Point(12, YCurrentLocation += 10);
                    btn2.Location = new Point(310, YCurrentLocation); 
                }
            }
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.No;
            this.Close();
        }

        private void btnTextCopy_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(lblContent.Text);
            //btnTextCopy.Visible = false;
        }
    }
}
