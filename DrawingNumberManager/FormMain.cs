﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.IO;

namespace DrawingNumberManager
{
    public partial class FormMain : Form
    {
		private System.Windows.Forms.NotifyIcon ni;
		Ini clsIni = new Ini();
		Authority clsAuthority = null;

        FormNoticeMessage frmNotice = null;

        private int maker_code;

        public FormMain()
        {
            InitializeComponent();
			this.ni = new System.Windows.Forms.NotifyIcon();
        }

        string current_user_id = "";
        string current_user_name = "";
        char current_user_department_cd;
        int current_user_key_id;
		int current_user_access_level;

		private void FormNewDrawingNumber_Load(object sender, EventArgs e)
        {
			//윈도우 메시지 처리 -- 쓰레드를 기동한다.(시작)
			clsIni.LoadIni();
			FormLogin frmlogin = new FormLogin();
            if (frmlogin.ShowDialog() != DialogResult.OK)
            {
                ni.Dispose();
                Application.Exit();
            }
            else
            {
                current_user_id = frmlogin.user.ID;
                current_user_name = frmlogin.user.Name;
                current_user_department_cd = frmlogin.user.DepartmentCode;
                current_user_key_id = frmlogin.user.Key;
				current_user_access_level = frmlogin.user.AccessLevel;
				clsAuthority = new Authority(current_user_id);
				frmlogin.Close();
            }

            if (current_user_access_level != 7)
                this.tsmiManagement.Visible = false;

            for (short i = 1; i < 3; i++)
                BindCmbBox(i);

            initialize();
        }

        private void initialize()
        {
			this.pnlNewDrawingNumber.Visible = true;
			this.pnlReviseDrawingNumber.Visible = false;
			this.DrawingNumberDisplayPanel.Visible = false;
			
            this.Text = Const.FORM_NEW_DRAWING_NUMBER + "[" + current_user_name + "]";
            this.lblByte.Text = string.Format("0/{0}Byte", Const.DRAWING_NO_NOTE_MAX_LENGTH);
            this.lblWarringContent.Text = string.Format("Note내용은 {0}Byte이상 작성을 해야 합니다.", Const.DRAWING_NO_NOTE_MIN_LENGTH);

			this.Width = 360;
			this.Height = 520;

			this.pnlReviseDrawingNumber.Location = new Point(0, 27);
			BindCmbDepartment();

            txtNote.Focus();

            //NoticeMessage---------
            frmNotice = new FormNoticeMessage();
            frmNotice.Show();

            System.Drawing.Rectangle ScreenRectangle = Screen.PrimaryScreen.WorkingArea;
            int xPos = ScreenRectangle.Width - frmNotice.Bounds.Width - 5;
            int yPos = ScreenRectangle.Height - frmNotice.Bounds.Height - 5;
            frmNotice.SetBounds(xPos, yPos, frmNotice.Size.Width, frmNotice.Size.Height, BoundsSpecified.Location);
            frmNotice.BringToFront();

            frmNotice.Visible = false;
            //NoticeMessage---------
        }

        #region NotifyIcon 처리루틴
        private void FormNewDrawingNumber_Shown(object sender, EventArgs e)
        {
            HideForm();
        }
        private void FormNewDrawingNumber_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                HideForm();
            }
        }

        private void tsmiReviseDrawinNumber_Click(object sender, EventArgs e)
        {
			this.pnlNewDrawingNumber.Visible = false;
			this.pnlReviseDrawingNumber.Visible = true;
            this.txtNote.Text = "";
			this.txtReviseDrawingNumber.Focus();

            this.ni.Visible = true;
            this.Visible = true;
            this.WindowState = FormWindowState.Normal;

            initializeDetailContent();
            this.Height = 177;
        }

        private void tsmiNewDrawingNumber_Click(object sender, EventArgs e)
        {
			this.pnlNewDrawingNumber.Visible = true;
			this.pnlReviseDrawingNumber.Visible = false;
            this.txtReviseDrawingNumber.Text = "";
			this.cmbDrawingCategory.Focus();
			this.cmbDrawingCategory.Select();

            this.ni.Visible = true;
            this.Visible = true;
            this.WindowState = FormWindowState.Normal;

            initializeDetailContent();
            this.Height = 514;
        }

        private void tsmiNewOfficialDrawingNumber_Click(object sender, EventArgs e)
        {
            object OpenedForm = GetForm("FormPublishOfficialDN");
            if (OpenedForm == null)
            {
                FormPublishOfficialDN frmNewODN = new FormPublishOfficialDN( "", "", current_user_access_level, Const.FLAG_CREATE_ODN, null);
                frmNewODN.ShowDialog();
            }
        }

        private void tsmiReviseOfficialDrawinNumber_Click(object sender, EventArgs e)
        {
            object OpenedForm = GetForm("FormPublishOfficialDN");
            if (OpenedForm == null)
            {
                FormPublishOfficialDN frmNewODN = new FormPublishOfficialDN("", "", current_user_access_level, Const.FLAG_REVISE_ODN, null);
                frmNewODN.ShowDialog();
            }
        }

        //swkang 20160907 Add -- History로 변경 이에 막음
        private void tsmiNewOrderType_Click(object sender, EventArgs e)
        {
            object OpenedForm = GetForm("FormPublishOfficialDN");
            if (OpenedForm == null)
            {
                FormPublishOfficialDN frmNewODN = new FormPublishOfficialDN("", "", current_user_access_level, Const.FLAG_ORDER_TYPE_ODN, null);
                frmNewODN.ShowDialog();
            }
        }

        //폼이 생성되었는지 여부 판단
        public static Form GetForm(string formName)
        {
            foreach (Form frm in Application.OpenForms)
                if (frm.Name == formName)
                    return frm;

            return null;
        }

        private void tsmiExit_Click(object sender, EventArgs e)
        {
            frmNotice.Close();
			ni.Dispose();
			Application.Exit();
        }

        private void HideForm()
        {
            try
            {
                ni.Icon = new System.Drawing.Icon("Drawing.ico");
                ni.Visible = true;
                this.Visible = false;
                ni.ContextMenuStrip = contextMenuStrip;      
            }
            catch (Exception ex)
            {
                MessageBox.Show("HideForm \r" + ex.ToString());
            }
        }

		private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (e.CloseReason == CloseReason.UserClosing)
			{
				e.Cancel = true;
				HideForm();
			}
		}

        #endregion

        private void tsmiDrawingNumberHistory_Click(object sender, EventArgs e)
        {
			FormDNHistory frmDrawingHistory = new FormDNHistory(current_user_department_cd, clsAuthority.NoteWrite);
            frmDrawingHistory.ShowDialog();
        }

        private void tsmiOfficialDrawingNumberHistory_Click(object sender, EventArgs e)
        {
            FormODNHistory frmODrawingHistory = new FormODNHistory(clsAuthority.NoteWrite);
            frmODrawingHistory.ShowDialog();
        }

		private void tsmiModifyPassword_Click(object sender, EventArgs e)
		{
			FormModifyPassword frmModifyPassword = new FormModifyPassword();
			frmModifyPassword.ShowDialog();
		}

        private void tsmiConfiguration_Click(object sender, EventArgs e)
        {
            FormConfiguration frmConfiguration = new FormConfiguration();
            frmConfiguration.ShowDialog();
        }

        private void tsmiOrder_Click(object sender, EventArgs e)
        {
            object OpenedForm = GetForm("FormOrder");
            if (OpenedForm == null)
            {
                FormOrder frmOrder = new FormOrder(current_user_access_level);
                frmOrder.ShowDialog();
            }
        }

        private void tsmiModel_Click(object sender, EventArgs e)
        {
            FormManagerProject frmModel = new FormManagerProject(Const.CODE_MODEL);
            frmModel.ShowDialog();
        }

        private void tsmiPart_Click(object sender, EventArgs e)
        {
            FormManagerProject frmPart = new FormManagerProject(Const.CODE_PART);
            frmPart.ShowDialog();
        }

        private void tsmiMaker_Click(object sender, EventArgs e)
        {
            FormMaker frmMaker = new FormMaker(true);
            frmMaker.ShowDialog();
        }

        private void tsmiPartList_Click(object sender, EventArgs e)
        {

            FormPartList frmPartList = new FormPartList(current_user_access_level);
            frmPartList.ShowDialog();
        }

        private void tsmiClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            pnlNewDrawingNumber.Visible = true;
            DrawingNumberDisplayPanel.Visible = false;

            this.Height = 520;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtNote.Text = "";
            BindCmbDepartment();
            initializeDetailContent();
            txtNote.Focus();
        }

        private void btnPublish_Click(object sender, EventArgs e)
        {
            if(cmbModel.SelectedValue.ToString() == "" || cmbPart.SelectedValue.ToString() == "")
            {
                MessageBox.Show("Model, Part는 필수 항목입니다.", "확인");
                return;
            }

			this.DrawingNumberDisplayPanel.Visible = true;
            cmbDepartment_SelectedIndexChanged(null, null);

            ArrayList alDetail = new ArrayList();
            alDetail.Add(cmbModel.SelectedValue.ToString() + cmbPart.SelectedValue.ToString());
            alDetail.Add(txtType.Text);
            alDetail.Add(txtPartName.Text);
            alDetail.Add(txtSpecfication.Text);
            alDetail.Add(txtMaterial.Text);
            alDetail.Add(txtAfterProcess.Text);

            alDetail.Add(txtQty.Text.Replace(",", ""));
            if(txtMaker.Text != "") 
                alDetail.Add(maker_code);
            else 
                alDetail.Add(0);
            alDetail.Add(txtUnitPrice.Text.Replace(",", ""));            
            alDetail.Add(txtPrice.Text.Replace(",", ""));

			TemporaryDrawingNumber CurrentTDN = new TemporaryDrawingNumber(txtNote.Text, current_user_key_id, alDetail);
            txtNewDrawNumber.Text = CurrentTDN.ToString();

            if (txtNewDrawNumber.Text.Length > 0)
            {
                DrawingNumberDisplayPanel.Visible = true;
                DrawingNumberDisplayPanel.Location = new Point(7, 3);
                txtNote.Text = "";
#if false
                //swkang 20200711 Start -- Clear를 하지 못하도록 기능 개선
                initializeDetailContent();
#endif                
                this.Height = 188;

                StaticData.category = DrawingCategory.code;
				clsIni.SaveIni();
            }  
        }

        private void btnReflesh_Click(object sender, EventArgs e)
        {
            BindCmbDepartment();

            for (short i = 1; i < 3; i++)
                BindCmbBox(i);
        }

        private void btnGoToNDN_Click(object sender, EventArgs e)
        {
            DrawingNumberDisplayPanel.Visible = false;
        }

        private void btnMaker_Click(object sender, EventArgs e)
        {
            FormMaker frmCompany = new FormMaker(false);
            if (frmCompany.ShowDialog() == DialogResult.OK)
            {
                txtMaker.Text = frmCompany.MakerName;
                maker_code = frmCompany.MakerCode;
            }
        }

        private void cmbDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbDrawingCategory.SelectedIndex < 0)
                return;

            DrawingCategory.code = cmbDrawingCategory.Text;
            DrawingCategory.name = cmbDrawingCategory.SelectedValue.ToString();
        }

        bool bUtf8; //한글 3byte, 영문 숫자 1
        private void txtNote_TextChanged(object sender, EventArgs e)
        {
			//2014년 01월 21일 - 강성환 추가
			if (this.txtNote.Text.Contains("'") || this.txtNote.Text.Contains("%"))
			{
				MessageBox.Show("문자 [', %]는 사용 할수 없습니다.", "Warning");
				this.txtNote.Text = this.txtNote.Text.Replace("'", "");
				this.txtNote.Text = this.txtNote.Text.Replace("%", "");

				if (txtNote.Text.Length > 0)
					txtNote.Select(txtNote.Text.Length - 1, 0); //TEXTBOX 제일마지막 쓴 포인트로 이동
				return;
			}

            int length = Encoding.UTF8.GetBytes(this.txtNote.Text).Length;
            if (length > Const.DRAWING_NO_NOTE_MAX_LENGTH)
            {
                this.txtNote.Text = this.txtNote.Text.Substring(0, this.txtNote.TextLength - (bUtf8 ? 1 : 2));
                bUtf8 = false;
                this.txtNote.Select(this.txtNote.TextLength, 0);
                return;
            }
            if (length >= Const.DRAWING_NO_NOTE_MIN_LENGTH)
            {
                lblByte.ForeColor = Color.Blue;
                btnPublish.Enabled = true;
            }
            else
            {
                lblByte.ForeColor = Color.OrangeRed;
                btnPublish.Enabled = false;
            }
            length = Encoding.UTF8.GetBytes(this.txtNote.Text).Length;
            this.lblByte.Text = string.Format("{0}/{1} byte", length, Const.DRAWING_NO_NOTE_MAX_LENGTH);
        }

        private void txtNewDrawNumber_TextChanged(object sender, EventArgs e)
        {
            ((TextBox)sender).SelectAll();
            ((TextBox)sender).Copy();
        }

        private void txtQty_TextChanged(object sender, EventArgs e)
        {
            if (txtQty.Text != "")
            {
                txtQty.Text = String.Format("{0:#,###}", int.Parse(txtQty.Text.Replace(",", "")));   //swkang 20201210
                txtQty.SelectionStart = txtQty.TextLength;
                txtQty.SelectionLength = 0;

                CalculationPrice();
            }
        }

        private void txtUnitPrice_TextChanged(object sender, EventArgs e)
        {
            if(txtUnitPrice.Text != "")
            {
                txtUnitPrice.Text = String.Format("{0:#,###}", int.Parse(txtUnitPrice.Text.Replace(",", "")));   //swkang 20201210
                txtUnitPrice.SelectionStart = txtUnitPrice.TextLength;
                txtUnitPrice.SelectionLength = 0;
                CalculationPrice();
            }
        }

        private void CalculationPrice()
        {
            if (txtQty.Text == "" || txtUnitPrice.Text == "")
                return;

            try
            {
                int qty = int.Parse(txtQty.Text.Replace(",", ""));
                int unit_price = int.Parse(txtUnitPrice.Text.Replace(",", ""));
                int price = unit_price * qty;

#if false
                txtPrice.Text = price.ToString();
#else
                txtPrice.Text = string.Format("{0:#,###}", price); //swkang 20201210
#endif
            }
            catch(Exception ex)
            {
                txtQty.Text = "0";
                txtUnitPrice.Text = "0";
                txtPrice.Text = "0";
                ex.ToString();
            }
        }

        private void BindCmbDepartment()
        {
			DataTable result = new DataTable();
			SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC department_code_info"), StaticData.db_connetion_string);
			query.Fill(result);

			if (result != null)
			{
				cmbDrawingCategory.ValueMember = "VALUE";
				cmbDrawingCategory.DisplayMember = "CODE";
				cmbDrawingCategory.DataSource = result;

				if (clsIni.Category.Length > 0)
					cmbDrawingCategory.Text = clsIni.Category; //Ini값으로 선택한다.
			}
        }

        private void initializeDetailContent()
        {
            cmbModel.SelectedValue = "";
            cmbPart.SelectedValue = "";
            txtType.Text = "";
            txtPartName.Text = "";
            txtSpecfication.Text = "";
            txtMaterial.Text = "";
            txtAfterProcess.Text = "";
            txtQty.Text = "0";
            txtMaker.Text = "";
            txtUnitPrice.Text = "0";
            txtPrice.Text = "0";
        }

        private void BindCmbBox(short sType)
        {
            DataTable result = new DataTable();
            SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC code_info {0}", sType), StaticData.db_connetion_string);
            query.Fill(result);

            if (result != null)
            {
                if (sType == 1)
                {
                    result.Rows.Add("");
                    cmbModel.ValueMember = "CODE_NAME";
                    cmbModel.DisplayMember = "CODE_VALUE";
                    cmbModel.DataSource = result;
                    cmbModel.SelectedValue = "";
                }
                else if (sType == 2)
                {
                    result.Rows.Add("");
                    cmbPart.ValueMember = "CODE_NAME";
                    cmbPart.DisplayMember = "CODE_VALUE";
                    cmbPart.DataSource = result;
                    cmbPart.SelectedValue = "";
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Revise
        private void txtReviseDrawingNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnCheckVaild_Click(null, null);
        }

        private void btnCheckVaild_Click(object sender, EventArgs e)
        {
            if (txtReviseDrawingNumber.Text.Length == 0)
                return;

			//txtReviseDrawingNumber.text무조건 대문자로 형변환 한다.
			TemporaryDrawingNumber CurrentTDN = new TemporaryDrawingNumber(txtReviseDrawingNumber.Text.ToUpper(), "", current_user_key_id, null);
            if (CurrentTDN.Issue() == true)
            {
                //입력한 도면번호에 리비전값이 최신리비전값인지 체크를 한다.
                int currentDNKey = GetDNKey();
                bool whetherLastRevsionCode = false;

                string drawingNumber = "";
                char dBLastRevisionCode = GetDNLastRevisionCode(currentDNKey);
                if (dBLastRevisionCode == 'Z')
                {
                    MessageBox.Show("REVISION CODE는 'Z' 이상 사용 할 수 없습니다.", "Warning");
                    return;
                }
                
                if (CurrentTDN.revision_code == dBLastRevisionCode)
                {
                    drawingNumber = DrawingCategory.code + CurrentTDN.created_year.ToString() + CurrentTDN.serial_number.ToString("0000.##") + CurrentTDN.revision_code;
                    whetherLastRevsionCode = true;
                }
                else
                    drawingNumber = DrawingCategory.code + CurrentTDN.created_year.ToString() + CurrentTDN.serial_number.ToString("0000.##") + dBLastRevisionCode.ToString();

                if (GetExistenceODN(currentDNKey) == true)
                {
                    MessageBox.Show("진행중인 (假)도면번호는 이미 (眞)도면번호로 구성되어\r 더이상 가도번으로 사용 할 수 없습니다.", "Warning");
                    return;
                }

                //기존 정보 Detail Content를 Read한다.
                //KEY값(currentDNKey) + 리비젼코드 값(CurrentTDN.revision_code) 
                DataTable detailContent = GetCurrentDetailContent(currentDNKey, CurrentTDN.revision_code);

                FormPublishRDN frmRDN = new FormPublishRDN(current_user_key_id, drawingNumber, txtReviseDrawingNumber.Text, whetherLastRevsionCode, detailContent);
                if (frmRDN.ShowDialog() == DialogResult.OK)
                    txtReviseDrawingNumber.Text = "";
            }
        }

		private int GetDNKey()
		{
			int resultDNKey = 0;
			SqlCommand cmd = new SqlCommand(String.Format("EXEC drawing_key_of '{0}'", this.txtReviseDrawingNumber.Text), new SqlConnection(StaticData.db_connetion_string));
			cmd.Connection.Open();
			resultDNKey = (int)cmd.ExecuteScalar();
			cmd.Connection.Close();
			return resultDNKey;
		}

        private char GetDNLastRevisionCode(int currentDNKey)
        {
			string resultDNLastRevisionCode = "";
			SqlCommand query = new SqlCommand(String.Format("EXEC last_revision_code {0}, '{1}'", currentDNKey, DrawingCategory.code), new SqlConnection(StaticData.db_connetion_string));
			query.Connection.Open();
			resultDNLastRevisionCode = (string) query.ExecuteScalar();
			query.Connection.Close();
			return resultDNLastRevisionCode[0];
        }

        private bool GetExistenceODN(int LastDNKey)
        {
            bool isResultODN = false;
            string strResultODN = "";

            SqlCommand query = new SqlCommand(String.Format("EXEC existence_ODN '{0}'", LastDNKey), new SqlConnection(StaticData.db_connetion_string));
            query.Connection.Open();

            if(query.ExecuteScalar() != DBNull.Value)
            {
                strResultODN = (string)query.ExecuteScalar();
                if (strResultODN != "NULL" || strResultODN != "")
                    isResultODN = true;
            }
            query.Connection.Close();
            return isResultODN;
        }

        private DataTable GetCurrentDetailContent(int CurrentDNKey, char InputRevisionCode)
        {
            DataTable result = new DataTable();
            SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC drawing_number_detail_content 'T', {0}, '{1}'", CurrentDNKey, InputRevisionCode), StaticData.db_connetion_string);
            query.Fill(result);

            return result;
        }

    }
}
