﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;

namespace DrawingNumberManager
{
    public partial class FormSendMail : Form
    {
        DataTable dtMaker = null;
        private string order_num;
        ArrayList al_Attachments = new ArrayList();

        public FormSendMail(DataTable Maker, string OrderNum, ArrayList alDrawings)
        {
            dtMaker = Maker;
            order_num = OrderNum;
            al_Attachments = alDrawings;

            InitializeComponent();
        }

        private void FormSendMail_Load(object sender, EventArgs e)
        {
            txtReceiverAddress.Text = dtMaker.Rows[0]["E메일"].ToString();
            txtReferenceAddress.Text = StaticData.user_mail;

            txtSubject.Text = string.Format("[케이엠디지텍] [{0}] {1} 발주서 첨부", StaticData.user_name, order_num);

            StringBuilder sbBody = new StringBuilder();
            sbBody.Append("안녕하십니까?\r\n\r\n");
            sbBody.Append(string.Format("케이엠디지텍 {0}입니다.\r\n\r\n", StaticData.user_name));
            sbBody.Append(string.Format("발주서(발주번호 {0})를 첨부하오니 확인 부탁드립니다.\r\n\r\n", order_num));
            sbBody.Append("감사합니다.\r\n\r\n\r\n");

            sbBody.Append(string.Format("(주)케이엠디지텍 {0}\r\n", StaticData.user_name));
            sbBody.Append(string.Format("T.{0}\r\n", StaticData.user_tel));
            sbBody.Append("F.031-8012-9912\r\n");
            sbBody.Append(string.Format("E.{0}\r\n", StaticData.user_mail));

            txtBodyContent.Text = sbBody.ToString();
            txtBodyContent.SelectionStart = txtBodyContent.TextLength;
        }

        private void btnAddFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofdlg = new OpenFileDialog();
            ofdlg.Filter = "모든 파일 (*.*) | *.*";
            if (ofdlg.ShowDialog() == DialogResult.OK)
            {
                al_Attachments.Add(ofdlg.FileName);
                string[] splitFile = ofdlg.SafeFileName.ToString().Split('.');
                if (splitFile.Length == 2)
                    dgvAppendFile.Rows.Add(splitFile[0], splitFile[1], ofdlg.FileName);
            }
        }

        private void btnDeleteFile_Click(object sender, EventArgs e)
        {
            if(dgvAppendFile.Rows.Count > 0)
            {
                string removeFile = dgvAppendFile.Rows[dgvAppendFile.CurrentRow.Index].Cells["FullName"].Value.ToString();
                dgvAppendFile.Rows.RemoveAt(dgvAppendFile.CurrentRow.Index);
                al_Attachments.Remove(removeFile);
            }
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            Mail mail = new Mail();
            if (mail.Send(txtReceiverAddress.Text, txtReferenceAddress.Text, txtSubject.Text, txtBodyContent.Text, al_Attachments) == "성공!")
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
                this.DialogResult = DialogResult.No;
        }
    }
}
