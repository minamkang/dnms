using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Collections;

namespace DrawingNumberManager
{
	public class TemporaryDrawingNumber : DrawingNumber
    {
        private string source = "";
        private string note = "";
        private string drawing_number = "";
        private int current_user_key;
        private string flag = "";

        private ArrayList detail_content = null;

        //Create DrawingNumber 생성자
        public TemporaryDrawingNumber(string Note, int user_key, ArrayList detailContent)
        {
            note = Note;
            current_user_key = user_key;
            flag = Const.FLAG_CREATE;
            detail_content = detailContent;

            drawing_number = DrawingNumber().ToString();
        }

        //Revise DrawingNumber 생성자
        public TemporaryDrawingNumber(string Source, string Note, int user_key, ArrayList detailContent)
        {
            source = Source;
            note = Note;
            current_user_key = user_key;
            flag = Const.FLAG_REVISE;
            detail_content = detailContent;

			if (Note.Length > 0)
				drawing_number = DrawingNumber().ToString();			
        }

        //유효성 검사
        new public bool Issue()
        {
            if (CheckVaild(source) != true)
                return false;
            return true;
        }

        //DrawingNumber 재정의
		public override string ToString()
        {
            return drawing_number;          
		}

        protected string DrawingNumber()
        {
			string resultDN = "";
			SqlCommand query = null;
            if (flag == Const.FLAG_CREATE)
            {
                query = new SqlCommand(String.Format("EXEC dbo.create_drawing_number {0}, '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', {9}, {10}, {11}, {12}",
                    current_user_key, DrawingCategory.code, note,
                    detail_content[0],      //모델, Part 
                    detail_content[1],      //Type
                    detail_content[2],      //Part Name
                    detail_content[3],      //Specfication
                    detail_content[4],      //Material 
                    detail_content[5],      //After Process
                    detail_content[6],      //Quantity 
                    detail_content[7],      //Maker
                    detail_content[8],      //Unit Price 
                    detail_content[9]       //Price
                    ), new SqlConnection(StaticData.db_connetion_string));
            }
            else
                query = new SqlCommand(String.Format("EXEC dbo.revise_drawing_number {0}, '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', {10}, {11}, {12}, {13}",
                    current_user_key, source, DrawingCategory.code, note,
                    detail_content[0],      //모델, Part 
                    detail_content[1],      //Type
                    detail_content[2],      //Part Name
                    detail_content[3],      //Specfication
                    detail_content[4],      //Material 
                    detail_content[5],      //After Process
                    detail_content[6],      //Quantity 
                    detail_content[7],      //Maker
                    detail_content[8],      //Unit Price 
                    detail_content[9]       //Price
                    ), new SqlConnection(StaticData.db_connetion_string));

			query.Connection.Open();
			resultDN = (string)query.ExecuteScalar();
			query.Connection.Close();
			return resultDN;
        }

        //DrawingNumber 정규성을 체크한다.
        protected bool CheckVaild(string source)
        {
            try
            {
                if (source.Length != Const.DRAWING_NO_LENGTH)
				{
                    MessageBox.Show(string.Format("도면번호는 {0}자리 입니다.\r 확인하시고 다시 입력하세요.", Const.DRAWING_NO_LENGTH), "Warning");
                    return false;
                }
                //DrawingCategory , 년도,일련번호 6자리, Revision code, DB에 일련번호일치성 검사
                if ((Regex.IsMatch(source.Substring(0, 1), string.Format(@"^[{0}]", GetKindDrawingCategory())) != true)
                    || (Regex.IsMatch(source.Substring(1, 6), @"^\d{6}$") != true)
                    || (Regex.IsMatch(source.Substring(7, 1), @"^[A-Z]$") != true)
                    || (SerialNumberCheck(source.Substring(0, 1), source.Substring(3, 4))) != true)
                {
                    MessageBox.Show("유효하지 않는 도면번호입니다.", "Warning");
                    return false;
                }

                DrawingCategory.code = source.Substring(0, 1);
                created_year = byte.Parse(source.Substring(1, 2));
                serial_number = UInt16.Parse(source.Substring(3, 4));
                revision_code = Convert.ToChar(source.Substring(7, 1));
            }
            catch (Exception ex)
            {
                MessageBox.Show("VaildCheck \r" + ex.ToString());
                return false;
            }
            return true;
        }

		protected bool SerialNumberCheck(string strDepartmentCode, string strSerialNumber)
		{
			string result = "";
			SqlCommand query = new SqlCommand(String.Format("EXEC check_invalid_serial_number '{0}', {1} ",
					strDepartmentCode, int.Parse(strSerialNumber), result), new SqlConnection(StaticData.db_connetion_string));

			query.Connection.Open();
			result = (string)query.ExecuteScalar();
			query.Connection.Close();

			if (result[0] == 'N')
				return false;

			return true;
		}

		protected string GetKindDrawingCategory()
		{
			string strVal = "";
			DataTable result = new DataTable();
			SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC department_code_info "), StaticData.db_connetion_string);
			query.Fill(result);

			for (int i = 0; i < result.Rows.Count; i++)
				strVal += result.Rows[i]["CODE"].ToString();

			return strVal;
		}
	}
}
