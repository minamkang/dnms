﻿namespace DrawingNumberManager
{
    partial class FormMessage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn1 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.lblContent = new System.Windows.Forms.Label();
            this.btnTextCopy = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn1
            // 
            this.btn1.BackColor = System.Drawing.Color.DodgerBlue;
            this.btn1.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn1.ForeColor = System.Drawing.Color.White;
            this.btn1.Location = new System.Drawing.Point(12, 121);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(104, 52);
            this.btn1.TabIndex = 0;
            this.btn1.Text = "승인";
            this.btn1.UseVisualStyleBackColor = false;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn2
            // 
            this.btn2.BackColor = System.Drawing.Color.OrangeRed;
            this.btn2.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn2.ForeColor = System.Drawing.Color.White;
            this.btn2.Location = new System.Drawing.Point(310, 121);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(104, 52);
            this.btn2.TabIndex = 1;
            this.btn2.Text = "반려";
            this.btn2.UseVisualStyleBackColor = false;
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // lblContent
            // 
            this.lblContent.AutoSize = true;
            this.lblContent.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblContent.ForeColor = System.Drawing.Color.Red;
            this.lblContent.Location = new System.Drawing.Point(12, 39);
            this.lblContent.Name = "lblContent";
            this.lblContent.Size = new System.Drawing.Size(17, 13);
            this.lblContent.TabIndex = 2;
            this.lblContent.Text = "..";
            // 
            // btnTextCopy
            // 
            this.btnTextCopy.Location = new System.Drawing.Point(373, 2);
            this.btnTextCopy.Name = "btnTextCopy";
            this.btnTextCopy.Size = new System.Drawing.Size(41, 23);
            this.btnTextCopy.TabIndex = 3;
            this.btnTextCopy.Text = "복사";
            this.btnTextCopy.UseVisualStyleBackColor = true;
            this.btnTextCopy.Visible = false;
            this.btnTextCopy.Click += new System.EventHandler(this.btnTextCopy_Click);
            // 
            // FormMessage
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(427, 183);
            this.Controls.Add(this.btnTextCopy);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.lblContent);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMessage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "메시지";
            this.Load += new System.EventHandler(this.FormMessage_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Label lblContent;
        private System.Windows.Forms.Button btnTextCopy;
    }
}