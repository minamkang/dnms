﻿namespace DrawingNumberManager
{
    partial class FormConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblServerPath = new System.Windows.Forms.Label();
            this.txtServerPath = new System.Windows.Forms.TextBox();
            this.gbPath = new System.Windows.Forms.GroupBox();
            this.bthLocalPath = new System.Windows.Forms.Button();
            this.lblLocalPath = new System.Windows.Forms.Label();
            this.txtLocalPath = new System.Windows.Forms.TextBox();
            this.btnServerPath = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.gbPath.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblServerPath
            // 
            this.lblServerPath.AutoSize = true;
            this.lblServerPath.Location = new System.Drawing.Point(6, 17);
            this.lblServerPath.Name = "lblServerPath";
            this.lblServerPath.Size = new System.Drawing.Size(41, 12);
            this.lblServerPath.TabIndex = 0;
            this.lblServerPath.Text = "Server";
            // 
            // txtServerPath
            // 
            this.txtServerPath.Enabled = false;
            this.txtServerPath.Location = new System.Drawing.Point(80, 14);
            this.txtServerPath.Name = "txtServerPath";
            this.txtServerPath.Size = new System.Drawing.Size(462, 21);
            this.txtServerPath.TabIndex = 1;
            // 
            // gbPath
            // 
            this.gbPath.Controls.Add(this.bthLocalPath);
            this.gbPath.Controls.Add(this.lblLocalPath);
            this.gbPath.Controls.Add(this.txtLocalPath);
            this.gbPath.Controls.Add(this.btnServerPath);
            this.gbPath.Controls.Add(this.lblServerPath);
            this.gbPath.Controls.Add(this.txtServerPath);
            this.gbPath.Location = new System.Drawing.Point(12, 12);
            this.gbPath.Name = "gbPath";
            this.gbPath.Size = new System.Drawing.Size(597, 70);
            this.gbPath.TabIndex = 2;
            this.gbPath.TabStop = false;
            this.gbPath.Text = "Path";
            // 
            // bthLocalPath
            // 
            this.bthLocalPath.Location = new System.Drawing.Point(548, 39);
            this.bthLocalPath.Name = "bthLocalPath";
            this.bthLocalPath.Size = new System.Drawing.Size(43, 23);
            this.bthLocalPath.TabIndex = 5;
            this.bthLocalPath.Text = "...";
            this.bthLocalPath.UseVisualStyleBackColor = true;
            this.bthLocalPath.Click += new System.EventHandler(this.bthLocalPath_Click);
            // 
            // lblLocalPath
            // 
            this.lblLocalPath.AutoSize = true;
            this.lblLocalPath.Location = new System.Drawing.Point(6, 44);
            this.lblLocalPath.Name = "lblLocalPath";
            this.lblLocalPath.Size = new System.Drawing.Size(36, 12);
            this.lblLocalPath.TabIndex = 3;
            this.lblLocalPath.Text = "Local";
            // 
            // txtLocalPath
            // 
            this.txtLocalPath.Enabled = false;
            this.txtLocalPath.Location = new System.Drawing.Point(80, 41);
            this.txtLocalPath.Name = "txtLocalPath";
            this.txtLocalPath.Size = new System.Drawing.Size(462, 21);
            this.txtLocalPath.TabIndex = 4;
            // 
            // btnServerPath
            // 
            this.btnServerPath.Location = new System.Drawing.Point(548, 12);
            this.btnServerPath.Name = "btnServerPath";
            this.btnServerPath.Size = new System.Drawing.Size(43, 23);
            this.btnServerPath.TabIndex = 2;
            this.btnServerPath.Text = "...";
            this.btnServerPath.UseVisualStyleBackColor = true;
            this.btnServerPath.Click += new System.EventHandler(this.btnServerPath_Click);
            // 
            // btnOK
            // 
            this.btnOK.BackColor = System.Drawing.Color.LimeGreen;
            this.btnOK.Location = new System.Drawing.Point(549, 427);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(60, 33);
            this.btnOK.TabIndex = 4;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = false;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // FormConfiguration
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.ClientSize = new System.Drawing.Size(624, 472);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.gbPath);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormConfiguration";
            this.Text = "Configuration";
            this.Load += new System.EventHandler(this.FormConfiguration_Load);
            this.gbPath.ResumeLayout(false);
            this.gbPath.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblServerPath;
        private System.Windows.Forms.TextBox txtServerPath;
        private System.Windows.Forms.GroupBox gbPath;
        private System.Windows.Forms.Button bthLocalPath;
        private System.Windows.Forms.Label lblLocalPath;
        private System.Windows.Forms.TextBox txtLocalPath;
        private System.Windows.Forms.Button btnServerPath;
        private System.Windows.Forms.Button btnOK;
    }
}