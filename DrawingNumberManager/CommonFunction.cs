﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingNumberManager
{
    public class CommonFunction
    {
        public string CalculationPrice(string strQty, string strUnitPrice)
        {
            if (strQty == "" || strUnitPrice == "")
                return "0";

            string resultPrice = "";

            try
            {
                int qty = int.Parse(strQty.Replace(",", ""));
                int unit_price = int.Parse(strUnitPrice.Replace(",", ""));
                int price = unit_price * qty;

                resultPrice = string.Format("{0:#,##0}", price);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return resultPrice;
        }

    }
}
