﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrawingNumberManager
{
    public class Const
    {
        //Ini 파일 관련
        public const string INI_FILE_NAME = "DrawingNumberManager.ini";
        public const string LOCAL_VIEW_PATH = "C:\\KM_ORDER\\DRAWING";


        //발주서 파일
        public const string ORDER_FILE_NAME = "발주서포맷.xlsx";

        public const string INI_SECTION_MAIN = "MAIN";
        public const string INI_KEY_DB_CONN_STR = "DB_CONN_STR";
        public const string INI_KEY_DEPARTMENT_STR = "DEPARTMENT";
        public const string INI_KEY_REMEMBER_ID_PW = "REMEMBER_ID_PW";
        public const string INI_KEY_ID = "ID";
        public const string INI_KEY_PW = "PW";
        public const string INI_SERVER_PATH = "SEVER_PATH";
        public const string INI_LOCAL_PATH = "LOCAL_PATH";
        public const string INI_LOCAL_VIEW_PATH = "LOCAL_VIEW_PATH";

        //Form Name
        public const string FORM_NEW_DRAWING_NUMBER = "도면생성";
        public const string FORM_LOGIN = "로그인";
        public const string FORM_REVISE_DRAWING_NUMBER = "도면갱신";
        public const string FORM_DRAWING_NUMBER_HISTORY = "도면보고서";
		public const string FORM_MODIFY_PASSWORD = "비밀번호 변경";
        public const string FORM_OFFICIAL_DRAWING_NUMBER_HISTORY = "진도면 보고서";
        public const string FORM_CONFIGURATION = "환경설정";
        public const string FORM_PART_LIST = "품목리스트";
        public const string FORM_MANAGEMENT_COMPANY = "업체관리";
        public const string FORM_ORDER_MANAGEMENT = "발주관리";
        public const string FORM_ORDER_REGEDITER = "발주등록";

        
        //Program Const Naming Length
        public const int DRAWING_NO_LENGTH = 8;
        public const int DRAWING_NO_NOTE_MAX_LENGTH = 300;
        public const int DRAWING_NO_NOTE_MIN_LENGTH = 30;
        public const int OFFICIAL_DRAWING_NO_LENGTH = 10;

        //Program Const Nameing Format
        public const string DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
        public const string DUE_DATE = "yyyyMMdd";

        //Program Const Edit_Mode
        public const string EditMode_Insert = "INSERT";
        public const string EditMode_Modify = "MODIFY";
        public const string EditMode_Delete = "DELETE";
        public const string EditMode_None = "NONE";

        //Program flag
        public const string FLAG_CREATE = "CREATE";
        public const string FLAG_REVISE = "REVISE";
        public const string FLAG_ORDERTYPE = "ORDERTYPE";

        //Program Official Drawing Flag
        public const short FLAG_CREATE_ODN = 1;
        public const short FLAG_REVISE_ODN = 2;
        public const short FLAG_TDN_TO_CREATE_ODN = 3;
        public const short FLAG_TDN_TO_REVISE_ODN = 4;
        public const short FLAG_ORDER_TYPE_ODN = 5; //swkang 20160907 

        //Progrm Official Project Code
        public const short CODE_MODEL = 1;
        public const short CODE_PART = 2;
        public const short CODE_PROCESS = 3;
    }
}
