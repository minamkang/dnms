﻿namespace DrawingNumberManager
{
    partial class FormPublishOfficialDN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbModel = new System.Windows.Forms.ComboBox();
            this.lblModel = new System.Windows.Forms.Label();
            this.lblPart = new System.Windows.Forms.Label();
            this.cmbPart = new System.Windows.Forms.ComboBox();
            this.lblProcess = new System.Windows.Forms.Label();
            this.cmbProcess = new System.Windows.Forms.ComboBox();
            this.btnPublish = new System.Windows.Forms.Button();
            this.lblOrderType = new System.Windows.Forms.Label();
            this.cmbOrderType = new System.Windows.Forms.ComboBox();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.lblNote = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.txtResultOfficialDN = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtRevisionCd = new System.Windows.Forms.TextBox();
            this.lblRevisionCd = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.tsmiManagement = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiModel = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiPart = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiProcess = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiMaker = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiPartList = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiReport = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiODNHistory = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlNewOfficialDrawingNumber = new System.Windows.Forms.Panel();
            this.gbODNDetail = new System.Windows.Forms.GroupBox();
            this.btnMaker = new System.Windows.Forms.Button();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.lblPrice = new System.Windows.Forms.Label();
            this.txtUnitPrice = new System.Windows.Forms.TextBox();
            this.lblUnitPrice = new System.Windows.Forms.Label();
            this.txtQty = new System.Windows.Forms.TextBox();
            this.lblQty = new System.Windows.Forms.Label();
            this.txtMaker = new System.Windows.Forms.TextBox();
            this.lblCustomer = new System.Windows.Forms.Label();
            this.txtAfterProcess = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMaterial = new System.Windows.Forms.TextBox();
            this.lblMaterial = new System.Windows.Forms.Label();
            this.txtSpecfication = new System.Windows.Forms.TextBox();
            this.lblSpecfication = new System.Windows.Forms.Label();
            this.txtPartName = new System.Windows.Forms.TextBox();
            this.lblPartName = new System.Windows.Forms.Label();
            this.txtType = new System.Windows.Forms.TextBox();
            this.lblType = new System.Windows.Forms.Label();
            this.lblSaveType = new System.Windows.Forms.Label();
            this.cmbSaveDNType = new System.Windows.Forms.ComboBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.txtSaveDN = new System.Windows.Forms.TextBox();
            this.pnlReviseOfficialDrawingNumber = new System.Windows.Forms.Panel();
            this.lblNewOrderType = new System.Windows.Forms.Label();
            this.txtNewOrderType = new System.Windows.Forms.TextBox();
            this.lblContent = new System.Windows.Forms.Label();
            this.lblReviseOfficialDrawingNumber = new System.Windows.Forms.Label();
            this.btnCheckVaild = new System.Windows.Forms.Button();
            this.txtReviseOfficialDrawingNumber = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            this.pnlNewOfficialDrawingNumber.SuspendLayout();
            this.gbODNDetail.SuspendLayout();
            this.pnlReviseOfficialDrawingNumber.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbModel
            // 
            this.cmbModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbModel.FormattingEnabled = true;
            this.cmbModel.Location = new System.Drawing.Point(12, 29);
            this.cmbModel.Name = "cmbModel";
            this.cmbModel.Size = new System.Drawing.Size(85, 20);
            this.cmbModel.TabIndex = 0;
            // 
            // lblModel
            // 
            this.lblModel.AutoSize = true;
            this.lblModel.Location = new System.Drawing.Point(10, 14);
            this.lblModel.Name = "lblModel";
            this.lblModel.Size = new System.Drawing.Size(40, 12);
            this.lblModel.TabIndex = 1;
            this.lblModel.Text = "Model";
            // 
            // lblPart
            // 
            this.lblPart.AutoSize = true;
            this.lblPart.Location = new System.Drawing.Point(103, 14);
            this.lblPart.Name = "lblPart";
            this.lblPart.Size = new System.Drawing.Size(27, 12);
            this.lblPart.TabIndex = 3;
            this.lblPart.Text = "Part";
            // 
            // cmbPart
            // 
            this.cmbPart.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPart.FormattingEnabled = true;
            this.cmbPart.Location = new System.Drawing.Point(105, 29);
            this.cmbPart.Name = "cmbPart";
            this.cmbPart.Size = new System.Drawing.Size(110, 20);
            this.cmbPart.TabIndex = 1;
            // 
            // lblProcess
            // 
            this.lblProcess.AutoSize = true;
            this.lblProcess.Location = new System.Drawing.Point(219, 14);
            this.lblProcess.Name = "lblProcess";
            this.lblProcess.Size = new System.Drawing.Size(52, 12);
            this.lblProcess.TabIndex = 5;
            this.lblProcess.Text = "Process";
            // 
            // cmbProcess
            // 
            this.cmbProcess.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProcess.FormattingEnabled = true;
            this.cmbProcess.Location = new System.Drawing.Point(221, 29);
            this.cmbProcess.Name = "cmbProcess";
            this.cmbProcess.Size = new System.Drawing.Size(84, 20);
            this.cmbProcess.TabIndex = 2;
            // 
            // btnPublish
            // 
            this.btnPublish.AutoSize = true;
            this.btnPublish.Location = new System.Drawing.Point(269, 447);
            this.btnPublish.Name = "btnPublish";
            this.btnPublish.Size = new System.Drawing.Size(75, 34);
            this.btnPublish.TabIndex = 17;
            this.btnPublish.Text = "Publish";
            this.btnPublish.UseVisualStyleBackColor = true;
            this.btnPublish.Click += new System.EventHandler(this.btnPublish_Click);
            // 
            // lblOrderType
            // 
            this.lblOrderType.AutoSize = true;
            this.lblOrderType.Location = new System.Drawing.Point(310, 14);
            this.lblOrderType.Name = "lblOrderType";
            this.lblOrderType.Size = new System.Drawing.Size(69, 12);
            this.lblOrderType.TabIndex = 11;
            this.lblOrderType.Text = "Order Type";
            // 
            // cmbOrderType
            // 
            this.cmbOrderType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbOrderType.FormattingEnabled = true;
            this.cmbOrderType.Location = new System.Drawing.Point(311, 29);
            this.cmbOrderType.Name = "cmbOrderType";
            this.cmbOrderType.Size = new System.Drawing.Size(53, 20);
            this.cmbOrderType.TabIndex = 3;
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(12, 112);
            this.txtNote.Multiline = true;
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(416, 48);
            this.txtNote.TabIndex = 6;
            // 
            // lblNote
            // 
            this.lblNote.AutoSize = true;
            this.lblNote.Location = new System.Drawing.Point(10, 89);
            this.lblNote.Name = "lblNote";
            this.lblNote.Size = new System.Drawing.Size(31, 12);
            this.lblNote.TabIndex = 13;
            this.lblNote.Text = "Note";
            // 
            // btnClear
            // 
            this.btnClear.AutoSize = true;
            this.btnClear.Location = new System.Drawing.Point(381, 84);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(47, 22);
            this.btnClear.TabIndex = 14;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnClose
            // 
            this.btnClose.AutoSize = true;
            this.btnClose.Location = new System.Drawing.Point(350, 447);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 34);
            this.btnClose.TabIndex = 18;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // txtResultOfficialDN
            // 
            this.txtResultOfficialDN.BackColor = System.Drawing.Color.Honeydew;
            this.txtResultOfficialDN.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtResultOfficialDN.ForeColor = System.Drawing.Color.Red;
            this.txtResultOfficialDN.Location = new System.Drawing.Point(17, 452);
            this.txtResultOfficialDN.Name = "txtResultOfficialDN";
            this.txtResultOfficialDN.ReadOnly = true;
            this.txtResultOfficialDN.Size = new System.Drawing.Size(144, 25);
            this.txtResultOfficialDN.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 437);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 12);
            this.label1.TabIndex = 18;
            this.label1.Text = "Drawing Number";
            // 
            // txtRevisionCd
            // 
            this.txtRevisionCd.BackColor = System.Drawing.Color.Honeydew;
            this.txtRevisionCd.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtRevisionCd.ForeColor = System.Drawing.Color.Red;
            this.txtRevisionCd.Location = new System.Drawing.Point(178, 452);
            this.txtRevisionCd.Name = "txtRevisionCd";
            this.txtRevisionCd.ReadOnly = true;
            this.txtRevisionCd.Size = new System.Drawing.Size(61, 25);
            this.txtRevisionCd.TabIndex = 19;
            // 
            // lblRevisionCd
            // 
            this.lblRevisionCd.AutoSize = true;
            this.lblRevisionCd.Location = new System.Drawing.Point(176, 437);
            this.lblRevisionCd.Name = "lblRevisionCd";
            this.lblRevisionCd.Size = new System.Drawing.Size(87, 12);
            this.lblRevisionCd.TabIndex = 20;
            this.lblRevisionCd.Text = "Revision Code";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiManagement,
            this.tsmiPartList,
            this.tsmiReport});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(886, 24);
            this.menuStrip1.TabIndex = 21;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // tsmiManagement
            // 
            this.tsmiManagement.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiModel,
            this.tsmiPart,
            this.tsmiProcess,
            this.toolStripSeparator1,
            this.tsmiMaker});
            this.tsmiManagement.Name = "tsmiManagement";
            this.tsmiManagement.Size = new System.Drawing.Size(90, 20);
            this.tsmiManagement.Text = "Management";
            // 
            // tsmiModel
            // 
            this.tsmiModel.Name = "tsmiModel";
            this.tsmiModel.Size = new System.Drawing.Size(114, 22);
            this.tsmiModel.Text = "Model";
            this.tsmiModel.Click += new System.EventHandler(this.tsmiModel_Click);
            // 
            // tsmiPart
            // 
            this.tsmiPart.Name = "tsmiPart";
            this.tsmiPart.Size = new System.Drawing.Size(114, 22);
            this.tsmiPart.Text = "Part";
            this.tsmiPart.Click += new System.EventHandler(this.tsmiPart_Click);
            // 
            // tsmiProcess
            // 
            this.tsmiProcess.Name = "tsmiProcess";
            this.tsmiProcess.Size = new System.Drawing.Size(114, 22);
            this.tsmiProcess.Text = "Process";
            this.tsmiProcess.Click += new System.EventHandler(this.tsmiProcess_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(111, 6);
            // 
            // tsmiMaker
            // 
            this.tsmiMaker.Name = "tsmiMaker";
            this.tsmiMaker.Size = new System.Drawing.Size(114, 22);
            this.tsmiMaker.Text = "Maker";
            this.tsmiMaker.Click += new System.EventHandler(this.tsmiMaker_Click);
            // 
            // tsmiPartList
            // 
            this.tsmiPartList.Name = "tsmiPartList";
            this.tsmiPartList.Size = new System.Drawing.Size(62, 20);
            this.tsmiPartList.Text = "Part List";
            this.tsmiPartList.Click += new System.EventHandler(this.tsmiPartList_Click);
            // 
            // tsmiReport
            // 
            this.tsmiReport.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiODNHistory});
            this.tsmiReport.Name = "tsmiReport";
            this.tsmiReport.Size = new System.Drawing.Size(54, 20);
            this.tsmiReport.Text = "Report";
            // 
            // tsmiODNHistory
            // 
            this.tsmiODNHistory.Name = "tsmiODNHistory";
            this.tsmiODNHistory.Size = new System.Drawing.Size(251, 22);
            this.tsmiODNHistory.Text = "Official Drawing Number History";
            this.tsmiODNHistory.Click += new System.EventHandler(this.tsmiODNHistory_Click);
            // 
            // pnlNewOfficialDrawingNumber
            // 
            this.pnlNewOfficialDrawingNumber.Controls.Add(this.gbODNDetail);
            this.pnlNewOfficialDrawingNumber.Controls.Add(this.lblSaveType);
            this.pnlNewOfficialDrawingNumber.Controls.Add(this.cmbSaveDNType);
            this.pnlNewOfficialDrawingNumber.Controls.Add(this.btnRefresh);
            this.pnlNewOfficialDrawingNumber.Controls.Add(this.txtSaveDN);
            this.pnlNewOfficialDrawingNumber.Controls.Add(this.cmbModel);
            this.pnlNewOfficialDrawingNumber.Controls.Add(this.lblRevisionCd);
            this.pnlNewOfficialDrawingNumber.Controls.Add(this.lblModel);
            this.pnlNewOfficialDrawingNumber.Controls.Add(this.txtRevisionCd);
            this.pnlNewOfficialDrawingNumber.Controls.Add(this.cmbPart);
            this.pnlNewOfficialDrawingNumber.Controls.Add(this.label1);
            this.pnlNewOfficialDrawingNumber.Controls.Add(this.lblPart);
            this.pnlNewOfficialDrawingNumber.Controls.Add(this.txtResultOfficialDN);
            this.pnlNewOfficialDrawingNumber.Controls.Add(this.cmbProcess);
            this.pnlNewOfficialDrawingNumber.Controls.Add(this.btnClose);
            this.pnlNewOfficialDrawingNumber.Controls.Add(this.lblProcess);
            this.pnlNewOfficialDrawingNumber.Controls.Add(this.btnClear);
            this.pnlNewOfficialDrawingNumber.Controls.Add(this.btnPublish);
            this.pnlNewOfficialDrawingNumber.Controls.Add(this.lblNote);
            this.pnlNewOfficialDrawingNumber.Controls.Add(this.cmbOrderType);
            this.pnlNewOfficialDrawingNumber.Controls.Add(this.txtNote);
            this.pnlNewOfficialDrawingNumber.Controls.Add(this.lblOrderType);
            this.pnlNewOfficialDrawingNumber.Location = new System.Drawing.Point(0, 25);
            this.pnlNewOfficialDrawingNumber.Name = "pnlNewOfficialDrawingNumber";
            this.pnlNewOfficialDrawingNumber.Size = new System.Drawing.Size(440, 490);
            this.pnlNewOfficialDrawingNumber.TabIndex = 22;
            // 
            // gbODNDetail
            // 
            this.gbODNDetail.Controls.Add(this.btnMaker);
            this.gbODNDetail.Controls.Add(this.txtPrice);
            this.gbODNDetail.Controls.Add(this.lblPrice);
            this.gbODNDetail.Controls.Add(this.txtUnitPrice);
            this.gbODNDetail.Controls.Add(this.lblUnitPrice);
            this.gbODNDetail.Controls.Add(this.txtQty);
            this.gbODNDetail.Controls.Add(this.lblQty);
            this.gbODNDetail.Controls.Add(this.txtMaker);
            this.gbODNDetail.Controls.Add(this.lblCustomer);
            this.gbODNDetail.Controls.Add(this.txtAfterProcess);
            this.gbODNDetail.Controls.Add(this.label2);
            this.gbODNDetail.Controls.Add(this.txtMaterial);
            this.gbODNDetail.Controls.Add(this.lblMaterial);
            this.gbODNDetail.Controls.Add(this.txtSpecfication);
            this.gbODNDetail.Controls.Add(this.lblSpecfication);
            this.gbODNDetail.Controls.Add(this.txtPartName);
            this.gbODNDetail.Controls.Add(this.lblPartName);
            this.gbODNDetail.Controls.Add(this.txtType);
            this.gbODNDetail.Controls.Add(this.lblType);
            this.gbODNDetail.Location = new System.Drawing.Point(12, 180);
            this.gbODNDetail.Name = "gbODNDetail";
            this.gbODNDetail.Size = new System.Drawing.Size(413, 251);
            this.gbODNDetail.TabIndex = 26;
            this.gbODNDetail.TabStop = false;
            this.gbODNDetail.Text = "Detail Content";
            // 
            // btnMaker
            // 
            this.btnMaker.Location = new System.Drawing.Point(360, 159);
            this.btnMaker.Name = "btnMaker";
            this.btnMaker.Size = new System.Drawing.Size(47, 22);
            this.btnMaker.TabIndex = 13;
            this.btnMaker.Text = "...";
            this.btnMaker.UseVisualStyleBackColor = true;
            this.btnMaker.Click += new System.EventHandler(this.btnMaker_Click);
            // 
            // txtPrice
            // 
            this.txtPrice.Location = new System.Drawing.Point(91, 215);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(122, 21);
            this.txtPrice.TabIndex = 16;
            this.txtPrice.Text = "0";
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Location = new System.Drawing.Point(11, 218);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(34, 12);
            this.lblPrice.TabIndex = 16;
            this.lblPrice.Text = "Price";
            // 
            // txtUnitPrice
            // 
            this.txtUnitPrice.Location = new System.Drawing.Point(285, 188);
            this.txtUnitPrice.Name = "txtUnitPrice";
            this.txtUnitPrice.Size = new System.Drawing.Size(122, 21);
            this.txtUnitPrice.TabIndex = 15;
            this.txtUnitPrice.Text = "0";
            this.txtUnitPrice.TextChanged += new System.EventHandler(this.txtUnitPrice_TextChanged);
            // 
            // lblUnitPrice
            // 
            this.lblUnitPrice.AutoSize = true;
            this.lblUnitPrice.Location = new System.Drawing.Point(223, 191);
            this.lblUnitPrice.Name = "lblUnitPrice";
            this.lblUnitPrice.Size = new System.Drawing.Size(46, 12);
            this.lblUnitPrice.TabIndex = 14;
            this.lblUnitPrice.Text = "U Price";
            // 
            // txtQty
            // 
            this.txtQty.Location = new System.Drawing.Point(90, 188);
            this.txtQty.Name = "txtQty";
            this.txtQty.Size = new System.Drawing.Size(122, 21);
            this.txtQty.TabIndex = 14;
            this.txtQty.Text = "0";
            this.txtQty.TextChanged += new System.EventHandler(this.txtQty_TextChanged);
            // 
            // lblQty
            // 
            this.lblQty.AutoSize = true;
            this.lblQty.Location = new System.Drawing.Point(10, 191);
            this.lblQty.Name = "lblQty";
            this.lblQty.Size = new System.Drawing.Size(24, 12);
            this.lblQty.TabIndex = 12;
            this.lblQty.Text = "Qty";
            // 
            // txtMaker
            // 
            this.txtMaker.Enabled = false;
            this.txtMaker.Location = new System.Drawing.Point(90, 161);
            this.txtMaker.Name = "txtMaker";
            this.txtMaker.Size = new System.Drawing.Size(264, 21);
            this.txtMaker.TabIndex = 12;
            // 
            // lblCustomer
            // 
            this.lblCustomer.AutoSize = true;
            this.lblCustomer.Location = new System.Drawing.Point(10, 164);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(40, 12);
            this.lblCustomer.TabIndex = 10;
            this.lblCustomer.Text = "Maker";
            // 
            // txtAfterProcess
            // 
            this.txtAfterProcess.Location = new System.Drawing.Point(90, 134);
            this.txtAfterProcess.Name = "txtAfterProcess";
            this.txtAfterProcess.Size = new System.Drawing.Size(317, 21);
            this.txtAfterProcess.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 137);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 12);
            this.label2.TabIndex = 8;
            this.label2.Text = "After Process";
            // 
            // txtMaterial
            // 
            this.txtMaterial.Location = new System.Drawing.Point(90, 107);
            this.txtMaterial.Name = "txtMaterial";
            this.txtMaterial.Size = new System.Drawing.Size(317, 21);
            this.txtMaterial.TabIndex = 10;
            // 
            // lblMaterial
            // 
            this.lblMaterial.AutoSize = true;
            this.lblMaterial.Location = new System.Drawing.Point(10, 110);
            this.lblMaterial.Name = "lblMaterial";
            this.lblMaterial.Size = new System.Drawing.Size(50, 12);
            this.lblMaterial.TabIndex = 6;
            this.lblMaterial.Text = "Material";
            // 
            // txtSpecfication
            // 
            this.txtSpecfication.Location = new System.Drawing.Point(90, 79);
            this.txtSpecfication.Name = "txtSpecfication";
            this.txtSpecfication.Size = new System.Drawing.Size(317, 21);
            this.txtSpecfication.TabIndex = 9;
            // 
            // lblSpecfication
            // 
            this.lblSpecfication.AutoSize = true;
            this.lblSpecfication.Location = new System.Drawing.Point(10, 82);
            this.lblSpecfication.Name = "lblSpecfication";
            this.lblSpecfication.Size = new System.Drawing.Size(74, 12);
            this.lblSpecfication.TabIndex = 4;
            this.lblSpecfication.Text = "Specfication";
            // 
            // txtPartName
            // 
            this.txtPartName.Location = new System.Drawing.Point(90, 25);
            this.txtPartName.Name = "txtPartName";
            this.txtPartName.Size = new System.Drawing.Size(317, 21);
            this.txtPartName.TabIndex = 7;
            // 
            // lblPartName
            // 
            this.lblPartName.AutoSize = true;
            this.lblPartName.Location = new System.Drawing.Point(10, 28);
            this.lblPartName.Name = "lblPartName";
            this.lblPartName.Size = new System.Drawing.Size(61, 12);
            this.lblPartName.TabIndex = 2;
            this.lblPartName.Text = "PartName";
            // 
            // txtType
            // 
            this.txtType.Location = new System.Drawing.Point(90, 52);
            this.txtType.Name = "txtType";
            this.txtType.Size = new System.Drawing.Size(317, 21);
            this.txtType.TabIndex = 8;
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(10, 55);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(34, 12);
            this.lblType.TabIndex = 0;
            this.lblType.Text = "Type";
            // 
            // lblSaveType
            // 
            this.lblSaveType.AutoSize = true;
            this.lblSaveType.Location = new System.Drawing.Point(10, 60);
            this.lblSaveType.Name = "lblSaveType";
            this.lblSaveType.Size = new System.Drawing.Size(34, 12);
            this.lblSaveType.TabIndex = 25;
            this.lblSaveType.Text = "Type";
            // 
            // cmbSaveDNType
            // 
            this.cmbSaveDNType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSaveDNType.FormattingEnabled = true;
            this.cmbSaveDNType.Location = new System.Drawing.Point(50, 55);
            this.cmbSaveDNType.Name = "cmbSaveDNType";
            this.cmbSaveDNType.Size = new System.Drawing.Size(178, 20);
            this.cmbSaveDNType.TabIndex = 4;
            this.cmbSaveDNType.SelectedIndexChanged += new System.EventHandler(this.cmbSaveDNType_SelectedIndexChanged);
            // 
            // btnRefresh
            // 
            this.btnRefresh.AutoSize = true;
            this.btnRefresh.Location = new System.Drawing.Point(370, 27);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(58, 22);
            this.btnRefresh.TabIndex = 23;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // txtSaveDN
            // 
            this.txtSaveDN.Location = new System.Drawing.Point(234, 55);
            this.txtSaveDN.Name = "txtSaveDN";
            this.txtSaveDN.Size = new System.Drawing.Size(194, 21);
            this.txtSaveDN.TabIndex = 5;
            // 
            // pnlReviseOfficialDrawingNumber
            // 
            this.pnlReviseOfficialDrawingNumber.Controls.Add(this.lblNewOrderType);
            this.pnlReviseOfficialDrawingNumber.Controls.Add(this.txtNewOrderType);
            this.pnlReviseOfficialDrawingNumber.Controls.Add(this.lblContent);
            this.pnlReviseOfficialDrawingNumber.Controls.Add(this.lblReviseOfficialDrawingNumber);
            this.pnlReviseOfficialDrawingNumber.Controls.Add(this.btnCheckVaild);
            this.pnlReviseOfficialDrawingNumber.Controls.Add(this.txtReviseOfficialDrawingNumber);
            this.pnlReviseOfficialDrawingNumber.Location = new System.Drawing.Point(446, 25);
            this.pnlReviseOfficialDrawingNumber.Name = "pnlReviseOfficialDrawingNumber";
            this.pnlReviseOfficialDrawingNumber.Size = new System.Drawing.Size(440, 171);
            this.pnlReviseOfficialDrawingNumber.TabIndex = 23;
            // 
            // lblNewOrderType
            // 
            this.lblNewOrderType.AutoSize = true;
            this.lblNewOrderType.Location = new System.Drawing.Point(263, 76);
            this.lblNewOrderType.Name = "lblNewOrderType";
            this.lblNewOrderType.Size = new System.Drawing.Size(69, 12);
            this.lblNewOrderType.TabIndex = 23;
            this.lblNewOrderType.Text = "Order Type";
            // 
            // txtNewOrderType
            // 
            this.txtNewOrderType.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtNewOrderType.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtNewOrderType.Location = new System.Drawing.Point(265, 91);
            this.txtNewOrderType.MaxLength = 1;
            this.txtNewOrderType.Name = "txtNewOrderType";
            this.txtNewOrderType.Size = new System.Drawing.Size(67, 32);
            this.txtNewOrderType.TabIndex = 22;
            // 
            // lblContent
            // 
            this.lblContent.AutoSize = true;
            this.lblContent.ForeColor = System.Drawing.Color.Red;
            this.lblContent.Location = new System.Drawing.Point(19, 32);
            this.lblContent.Name = "lblContent";
            this.lblContent.Size = new System.Drawing.Size(313, 12);
            this.lblContent.TabIndex = 21;
            this.lblContent.Text = "※ Revision Code는 반드시 입력해야 합니다.(총 11자리)";
            // 
            // lblReviseOfficialDrawingNumber
            // 
            this.lblReviseOfficialDrawingNumber.AutoSize = true;
            this.lblReviseOfficialDrawingNumber.Location = new System.Drawing.Point(19, 76);
            this.lblReviseOfficialDrawingNumber.Name = "lblReviseOfficialDrawingNumber";
            this.lblReviseOfficialDrawingNumber.Size = new System.Drawing.Size(184, 12);
            this.lblReviseOfficialDrawingNumber.TabIndex = 8;
            this.lblReviseOfficialDrawingNumber.Text = "Revise Official Drawing Number";
            // 
            // btnCheckVaild
            // 
            this.btnCheckVaild.Location = new System.Drawing.Point(338, 89);
            this.btnCheckVaild.Name = "btnCheckVaild";
            this.btnCheckVaild.Size = new System.Drawing.Size(90, 34);
            this.btnCheckVaild.TabIndex = 10;
            this.btnCheckVaild.Text = "Check Vaild";
            this.btnCheckVaild.UseVisualStyleBackColor = true;
            this.btnCheckVaild.Click += new System.EventHandler(this.btnCheckVaild_Click);
            // 
            // txtReviseOfficialDrawingNumber
            // 
            this.txtReviseOfficialDrawingNumber.Font = new System.Drawing.Font("굴림", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtReviseOfficialDrawingNumber.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtReviseOfficialDrawingNumber.Location = new System.Drawing.Point(21, 91);
            this.txtReviseOfficialDrawingNumber.MaxLength = 11;
            this.txtReviseOfficialDrawingNumber.Name = "txtReviseOfficialDrawingNumber";
            this.txtReviseOfficialDrawingNumber.Size = new System.Drawing.Size(225, 32);
            this.txtReviseOfficialDrawingNumber.TabIndex = 9;
            // 
            // FormPublishOfficialDN
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.ClientSize = new System.Drawing.Size(886, 519);
            this.Controls.Add(this.pnlReviseOfficialDrawingNumber);
            this.Controls.Add(this.pnlNewOfficialDrawingNumber);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "FormPublishOfficialDN";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Publish Real Drawing Number";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormPublishOfficialDN_FormClosed);
            this.Load += new System.EventHandler(this.FormPublishOfficialDN_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.pnlNewOfficialDrawingNumber.ResumeLayout(false);
            this.pnlNewOfficialDrawingNumber.PerformLayout();
            this.gbODNDetail.ResumeLayout(false);
            this.gbODNDetail.PerformLayout();
            this.pnlReviseOfficialDrawingNumber.ResumeLayout(false);
            this.pnlReviseOfficialDrawingNumber.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbModel;
        private System.Windows.Forms.Label lblModel;
        private System.Windows.Forms.Label lblPart;
        private System.Windows.Forms.ComboBox cmbPart;
        private System.Windows.Forms.Label lblProcess;
        private System.Windows.Forms.ComboBox cmbProcess;
        private System.Windows.Forms.Button btnPublish;
        private System.Windows.Forms.Label lblOrderType;
        private System.Windows.Forms.ComboBox cmbOrderType;
        private System.Windows.Forms.TextBox txtNote;
        private System.Windows.Forms.Label lblNote;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TextBox txtResultOfficialDN;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtRevisionCd;
        private System.Windows.Forms.Label lblRevisionCd;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tsmiReport;
        private System.Windows.Forms.ToolStripMenuItem tsmiODNHistory;
        private System.Windows.Forms.ToolStripMenuItem tsmiManagement;
        private System.Windows.Forms.ToolStripMenuItem tsmiModel;
        private System.Windows.Forms.ToolStripMenuItem tsmiPart;
        private System.Windows.Forms.ToolStripMenuItem tsmiProcess;
        private System.Windows.Forms.Panel pnlNewOfficialDrawingNumber;
        private System.Windows.Forms.Panel pnlReviseOfficialDrawingNumber;
        private System.Windows.Forms.Label lblReviseOfficialDrawingNumber;
        private System.Windows.Forms.Button btnCheckVaild;
        private System.Windows.Forms.TextBox txtReviseOfficialDrawingNumber;
        private System.Windows.Forms.Label lblContent;
        private System.Windows.Forms.TextBox txtSaveDN;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.ComboBox cmbSaveDNType;
        private System.Windows.Forms.Label lblSaveType;
        private System.Windows.Forms.Label lblNewOrderType;
        private System.Windows.Forms.TextBox txtNewOrderType;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.GroupBox gbODNDetail;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.TextBox txtUnitPrice;
        private System.Windows.Forms.Label lblUnitPrice;
        private System.Windows.Forms.TextBox txtQty;
        private System.Windows.Forms.Label lblQty;
        private System.Windows.Forms.TextBox txtMaker;
        private System.Windows.Forms.Label lblCustomer;
        private System.Windows.Forms.TextBox txtAfterProcess;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtMaterial;
        private System.Windows.Forms.Label lblMaterial;
        private System.Windows.Forms.TextBox txtSpecfication;
        private System.Windows.Forms.Label lblSpecfication;
        private System.Windows.Forms.TextBox txtPartName;
        private System.Windows.Forms.Label lblPartName;
        private System.Windows.Forms.TextBox txtType;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.Button btnMaker;
        private System.Windows.Forms.ToolStripMenuItem tsmiMaker;
        private System.Windows.Forms.ToolStripMenuItem tsmiPartList;
    }
}