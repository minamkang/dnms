﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Forms;
using System.Net.Mail;
using System.Net;
using System.Collections;

namespace DrawingNumberManager
{
    public class Mail
    {
        private MailAddress sender_Address = null;
        private MailAddress cc_Address = null;
        string strSenderAddress = "kmrnd@kmdigitech.com";
        private string send_Password = "KMr&d123";

        public Mail()
        {
            try
            {
                sender_Address = new MailAddress(strSenderAddress);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        public string Send(string strReceiverAddress, string strReferenceAddress, string strSubject, string strBody, ArrayList alAttachments)
        {
            SmtpClient smtp = null;
            MailMessage message = null;

            try
            {
                smtp = new SmtpClient
                {
                    Host = "mail.kmdigitech.com",
                    EnableSsl = false,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new NetworkCredential(sender_Address.Address, send_Password),
                    Timeout = 20000
                };

                using (message = new MailMessage())
                {
                    message.From = sender_Address;

                    string[] split_receiver = strReceiverAddress.ToString().Split(',');
                    for (int i = 0; i < split_receiver.Length; i++)
                        message.To.Add(split_receiver[i].Trim());

                    string[] split_cc = strReferenceAddress.ToString().Split(',');
                    for (int i = 0; i < split_cc.Length; i++)
                    {
                        cc_Address = new MailAddress(split_cc[i].Trim());
                        message.CC.Add(cc_Address);
                    }
                    cc_Address = new MailAddress(strSenderAddress);
                    message.CC.Add(cc_Address);

                    message.Subject = strSubject;
                    message.Body = strBody;

                    //첨부파일
                    System.Net.Mail.Attachment attachment;
                    for (int i = 0; i < alAttachments.Count; i++)
                    {
                        attachment = new System.Net.Mail.Attachment(alAttachments[i].ToString());
                        message.Attachments.Add(attachment);
                    }

                    smtp.Send(message);
                    return "성공!";
                }
            }
            catch (SmtpFailedRecipientsException ex)
            {
                string strMessage = "";
                for (int i = 0; i < ex.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = ex.InnerExceptions[i].StatusCode;
                    if (status == SmtpStatusCode.MailboxBusy || status == SmtpStatusCode.MailboxUnavailable)
                    {
                        MessageBox.Show("Delivery failed - retrying in 5 seconds.");
                        System.Threading.Thread.Sleep(5000);
                        smtp.Send(message);
                    }
                    else
                    {
                        strMessage = string.Format("Failed to deliver message to {0}", ex.InnerExceptions[i]);
                        MessageBox.Show(strMessage);
                        break;
                    }
                }
                return strMessage;
            }
            catch (Exception ex)
            {
                return "실패!\r" + ex.ToString();
            }
            finally
            {
                if (smtp != null)
                    smtp.Dispose();
                if (message != null)
                    message.Dispose();
            }

        }

    }
}
