﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DrawingNumberManager
{
    public class Notice
    {
        public FormNoticeMessage frm;
        public string strOrderNumber = "";

        private int cycle_time = 1000;
        private bool isStart = false;

        public Notice(FormNoticeMessage noticeMessage)
        {
            frm = noticeMessage;
            if (StaticData.db_connetion_string != "")
                isStart = true;
        }

        public void KeepAliveNotice()
        {
            try
            {
                while (isStart)
                {
                    Thread.Sleep(cycle_time);
                    DisplayhNotice();
                }
            }
            catch (ThreadAbortException e)
            {
                e.ToString();
                Thread.ResetAbort();
            }
            finally
            {
            }
        }

        private void DisplayhNotice()
        {
            DataTable result1 = new DataTable();
            DataTable result2 = new DataTable();

            try
            {
                SqlDataAdapter queryAdapter = new SqlDataAdapter(string.Format("EXEC search_notify_display {0}", StaticData.user_key), StaticData.db_connetion_string);
                queryAdapter.Fill(result1);

                queryAdapter = new SqlDataAdapter(string.Format("EXEC search_notify_duedate {0}", StaticData.user_key), StaticData.db_connetion_string);
                queryAdapter.Fill(result2);

                if (result1 != null && result1.Rows.Count > 0 || result2 != null && result2.Rows.Count > 0)
                {
                    frm.Invoke(new MethodInvoker(delegate()
                    {
                        if (result1.Rows.Count > 0)
                        {
                            frm.lblMessage.Text = string.Format("발주서[{0}]가 도착하였습니다.\r\n\r\n작성자: {1}",
                            result1.Rows[0]["발주번호"].ToString(), result1.Rows[0]["담당자"].ToString());

                            frm.Visible = true;
                            frm.lblOrderNumber.Text = "O" + result1.Rows[0]["발주번호"].ToString();
                        }
                        else if (result2.Rows.Count > 0)
                        {
                            frm.lblMessage.Text = string.Format("금일 납기 예정\r\n\r\n발주번호[{0}]\r\n\r\n업체명[{1}] ", result2.Rows[0]["발주번호"].ToString(), result2.Rows[0]["업체명"].ToString());

                            frm.Visible = true;
                            frm.lblOrderNumber.Text = "D" + result2.Rows[0]["발주번호"].ToString();
                        }
                    }));
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }
    }
}
