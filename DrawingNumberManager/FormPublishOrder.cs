﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using System.Collections;

namespace DrawingNumberManager
{
    public partial class FormPublishOrder : Form
    {
        private bool is_add_order;
        private bool ServerSearch = false;		        //날짜관련 Data가 변경 되면 Server에서 Search여부
        
        private int maker_code = -1;
        private string order_number;
        private string save_err_message;

        DateTime Now = DateTime.Now.Date;

        System.Data.DataTable result = null;
        System.Data.DataTable tempResult = null;
        System.Data.DataTable dtPartsToOrder = null;

        DateTimePicker dtpickerDueDate = new DateTimePicker();
        Rectangle _Rectangle; 

        Drawing dwg = new Drawing();
        ArrayList al_Drwaing = null;

        public FormPublishOrder(bool isAddOrder, string OrderNumber, int imaker_code)
        {
            InitializeComponent();

            is_add_order = isAddOrder;
            order_number = OrderNumber;
            maker_code = imaker_code;
        }

        private void FormPublishOrder_Load(object sender, EventArgs e)
        {
            pnlLeft.Size = new System.Drawing.Size(785, 628);
            ServerSearch = true;

            if (is_add_order)
                this.Text = string.Format("{0}[{1}]-추가", Const.FORM_ORDER_REGEDITER, StaticData.user_name);
            else
                this.Text = string.Format("{0}[{1}]-수정", Const.FORM_ORDER_REGEDITER, StaticData.user_name);

            BindUserID(true);
            BindUserID(false);

            for (short i = 1; i < 6; i++)
                BindCmbBox(i);

            dtpickerStartDate.Value = Now.AddDays(-1);
            dtpickerEndDate.Value = Now.AddDays(1).AddSeconds(-1);
            dtpickerDueDate.Value = Now.AddDays(14);
            dgvOrderDetail.Controls.Add(dtpickerDueDate);
            dtpickerDueDate.Visible = false;
            dtpickerDueDate.Format = DateTimePickerFormat.Custom;

            InitializeDtPartsToOrder();
        }

        private void InitializeDtPartsToOrder()
        {
            dtPartsToOrder = new DataTable { TableName = "ORDER_DETAIL"};
            dtPartsToOrder.Columns.AddRange(new DataColumn[] 
            {
                new DataColumn {ColumnName = "NO"},
                new DataColumn {ColumnName = "모델"},
                new DataColumn {ColumnName = "도면"},
                new DataColumn {ColumnName = "도번"},
                new DataColumn {ColumnName = "REV"},
                new DataColumn {ColumnName = "품명"},
                new DataColumn {ColumnName = "규격"},
                new DataColumn {ColumnName = "단위"},
                new DataColumn {ColumnName = "수량",   DataType = typeof(int)},
                new DataColumn {ColumnName = "단가",   DataType = typeof(int)},
                new DataColumn {ColumnName = "소계",   DataType = typeof(int)},
                new DataColumn {ColumnName = "부가세", DataType = typeof(int)},
                new DataColumn {ColumnName = "납기일", DataType = typeof(DateTime)},
                new DataColumn {ColumnName = "비고"},
                new DataColumn {ColumnName = "KEY_ID"}

            });

            dgvOrderDetail.DataSource = dtPartsToOrder;
            ColumnsSortDisable();
            DgvOrderDetailFormated();
        }

        private void FormPublishOrder_Shown(object sender, EventArgs e)
        {
            if (is_add_order)
                AddInitialize();
            else
                ModifyInitialize();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            ServerSearch = true;
            BindDgvPartList();
        }

        private void cmbModel_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDgvPartList();
        }

        private void cmbPart_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDgvPartList();
        }

        private void cmbUserID_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDgvPartList();
        }

        private void txtPartName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                BindDgvPartList();
        }

        private void txtNote_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                BindDgvPartList();
        }

        private void btnCreatedOrder_Click(object sender, EventArgs e)
        {
            if (txtOrderNumber.Text != "")
            {
                if (CheckSaveOrderVaild() == true)
                {
                    string strMessage = "";
                    strMessage += "작성한 발주서 내역이 있습니다..\r";
                    strMessage += "저장하시려면 '예(Y)', 그렇지 않으면 '아니오(N)' 버튼을 누르세요";

                    if (MessageBox.Show(strMessage, "확인", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        SaveOrder();
                        ClearOrder(false);
                    }
                }
                else
                    MessageBox.Show(save_err_message, "확인");
            }
            else
                CreateOrderNumber();
        }

        private void btnMaker_Click(object sender, EventArgs e)
        {
            FormMaker frmCompany = new FormMaker(false);
            if (frmCompany.ShowDialog() == DialogResult.OK)
            {
                txtMaker.Text = frmCompany.MakerName;
                maker_code = frmCompany.MakerCode;
            }
        }

        private void btnPartToOrder_Click(object sender, EventArgs e)
        {
            if (dgvPartList.Rows.Count == 0)
                return;

            if (dtPartsToOrder.Columns.Count == 0) //최초 생성시에만 추가 한다.
            {
                foreach (DataGridViewColumn column in dgvPartList.Columns)
                    dtPartsToOrder.Columns.Add(column.Name);
            }

            for (int i = (dgvPartList.SelectedRows.Count - 1); i >= 0; i--)
            {
                //중복 품번 삭제
                string drawingNumber = dgvPartList.SelectedRows[i].Cells["도번"].Value.ToString();
                string revice = dgvPartList.SelectedRows[i].Cells["REV"].Value.ToString();

                if (CheckDuplicatePart(drawingNumber, revice) == true)
                    continue;
                
                dtPartsToOrder.Rows.Add();
                dtPartsToOrder.Rows[dtPartsToOrder.Rows.Count - 1]["모델"]   = dgvPartList.SelectedRows[i].Cells["모델"].Value;
                //도면은 버튼이다.
                dtPartsToOrder.Rows[dtPartsToOrder.Rows.Count - 1]["도번"]   = dgvPartList.SelectedRows[i].Cells["도번"].Value;
                dtPartsToOrder.Rows[dtPartsToOrder.Rows.Count - 1]["REV"]    = dgvPartList.SelectedRows[i].Cells["REV"].Value;
                dtPartsToOrder.Rows[dtPartsToOrder.Rows.Count - 1]["품명"]   = dgvPartList.SelectedRows[i].Cells["품명"].Value;
                dtPartsToOrder.Rows[dtPartsToOrder.Rows.Count - 1]["규격"]   = dgvPartList.SelectedRows[i].Cells["규격"].Value;
                //단위는 ComboBox
                dtPartsToOrder.Rows[dtPartsToOrder.Rows.Count - 1]["수량"]   = dgvPartList.SelectedRows[i].Cells["수량"].Value;
                dtPartsToOrder.Rows[dtPartsToOrder.Rows.Count - 1]["단가"]   = dgvPartList.SelectedRows[i].Cells["단가"].Value;
                dtPartsToOrder.Rows[dtPartsToOrder.Rows.Count - 1]["소계"]   = 0;
                dtPartsToOrder.Rows[dtPartsToOrder.Rows.Count - 1]["부가세"] = 0;
                //납기일은 DateTimePickup
                dtPartsToOrder.Rows[dtPartsToOrder.Rows.Count - 1]["납기일"] = Now.AddDays(14);
                dtPartsToOrder.Rows[dtPartsToOrder.Rows.Count - 1]["비고"] = "";
                dtPartsToOrder.Rows[dtPartsToOrder.Rows.Count - 1]["KEY_ID"] = dgvPartList.SelectedRows[i].Cells["KEY_ID"].Value;
            }

            for (int i = 0; i < dtPartsToOrder.Rows.Count; i++)  //순번 재정렬
            {
                dtPartsToOrder.Rows[i]["NO"] = i + 1;
            }

            dgvOrderDetail.DataSource = dtPartsToOrder;
            ColumnsSortDisable();
            DgvOrderDetailControlAdd();
            PriceCalculartion();
        }

        private void dgvOrderDetail_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvOrderDetail.Rows.Count == 0)
                return;

            if (dgvOrderDetail.Columns[e.ColumnIndex].Name == "도면")
            {
                string dwgDrawingNumber = dgvOrderDetail.Rows[e.RowIndex].Cells["도번"].Value.ToString();
                string dwgRevice = dgvOrderDetail.Rows[e.RowIndex].Cells["REV"].Value.ToString();

                if (dwgDrawingNumber == "")
                    return;

                if (dwgRevice != "")
                {
                    if (dwgDrawingNumber.Length == 10)
                        dwgDrawingNumber += string.Format("_{0}", dwgRevice);
                }

                DirectoryInfo di = new DirectoryInfo(Const.LOCAL_VIEW_PATH);
                if (di.Exists == false)
                    di.Create();

                Drawing dwg = new Drawing();
                string strResult = dwg.Download(dwgDrawingNumber);
                if (strResult.Length > 0)
                {
                    MessageBox.Show(string.Format("{0}", strResult), "확인");
                    return;
                }
                else
                {
                    string ViewPath = "";

                    foreach (var file in di.GetFiles())
                    {
                        ViewPath = Path.Combine(Const.LOCAL_VIEW_PATH, file.Name);
                        break;
                    }

                    Process process = new Process();
                    process.StartInfo.FileName = ViewPath;
                    process.StartInfo.UseShellExecute = true;
                    process.StartInfo.ErrorDialog = true;
                    process.Start();
                }
            }
        }

        private void dgvOrderDetail_Click(object sender, EventArgs e)
        {
            if (dgvOrderDetail.Rows.Count == 0)
                return;

            if (dgvOrderDetail.Columns[dgvOrderDetail.CurrentCell.ColumnIndex].Name == "납기일")
            {
                _Rectangle = dgvOrderDetail.GetCellDisplayRectangle(dgvOrderDetail.CurrentCell.ColumnIndex, dgvOrderDetail.CurrentRow.Index, true);
                dtpickerDueDate.Size = new Size(_Rectangle.Width, _Rectangle.Height); //  
                dtpickerDueDate.Location = new Point(_Rectangle.X, _Rectangle.Y); //  
                dtpickerDueDate.Visible = true;
            }
        }

        private void dgvOrderDetail_CellLeave(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvOrderDetail.Columns[e.ColumnIndex].Name == "납기일")
            {
                if (int.Parse(Now.ToString("yyyyMMdd")) > int.Parse(dtpickerDueDate.Text.Replace("-", "")))
                    dgvOrderDetail.Rows[e.RowIndex].Cells["납기일"].Value = Now.AddDays(14).ToString("yyyy-MM-dd");
                else
                    dgvOrderDetail.Rows[e.RowIndex].Cells["납기일"].Value = dtpickerDueDate.Text;
            }
        }

        private void cmbVAT_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dgvOrderDetail.Rows.Count == 0)
                return;

            PriceCalculartion();
        }

        private void dgvOrderDetail_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvOrderDetail.Rows.Count == 0)
                return;

            if (dgvOrderDetail.Columns[e.ColumnIndex].Name == "수량" || dgvOrderDetail.Columns[e.ColumnIndex].Name == "단가")
            {
                int iUnitPrice;
                int iQty;

                if (dgvOrderDetail.Rows[dgvOrderDetail.CurrentRow.Index].Cells["단가"].Value == null || dgvOrderDetail.Rows[dgvOrderDetail.CurrentRow.Index].Cells["단가"].Value.ToString() == "")
                    iUnitPrice = 0;
                else
                    iUnitPrice = int.Parse(dgvOrderDetail.Rows[dgvOrderDetail.CurrentRow.Index].Cells["단가"].Value.ToString());

                if (dgvOrderDetail.Rows[dgvOrderDetail.CurrentRow.Index].Cells["수량"].Value == null || dgvOrderDetail.Rows[dgvOrderDetail.CurrentRow.Index].Cells["수량"].Value.ToString() == "")
                    iQty = 0;
                else
                    iQty = int.Parse(dgvOrderDetail.Rows[dgvOrderDetail.CurrentRow.Index].Cells["수량"].Value.ToString());

                dgvOrderDetail.Rows[dgvOrderDetail.CurrentRow.Index].Cells["소계"].Value = iUnitPrice * iQty;

                if (cmbVAT.Text == "별도")
                    dgvOrderDetail.Rows[dgvOrderDetail.CurrentRow.Index].Cells["부가세"].Value = (iUnitPrice * iQty) * 0.1;

                //합계 계산
                int iTotal = 0;
                int iVatTotal = 0;
                int iGrandTotal = 0;
                for (int i = 0; i < dgvOrderDetail.Rows.Count; i++)
                {
                    if (dgvOrderDetail.Rows[i].Cells["소계"].Value == null)
                        dgvOrderDetail.Rows[i].Cells["소계"].Value = 0;
                    if (dgvOrderDetail.Rows[i].Cells["부가세"].Value == null)
                        dgvOrderDetail.Rows[i].Cells["부가세"].Value = 0;

                    iTotal += int.Parse(dgvOrderDetail.Rows[i].Cells["소계"].Value.ToString().Replace(",", ""));
                    iVatTotal += int.Parse(dgvOrderDetail.Rows[i].Cells["부가세"].Value.ToString().Replace(",", ""));
                }

                iGrandTotal = iTotal + iVatTotal;

                txtTotal.Text = String.Format("{0:#,###}", iTotal);
                txtTotalVAT.Text = String.Format("{0:#,###}", iVatTotal);
                txtGrandTotal.Text = String.Format("{0:#,###}", iGrandTotal);
            }
        }

        private void dgvOrderDetail_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            dtpickerDueDate.Visible = false;
        }

        private void dgvOrderDetail_Scroll(object sender, ScrollEventArgs e)
        {
            dtpickerDueDate.Visible = false;
        }

        private void tsmiAddRow_Click(object sender, EventArgs e)
        {            
            DataRow newRow = dtPartsToOrder.NewRow();
            dtPartsToOrder.Rows.Add(newRow);

            dgvOrderDetail.DataSource = dtPartsToOrder;
            ColumnsSortDisable();

            dtPartsToOrder.Rows[dtPartsToOrder.Rows.Count - 1]["수량"] = 0;
            dtPartsToOrder.Rows[dtPartsToOrder.Rows.Count - 1]["단가"] = 0;
            dtPartsToOrder.Rows[dtPartsToOrder.Rows.Count - 1]["소계"] = 0;
            dtPartsToOrder.Rows[dtPartsToOrder.Rows.Count - 1]["부가세"] = 0;
            dtPartsToOrder.Rows[dtPartsToOrder.Rows.Count - 1]["납기일"] = Now.AddDays(7).ToString("yyyy-MM-dd");
            DgvOrderDetailControlAdd();

            for (int i = 0; i < dtPartsToOrder.Rows.Count; i++)  //순번 재정렬
            {
                dtPartsToOrder.Rows[i]["NO"] = i + 1;
            }

            PriceCalculartion();
        }

        private void tsmiDeleteRow_Click(object sender, EventArgs e)
        {
            if (dgvOrderDetail.Rows.Count == 0)
                return;

            string dgvDrawingNumber = dgvOrderDetail.Rows[dgvOrderDetail.CurrentRow.Index].Cells["도번"].Value.ToString();
            string dgvRevice = dgvOrderDetail.Rows[dgvOrderDetail.CurrentRow.Index].Cells["REV"].Value.ToString();

            if (dgvDrawingNumber != "")
            {
                for (int i = 0; i < dtPartsToOrder.Rows.Count; i++)
                {
                    if (dtPartsToOrder.Rows[i]["도번"].ToString() == dgvDrawingNumber)
                    {
                        if (dtPartsToOrder.Rows[i]["REV"].ToString() == dgvRevice)
                        {
                            dtPartsToOrder.Rows[i].Delete();
                            break;
                        }
                        else if (dtPartsToOrder.Rows[i]["REV"].ToString() == "")
                        {
                            dtPartsToOrder.Rows[i].Delete();
                            break;
                        }
                    }
                }
            }
            else
                dtPartsToOrder.Rows[dgvOrderDetail.CurrentRow.Index].Delete();

            dtPartsToOrder.AcceptChanges();

            for (int i = 0; i < dtPartsToOrder.Rows.Count; i++)  //순번 재정렬
            {
                dtPartsToOrder.Rows[i]["NO"] = i + 1;
            }

            dgvOrderDetail.DataSource = dtPartsToOrder;
            ColumnsSortDisable();
            DgvOrderDetailControlAdd();

            PriceCalculartion();

            if (dgvOrderDetail.Rows.Count == 0)
                dtpickerDueDate.Visible = false;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (CheckSaveOrderVaild() == true)
            {
                if(is_add_order == true)
                    CreateOrderNumber();

                SaveOrder();
                ClearOrder(false);
            }
            else
                MessageBox.Show(save_err_message, "확인");
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearOrder(true);
        }

        private void SaveOrder()
        {
            try
            {
                int Approver = 0;
                if (cmbApprover.SelectedValue == null || cmbApprover.Text == "")
                    Approver = 0;
                else
                    Approver = int.Parse(cmbApprover.SelectedValue.ToString());

                System.Data.DataTable result = new System.Data.DataTable();
                SqlDataAdapter query = null;

                if (is_add_order == true)
                {
                    query = new SqlDataAdapter(
                    String.Format("EXEC order_create '{0}', {1}, '{2}', {3}, {4}, '{5}'",
                    txtOrderNumber.Text,
                    maker_code,
                    cmbVAT.SelectedValue,
                    StaticData.user_key,
                    Approver,
                    txtMakerMessage.Text), StaticData.db_connetion_string);
                    query.Fill(result);
                }
                else
                {
                    query = new SqlDataAdapter(
                    String.Format("EXEC order_update '{0}', {1}, '{2}', {3}, {4}, '{5}'",
                    txtOrderNumber.Text,
                    maker_code,
                    cmbVAT.SelectedValue,
                    StaticData.user_key,
                    Approver,
                    txtMakerMessage.Text), StaticData.db_connetion_string);
                    query.Fill(result);
                }

                for (int i = 0; i < dgvOrderDetail.Rows.Count; i++)
                {
                    string strUnit = "";
                    switch (dgvOrderDetail.Rows[i].Cells["단위"].Value.ToString())
                    {
                        case "EA":          strUnit = "E";      break;
                        case "BOX":         strUnit = "B";      break;
                        case "KG":          strUnit = "K";      break;
                        case "LITER":       strUnit = "L";      break;
                        case "SET":         strUnit = "S";      break;
                        case "UNDEFIND":    strUnit = "U";      break;
                    }

                    result = new System.Data.DataTable();
                    query = new SqlDataAdapter(
                    String.Format("EXEC order_create_detail '{0}', {1}, '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', {8}, {9}, {10}, {11}, '{12}', '{13}'",
                    txtOrderNumber.Text,
                    dgvOrderDetail.Rows[i].Cells["NO"].Value.ToString(),
                    dgvOrderDetail.Rows[i].Cells["도번"].Value.ToString(),
                    dgvOrderDetail.Rows[i].Cells["REV"].Value.ToString(),
                    dgvOrderDetail.Rows[i].Cells["모델"].Value.ToString(),
                    dgvOrderDetail.Rows[i].Cells["품명"].Value.ToString(),
                    dgvOrderDetail.Rows[i].Cells["규격"].Value.ToString(),
                    strUnit,
                    dgvOrderDetail.Rows[i].Cells["단가"].Value.ToString(),
                    dgvOrderDetail.Rows[i].Cells["수량"].Value.ToString(),
                    dgvOrderDetail.Rows[i].Cells["소계"].Value.ToString(),
                    dgvOrderDetail.Rows[i].Cells["부가세"].Value.ToString(),
                    dgvOrderDetail.Rows[i].Cells["납기일"].Value.ToString(),
                    dgvOrderDetail.Rows[i].Cells["비고"].Value.ToString()),
                    StaticData.db_connetion_string);
                    query.Fill(result);
                }
                MessageBox.Show("저장이 완료 되었습니다.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("DB 담당자에게 연락 부탁드립니다." + ex.ToString());
            }
        }

        private void ClearOrder(bool isClear)
        {
            if (is_add_order || isClear)
            {
                if (is_add_order)
                {
                    txtMaker.Text = "";
                    txtOrderNumber.Text = "";
                    cmbVAT.SelectedValue = 'E';
                    cmbApprover.SelectedText = "";
                    txtMakerMessage.Text = "";
                }

                if (is_add_order || isClear)
                {
                    txtTotalVAT.Text = "0";
                    txtTotal.Text = "0";
                    txtGrandTotal.Text = "0";

                    dtpickerDueDate.Visible = false;
                    dtPartsToOrder.Rows.Clear();
                    dgvOrderDetail.DataSource = dtPartsToOrder;
                    ColumnsSortDisable();
                }
            }
            else
            {
                this.Close();
            }
        }

        private void DeleteOrder()
        {
            System.Data.DataTable result = new System.Data.DataTable();
            SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC delete_order '{0}', ''", txtMakerMessage.Text), StaticData.db_connetion_string);
            query.Fill(result);

            MessageBox.Show("삭제 하였습니다.");
        }

        private void BindUserID(bool isBasic)
        {
            System.Data.DataTable result = new System.Data.DataTable();
            SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC user_infomation 0"), StaticData.db_connetion_string);
            query.Fill(result);

            if (result != null)
            {
                result.DefaultView.Sort = "NAME ASC";

                if (isBasic)
                {
                    result.Rows.Add("ALL", "ALL", "ALL", "ALL", "ALL");
                    cmbUserID.ValueMember = "ID";
                    cmbUserID.DisplayMember = "NAME";
                    cmbUserID.DataSource = result;
                    cmbUserID.SelectedValue = StaticData.user_id;
                }
                else
                {
                    result.Rows.Add("");
                    cmbApprover.ValueMember = "USER_KEY";
                    cmbApprover.DisplayMember = "NAME";
                    cmbApprover.DataSource = result;
                    cmbApprover.SelectedValue = 0;
                }
            }
        }

        private void BindCmbBox(short sType)
        {
            DataTable result = new DataTable();
            SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC code_info {0}", sType), StaticData.db_connetion_string);
            query.Fill(result);

            if (result != null)
            {
                if (sType == 1)
                {
                    result.Rows.Add("");
                    cmbModel.ValueMember = "CODE_NAME";
                    cmbModel.DisplayMember = "CODE_VALUE";
                    cmbModel.DataSource = result;
                    cmbModel.SelectedValue = "";
                }
                else if (sType == 2)
                {
                    result.Rows.Add("");
                    cmbPart.ValueMember = "CODE_NAME";
                    cmbPart.DisplayMember = "CODE_VALUE";
                    cmbPart.DataSource = result;
                    cmbPart.SelectedValue = "";
                }
                else if (sType == 5)
                {
                    cmbVAT.ValueMember = "CODE_NAME";
                    cmbVAT.DisplayMember = "CODE_VALUE";
                    cmbVAT.DataSource = result;
                    cmbVAT.SelectedValue = "E";
                }
            }
        }

        private void BindDgvPartList()
        {
            if (ServerSearch)
            {
                result = new System.Data.DataTable();
                tempResult = new System.Data.DataTable();

                //Data History정보를 모두 읽어온다.
                SqlDataAdapter query = null;

                if (txtDrawingNumber.Text.Length == 0 && txtDrawingNumber.Text == "")
                    query = new SqlDataAdapter(String.Format("EXEC part_list '{0}', '{1}', ' '",
                        dtpickerStartDate.Value.ToString(Const.DATE_TIME_FORMAT), dtpickerEndDate.Value.ToString(Const.DATE_TIME_FORMAT)), StaticData.db_connetion_string);
                else if (txtDrawingNumber.Text.Length > 0 && txtDrawingNumber.Text != "")
                    query = new SqlDataAdapter(String.Format("EXEC part_list '', '', '{0}%'", txtDrawingNumber.Text), StaticData.db_connetion_string);

                query.Fill(result);
                ServerSearch = false;

                result.Columns[1].ColumnName = "생성일자";
                result.Columns[2].ColumnName = "생성자";
                result.Columns[3].ColumnName = "수정자";
                result.Columns[4].ColumnName = "眞도번";
                result.Columns[5].ColumnName = "REV";
                result.Columns[6].ColumnName = "假도번";
                result.Columns[7].ColumnName = "도번";
                result.Columns[8].ColumnName = "모델";
                result.Columns[9].ColumnName = "파트";
                result.Columns[10].ColumnName = "품명";
                result.Columns[11].ColumnName = "종류";
                result.Columns[12].ColumnName = "규격";
                result.Columns[13].ColumnName = "재질";
                result.Columns[14].ColumnName = "후처리";
                result.Columns[15].ColumnName = "수량";
                result.Columns[16].ColumnName = "업체";
                result.Columns[17].ColumnName = "단가";
                result.Columns[18].ColumnName = "금액";
                result.Columns[19].ColumnName = "공용";
                result.Columns[20].ColumnName = "Note";
                dgvPartList.DataSource = result;
            }

            tempResult = result.Copy();

            if (tempResult != null && tempResult.Rows.Count > 0)
            {
                DataRow[] dtrow = null;

                string Model = cmbModel.Text.ToString();
                string Part = cmbPart.Text.ToString();
                string ID = cmbUserID.SelectedValue.ToString();
                string PartName = "";
                string Note = "";

                if (txtPartName.Text.Length > 0)
                    PartName = txtPartName.Text.Replace("\r\n", "");
                if (txtNote.Text.Length > 0)
                    Note = txtNote.Text.Replace("\r\n", "");

                if (Model != "" && Part == "" && PartName == "" && ID == "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("모델 = '{0}' ", Model));
                else if (Model == "" && Part != "" && PartName == "" && ID == "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("파트 = '{0}'", Part));
                else if (Model == "" && Part == "" && PartName != "" && ID == "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("품명 LIKE '%{0}%'", PartName));
                else if (Model == "" && Part == "" && PartName == "" && ID != "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("생성자 = '{0}'", ID));
                else if (Model == "" && Part == "" && PartName == "" && ID == "ALL" && Note != "")
                    dtrow = tempResult.Select(string.Format("NOTE LIKE '%{0}%'", Note));

                else if (Model != "" && Part != "" && PartName == "" && ID == "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("모델 = '{0}' AND 파트 = '{1}'", Model, Part));
                else if (Model != "" && Part == "" && PartName != "" && ID == "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("모델 = '{0}' AND 품명 LIKE '%{1}%'", Model, PartName));
                else if (Model != "" && Part == "" && PartName == "" && ID != "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("모델 = '{0}' AND 생성자 = '{1}'", Model, ID));
                else if (Model != "" && Part == "" && PartName == "" && ID == "ALL" && Note != "")
                    dtrow = tempResult.Select(string.Format("모델 = '{0}' AND NOTE LIKE '%{1}%'", Model, Note));
                else if (Model == "" && Part != "" && PartName != "" && ID == "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("파트 = '{0}' AND 품명 LIKE '%{1}%'", Part, PartName));
                else if (Model == "" && Part != "" && PartName == "" && ID != "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("파트 = '{0}' AND 생성자 = '{1}'", Part, ID));
                else if (Model == "" && Part != "" && PartName == "" && ID == "ALL" && Note != "")
                    dtrow = tempResult.Select(string.Format("파트 = '{0}' AND NOTE LIKE '%{1}%'", Part, Note));
                else if (Model == "" && Part == "" && PartName != "" && ID != "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("품명 LIKE '%{0}%' AND 생성자 = '{1}'", PartName, ID));
                else if (Model == "" && Part == "" && PartName != "" && ID == "ALL" && Note != "")
                    dtrow = tempResult.Select(string.Format("품명 LIKE '%{0}%' AND NOTE LIKE '%{1}%'", PartName, Note));
                else if (Model == "" && Part == "" && PartName == "" && ID != "ALL" && Note != "")
                    dtrow = tempResult.Select(string.Format("생성자 = '{0}' AND NOTE LIKE '%{1}%'", ID, Note));

                else if (Model != "" && Part != "" && PartName != "" && ID == "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("모델 = '{0}' AND 파트 = '{1}' AND 품명 LIKE '%{2}%'", Model, Part, PartName));
                else if (Model != "" && Part != "" && PartName == "" && ID != "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("모델 = '{0}' AND 파트 = '{1}' AND 생성자 = '{2}'", Model, Part, ID));
                else if (Model != "" && Part != "" && PartName == "" && ID == "ALL" && Note != "")
                    dtrow = tempResult.Select(string.Format("모델 = '{0}' AND 파트 = '{1}' AND NOTE LIKE '%{2}%'", Model, Part, Note));
                else if (Model != "" && Part == "" && PartName != "" && ID != "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("모델 = '{0}' AND 품명 LIKE '%{1}%' AND 생성자 = '{2}'", Model, PartName, ID));
                else if (Model != "" && Part == "" && PartName == "" && ID != "ALL" && Note != "")
                    dtrow = tempResult.Select(string.Format("모델 = '{0}' AND 생성자 = '{1}' AND NOTE LIKE '%{2}%'", Model, ID, Note));

                else if (Model == "" && Part != "" && PartName != "" && ID != "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("파트 = '{0}' AND 품명 LIKE '%{1}%' AND 생성자 = '{2}'", Part, PartName, ID));
                else if (Model == "" && Part != "" && PartName != "" && ID != "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("파트 = '{0}' AND 생성자 = '{1}' AND NOTE LIKE '%{2}%' ", Part, ID, Note));
                else if (Model == "" && Part == "" && PartName != "" && ID != "ALL" && Note != "")
                    dtrow = tempResult.Select(string.Format("품명 LIKE '%{0}%' AND 생성자 = '{1}' AND Note LIKE '%{2}%'", PartName, ID, Note));

                else if (Model != "" && Part != "" && PartName != "" && ID != "ALL" && Note == "")
                    dtrow = tempResult.Select(string.Format("모델 = '{0}' AND 파트 = '{1}' AND 품명 LIKE '%{2}%' AND 생성자 = '{3}'", Model, Part, PartName, ID));
                else if (Model != "" && Part == "" && PartName != "" && ID != "ALL" && Note != "")
                    dtrow = tempResult.Select(string.Format("모델 = '{0}' AND 품명 LIKE '%{1}%' AND 생성자 = '{2}' AND NOTE LIKE '%{3}%'", Model, PartName, ID, Note));
                else if (Model != "" && Part != "" && PartName == "" && ID != "ALL" && Note != "")
                    dtrow = tempResult.Select(string.Format("모델 = '{0}' AND 파트 = '{1}' AND 생성자 = '{2}' AND NOTE LIKE '%{3}%'", Model, Part, ID, Note));
                else if (Model == "" && Part != "" && PartName != "" && ID != "ALL" && Note != "")
                    dtrow = tempResult.Select(string.Format("파트 = '{0}' AND 품명 LIKE '%{1}%' AND 생성자 = '{2}' AND NOTE LIKE '%{3}%'", Part, PartName, ID, Note));
                else if (Model != "" && Part != "" && PartName != "" && ID == "ALL" && Note != "")
                    dtrow = tempResult.Select(string.Format("모델 = '{0}' AND 파트 = '{1}' AND 품명 LIKE '%{2}%' AND Note LIKE '%{3}%'", Model, Part, PartName, Note));

                else if (Model != "" && Part != "" && PartName != "" && ID != "ALL" && Note != "")
                    dtrow = tempResult.Select(string.Format("모델 = '{0}' AND 파트 = '{1}' AND 품명 LIKE '%{2}%' AND 생성자 = '{3}' AND Note LIKE '%{4}%'", Model, Part, PartName, ID, Note));

                else if (Model == "" && Part == "" && PartName == "" && ID == "ALL" && Note == "")
                {
                    dgvPartList.DataSource = tempResult;
                    dgvPartListFormated();
                }

                if (dtrow == null)
                    return;

                if (dtrow.Length > 0)
                    tempResult = dtrow.CopyToDataTable();
                else
                    tempResult.Rows.Clear();

                dgvPartList.DataSource = tempResult;
            }

            dgvPartListFormated();
        }

        private void dgvPartListFormated()
        {
            this.dgvPartList.Columns["KEY_ID"].Visible = false;
            this.dgvPartList.Columns["BEFORE_NOTE"].Visible = false;
            this.dgvPartList.Columns["생성일자"].Visible = false;
            this.dgvPartList.Columns["생성자"].Visible = false;
            this.dgvPartList.Columns["수정자"].Visible = false;
            this.dgvPartList.Columns["眞도번"].Visible = false;
            this.dgvPartList.Columns["假도번"].Visible = false;
            this.dgvPartList.Columns["재질"].Visible = false;
            this.dgvPartList.Columns["후처리"].Visible = false;
            this.dgvPartList.Columns["수량"].Visible = false;
#if false
            this.dgvPartList.Columns["업체"].Visible = false;
#endif            
            this.dgvPartList.Columns["단가"].Visible = false;
            this.dgvPartList.Columns["금액"].Visible = false;
            this.dgvPartList.Columns["Note"].Visible = false;
            this.dgvPartList.Columns["공용"].Visible = false;
            this.dgvPartList.Columns["종류"].Visible = false;

            this.dgvPartList.Columns["도번"].Width = 90;
            this.dgvPartList.Columns["REV"].Width = 50;
            this.dgvPartList.Columns["모델"].Width = 70;
            this.dgvPartList.Columns["파트"].Width = 110;
            this.dgvPartList.Columns["품명"].Width = 120;
            this.dgvPartList.Columns["규격"].Width = 100;
            
            this.dgvPartList.Columns["도번"].DisplayIndex = 1;
            this.dgvPartList.Columns["REV"].DisplayIndex = 2;
            this.dgvPartList.Columns["모델"].DisplayIndex = 3;
            this.dgvPartList.Columns["파트"].DisplayIndex = 4;
            this.dgvPartList.Columns["품명"].DisplayIndex = 5;
            this.dgvPartList.Columns["규격"].DisplayIndex = 6;
            this.dgvPartList.Columns["업체"].DisplayIndex = 7;
        }

        private void DgvOrderDetailFormated()
        {
            this.dgvOrderDetail.Columns["NO"].Width = 65;
            this.dgvOrderDetail.Columns["모델"].Width = 70;
            this.dgvOrderDetail.Columns["도면"].Width = 65;
            this.dgvOrderDetail.Columns["도번"].Width = 90;
            this.dgvOrderDetail.Columns["REV"].Width = 65;
            this.dgvOrderDetail.Columns["품명"].Width = 120;
            this.dgvOrderDetail.Columns["규격"].Width = 100;
            this.dgvOrderDetail.Columns["단위"].Width = 65;
            this.dgvOrderDetail.Columns["수량"].Width = 70;
            this.dgvOrderDetail.Columns["단가"].Width = 100;
            this.dgvOrderDetail.Columns["소계"].Width = 100;
            this.dgvOrderDetail.Columns["부가세"].Width = 100;
            this.dgvOrderDetail.Columns["납기일"].Width = 120;
            this.dgvOrderDetail.Columns["비고"].Width = 200;
            this.dgvOrderDetail.Columns["KEY_ID"].Width = 1;

            this.dgvOrderDetail.Columns["NO"].ReadOnly = true;
            this.dgvOrderDetail.Columns["도면"].ReadOnly = true;
            this.dgvOrderDetail.Columns["도번"].ReadOnly = true;
            this.dgvOrderDetail.Columns["REV"].ReadOnly = true;
            this.dgvOrderDetail.Columns["소계"].ReadOnly = true;
            this.dgvOrderDetail.Columns["부가세"].ReadOnly = true;
            this.dgvOrderDetail.Columns["KEY_ID"].ReadOnly = true;

            this.dgvOrderDetail.Columns["수량"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvOrderDetail.Columns["수량"].DefaultCellStyle.Format = "#,###";
            this.dgvOrderDetail.Columns["단가"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvOrderDetail.Columns["단가"].DefaultCellStyle.Format = "#,###";
            this.dgvOrderDetail.Columns["소계"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvOrderDetail.Columns["소계"].DefaultCellStyle.Format = "#,###";
            this.dgvOrderDetail.Columns["부가세"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            this.dgvOrderDetail.Columns["부가세"].DefaultCellStyle.Format = "#,###";
        }

        private void AddInitialize()
        {
            txtOrderNumber.Text = order_number;
            btnMaker.Enabled = true;
            cmbVAT.Enabled = true;
            cmbApprover.Enabled = true;
            txtMakerMessage.Enabled = true;
        }

        private void ModifyInitialize()
        {
            txtOrderNumber.Text = order_number;
            btnCreatedOrder.Enabled = false;
            btnMaker.Enabled = true;
            cmbVAT.Enabled = true;
            cmbApprover.Enabled = true;
            txtMakerMessage.Enabled = true;

            //1. 발주번호로 검색해서 해당 항목을 로드 한다.
            DataTable dtModifySearch = new DataTable();
            SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC order_list '', '', {0} , '{1}'", maker_code, order_number), StaticData.db_connetion_string);
            query.Fill(dtModifySearch);

            if (dtModifySearch.Rows.Count == 1)
            {
                txtOrderNumber.Text = dtModifySearch.Rows[0]["ORDER_KEY"].ToString();
                txtMaker.Text = dtModifySearch.Rows[0]["NAME"].ToString();
                cmbVAT.SelectedValue = dtModifySearch.Rows[0]["VAT"].ToString();
                cmbApprover.SelectedValue = dtModifySearch.Rows[0]["APPROVER"].ToString();
                txtMakerMessage.Text = dtModifySearch.Rows[0]["MAKER_MESSAGE"].ToString();

                dtModifySearch = new DataTable();
                query = new SqlDataAdapter(String.Format("EXEC order_detail_list 'S', '', '{0}'", order_number), StaticData.db_connetion_string);
                query.Fill(dtModifySearch);

                dtPartsToOrder = dtModifySearch;
                dgvOrderDetail.DataSource = dtPartsToOrder;

                DgvOrderDetailControlAdd();
                PriceCalculartion();
            }
        }

        private void DgvOrderDetailControlAdd()
        {
            for (int i = 0; i < dgvOrderDetail.Rows.Count; i++)
            {
                if (dgvOrderDetail.Rows[i].Cells["도번"].Value.ToString() != "")
                {
                    DataGridViewButtonCell btnDrawingViewColumn = new DataGridViewButtonCell();
                    dgvOrderDetail.Rows[i].Cells[2] = btnDrawingViewColumn;
                }

                DataGridViewComboBoxCell cmbUnit = new DataGridViewComboBoxCell();
                cmbUnit.DisplayStyle = DataGridViewComboBoxDisplayStyle.DropDownButton;

                cmbUnit.Items.Add("EA");
                cmbUnit.Items.Add("BOX");
                cmbUnit.Items.Add("KG");
                cmbUnit.Items.Add("LITER");
                cmbUnit.Items.Add("SET");
                cmbUnit.Items.Add("UNDEFINED");

                dgvOrderDetail.Rows[i].Cells["단위"] = cmbUnit;
                dgvOrderDetail.Rows[i].Cells["단위"].Value = "EA";
            }
        }

        private bool CheckDuplicatePart(string drawing, string revice_cd)  //Part to Order Detail 중복체크
        {
            bool checkValue = false;
            string message = "";
            for (int i = 0; i < dtPartsToOrder.Rows.Count; i++)
            {
                if (dtPartsToOrder.Rows[i]["도번"] != null)
                {
                    if (dtPartsToOrder.Rows[i]["도번"].ToString() == drawing)
                    {
                        if (dtPartsToOrder.Rows[i]["REV"].ToString() == revice_cd)  //진도번
                            message = "중복된 품번이 포함되어 있습니다";
                        else if (revice_cd == "")    //가도번
                            message = "중복된 품번이 포함되어 있습니다.";

                        if (message.Length > 0)
                        {
                            MessageBox.Show(message, "확인요청");
                            message = "";
                            checkValue = true;

                        }
                    }
                }
            }
            return checkValue;
        }

        private void CreateOrderNumber()
        {
            DataTable result = new DataTable();
            SqlDataAdapter query = query = new SqlDataAdapter(String.Format("EXEC create_order_number"), StaticData.db_connetion_string);
            query.Fill(result);

            if (result != null && result.Rows.Count == 1)
                txtOrderNumber.Text = result.Rows[0]["CON"].ToString();
        }

        private bool CheckSaveOrderVaild()
        {
            save_err_message = "";
            bool result = false;

            if (dgvOrderDetail.Rows.Count == 0)
                save_err_message += "발주 상세 내역";

            if (dgvOrderDetail.Rows.Count > 0)
            {
                for (int i = 0; i < dgvOrderDetail.Rows.Count; i++)
                {
                    if (dgvOrderDetail.Rows[i].Cells["모델"].Value.ToString() == "")
                        save_err_message += "[모델]";

                    if (dgvOrderDetail.Rows[i].Cells["수량"].Value.ToString() == "0")
                        save_err_message += "[수량]";

                    if (save_err_message != "")
                    {
                        save_err_message += "항목 ";
                        break;
                    }   
                }
            }

            if (txtMaker.Text == "")
                save_err_message += ", \r\n거래처";

            if (cmbApprover.Text == "")
                save_err_message += ", \r\n승인자";

            if (save_err_message.Length == 0)
                result = true;
            else
                save_err_message += "(이)가 없습니다.";

            return result;
        }

        private void ColumnsSortDisable()
        {
            foreach (DataGridViewColumn i in dgvOrderDetail.Columns)
                i.SortMode = DataGridViewColumnSortMode.NotSortable;
        }

        private void IsServerDrawingFile()      //도면 유무 검색
        {
            al_Drwaing = new ArrayList();
            for (int i = 0; i < dgvPartList.Rows.Count; i++)
                CreateDrawingName(i);
            ArrayList al_result = dwg.SearchFile(al_Drwaing);
            for (int i = 0; i < dgvPartList.Rows.Count; i++)
            {
                for (int j = 0; j < al_result.Count; j++)
                {
                    if (i == int.Parse(al_result[j].ToString()))
                        dgvPartList.Rows[i].Cells["T도번"].Style.BackColor = Color.Pink;
                }
            }
        }

        private void CreateDrawingName(int dgvTotalCount) // 도면파일이름 만들기
        {
            if (dgvPartList.Rows[dgvTotalCount].Cells["眞도번"].Value.ToString().Trim() != "")
                al_Drwaing.Add(string.Format("{0}_{1}", dgvPartList.Rows[dgvTotalCount].Cells["眞도번"].Value.ToString().Trim(), dgvPartList.Rows[dgvTotalCount].Cells["REV"].Value.ToString().Trim()));
            else if (dgvPartList.Rows[dgvTotalCount].Cells["眞도번"].Value.ToString().Trim() == "" && dgvPartList.Rows[dgvTotalCount].Cells["假도번"].Value.ToString().Trim() != "")
                al_Drwaing.Add(dgvPartList.Rows[dgvTotalCount].Cells["假도번"].Value.ToString().Trim());
            else if (dgvPartList.Rows[dgvTotalCount].Cells["眞도번"].Value.ToString().Trim() == "" && dgvPartList.Rows[dgvTotalCount].Cells["假도번"].Value.ToString().Trim() == "" && dgvPartList.Rows[dgvTotalCount].Cells["T도번"].Value.ToString().Trim() != "")
            {
                if (dgvPartList.Rows[dgvTotalCount].Cells["T도번"].Value.ToString().Trim().Length == 10)
                    al_Drwaing.Add(string.Format("{0}_{1}", dgvPartList.Rows[dgvTotalCount].Cells["T도번"].Value.ToString().Trim(), dgvPartList.Rows[dgvTotalCount].Cells["REV"].Value.ToString().Trim()));
                else
                    al_Drwaing.Add(dgvPartList.Rows[dgvTotalCount].Cells["T도번"].Value.ToString().Trim());
            }
        }

        private void PriceCalculartion()
        {
            int iTotal = 0;
            int iVatTotal = 0;
            int iGrandTotal = 0;

            for (int i = 0; i < dgvOrderDetail.Rows.Count; i++)
            {
                int iUnitPrice;
                int iQty;

                if (dgvOrderDetail.Rows[i].Cells["단가"].Value == null || dgvOrderDetail.Rows[i].Cells["단가"].Value.ToString() == "")
                    iUnitPrice = 0;
                else
                    iUnitPrice = int.Parse(dgvOrderDetail.Rows[i].Cells["단가"].Value.ToString());

                if (dgvOrderDetail.Rows[i].Cells["수량"].Value == null || dgvOrderDetail.Rows[i].Cells["수량"].Value.ToString() == "")
                    iQty = 0;
                else
                    iQty = int.Parse(dgvOrderDetail.Rows[i].Cells["수량"].Value.ToString());

                if (iUnitPrice == 0 || iQty == 0)
                    continue;

                dgvOrderDetail.Rows[i].Cells["소계"].Value = iUnitPrice * iQty;

                if (cmbVAT.Text == "별도")
                    dgvOrderDetail.Rows[i].Cells["부가세"].Value = (iUnitPrice * iQty) * 0.1;
                else if (cmbVAT.Text == "포함")
                    dgvOrderDetail.Rows[i].Cells["부가세"].Value = 0;

                iTotal += int.Parse(dgvOrderDetail.Rows[i].Cells["소계"].Value.ToString());
                iVatTotal += int.Parse(dgvOrderDetail.Rows[i].Cells["부가세"].Value.ToString());
            }

            iGrandTotal = iTotal + iVatTotal;
            txtTotal.Text = string.Format("{0:#,### }", iTotal);
            txtTotalVAT.Text = string.Format("{0:#,### }", iVatTotal);
            txtGrandTotal.Text = string.Format("{0:#,### }", iGrandTotal);
        }
    }
}
