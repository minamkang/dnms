﻿namespace DrawingNumberManager
{
    partial class FormMaker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlButtom = new System.Windows.Forms.Panel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnComfirm = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnModify = new System.Windows.Forms.Button();
            this.btnInsert = new System.Windows.Forms.Button();
            this.pnlDGV = new System.Windows.Forms.Panel();
            this.dgvCompany = new System.Windows.Forms.DataGridView();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtSMaker = new System.Windows.Forms.TextBox();
            this.lblCustomer = new System.Windows.Forms.Label();
            this.txtCompanyName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.lblAddress = new System.Windows.Forms.Label();
            this.txtAccountCode = new System.Windows.Forms.TextBox();
            this.lblAccountCode = new System.Windows.Forms.Label();
            this.txtBusinessNumber = new System.Windows.Forms.TextBox();
            this.lblBusinessNumber = new System.Windows.Forms.Label();
            this.txtRepresentative = new System.Windows.Forms.TextBox();
            this.lblRepresentative = new System.Windows.Forms.Label();
            this.txtIndustryType = new System.Windows.Forms.TextBox();
            this.lblIndustryType = new System.Windows.Forms.Label();
            this.txtBusinessType = new System.Windows.Forms.TextBox();
            this.lblBusinessType = new System.Windows.Forms.Label();
            this.txtManager = new System.Windows.Forms.TextBox();
            this.lblManager = new System.Windows.Forms.Label();
            this.txtTel = new System.Windows.Forms.TextBox();
            this.lblTel = new System.Windows.Forms.Label();
            this.txtManagerMail = new System.Windows.Forms.TextBox();
            this.lblManagerMail = new System.Windows.Forms.Label();
            this.txtFax = new System.Windows.Forms.TextBox();
            this.lblFax = new System.Windows.Forms.Label();
            this.lblPaymentType = new System.Windows.Forms.Label();
            this.cmbPaymentType = new System.Windows.Forms.ComboBox();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.lblNote = new System.Windows.Forms.Label();
            this.btnMakerImport = new System.Windows.Forms.Button();
            this.pnlButtom.SuspendLayout();
            this.pnlDGV.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCompany)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlButtom
            // 
            this.pnlButtom.BackColor = System.Drawing.Color.FloralWhite;
            this.pnlButtom.Controls.Add(this.btnCancel);
            this.pnlButtom.Controls.Add(this.btnComfirm);
            this.pnlButtom.Controls.Add(this.btnDelete);
            this.pnlButtom.Controls.Add(this.btnModify);
            this.pnlButtom.Controls.Add(this.btnInsert);
            this.pnlButtom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlButtom.Location = new System.Drawing.Point(0, 550);
            this.pnlButtom.Name = "pnlButtom";
            this.pnlButtom.Size = new System.Drawing.Size(864, 72);
            this.pnlButtom.TabIndex = 0;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(743, 19);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(110, 50);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "취소";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnComfirm
            // 
            this.btnComfirm.Location = new System.Drawing.Point(627, 19);
            this.btnComfirm.Name = "btnComfirm";
            this.btnComfirm.Size = new System.Drawing.Size(110, 50);
            this.btnComfirm.TabIndex = 3;
            this.btnComfirm.Text = "확인";
            this.btnComfirm.UseVisualStyleBackColor = true;
            this.btnComfirm.Click += new System.EventHandler(this.btnComfirm_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(246, 13);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(110, 50);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.Text = "삭제";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnModify
            // 
            this.btnModify.Location = new System.Drawing.Point(130, 13);
            this.btnModify.Name = "btnModify";
            this.btnModify.Size = new System.Drawing.Size(110, 50);
            this.btnModify.TabIndex = 1;
            this.btnModify.Text = "수정";
            this.btnModify.UseVisualStyleBackColor = true;
            this.btnModify.Click += new System.EventHandler(this.btnModify_Click);
            // 
            // btnInsert
            // 
            this.btnInsert.Location = new System.Drawing.Point(14, 13);
            this.btnInsert.Name = "btnInsert";
            this.btnInsert.Size = new System.Drawing.Size(110, 50);
            this.btnInsert.TabIndex = 0;
            this.btnInsert.Text = "추가";
            this.btnInsert.UseVisualStyleBackColor = true;
            this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
            // 
            // pnlDGV
            // 
            this.pnlDGV.Controls.Add(this.dgvCompany);
            this.pnlDGV.Controls.Add(this.btnSearch);
            this.pnlDGV.Controls.Add(this.txtSMaker);
            this.pnlDGV.Controls.Add(this.lblCustomer);
            this.pnlDGV.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlDGV.Location = new System.Drawing.Point(0, 0);
            this.pnlDGV.Name = "pnlDGV";
            this.pnlDGV.Size = new System.Drawing.Size(425, 550);
            this.pnlDGV.TabIndex = 1;
            // 
            // dgvCompany
            // 
            this.dgvCompany.AllowUserToAddRows = false;
            this.dgvCompany.AllowUserToDeleteRows = false;
            this.dgvCompany.AllowUserToResizeRows = false;
            this.dgvCompany.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvCompany.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCompany.Location = new System.Drawing.Point(14, 41);
            this.dgvCompany.MultiSelect = false;
            this.dgvCompany.Name = "dgvCompany";
            this.dgvCompany.ReadOnly = true;
            this.dgvCompany.RowHeadersVisible = false;
            this.dgvCompany.RowTemplate.Height = 23;
            this.dgvCompany.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCompany.Size = new System.Drawing.Size(398, 497);
            this.dgvCompany.TabIndex = 3;
            this.dgvCompany.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCompany_RowEnter);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(350, 4);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(62, 23);
            this.btnSearch.TabIndex = 2;
            this.btnSearch.Text = "검색";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtSMaker
            // 
            this.txtSMaker.Location = new System.Drawing.Point(47, 6);
            this.txtSMaker.Name = "txtSMaker";
            this.txtSMaker.Size = new System.Drawing.Size(297, 21);
            this.txtSMaker.TabIndex = 1;
            // 
            // lblCustomer
            // 
            this.lblCustomer.AutoSize = true;
            this.lblCustomer.Location = new System.Drawing.Point(12, 9);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(29, 12);
            this.lblCustomer.TabIndex = 0;
            this.lblCustomer.Text = "업체";
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.Location = new System.Drawing.Point(553, 41);
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Size = new System.Drawing.Size(300, 21);
            this.txtCompanyName.TabIndex = 3;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(431, 44);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(45, 12);
            this.lblName.TabIndex = 2;
            this.lblName.Text = "업체명:";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(553, 68);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(300, 21);
            this.txtAddress.TabIndex = 4;
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(431, 71);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(33, 12);
            this.lblAddress.TabIndex = 4;
            this.lblAddress.Text = "주소:";
            // 
            // txtAccountCode
            // 
            this.txtAccountCode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtAccountCode.Location = new System.Drawing.Point(553, 14);
            this.txtAccountCode.Name = "txtAccountCode";
            this.txtAccountCode.Size = new System.Drawing.Size(300, 21);
            this.txtAccountCode.TabIndex = 2;
            // 
            // lblAccountCode
            // 
            this.lblAccountCode.AutoSize = true;
            this.lblAccountCode.Location = new System.Drawing.Point(431, 17);
            this.lblAccountCode.Name = "lblAccountCode";
            this.lblAccountCode.Size = new System.Drawing.Size(73, 12);
            this.lblAccountCode.TabIndex = 6;
            this.lblAccountCode.Text = "거래처 코드:";
            // 
            // txtBusinessNumber
            // 
            this.txtBusinessNumber.Location = new System.Drawing.Point(553, 95);
            this.txtBusinessNumber.Name = "txtBusinessNumber";
            this.txtBusinessNumber.Size = new System.Drawing.Size(300, 21);
            this.txtBusinessNumber.TabIndex = 5;
            // 
            // lblBusinessNumber
            // 
            this.lblBusinessNumber.AutoSize = true;
            this.lblBusinessNumber.Location = new System.Drawing.Point(431, 98);
            this.lblBusinessNumber.Name = "lblBusinessNumber";
            this.lblBusinessNumber.Size = new System.Drawing.Size(73, 12);
            this.lblBusinessNumber.TabIndex = 8;
            this.lblBusinessNumber.Text = "사업자 번호:";
            // 
            // txtRepresentative
            // 
            this.txtRepresentative.Location = new System.Drawing.Point(553, 122);
            this.txtRepresentative.Name = "txtRepresentative";
            this.txtRepresentative.Size = new System.Drawing.Size(300, 21);
            this.txtRepresentative.TabIndex = 6;
            // 
            // lblRepresentative
            // 
            this.lblRepresentative.AutoSize = true;
            this.lblRepresentative.Location = new System.Drawing.Point(431, 125);
            this.lblRepresentative.Name = "lblRepresentative";
            this.lblRepresentative.Size = new System.Drawing.Size(45, 12);
            this.lblRepresentative.TabIndex = 10;
            this.lblRepresentative.Text = "대표자:";
            // 
            // txtIndustryType
            // 
            this.txtIndustryType.Location = new System.Drawing.Point(553, 149);
            this.txtIndustryType.Name = "txtIndustryType";
            this.txtIndustryType.Size = new System.Drawing.Size(300, 21);
            this.txtIndustryType.TabIndex = 7;
            // 
            // lblIndustryType
            // 
            this.lblIndustryType.AutoSize = true;
            this.lblIndustryType.Location = new System.Drawing.Point(431, 152);
            this.lblIndustryType.Name = "lblIndustryType";
            this.lblIndustryType.Size = new System.Drawing.Size(33, 12);
            this.lblIndustryType.TabIndex = 12;
            this.lblIndustryType.Text = "업태:";
            // 
            // txtBusinessType
            // 
            this.txtBusinessType.Location = new System.Drawing.Point(553, 176);
            this.txtBusinessType.Name = "txtBusinessType";
            this.txtBusinessType.Size = new System.Drawing.Size(300, 21);
            this.txtBusinessType.TabIndex = 8;
            // 
            // lblBusinessType
            // 
            this.lblBusinessType.AutoSize = true;
            this.lblBusinessType.Location = new System.Drawing.Point(431, 179);
            this.lblBusinessType.Name = "lblBusinessType";
            this.lblBusinessType.Size = new System.Drawing.Size(33, 12);
            this.lblBusinessType.TabIndex = 14;
            this.lblBusinessType.Text = "종목:";
            // 
            // txtManager
            // 
            this.txtManager.Location = new System.Drawing.Point(553, 203);
            this.txtManager.Name = "txtManager";
            this.txtManager.Size = new System.Drawing.Size(300, 21);
            this.txtManager.TabIndex = 9;
            // 
            // lblManager
            // 
            this.lblManager.AutoSize = true;
            this.lblManager.Location = new System.Drawing.Point(431, 206);
            this.lblManager.Name = "lblManager";
            this.lblManager.Size = new System.Drawing.Size(45, 12);
            this.lblManager.TabIndex = 16;
            this.lblManager.Text = "담당자:";
            // 
            // txtTel
            // 
            this.txtTel.Location = new System.Drawing.Point(553, 230);
            this.txtTel.Name = "txtTel";
            this.txtTel.Size = new System.Drawing.Size(300, 21);
            this.txtTel.TabIndex = 10;
            // 
            // lblTel
            // 
            this.lblTel.AutoSize = true;
            this.lblTel.Location = new System.Drawing.Point(431, 233);
            this.lblTel.Name = "lblTel";
            this.lblTel.Size = new System.Drawing.Size(33, 12);
            this.lblTel.TabIndex = 18;
            this.lblTel.Text = "전화:";
            // 
            // txtManagerMail
            // 
            this.txtManagerMail.Location = new System.Drawing.Point(553, 257);
            this.txtManagerMail.Name = "txtManagerMail";
            this.txtManagerMail.Size = new System.Drawing.Size(300, 21);
            this.txtManagerMail.TabIndex = 11;
            // 
            // lblManagerMail
            // 
            this.lblManagerMail.AutoSize = true;
            this.lblManagerMail.Location = new System.Drawing.Point(431, 260);
            this.lblManagerMail.Name = "lblManagerMail";
            this.lblManagerMail.Size = new System.Drawing.Size(33, 12);
            this.lblManagerMail.TabIndex = 20;
            this.lblManagerMail.Text = "메일:";
            // 
            // txtFax
            // 
            this.txtFax.Location = new System.Drawing.Point(553, 284);
            this.txtFax.Name = "txtFax";
            this.txtFax.Size = new System.Drawing.Size(300, 21);
            this.txtFax.TabIndex = 12;
            // 
            // lblFax
            // 
            this.lblFax.AutoSize = true;
            this.lblFax.Location = new System.Drawing.Point(431, 287);
            this.lblFax.Name = "lblFax";
            this.lblFax.Size = new System.Drawing.Size(33, 12);
            this.lblFax.TabIndex = 22;
            this.lblFax.Text = "팩스:";
            // 
            // lblPaymentType
            // 
            this.lblPaymentType.AutoSize = true;
            this.lblPaymentType.Location = new System.Drawing.Point(431, 314);
            this.lblPaymentType.Name = "lblPaymentType";
            this.lblPaymentType.Size = new System.Drawing.Size(57, 12);
            this.lblPaymentType.TabIndex = 24;
            this.lblPaymentType.Text = "결제구분:";
            // 
            // cmbPaymentType
            // 
            this.cmbPaymentType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPaymentType.FormattingEnabled = true;
            this.cmbPaymentType.Location = new System.Drawing.Point(553, 311);
            this.cmbPaymentType.Name = "cmbPaymentType";
            this.cmbPaymentType.Size = new System.Drawing.Size(300, 20);
            this.cmbPaymentType.TabIndex = 13;
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(553, 337);
            this.txtNote.Multiline = true;
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(300, 108);
            this.txtNote.TabIndex = 14;
            // 
            // lblNote
            // 
            this.lblNote.AutoSize = true;
            this.lblNote.Location = new System.Drawing.Point(431, 340);
            this.lblNote.Name = "lblNote";
            this.lblNote.Size = new System.Drawing.Size(35, 12);
            this.lblNote.TabIndex = 26;
            this.lblNote.Text = "Note:";
            // 
            // btnMakerImport
            // 
            this.btnMakerImport.Location = new System.Drawing.Point(787, 451);
            this.btnMakerImport.Name = "btnMakerImport";
            this.btnMakerImport.Size = new System.Drawing.Size(66, 50);
            this.btnMakerImport.TabIndex = 5;
            this.btnMakerImport.Text = "Import";
            this.btnMakerImport.UseVisualStyleBackColor = true;
            this.btnMakerImport.Visible = false;
            this.btnMakerImport.Click += new System.EventHandler(this.btnMakerImport_Click);
            // 
            // FormMaker
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FloralWhite;
            this.ClientSize = new System.Drawing.Size(864, 622);
            this.Controls.Add(this.btnMakerImport);
            this.Controls.Add(this.txtNote);
            this.Controls.Add(this.lblNote);
            this.Controls.Add(this.cmbPaymentType);
            this.Controls.Add(this.lblPaymentType);
            this.Controls.Add(this.txtFax);
            this.Controls.Add(this.lblFax);
            this.Controls.Add(this.txtManagerMail);
            this.Controls.Add(this.lblManagerMail);
            this.Controls.Add(this.txtTel);
            this.Controls.Add(this.lblTel);
            this.Controls.Add(this.txtManager);
            this.Controls.Add(this.lblManager);
            this.Controls.Add(this.txtBusinessType);
            this.Controls.Add(this.lblBusinessType);
            this.Controls.Add(this.txtIndustryType);
            this.Controls.Add(this.lblIndustryType);
            this.Controls.Add(this.txtRepresentative);
            this.Controls.Add(this.lblRepresentative);
            this.Controls.Add(this.txtBusinessNumber);
            this.Controls.Add(this.lblBusinessNumber);
            this.Controls.Add(this.txtAccountCode);
            this.Controls.Add(this.lblAccountCode);
            this.Controls.Add(this.txtAddress);
            this.Controls.Add(this.lblAddress);
            this.Controls.Add(this.txtCompanyName);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.pnlDGV);
            this.Controls.Add(this.pnlButtom);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "FormMaker";
            this.Text = "Maker";
            this.Load += new System.EventHandler(this.FormManagementCompany_Load);
            this.pnlButtom.ResumeLayout(false);
            this.pnlDGV.ResumeLayout(false);
            this.pnlDGV.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCompany)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlButtom;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnComfirm;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnModify;
        private System.Windows.Forms.Button btnInsert;
        private System.Windows.Forms.Panel pnlDGV;
        private System.Windows.Forms.DataGridView dgvCompany;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtSMaker;
        private System.Windows.Forms.Label lblCustomer;
        private System.Windows.Forms.TextBox txtCompanyName;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.TextBox txtAccountCode;
        private System.Windows.Forms.Label lblAccountCode;
        private System.Windows.Forms.TextBox txtBusinessNumber;
        private System.Windows.Forms.Label lblBusinessNumber;
        private System.Windows.Forms.TextBox txtRepresentative;
        private System.Windows.Forms.Label lblRepresentative;
        private System.Windows.Forms.TextBox txtIndustryType;
        private System.Windows.Forms.Label lblIndustryType;
        private System.Windows.Forms.TextBox txtBusinessType;
        private System.Windows.Forms.Label lblBusinessType;
        private System.Windows.Forms.TextBox txtManager;
        private System.Windows.Forms.Label lblManager;
        private System.Windows.Forms.TextBox txtTel;
        private System.Windows.Forms.Label lblTel;
        private System.Windows.Forms.TextBox txtManagerMail;
        private System.Windows.Forms.Label lblManagerMail;
        private System.Windows.Forms.TextBox txtFax;
        private System.Windows.Forms.Label lblFax;
        private System.Windows.Forms.Label lblPaymentType;
        private System.Windows.Forms.ComboBox cmbPaymentType;
        private System.Windows.Forms.TextBox txtNote;
        private System.Windows.Forms.Label lblNote;
        private System.Windows.Forms.Button btnMakerImport;
    }
}