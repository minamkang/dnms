﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;

namespace DrawingNumberManager
{
    public partial class FormPublishRDN : Form
    {
        private string revise_drawing_number;
        private string input_drawing_number;
        private bool whether_Last_Revision_Code;
        private int current_user_key = 0;
        private DataTable detail_content = null;
        private int maker_code;
        
        public FormPublishRDN(int UserKey, string strReviseDN, string strInputDN, bool whetherLastRevisionCode, DataTable dtDetailContent)
        {
            revise_drawing_number = strReviseDN;
            input_drawing_number = strInputDN;
            whether_Last_Revision_Code = whetherLastRevisionCode;
			current_user_key = UserKey;
            detail_content = dtDetailContent;

            InitializeComponent();
        }

        private void FormReviseDrawingNumber_Load(object sender, EventArgs e)
        {
            this.Text = Const.FORM_REVISE_DRAWING_NUMBER;
            this.lblByte.Text = string.Format("0/{0}Byte", Const.DRAWING_NO_NOTE_MAX_LENGTH);
            this.lblWarringContent.Text = string.Format("Note내용은 {0}Byte이상 작성을 해야 합니다.", Const.DRAWING_NO_NOTE_MIN_LENGTH);

            if (whether_Last_Revision_Code == false)
            {
                string strMessage = "";
                strMessage += "입력하신 도면은 최신도면이 아닙니다.\r";
                strMessage += string.Format("최신 도면번호는  [{0}]입니다 \r", revise_drawing_number);
                strMessage += "그래도 갱신을 하려면 '예(Y)', 그렇지 않으면 '아니오(N)' 버튼을 누르세요.";

                if (MessageBox.Show(strMessage, "Warning", MessageBoxButtons.YesNo) == DialogResult.No)
                    this.Close();
            }

            lblInputDrawingNumber.Text = "입력도면:" + input_drawing_number;
            this.Height = 520;

            for (short i = 1; i < 3; i++)
                BindCmbBox(i);

            //기존 정보 Read
            if (CheckDBNull(detail_content.Rows[0]["PROJECT_CODE"]).Length == 5)
            {
                cmbModel.SelectedValue = detail_content.Rows[0]["PROJECT_CODE"].ToString().Substring(0, 3);
                cmbPart.SelectedValue = detail_content.Rows[0]["PROJECT_CODE"].ToString().Substring(3, 2);
            }
            else
            {
                cmbModel.SelectedValue = "";
                cmbPart.SelectedValue = "";
            }
            txtType.Text = CheckDBNull(detail_content.Rows[0]["TYPE"]);
            txtPartName.Text = CheckDBNull(detail_content.Rows[0]["PART_NAME"]);
            txtSpecfication.Text = CheckDBNull(detail_content.Rows[0]["SPECFICATION"]);
            txtMaterial.Text = CheckDBNull(detail_content.Rows[0]["MATERIAL"]);
            txtAfterProcess.Text = CheckDBNull(detail_content.Rows[0]["APROCESS"]);
            txtQty.Text = CheckDBNull(detail_content.Rows[0]["QUANTITY"]);
            txtMaker.Text = CheckDBNull(detail_content.Rows[0]["NAME"]);
            txtUnitPrice.Text = CheckDBNull(detail_content.Rows[0]["UNIT_PRICE"]);
            txtPrice.Text = CheckDBNull(detail_content.Rows[0]["PRICE"]);
        }

        private void btnRevise_Click(object sender, EventArgs e)
        {
            if(cmbModel.SelectedValue.ToString() == "" || cmbPart.SelectedValue.ToString() == "")
            {
                MessageBox.Show("Model, Part는 필수 항목입니다.", "확인");
                return;
            }

            ArrayList alDetail = new ArrayList();
            alDetail.Add(cmbModel.SelectedValue.ToString() + cmbPart.SelectedValue.ToString());
            alDetail.Add(txtType.Text);
            alDetail.Add(txtPartName.Text);
            alDetail.Add(txtSpecfication.Text);
            alDetail.Add(txtMaterial.Text);
            alDetail.Add(txtAfterProcess.Text);
            if (txtQty.Text == "")
                txtQty.Text = "0";
            alDetail.Add(txtQty.Text.Replace(",", ""));
            
            if(txtMaker.Text == "")
                alDetail.Add(0);
            else
                alDetail.Add(maker_code);

            if (txtUnitPrice.Text == "")
                txtUnitPrice.Text = "0";
            alDetail.Add(txtUnitPrice.Text.Replace(",", ""));
            
            if (txtPrice.Text == "")
                txtPrice.Text = "0";
            alDetail.Add(txtPrice.Text.Replace(",", ""));

            TemporaryDrawingNumber currentTDN = new TemporaryDrawingNumber(revise_drawing_number, txtNote.Text, current_user_key, alDetail);
            string revisionDrawingNumber = currentTDN.ToString();

            if (revisionDrawingNumber.Length == Const.DRAWING_NO_LENGTH)
            {
                ReviseDrawingNumberDisplayPanel.Location = new Point(1, 9);
                ReviseDrawingNumberDisplayPanel.Visible = true;
                txtReviseDrawNumber.Text = revisionDrawingNumber;
                txtNote.Text = "";

                this.Height = 192;
                initializeDetailContent();

                for (short i = 1; i < 3; i++)
                    BindCmbBox(i);

                txtNote.Focus();
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            for (short i = 1; i < 3; i++)
                BindCmbBox(i);

            initializeDetailContent();

            txtNote.Focus();
        }

        private void btnMaker_Click(object sender, EventArgs e)
        {
            FormMaker frmCompany = new FormMaker(false);
            if (frmCompany.ShowDialog() == DialogResult.OK)
            {
                txtMaker.Text = frmCompany.MakerName;
                maker_code = frmCompany.MakerCode;
            }
        }

        private void btnMain_Click(object sender, EventArgs e)
        {
            this.Close();
            DialogResult = DialogResult.OK;
        }

		bool bUtf8; //한글 3byte, 영문 숫자 1
        private void txtNote_TextChanged(object sender, EventArgs e)
        {
			//2014년 01월 21일 - 강성환 추가
			if (this.txtNote.Text.Contains("'") || this.txtNote.Text.Contains("%"))
			{
				MessageBox.Show("문자 [', %]는 사용 할수 없습니다.", "Warning");
				this.txtNote.Text = this.txtNote.Text.Replace("'", "");
				this.txtNote.Text = this.txtNote.Text.Replace("%", "");

				if (txtNote.Text.Length > 0)
					txtNote.Select(txtNote.Text.Length - 1, 0); //TEXTBOX 제일마지막 쓴 포인트로 이동
				return;
			}

			int length = Encoding.UTF8.GetBytes(this.txtNote.Text).Length;
			if (length > Const.DRAWING_NO_NOTE_MAX_LENGTH)
			{
				this.txtNote.Text = this.txtNote.Text.Substring(0, this.txtNote.TextLength - (bUtf8 ? 1 : 2));
				bUtf8 = false;
				this.txtNote.Select(this.txtNote.TextLength, 0);
				return;
			}
			if (length >= Const.DRAWING_NO_NOTE_MIN_LENGTH)
			{
				lblByte.ForeColor = Color.Blue;
				btnRevise.Enabled = true;
			}
			else
			{
				lblByte.ForeColor = Color.OrangeRed;
				btnRevise.Enabled = false;
			}
			length = Encoding.UTF8.GetBytes(this.txtNote.Text).Length;
			this.lblByte.Text = string.Format("{0}/{1} byte", length, Const.DRAWING_NO_NOTE_MAX_LENGTH);
        }

        private void txtReviseDrawNumber_TextChanged(object sender, EventArgs e)
        {
            ((TextBox)sender).SelectAll();
            ((TextBox)sender).Copy();
        }

        private void txtQty_TextChanged(object sender, EventArgs e)
        {
            if (txtQty.Text != "")
            {
                txtQty.Text = String.Format("{0:#,###}", int.Parse(txtQty.Text.Replace(",", "")));   //swkang 20201210
                txtQty.SelectionStart = txtQty.TextLength;
                txtQty.SelectionLength = 0;

                CalculationPrice();
            }
        }

        private void txtUnitPrice_TextChanged(object sender, EventArgs e)
        {
            if (txtUnitPrice.Text != "")
            {
                txtUnitPrice.Text = String.Format("{0:#,###}", int.Parse(txtUnitPrice.Text.Replace(",", "")));   //swkang 20201210
                txtUnitPrice.SelectionStart = txtUnitPrice.TextLength;
                txtUnitPrice.SelectionLength = 0;

                CalculationPrice();
            }
        }

        private void BindCmbBox(short sType)
        {
            DataTable result = new DataTable();
            SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC code_info {0}", sType), StaticData.db_connetion_string);
            query.Fill(result);

            if (result != null)
            {
                if (sType == 1)
                {
                    result.Rows.Add("");
                    cmbModel.ValueMember = "CODE_NAME";
                    cmbModel.DisplayMember = "CODE_VALUE";
                    cmbModel.DataSource = result;
                    cmbModel.SelectedValue = "";
                }
                else if (sType == 2)
                {
                    result.Rows.Add("");
                    cmbPart.ValueMember = "CODE_NAME";
                    cmbPart.DisplayMember = "CODE_VALUE";
                    cmbPart.DataSource = result;
                    cmbPart.SelectedValue = "";
                }
            }
        }

        private string CheckDBNull(object oCheckData)
        {
            string result = "";
            if (oCheckData != DBNull.Value)
                result = oCheckData.ToString();
           
            return result;
        }

        private void CalculationPrice()
        {
            if (txtQty.Text == "" || txtUnitPrice.Text == "")
                return;

            try
            {
                int qty = int.Parse(txtQty.Text.Replace(",", ""));
                int unit_price = int.Parse(txtUnitPrice.Text.Replace(",", ""));
                int price = unit_price * qty;

#if false
                txtPrice.Text = price.ToString();
#else
                txtPrice.Text = string.Format("{0:#,###}", price); //swkang 20201210
#endif
            }
            catch (Exception ex)
            {
                txtQty.Text = "0";
                txtUnitPrice.Text = "0";
                txtPrice.Text = "0";
                ex.ToString();
            }
        }

        private void initializeDetailContent()
        {
            cmbModel.SelectedValue = "";
            cmbPart.SelectedValue = "";
            txtType.Text = "";
            txtPartName.Text = "";
            txtSpecfication.Text = "";
            txtMaterial.Text = "";
            txtAfterProcess.Text = "";
            txtQty.Text = "0";
            txtMaker.Text = "";
            txtUnitPrice.Text = "0";
            txtPrice.Text = "0";
        }

    }
}
