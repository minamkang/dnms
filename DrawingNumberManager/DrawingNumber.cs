using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DrawingNumberManager
{
    public abstract class DrawingNumber
    {
        public UInt16 serial_number;
        public byte created_year;
        public DrawingCategory department_code;
        public char revision_code;
        public string strOldDrawingNumber;

        public bool Issue()
        {
            return false;
        }

        new public virtual string ToString()
        {
            return "";
        }
    }
}
