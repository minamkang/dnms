﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DrawingNumberManager
{
	public partial class FormModifyPassword : Form
	{
		User user = null;
		String ReslutPassword = null;

		public FormModifyPassword()
		{
			InitializeComponent();
		}

		private void initialize()
		{
			this.Text = Const.FORM_MODIFY_PASSWORD;
			this.txtBeforePassword.MaxLength = 10;
			this.txtModifyPassword.MaxLength = 10;
			this.txtComfirmPassword.MaxLength = 10;
			this.txtBeforePassword.Focus();
		}

		private void FormModifyPassword_Load(object sender, EventArgs e)
		{
			initialize();
		}

		private void btnOK_Click(object sender, EventArgs e)
		{
			try
			{
				bool bCheckedInvaild = CheckInvaild();
				if (bCheckedInvaild == true)
				{
					bool bModifyPW = ModifyPassword(ReslutPassword);
					if (bModifyPW == true)
					{
						MessageBox.Show("Password가 변경 되었습니다.", "확인");
						SaveIni();
						this.Close();
					}
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private bool CheckInvaild()
		{
			if (txtBeforePassword.Text.Length == 0)
			{
				MessageBox.Show("현재 비밀번호가 비어있습니다.", "Warning");
				txtBeforePassword.Focus();
				return false;
			}

			if (txtModifyPassword.Text.Length == 0)
			{
				MessageBox.Show("새 비밀번호가 비어있습니다.", "Warning");
				txtModifyPassword.Focus();
				return false;
			}

			if (txtComfirmPassword.Text.Length == 0)
			{
				MessageBox.Show("새 비밀번호 확인이 비어있습니다.", "Warning");
				txtComfirmPassword.Focus();
				return false;
			}

			if (txtBeforePassword.Text.Length > 0)
			{
				String currentPassword = MakePassword(txtBeforePassword.Text);
				user = User.Login(StaticData.user_id, currentPassword);
				if (user == null)
				{
					MessageBox.Show("현재 비밀번호가 비밀번호와 일치하지 않습니다.", "Warning");
					txtBeforePassword.Text = "";
					txtComfirmPassword.Text = "";
					txtModifyPassword.Text = "";
					return false;
				}
			}

			if (txtBeforePassword.Text == txtModifyPassword.Text)
			{
				MessageBox.Show("현재 비밀번호와 새 비밀번호에 비밀번호가 동일합니다.", "Warning");
				txtModifyPassword.Text = "";
				if (txtComfirmPassword.Text.Length > 0)
					txtComfirmPassword.Text = "";

				txtModifyPassword.Focus();
				return false;
			}

			if (txtModifyPassword.Text != txtComfirmPassword.Text)
			{
				MessageBox.Show("새 비밀번호와 새 비밀번호 확인에 비밀번호가 동일하지 않습니다.", "Warning");
				txtComfirmPassword.Text = "";
				txtComfirmPassword.Focus();
				return false;
			}

			ReslutPassword = MakePassword(txtComfirmPassword.Text);
			return true;
		}

		private String MakePassword(string password)
		{
			String crc32Password = String.Empty;
			Crc32 crc32 = new Crc32();
			byte[] bPassword = Encoding.UTF8.GetBytes(password);

			foreach (byte b in crc32.ComputeHash(bPassword))
				crc32Password += b.ToString("x2").ToUpper();

			return crc32Password;
		}

		private bool ModifyPassword(string modifyPassword)
		{
			User.ModifyPassword(user.ID, modifyPassword);
			return true;
		}

		Ini clsIni = new Ini();
		private void SaveIni()
		{
            StaticData.user_password = ReslutPassword;
			clsIni.SaveIni();
		}

	}
}
