﻿namespace DrawingNumberManager
{
    partial class FormODNHistory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblName = new System.Windows.Forms.Label();
            this.cmbUserID = new System.Windows.Forms.ComboBox();
            this.dgvODNHistory = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiSendODN = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSaveNote = new System.Windows.Forms.Button();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.lblNote = new System.Windows.Forms.Label();
            this.btnExcel = new System.Windows.Forms.Button();
            this.lblModel = new System.Windows.Forms.Label();
            this.cmbModel = new System.Windows.Forms.ComboBox();
            this.pnlDataGrideView = new System.Windows.Forms.Panel();
            this.btnSearch = new System.Windows.Forms.Button();
            this.pnlSearch = new System.Windows.Forms.Panel();
            this.gbCondition = new System.Windows.Forms.GroupBox();
            this.txtSerialNumber = new System.Windows.Forms.TextBox();
            this.lblSerialNumber = new System.Windows.Forms.Label();
            this.lblProcess = new System.Windows.Forms.Label();
            this.cmbProcess = new System.Windows.Forms.ComboBox();
            this.lblPart = new System.Windows.Forms.Label();
            this.cmbPart = new System.Windows.Forms.ComboBox();
            this.gbBasics = new System.Windows.Forms.GroupBox();
            this.lblOldDrawingNumber = new System.Windows.Forms.Label();
            this.txtOldDrawingNumber = new System.Windows.Forms.TextBox();
            this.btn1Month = new System.Windows.Forms.Button();
            this.btn1Year = new System.Windows.Forms.Button();
            this.dtpickerStartDate = new System.Windows.Forms.DateTimePicker();
            this.btn9Month = new System.Windows.Forms.Button();
            this.lblStartDate = new System.Windows.Forms.Label();
            this.btn6Month = new System.Windows.Forms.Button();
            this.dtpickerEndDate = new System.Windows.Forms.DateTimePicker();
            this.btn3Month = new System.Windows.Forms.Button();
            this.lblEndDate = new System.Windows.Forms.Label();
            this.lblFrom = new System.Windows.Forms.Label();
            this.btn15Day = new System.Windows.Forms.Button();
            this.btnNowDay = new System.Windows.Forms.Button();
            this.btn1Week = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvODNHistory)).BeginInit();
            this.contextMenuStrip.SuspendLayout();
            this.pnlDataGrideView.SuspendLayout();
            this.pnlSearch.SuspendLayout();
            this.gbCondition.SuspendLayout();
            this.gbBasics.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(5, 66);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(39, 12);
            this.lblName.TabIndex = 29;
            this.lblName.Text = "Name";
            // 
            // cmbUserID
            // 
            this.cmbUserID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUserID.FormattingEnabled = true;
            this.cmbUserID.Location = new System.Drawing.Point(7, 81);
            this.cmbUserID.Name = "cmbUserID";
            this.cmbUserID.Size = new System.Drawing.Size(121, 20);
            this.cmbUserID.TabIndex = 28;
            this.cmbUserID.SelectedValueChanged += new System.EventHandler(this.cmbUserID_SelectedValueChanged);
            // 
            // dgvODNHistory
            // 
            this.dgvODNHistory.AllowUserToAddRows = false;
            this.dgvODNHistory.AllowUserToDeleteRows = false;
            this.dgvODNHistory.AllowUserToResizeRows = false;
            this.dgvODNHistory.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvODNHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvODNHistory.ContextMenuStrip = this.contextMenuStrip;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("굴림체", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvODNHistory.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvODNHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvODNHistory.Location = new System.Drawing.Point(0, 0);
            this.dgvODNHistory.MultiSelect = false;
            this.dgvODNHistory.Name = "dgvODNHistory";
            this.dgvODNHistory.RowHeadersVisible = false;
            this.dgvODNHistory.RowTemplate.Height = 23;
            this.dgvODNHistory.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvODNHistory.Size = new System.Drawing.Size(1117, 462);
            this.dgvODNHistory.TabIndex = 0;
            this.dgvODNHistory.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvODNHistory_CellContentClick);
            this.dgvODNHistory.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvODNHistory_CellEndEdit);
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiSendODN});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(133, 26);
            // 
            // tsmiSendODN
            // 
            this.tsmiSendODN.Name = "tsmiSendODN";
            this.tsmiSendODN.Size = new System.Drawing.Size(132, 22);
            this.tsmiSendODN.Text = "Send ODN";
            this.tsmiSendODN.Click += new System.EventHandler(this.tsmiSendODN_Click);
            // 
            // btnSaveNote
            // 
            this.btnSaveNote.Location = new System.Drawing.Point(944, 17);
            this.btnSaveNote.Name = "btnSaveNote";
            this.btnSaveNote.Size = new System.Drawing.Size(80, 49);
            this.btnSaveNote.TabIndex = 26;
            this.btnSaveNote.Text = "Save Note";
            this.btnSaveNote.UseVisualStyleBackColor = true;
            this.btnSaveNote.Click += new System.EventHandler(this.btnSaveNote_Click);
            // 
            // txtNote
            // 
            this.txtNote.Location = new System.Drawing.Point(141, 81);
            this.txtNote.MaxLength = 500;
            this.txtNote.Multiline = true;
            this.txtNote.Name = "txtNote";
            this.txtNote.Size = new System.Drawing.Size(279, 20);
            this.txtNote.TabIndex = 25;
            // 
            // lblNote
            // 
            this.lblNote.AutoSize = true;
            this.lblNote.Location = new System.Drawing.Point(139, 66);
            this.lblNote.Name = "lblNote";
            this.lblNote.Size = new System.Drawing.Size(31, 12);
            this.lblNote.TabIndex = 24;
            this.lblNote.Text = "Note";
            // 
            // btnExcel
            // 
            this.btnExcel.Location = new System.Drawing.Point(1030, 17);
            this.btnExcel.Name = "btnExcel";
            this.btnExcel.Size = new System.Drawing.Size(80, 49);
            this.btnExcel.TabIndex = 21;
            this.btnExcel.Text = "Excel";
            this.btnExcel.UseVisualStyleBackColor = true;
            this.btnExcel.Click += new System.EventHandler(this.btnExcel_Click);
            // 
            // lblModel
            // 
            this.lblModel.AutoSize = true;
            this.lblModel.Location = new System.Drawing.Point(5, 21);
            this.lblModel.Name = "lblModel";
            this.lblModel.Size = new System.Drawing.Size(40, 12);
            this.lblModel.TabIndex = 20;
            this.lblModel.Text = "Model";
            // 
            // cmbModel
            // 
            this.cmbModel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbModel.FormattingEnabled = true;
            this.cmbModel.Location = new System.Drawing.Point(7, 36);
            this.cmbModel.Name = "cmbModel";
            this.cmbModel.Size = new System.Drawing.Size(92, 20);
            this.cmbModel.TabIndex = 3;
            this.cmbModel.SelectedValueChanged += new System.EventHandler(this.cmbModel_SelectedValueChanged);
            // 
            // pnlDataGrideView
            // 
            this.pnlDataGrideView.Controls.Add(this.dgvODNHistory);
            this.pnlDataGrideView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDataGrideView.Location = new System.Drawing.Point(0, 166);
            this.pnlDataGrideView.Name = "pnlDataGrideView";
            this.pnlDataGrideView.Size = new System.Drawing.Size(1117, 462);
            this.pnlDataGrideView.TabIndex = 10;
            this.pnlDataGrideView.TabStop = true;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(858, 17);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(80, 49);
            this.btnSearch.TabIndex = 4;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // pnlSearch
            // 
            this.pnlSearch.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlSearch.Controls.Add(this.gbCondition);
            this.pnlSearch.Controls.Add(this.gbBasics);
            this.pnlSearch.Controls.Add(this.btnSaveNote);
            this.pnlSearch.Controls.Add(this.btnExcel);
            this.pnlSearch.Controls.Add(this.btnSearch);
            this.pnlSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSearch.Location = new System.Drawing.Point(0, 0);
            this.pnlSearch.Name = "pnlSearch";
            this.pnlSearch.Size = new System.Drawing.Size(1117, 166);
            this.pnlSearch.TabIndex = 9;
            // 
            // gbCondition
            // 
            this.gbCondition.Controls.Add(this.txtSerialNumber);
            this.gbCondition.Controls.Add(this.lblSerialNumber);
            this.gbCondition.Controls.Add(this.cmbUserID);
            this.gbCondition.Controls.Add(this.cmbModel);
            this.gbCondition.Controls.Add(this.lblProcess);
            this.gbCondition.Controls.Add(this.lblNote);
            this.gbCondition.Controls.Add(this.txtNote);
            this.gbCondition.Controls.Add(this.lblModel);
            this.gbCondition.Controls.Add(this.cmbProcess);
            this.gbCondition.Controls.Add(this.lblName);
            this.gbCondition.Controls.Add(this.lblPart);
            this.gbCondition.Controls.Add(this.cmbPart);
            this.gbCondition.Location = new System.Drawing.Point(423, 10);
            this.gbCondition.Name = "gbCondition";
            this.gbCondition.Size = new System.Drawing.Size(426, 148);
            this.gbCondition.TabIndex = 48;
            this.gbCondition.TabStop = false;
            this.gbCondition.Text = "조건검색";
            // 
            // txtSerialNumber
            // 
            this.txtSerialNumber.Location = new System.Drawing.Point(311, 36);
            this.txtSerialNumber.MaxLength = 3;
            this.txtSerialNumber.Multiline = true;
            this.txtSerialNumber.Name = "txtSerialNumber";
            this.txtSerialNumber.Size = new System.Drawing.Size(84, 20);
            this.txtSerialNumber.TabIndex = 35;
            // 
            // lblSerialNumber
            // 
            this.lblSerialNumber.AutoSize = true;
            this.lblSerialNumber.Location = new System.Drawing.Point(309, 21);
            this.lblSerialNumber.Name = "lblSerialNumber";
            this.lblSerialNumber.Size = new System.Drawing.Size(86, 12);
            this.lblSerialNumber.TabIndex = 34;
            this.lblSerialNumber.Text = "Serial Number";
            // 
            // lblProcess
            // 
            this.lblProcess.AutoSize = true;
            this.lblProcess.Location = new System.Drawing.Point(240, 21);
            this.lblProcess.Name = "lblProcess";
            this.lblProcess.Size = new System.Drawing.Size(52, 12);
            this.lblProcess.TabIndex = 33;
            this.lblProcess.Text = "Process";
            // 
            // cmbProcess
            // 
            this.cmbProcess.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProcess.FormattingEnabled = true;
            this.cmbProcess.Location = new System.Drawing.Point(242, 36);
            this.cmbProcess.Name = "cmbProcess";
            this.cmbProcess.Size = new System.Drawing.Size(63, 20);
            this.cmbProcess.TabIndex = 32;
            this.cmbProcess.SelectedValueChanged += new System.EventHandler(this.cmbProcess_SelectedValueChanged);
            // 
            // lblPart
            // 
            this.lblPart.AutoSize = true;
            this.lblPart.Location = new System.Drawing.Point(103, 21);
            this.lblPart.Name = "lblPart";
            this.lblPart.Size = new System.Drawing.Size(27, 12);
            this.lblPart.TabIndex = 31;
            this.lblPart.Text = "Part";
            // 
            // cmbPart
            // 
            this.cmbPart.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPart.FormattingEnabled = true;
            this.cmbPart.Location = new System.Drawing.Point(105, 36);
            this.cmbPart.Name = "cmbPart";
            this.cmbPart.Size = new System.Drawing.Size(131, 20);
            this.cmbPart.TabIndex = 30;
            this.cmbPart.SelectedValueChanged += new System.EventHandler(this.cmbPart_SelectedValueChanged);
            // 
            // gbBasics
            // 
            this.gbBasics.Controls.Add(this.lblOldDrawingNumber);
            this.gbBasics.Controls.Add(this.txtOldDrawingNumber);
            this.gbBasics.Controls.Add(this.btn1Month);
            this.gbBasics.Controls.Add(this.btn1Year);
            this.gbBasics.Controls.Add(this.dtpickerStartDate);
            this.gbBasics.Controls.Add(this.btn9Month);
            this.gbBasics.Controls.Add(this.lblStartDate);
            this.gbBasics.Controls.Add(this.btn6Month);
            this.gbBasics.Controls.Add(this.dtpickerEndDate);
            this.gbBasics.Controls.Add(this.btn3Month);
            this.gbBasics.Controls.Add(this.lblEndDate);
            this.gbBasics.Controls.Add(this.lblFrom);
            this.gbBasics.Controls.Add(this.btn15Day);
            this.gbBasics.Controls.Add(this.btnNowDay);
            this.gbBasics.Controls.Add(this.btn1Week);
            this.gbBasics.Location = new System.Drawing.Point(3, 10);
            this.gbBasics.Name = "gbBasics";
            this.gbBasics.Size = new System.Drawing.Size(414, 148);
            this.gbBasics.TabIndex = 47;
            this.gbBasics.TabStop = false;
            this.gbBasics.Text = "기본 검색";
            // 
            // lblOldDrawingNumber
            // 
            this.lblOldDrawingNumber.AutoSize = true;
            this.lblOldDrawingNumber.Location = new System.Drawing.Point(14, 119);
            this.lblOldDrawingNumber.Name = "lblOldDrawingNumber";
            this.lblOldDrawingNumber.Size = new System.Drawing.Size(149, 12);
            this.lblOldDrawingNumber.TabIndex = 47;
            this.lblOldDrawingNumber.Text = "The Old Drawing Number";
            // 
            // txtOldDrawingNumber
            // 
            this.txtOldDrawingNumber.Location = new System.Drawing.Point(169, 116);
            this.txtOldDrawingNumber.MaxLength = 500;
            this.txtOldDrawingNumber.Multiline = true;
            this.txtOldDrawingNumber.Name = "txtOldDrawingNumber";
            this.txtOldDrawingNumber.Size = new System.Drawing.Size(239, 20);
            this.txtOldDrawingNumber.TabIndex = 48;
            this.txtOldDrawingNumber.TextChanged += new System.EventHandler(this.txtOldDrawingNumber_TextChanged);
            // 
            // btn1Month
            // 
            this.btn1Month.BackColor = System.Drawing.Color.LightBlue;
            this.btn1Month.Location = new System.Drawing.Point(165, 74);
            this.btn1Month.Name = "btn1Month";
            this.btn1Month.Size = new System.Drawing.Size(44, 27);
            this.btn1Month.TabIndex = 41;
            this.btn1Month.Text = "1개월";
            this.btn1Month.UseVisualStyleBackColor = false;
            this.btn1Month.Click += new System.EventHandler(this.btn1Month_Click);
            // 
            // btn1Year
            // 
            this.btn1Year.BackColor = System.Drawing.Color.LightBlue;
            this.btn1Year.Location = new System.Drawing.Point(364, 74);
            this.btn1Year.Name = "btn1Year";
            this.btn1Year.Size = new System.Drawing.Size(44, 27);
            this.btn1Year.TabIndex = 46;
            this.btn1Year.Text = "1년";
            this.btn1Year.UseVisualStyleBackColor = false;
            this.btn1Year.Click += new System.EventHandler(this.btn1Year_Click);
            // 
            // dtpickerStartDate
            // 
            this.dtpickerStartDate.CustomFormat = "";
            this.dtpickerStartDate.Location = new System.Drawing.Point(16, 40);
            this.dtpickerStartDate.MaxDate = new System.DateTime(9998, 12, 1, 0, 0, 0, 0);
            this.dtpickerStartDate.Name = "dtpickerStartDate";
            this.dtpickerStartDate.Size = new System.Drawing.Size(163, 21);
            this.dtpickerStartDate.TabIndex = 34;
            this.dtpickerStartDate.Value = new System.DateTime(2013, 11, 11, 0, 0, 0, 0);
            this.dtpickerStartDate.ValueChanged += new System.EventHandler(this.dtpickerStartDate_ValueChanged);
            // 
            // btn9Month
            // 
            this.btn9Month.BackColor = System.Drawing.Color.LightBlue;
            this.btn9Month.Location = new System.Drawing.Point(314, 74);
            this.btn9Month.Name = "btn9Month";
            this.btn9Month.Size = new System.Drawing.Size(44, 27);
            this.btn9Month.TabIndex = 45;
            this.btn9Month.Text = "9개월";
            this.btn9Month.UseVisualStyleBackColor = false;
            this.btn9Month.Click += new System.EventHandler(this.btn9Month_Click);
            // 
            // lblStartDate
            // 
            this.lblStartDate.AutoSize = true;
            this.lblStartDate.Location = new System.Drawing.Point(14, 25);
            this.lblStartDate.Name = "lblStartDate";
            this.lblStartDate.Size = new System.Drawing.Size(59, 12);
            this.lblStartDate.TabIndex = 38;
            this.lblStartDate.Text = "Start Date";
            // 
            // btn6Month
            // 
            this.btn6Month.BackColor = System.Drawing.Color.LightBlue;
            this.btn6Month.Location = new System.Drawing.Point(264, 74);
            this.btn6Month.Name = "btn6Month";
            this.btn6Month.Size = new System.Drawing.Size(44, 27);
            this.btn6Month.TabIndex = 44;
            this.btn6Month.Text = "6개월";
            this.btn6Month.UseVisualStyleBackColor = false;
            this.btn6Month.Click += new System.EventHandler(this.btn6Month_Click);
            // 
            // dtpickerEndDate
            // 
            this.dtpickerEndDate.CustomFormat = "";
            this.dtpickerEndDate.Location = new System.Drawing.Point(245, 40);
            this.dtpickerEndDate.MaxDate = new System.DateTime(9998, 12, 1, 0, 0, 0, 0);
            this.dtpickerEndDate.Name = "dtpickerEndDate";
            this.dtpickerEndDate.Size = new System.Drawing.Size(163, 21);
            this.dtpickerEndDate.TabIndex = 35;
            this.dtpickerEndDate.Value = new System.DateTime(2013, 11, 19, 0, 0, 0, 0);
            this.dtpickerEndDate.ValueChanged += new System.EventHandler(this.dtpickerEndDate_ValueChanged);
            // 
            // btn3Month
            // 
            this.btn3Month.BackColor = System.Drawing.Color.LightBlue;
            this.btn3Month.Location = new System.Drawing.Point(214, 74);
            this.btn3Month.Name = "btn3Month";
            this.btn3Month.Size = new System.Drawing.Size(44, 27);
            this.btn3Month.TabIndex = 43;
            this.btn3Month.Text = "3개월";
            this.btn3Month.UseVisualStyleBackColor = false;
            this.btn3Month.Click += new System.EventHandler(this.btn3Month_Click);
            // 
            // lblEndDate
            // 
            this.lblEndDate.AutoSize = true;
            this.lblEndDate.Location = new System.Drawing.Point(243, 25);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(56, 12);
            this.lblEndDate.TabIndex = 40;
            this.lblEndDate.Text = "End Date";
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(207, 44);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(14, 12);
            this.lblFrom.TabIndex = 42;
            this.lblFrom.Text = "~";
            // 
            // btn15Day
            // 
            this.btn15Day.BackColor = System.Drawing.Color.LightBlue;
            this.btn15Day.Location = new System.Drawing.Point(115, 74);
            this.btn15Day.Name = "btn15Day";
            this.btn15Day.Size = new System.Drawing.Size(44, 27);
            this.btn15Day.TabIndex = 39;
            this.btn15Day.Text = "15일";
            this.btn15Day.UseVisualStyleBackColor = false;
            this.btn15Day.Click += new System.EventHandler(this.btn15Day_Click);
            // 
            // btnNowDay
            // 
            this.btnNowDay.BackColor = System.Drawing.Color.LightBlue;
            this.btnNowDay.Location = new System.Drawing.Point(15, 74);
            this.btnNowDay.Name = "btnNowDay";
            this.btnNowDay.Size = new System.Drawing.Size(44, 27);
            this.btnNowDay.TabIndex = 36;
            this.btnNowDay.Text = "당일";
            this.btnNowDay.UseVisualStyleBackColor = false;
            this.btnNowDay.Click += new System.EventHandler(this.btnNowDay_Click);
            // 
            // btn1Week
            // 
            this.btn1Week.BackColor = System.Drawing.Color.LightBlue;
            this.btn1Week.Location = new System.Drawing.Point(65, 74);
            this.btn1Week.Name = "btn1Week";
            this.btn1Week.Size = new System.Drawing.Size(44, 27);
            this.btn1Week.TabIndex = 37;
            this.btn1Week.Text = "1주일";
            this.btn1Week.UseVisualStyleBackColor = false;
            this.btn1Week.Click += new System.EventHandler(this.btn1Week_Click);
            // 
            // FormODNHistory
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Honeydew;
            this.ClientSize = new System.Drawing.Size(1117, 628);
            this.Controls.Add(this.pnlDataGrideView);
            this.Controls.Add(this.pnlSearch);
            this.Name = "FormODNHistory";
            this.Text = "FormODNHistory";
            this.Load += new System.EventHandler(this.FormODNHistory_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvODNHistory)).EndInit();
            this.contextMenuStrip.ResumeLayout(false);
            this.pnlDataGrideView.ResumeLayout(false);
            this.pnlSearch.ResumeLayout(false);
            this.gbCondition.ResumeLayout(false);
            this.gbCondition.PerformLayout();
            this.gbBasics.ResumeLayout(false);
            this.gbBasics.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.ComboBox cmbUserID;
        private System.Windows.Forms.DataGridView dgvODNHistory;
        private System.Windows.Forms.Button btnSaveNote;
        private System.Windows.Forms.TextBox txtNote;
        private System.Windows.Forms.Label lblNote;
        private System.Windows.Forms.Button btnExcel;
        private System.Windows.Forms.Label lblModel;
        private System.Windows.Forms.ComboBox cmbModel;
        private System.Windows.Forms.Panel pnlDataGrideView;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Panel pnlSearch;
        private System.Windows.Forms.Label lblPart;
        private System.Windows.Forms.ComboBox cmbPart;
        private System.Windows.Forms.Label lblProcess;
        private System.Windows.Forms.ComboBox cmbProcess;
        private System.Windows.Forms.Button btn1Year;
        private System.Windows.Forms.Button btn9Month;
        private System.Windows.Forms.Button btn6Month;
        private System.Windows.Forms.Button btn3Month;
        private System.Windows.Forms.Button btn1Month;
        private System.Windows.Forms.Button btn15Day;
        private System.Windows.Forms.Button btn1Week;
        private System.Windows.Forms.Button btnNowDay;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.Label lblEndDate;
        private System.Windows.Forms.DateTimePicker dtpickerEndDate;
        private System.Windows.Forms.Label lblStartDate;
        private System.Windows.Forms.DateTimePicker dtpickerStartDate;
        private System.Windows.Forms.GroupBox gbCondition;
        private System.Windows.Forms.GroupBox gbBasics;
        private System.Windows.Forms.TextBox txtSerialNumber;
        private System.Windows.Forms.Label lblSerialNumber;
        private System.Windows.Forms.Label lblOldDrawingNumber;
        private System.Windows.Forms.TextBox txtOldDrawingNumber;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem tsmiSendODN;
    }
}