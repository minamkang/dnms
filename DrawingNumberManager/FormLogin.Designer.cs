﻿namespace DrawingNumberManager
{
    partial class FormLogin
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
			this.lblLogo = new System.Windows.Forms.Label();
			this.lblID = new System.Windows.Forms.Label();
			this.txtID = new System.Windows.Forms.TextBox();
			this.txtPW = new System.Windows.Forms.TextBox();
			this.lblPassword = new System.Windows.Forms.Label();
			this.btnLogin = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.cbRemermberID_PW = new System.Windows.Forms.CheckBox();
			this.lblVersion = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// lblLogo
			// 
			this.lblLogo.AutoSize = true;
			this.lblLogo.BackColor = System.Drawing.Color.Transparent;
			this.lblLogo.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.lblLogo.ForeColor = System.Drawing.Color.Blue;
			this.lblLogo.Location = new System.Drawing.Point(6, 9);
			this.lblLogo.Name = "lblLogo";
			this.lblLogo.Size = new System.Drawing.Size(98, 16);
			this.lblLogo.TabIndex = 1;
			this.lblLogo.Text = "KMDigitech";
			// 
			// lblID
			// 
			this.lblID.AutoSize = true;
			this.lblID.BackColor = System.Drawing.Color.Transparent;
			this.lblID.Font = new System.Drawing.Font("굴림체", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.lblID.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.lblID.Location = new System.Drawing.Point(12, 76);
			this.lblID.Name = "lblID";
			this.lblID.Size = new System.Drawing.Size(34, 15);
			this.lblID.TabIndex = 2;
			this.lblID.Text = "ID:";
			// 
			// txtID
			// 
			this.txtID.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.txtID.Location = new System.Drawing.Point(125, 75);
			this.txtID.Name = "txtID";
			this.txtID.Size = new System.Drawing.Size(172, 22);
			this.txtID.TabIndex = 4;
			this.txtID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtID_KeyDown);
			// 
			// txtPW
			// 
			this.txtPW.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.txtPW.Location = new System.Drawing.Point(125, 103);
			this.txtPW.Name = "txtPW";
			this.txtPW.Size = new System.Drawing.Size(172, 22);
			this.txtPW.TabIndex = 5;
			this.txtPW.TextChanged += new System.EventHandler(this.txtPW_TextChanged);
			this.txtPW.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPW_KeyDown);
			// 
			// lblPassword
			// 
			this.lblPassword.AutoSize = true;
			this.lblPassword.BackColor = System.Drawing.Color.Transparent;
			this.lblPassword.Font = new System.Drawing.Font("굴림체", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.lblPassword.ForeColor = System.Drawing.Color.DarkSlateGray;
			this.lblPassword.Location = new System.Drawing.Point(12, 104);
			this.lblPassword.Name = "lblPassword";
			this.lblPassword.Size = new System.Drawing.Size(34, 15);
			this.lblPassword.TabIndex = 4;
			this.lblPassword.Text = "PW:";
			// 
			// btnLogin
			// 
			this.btnLogin.BackColor = System.Drawing.Color.LimeGreen;
			this.btnLogin.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.btnLogin.Location = new System.Drawing.Point(164, 131);
			this.btnLogin.Name = "btnLogin";
			this.btnLogin.Size = new System.Drawing.Size(62, 35);
			this.btnLogin.TabIndex = 2;
			this.btnLogin.Text = "Login";
			this.btnLogin.UseVisualStyleBackColor = false;
			this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.BackColor = System.Drawing.Color.DarkOrange;
			this.btnCancel.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.btnCancel.Location = new System.Drawing.Point(232, 131);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(65, 35);
			this.btnCancel.TabIndex = 3;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = false;
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// cbRemermberID_PW
			// 
			this.cbRemermberID_PW.AutoSize = true;
			this.cbRemermberID_PW.BackColor = System.Drawing.Color.Transparent;
			this.cbRemermberID_PW.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.cbRemermberID_PW.ForeColor = System.Drawing.Color.OrangeRed;
			this.cbRemermberID_PW.Location = new System.Drawing.Point(15, 142);
			this.cbRemermberID_PW.Name = "cbRemermberID_PW";
			this.cbRemermberID_PW.Size = new System.Drawing.Size(146, 16);
			this.cbRemermberID_PW.TabIndex = 1;
			this.cbRemermberID_PW.Text = "Remermber ID&&PW";
			this.cbRemermberID_PW.UseVisualStyleBackColor = false;
			this.cbRemermberID_PW.CheckedChanged += new System.EventHandler(this.cbRemermberID_PW_CheckedChanged);
			// 
			// lblVersion
			// 
			this.lblVersion.AutoSize = true;
			this.lblVersion.BackColor = System.Drawing.Color.Transparent;
			this.lblVersion.Font = new System.Drawing.Font("굴림", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.lblVersion.Location = new System.Drawing.Point(166, 13);
			this.lblVersion.Name = "lblVersion";
			this.lblVersion.Size = new System.Drawing.Size(60, 12);
			this.lblVersion.TabIndex = 6;
			this.lblVersion.Text = "Version:";
			// 
			// FormLogin
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.BackgroundImage = global::DrawingNumberManager.Properties.Resources.login;
			this.ClientSize = new System.Drawing.Size(309, 170);
			this.Controls.Add(this.lblVersion);
			this.Controls.Add(this.cbRemermberID_PW);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnLogin);
			this.Controls.Add(this.txtPW);
			this.Controls.Add(this.lblPassword);
			this.Controls.Add(this.txtID);
			this.Controls.Add(this.lblID);
			this.Controls.Add(this.lblLogo);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Name = "FormLogin";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Login";
			this.Load += new System.EventHandler(this.FormLogin_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblLogo;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.TextBox txtPW;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.CheckBox cbRemermberID_PW;
		private System.Windows.Forms.Label lblVersion;
    }
}

