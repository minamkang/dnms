﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DrawingNumberManager
{
    public partial class ucProgressBar : UserControl
    {
        public string StatusText
        {
            get { return lblStatus.Text; }
            set { lblStatus.Text = value; }
        }

        public Color StatusFontColor
        {
            get { return lblStatus.ForeColor; }
            set { lblStatus.ForeColor = value; }
        }

        public string Message
        {
            get { return lblMessage.Text; }
            set { lblMessage.Text = value; }
        }


        public int Progress
        {
            get { return progress.Value; }
            set { progress.Value = value; }
        }

        public ucProgressBar()
        {
            InitializeComponent();
        }
    }
}
