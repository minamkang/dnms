﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Data.SqlClient;
using Microsoft.Office.Interop.Excel;

namespace DrawingNumberManager
{
    public partial class FormPublishOfficialDN : Form
    {
        private string TDN;
        private string ReviseODN;
        private char TDNLastRevisionCode;
        private int TDNKey;
        private int maker_code;

        int current_user_access_level;
        short Flag = 0;
        Authority clsAuthority = null;

        ArrayList tdn_detail_content;

        private string odn;
        public string OfficialDrawingNumber
        {
            get { return (odn); }
            set { odn = value; }
        }

        public FormPublishOfficialDN(string strTDN, string strReviseODN, int iUserLevel, short iFlag, ArrayList alContent)
        {
            current_user_access_level = iUserLevel;
            TDN = strTDN;
            ReviseODN = strReviseODN;
            Flag = iFlag;
            tdn_detail_content = alContent;

            InitializeComponent();
        }

        private void FormPublishOfficialDN_Load(object sender, EventArgs e)
        {
            if (current_user_access_level != 7)
                this.tsmiManagement.Visible = false;

            if (Flag == Const.FLAG_CREATE_ODN || Flag == Const.FLAG_TDN_TO_CREATE_ODN)
            {
                pnlNewOfficialDrawingNumber.Visible = true;
                pnlReviseOfficialDrawingNumber.Visible = false;

                for (short i = 1; i < 4; i++)
                    BindCmbBox(i);

                BindCmbOrderType();
                BindCmbSaveDNType();

                //20200417 추가
                if (tdn_detail_content != null && tdn_detail_content.Count > 0)
                {
                    txtType.Text = tdn_detail_content[0].ToString();
                    cmbModel.SelectedValue = tdn_detail_content[1].ToString();
                    cmbPart.SelectedValue = tdn_detail_content[2].ToString();
                    txtPartName.Text = tdn_detail_content[3].ToString();
                    txtSpecfication.Text = tdn_detail_content[4].ToString();
                    txtMaterial.Text = tdn_detail_content[5].ToString();
                    txtAfterProcess.Text = tdn_detail_content[6].ToString();
                    txtQty.Text = tdn_detail_content[7].ToString();
                    txtMaker.Text = tdn_detail_content[8].ToString();
                    txtUnitPrice.Text = tdn_detail_content[9].ToString();
                    txtPrice.Text = tdn_detail_content[10].ToString();
                }
                //20200417 추가

                this.Height = 560;
            }
            else if (Flag == Const.FLAG_TDN_TO_REVISE_ODN)
            {
                pnlNewOfficialDrawingNumber.Visible = true;
                pnlReviseOfficialDrawingNumber.Visible = false;

                cmbModel.Enabled = false;
                cmbPart.Enabled = false;
                cmbProcess.Enabled = false;
                cmbOrderType.Enabled = false;
                cmbSaveDNType.Enabled = false;
                txtSaveDN.Enabled = false;

                //최종 리비젼 결과를 표시
                int iCurrentRevision = GetODNKey(ReviseODN);
                txtRevisionCd.Text = GetODNLastRevisionCode(iCurrentRevision, ReviseODN).ToString();
                this.Height = 250;

            }
            else if (Flag == Const.FLAG_REVISE_ODN)
            {
                pnlNewOfficialDrawingNumber.Visible = false;
                pnlReviseOfficialDrawingNumber.Visible = true;
                pnlReviseOfficialDrawingNumber.Location = new System.Drawing.Point(0, 25);

                //swkang 20160907 Add
                lblNewOrderType.Visible = false;
                txtNewOrderType.Visible = false;
                txtReviseOfficialDrawingNumber.Size = new Size(290, 32);
                this.Height = 250;

            }
            else if (Flag == Const.FLAG_ORDER_TYPE_ODN) //swkang 20160907 Add
            {
                pnlNewOfficialDrawingNumber.Visible = false;
                pnlReviseOfficialDrawingNumber.Visible = true;
                pnlReviseOfficialDrawingNumber.Location = new System.Drawing.Point(0, 32);

                lblReviseOfficialDrawingNumber.Text = "Official Drawing Number";
                lblContent.Text = "※ 진도면번호(총 11자리)와 New Order Type(1자리)는 반드시 입력해야 \r\n    합니다.";
                this.Height = 250;
            }

            this.Width = 450;
        }


        //20160921 swakng add
        Form frmTempODNHistory = null;
        private void tsmiODNHistory_Click(object sender, EventArgs e)
        {
            object OpenedForm = GetForm("FormODNHistory");
            if (OpenedForm == null)
            {
                clsAuthority = new Authority(StaticData.user_id);
                FormODNHistory frmODrawingHistory = new FormODNHistory(clsAuthority.NoteWrite);
                if (Flag == Const.FLAG_ORDER_TYPE_ODN)
                    frmODrawingHistory.FormSendEvent += new FormODNHistory.FormSendDataHandler(DieaseUpdateEventMethod);

                frmODrawingHistory.Show();
                frmTempODNHistory = frmODrawingHistory;

            }
        }

        public static Form GetForm(string formName)
        {
	        foreach (Form frm in System.Windows.Forms.Application.OpenForms)
		        if (frm.Name == formName)
			        return frm;

	        return null;
        }

        private void DieaseUpdateEventMethod(object sender)
        {
            //최종 리비젼 결과를 표시
            if (sender == null)
                return;

            if (sender.ToString() != "")
            {
                int iCurrentRevision = GetODNKey(sender.ToString());
                txtReviseOfficialDrawingNumber.Text = sender.ToString() + GetODNLastRevisionCode(iCurrentRevision, sender.ToString()).ToString();
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtNote.Text = "";
            txtNote.Focus();
        }

        private void btnMaker_Click(object sender, EventArgs e)
        {
            FormMaker frmCompany = new FormMaker(false);
            if (frmCompany.ShowDialog() == DialogResult.OK)
            {
                txtMaker.Text = frmCompany.MakerName;
                maker_code = frmCompany.MakerCode;
            }
        }

        private void btnPublish_Click(object sender, EventArgs e)
        {
            if (ReviseODN.Length == 0)
            {
                //진도번 Project Code 
                string strModel = cmbModel.SelectedValue.ToString();
                string strPart = cmbPart.SelectedValue.ToString();
                string strProcess = cmbProcess.SelectedValue.ToString();

                //주문 방식 결정
                string strOrderType = cmbOrderType.SelectedItem.ToString();
                string strOfficialSource = strModel + strPart + strProcess;

                if (cmbSaveDNType.SelectedItem.ToString() == "Temporary Drawing Number")
                {
                    if (txtSaveDN.Text.Length < 8)
                    {
                        MessageBox.Show("가도번은 8자리입니다.");
                        return;
                    }

                    TDN = txtSaveDN.Text.ToUpper();
                    txtSaveDN.Text = "";
                    if (CheckLastTDN() == false) //작성된 가도번이 최신인지 판단한다.
                        return;
                }

                ArrayList detail_datat = new ArrayList();
                detail_datat.Add(txtType.Text);
                detail_datat.Add(txtPartName.Text);
                detail_datat.Add(txtSpecfication.Text);
                detail_datat.Add(txtMaterial.Text);
                detail_datat.Add(txtAfterProcess.Text);
                detail_datat.Add(txtQty.Text.Replace(",", ""));
                if (txtMaker.Text != "") 
                    detail_datat.Add(maker_code);
                else 
                    detail_datat.Add(0);
                detail_datat.Add(txtUnitPrice.Text.Replace(",", ""));
                detail_datat.Add(txtPrice.Text.Replace(",", ""));

                OfficialDrawingNumber CurrentODN = new OfficialDrawingNumber(strOfficialSource, strOrderType, TDN, txtNote.Text, txtSaveDN.Text, detail_datat);
                if (CurrentODN.ToString().Length > 0)
                {
                    OfficialDrawingNumber = CurrentODN.ToString().Substring(0, 10);
                    txtResultOfficialDN.Text = OfficialDrawingNumber;
                    txtResultOfficialDN.Focus();
                    txtResultOfficialDN.SelectAll();
                    txtRevisionCd.Text = CurrentODN.ToString().Substring(10, 1);

                    //가도번 Revise Table에 저장을 한다.
                    if (cmbSaveDNType.SelectedItem.ToString() == "Temporary Drawing Number")
                    {
                        SqlCommand query = null;
                        query = new SqlCommand(String.Format("EXEC modify_ODN '{0}', '{1}', {2}, '{3}'", TDNLastRevisionCode, TDNKey, OfficialDrawingNumber), new SqlConnection(StaticData.db_connetion_string));
                        query.Connection.Open();
                        query.ExecuteScalar();
                        query.Connection.Close();
                    }
                }
            }
            else
            {
                //Revise
                ArrayList null_array_list = null;
                OfficialDrawingNumber CurrentRODN = new OfficialDrawingNumber(ReviseODN, TDN, txtNote.Text, null_array_list);
                if (CurrentRODN.ToString().Length > 0)
                {
                    OfficialDrawingNumber = CurrentRODN.ToString().Substring(0, 10);
                    txtResultOfficialDN.Text = OfficialDrawingNumber;
                    txtResultOfficialDN.Focus();
                    txtResultOfficialDN.SelectAll();

                    txtRevisionCd.Text = CurrentRODN.ToString().Substring(10, 1);
                }
            }

            //발행되면 초기화를 한다. - 20160125
            TDN = "";
            ReviseODN = "";
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            for (short i = 1; i < 4; i++)
                BindCmbBox(i);

            BindCmbOrderType();
        }

        private void cmbSaveDNType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbSaveDNType.SelectedItem.ToString() == "Old Drawing Number")
                txtSaveDN.MaxLength = 100;
            else if(cmbSaveDNType.SelectedItem.ToString() == "Temporary Drawing Number")
                txtSaveDN.MaxLength = 8;

            txtSaveDN.Text = "";
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            //swkang 20160921 start
            object OpenedForm = GetForm("FormODNHistory");
            if (OpenedForm != null)
                frmTempODNHistory.Close(); // 자식폼 닫기

            this.Close();

            if (OfficialDrawingNumber != null)
                DialogResult = DialogResult.OK;
            else
                DialogResult = DialogResult.Cancel;
        }

        private void FormPublishOfficialDN_FormClosed(object sender, FormClosedEventArgs e)
        {
            //swkang 20160921 start
            object OpenedForm = GetForm("FormODNHistory");
            if (OpenedForm != null)
                frmTempODNHistory.Close(); // 자식폼 닫기
        }

        private void btnCheckVaild_Click(object sender, EventArgs e)
        {
            if (txtReviseOfficialDrawingNumber.Text.Length != 11)
            {
                if (Flag == Const.FLAG_ORDER_TYPE_ODN)
                    MessageBox.Show("진도면번호 또는 OrderType이 입력되지 않았습니다.");
                else
                    MessageBox.Show("진도면번호가 입력되지 않았습니다.");
                return;
            }

            string strRODN = txtReviseOfficialDrawingNumber.Text.Substring(0, 10).ToUpper();
            string strOrderType = txtReviseOfficialDrawingNumber.Text.Substring(9, 1).ToUpper();
            string strRevision = "";

            if (txtReviseOfficialDrawingNumber.Text.Length == 11)
                 strRevision = txtReviseOfficialDrawingNumber.Text.Substring(10, 1).ToUpper();
#if false
            OfficialDrawingNumber CurrentODN = new OfficialDrawingNumber(Database, txtReviseOfficialDrawingNumber.Text.ToUpper(), UserID, "", "");
#else
            //swkang 20160908 add
            OfficialDrawingNumber CurrentODN = null;
            if (Flag == Const.FLAG_REVISE_ODN)
            {
                ArrayList null_array_list = null;
                CurrentODN = new OfficialDrawingNumber(txtReviseOfficialDrawingNumber.Text.ToUpper(), "", "", null_array_list);
            }
            else if (Flag == Const.FLAG_ORDER_TYPE_ODN)
                CurrentODN = new OfficialDrawingNumber(txtReviseOfficialDrawingNumber.Text.ToUpper(), "", txtNewOrderType.Text.ToUpper(), "");
#endif
            if (CurrentODN.Issue() == true)
            {
                //입력한 도면번호에 리비전값이 최신리비전값인지 체크를 한다.
                int currentDNKey = GetODNKey(strRODN);
                bool whetherLastRevsionCode = false;
                string drawingNumber = "";

                char dBLastRevisionCode = GetODNLastRevisionCode(currentDNKey, strRODN);
                if (dBLastRevisionCode == 'Z')
                {
                    MessageBox.Show("REVISION CODE는 'Z' 이상 사용 할 수 없습니다.", "Warning");
                    return;
                }

                //swkang 20160908 add
                if (Flag == Const.FLAG_ORDER_TYPE_ODN)
                {
                    if (CheckOrderType(currentDNKey, strRODN, dBLastRevisionCode) == false)
                        return;
                }
                //swkang 20160908 end

                if (CurrentODN.revision_code == dBLastRevisionCode)
                {
                    if (Flag == Const.FLAG_REVISE_ODN)
                        drawingNumber = DrawingCategory.project_code + CurrentODN.serial_number.ToString("000.##") + strOrderType + CurrentODN.revision_code;
                    else if (Flag == Const.FLAG_ORDER_TYPE_ODN)
                        drawingNumber = DrawingCategory.project_code + CurrentODN.serial_number.ToString("000.##") + txtNewOrderType.Text.ToUpper() + dBLastRevisionCode.ToString();

                    whetherLastRevsionCode = true;
                }
                else
                {
                    if (Flag == Const.FLAG_REVISE_ODN)
                        drawingNumber = DrawingCategory.project_code + CurrentODN.serial_number.ToString("000.##") + strOrderType + dBLastRevisionCode.ToString();
                    else if(Flag == Const.FLAG_ORDER_TYPE_ODN)
                        drawingNumber = DrawingCategory.project_code + CurrentODN.serial_number.ToString("000.##") + strOrderType + dBLastRevisionCode.ToString();   
                }

                //기존 정보 Detail Content를 Read한다.
                //KEY값(currentDNKey) + 리비젼코드 값(CurrentTDN.revision_code) 
                System.Data.DataTable detailContent = GetCurrentDetailContent(currentDNKey, CurrentODN.revision_code);

                //Revision 발번
                FormPublishRODN frmRODN = new FormPublishRODN(drawingNumber, txtReviseOfficialDrawingNumber.Text, whetherLastRevsionCode, txtNewOrderType.Text.ToUpper(), detailContent);
                if (frmRODN.ShowDialog() == DialogResult.OK)
                {
                    txtReviseOfficialDrawingNumber.Text = "";
                    txtNewOrderType.Text = "";
                }

            }
        }

        private void tsmiModel_Click(object sender, EventArgs e)
        {
            FormManagerProject frmModel = new FormManagerProject(Const.CODE_MODEL);
            frmModel.ShowDialog();
        }

        private void tsmiPart_Click(object sender, EventArgs e)
        {
            FormManagerProject frmPart = new FormManagerProject(Const.CODE_PART);
            frmPart.ShowDialog();
        }

        private void tsmiProcess_Click(object sender, EventArgs e)
        {
            FormManagerProject frmProcess = new FormManagerProject(Const.CODE_PROCESS);
            frmProcess.ShowDialog();
        }

        private void tsmiMaker_Click(object sender, EventArgs e)
        {
            FormMaker frmMaker = new FormMaker(true);
            frmMaker.ShowDialog();
        }

        private void tsmiPartList_Click(object sender, EventArgs e)
        {
            FormPartList frmPartList = new FormPartList(current_user_access_level);
            frmPartList.ShowDialog();
        }

        private void txtQty_TextChanged(object sender, EventArgs e)
        {
            if (txtQty.Text != "")
            {
                txtQty.Text = String.Format("{0:#,###}", int.Parse(txtQty.Text.Replace(",", "")));   //swkang 20201210
                txtQty.SelectionStart = txtQty.TextLength;
                txtQty.SelectionLength = 0;

                CalculationPrice();
            }
        }

        private void txtUnitPrice_TextChanged(object sender, EventArgs e)
        {
            if (txtUnitPrice.Text != "")
            {
                txtUnitPrice.Text = String.Format("{0:#,###}", int.Parse(txtUnitPrice.Text.Replace(",", "")));   //swkang 20201210
                txtUnitPrice.SelectionStart = txtUnitPrice.TextLength;
                txtUnitPrice.SelectionLength = 0;

                CalculationPrice();
            }
        }

        private void BindCmbBox(short sType)
        {
            System.Data.DataTable result = new System.Data.DataTable();
            SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC code_info {0}", sType), StaticData.db_connetion_string);
            query.Fill(result);
            if (result != null)
            {
                if (sType == 1)
                {
                    cmbModel.ValueMember = "CODE_NAME";
                    cmbModel.DisplayMember = "CODE_VALUE";
                    cmbModel.DataSource = result;
                }
                else if (sType == 2)
                {
                    cmbPart.ValueMember = "CODE_NAME";
                    cmbPart.DisplayMember = "CODE_VALUE";
                    cmbPart.DataSource = result;
                }
                else if (sType == 3)
                {
                    cmbProcess.ValueMember = "CODE_NAME";
                    cmbProcess.DisplayMember = "CODE_VALUE";
                    cmbProcess.DataSource = result;
                }
            }
        }

        private void BindCmbOrderType()
        {
            for (int i = (int)'A'; i < (int)'Z' + 1; i++)
                cmbOrderType.Items.Add((char)i);

            cmbOrderType.SelectedIndex = 0;
        }

        private void BindCmbSaveDNType()
        {
            cmbSaveDNType.Items.Add("Old Drawing Number");
            cmbSaveDNType.Items.Add("Temporary Drawing Number");
            cmbSaveDNType.SelectedIndex = 0;
        }


        private int GetODNKey(string strODN)
        {
            int resultDNKey = 0;
            SqlCommand cmd = new SqlCommand(String.Format("EXEC official_drawing_key_of '{0}'", strODN), new SqlConnection(StaticData.db_connetion_string));
            cmd.Connection.Open();
            resultDNKey = (int)cmd.ExecuteScalar();
            cmd.Connection.Close();
            return resultDNKey;
        }

        private char GetODNLastRevisionCode(int currentDNKey, string strODN)
        {
            string resultDNLastRevisionCode = "";
            string ProjectCode = strODN.Substring(0, 6);
            string OrderType = strODN.Substring(9, 1);      //swkang 20160908 start

            SqlCommand query = new SqlCommand(String.Format("EXEC official_last_revision_code {0}, '{1}', '{2}'", currentDNKey, ProjectCode, OrderType), new SqlConnection(StaticData.db_connetion_string));
            query.Connection.Open();
            resultDNLastRevisionCode = (string)query.ExecuteScalar();
            query.Connection.Close();

            return resultDNLastRevisionCode[0];
        }

        //swkang 20160908
        private bool CheckOrderType(int currentDNKey, string strODN, char dBLastRevisionCode)
        {
            bool isCheckResult = false;

            string CurrentOrderType = strODN.Substring(9, 1);
            if (CurrentOrderType == txtNewOrderType.Text)
            {
                MessageBox.Show("동일 Order Type은 지정 할 수 없습니다.");
                txtNewOrderType.Text = "";
                isCheckResult = false;
            }
            else
            {
                string result = "";
                SqlCommand query = new SqlCommand(String.Format("EXEC official_check_order_type {0}, '{1}', '{2}'", currentDNKey, dBLastRevisionCode, txtNewOrderType.Text), new SqlConnection(StaticData.db_connetion_string));
                query.Connection.Open();
                result = (string)query.ExecuteScalar();
                query.Connection.Close();

                if (result == "NN")
                {
                    MessageBox.Show("이미 작성된 Order Type입니다.");
                    txtNewOrderType.Text = "";
                    isCheckResult = false;
                }
                else
                    isCheckResult = true;
            }
            return isCheckResult;
        }


        private bool CheckLastTDN()
        {
            bool CheckResult = false; 

            int iTDNKey = GetDNKey(TDN);
            char lastRevision = GetDNLastRevisionCode(iTDNKey);

            if (TDN.Substring(7, 1) != lastRevision.ToString())
            {
                string strMessage = "";
                strMessage += "작성된 가도번은 최신이 아닙니다.\r";
                strMessage += string.Format("최신 가도번은 {0}{1}입니다.", TDN.Substring(0, 7), lastRevision.ToString());
                MessageBox.Show(strMessage);
                CheckResult = false;
            }
            else
            {
                TDNKey = iTDNKey;
                TDNLastRevisionCode = lastRevision;
                CheckResult = true;
            }
            return CheckResult;
        }

        private int GetDNKey(string strTDN)
        {
            int resultDNKey = 0;
            SqlCommand cmd = new SqlCommand(String.Format("EXEC drawing_key_of '{0}'", strTDN), new SqlConnection(StaticData.db_connetion_string));
            cmd.Connection.Open();
            resultDNKey = (int)cmd.ExecuteScalar();
            cmd.Connection.Close();
            return resultDNKey;
        }

        private char GetDNLastRevisionCode(int currentDNKey)
        {
            string resultDNLastRevisionCode = "";
            SqlCommand query = new SqlCommand(String.Format("EXEC last_revision_code {0}, '{1}'", currentDNKey, DrawingCategory.code), new SqlConnection(StaticData.db_connetion_string));
            query.Connection.Open();
            resultDNLastRevisionCode = (string)query.ExecuteScalar();
            query.Connection.Close();
            return resultDNLastRevisionCode[0];
        }

        private System.Data.DataTable GetCurrentDetailContent(int CurrentDNKey, char InputRevisionCode)
        {
            System.Data.DataTable result = new System.Data.DataTable();
            SqlDataAdapter query = new SqlDataAdapter(String.Format("EXEC drawing_number_detail_content 'O', {0}, '{1}'", CurrentDNKey, InputRevisionCode), StaticData.db_connetion_string);
            query.Fill(result);

            return result;
        }

        private void CalculationPrice()
        {
            if (txtQty.Text == "" || txtUnitPrice.Text == "")
                return;

            try
            {
                int qty = int.Parse(txtQty.Text.Replace(",", ""));
                int unit_price = int.Parse(txtUnitPrice.Text.Replace(",", ""));
                int price = unit_price * qty;

#if false
                txtPrice.Text = price.ToString();
#else
                txtPrice.Text = string.Format("{0:#,###}", price); //swkang 20201210
#endif
            }
            catch (Exception ex)
            {
                txtQty.Text = "0";
                txtUnitPrice.Text = "0";
                txtPrice.Text = "0";
                ex.ToString();
            }
        }
    }
}
